#version 330 core

//uniform mat4 mvp_matrix;
//uniform mat4 mv_matrix;

uniform mat4 model_matrix;
uniform mat4 perspective_matrix;
uniform mat4 camera_matrix;

uniform mat3 normal_matrix;
//uniform int material;

in vec3 vertex_position;
in vec3 vertex_normal;
//in vec4 vertex_color;

//flat out int mindex;			// -> for per vertex material

out vec4 position;
out vec3 normal;
out vec4 color;

void main(){
	
	//mindex = material;
	
	//position = mv_matrix * vec4(vertex_position, 0.0);
	position = camera_matrix * model_matrix * vec4(vertex_position, 0.0);
	
//	mat3 m=mat3(camera_matrix)*mat3(inverse(transpose(model_matrix)));
//	normal = normalize(m*vertex_normal);
	normal = normalize(normal_matrix * vertex_normal);
//	normal=normalize(mat3(perspective_matrix)*mat3(model_matrix)*vertex_normal);
	//color = vertex_color;
	color = vec4(0.5, 0.5, 0.5, 1.0);
	//gl_Position = mvp_matrix * vec4(vertex_position, 1.0);
	gl_Position = perspective_matrix * camera_matrix * model_matrix * vec4(vertex_position, 1.0);
}