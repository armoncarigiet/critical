#version 330 core
in vec3 vertex_position;
in vec3 vertex_normal;
//in vec3 vertex_color;

uniform mat4 model_matrix;
uniform mat4 perspective_matrix;
uniform mat4 camera_matrix;


out vec3 color;
void main(){
	color = vec3(0.5, 0.5, 0.5);
	gl_Position =  perspective_matrix *  camera_matrix * model_matrix * vec4(vertex_position, 1.0);
	//gl_Position = model_matrix * camera_matrix * perspective_matrix * vec4(vertex_position, 1.0);
}