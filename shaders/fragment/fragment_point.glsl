#version 330 core

struct LightProperties {
	bool isEnabled;
	bool isLocal;
	bool isSpot;
	vec3 ambient;
	vec3 color;
	vec3 position;
	vec3 halfVector;
	vec3 coneDirection;
	float spotCosCutoff;
	float spotExponent;
	float constantAttenuation;
	float linearAttenuation;
	float quadraticAttenuation;
};

struct MaterialProperties{
	vec3 ambient;
	vec3 emission;
	vec3 diffuse;
	vec3 specular;
	float shininess;
};

const int MAX_LIGHTS = 10;
const int MAX_MATERIALS = 10;
uniform MaterialProperties materials[MAX_MATERIALS];
uniform LightProperties lights[MAX_LIGHTS];

uniform vec3 eye_direction;
uniform float strength;
uniform int material_index;

//flat in int material_index;				// after for per vertex material
in vec4 position;
in vec3 normal;
in vec4 color;

out vec4 FragColor;

void main(){
	
	vec3 scatteredLight=vec3(0.0), reflectedLight=vec3(0.0);
	
	for (int i=0; i<MAX_LIGHTS; i++) {
		if(lights[i].isEnabled){
			vec3 lightDirection = lights[i].position - vec3(position);
			float lightDistance = length(lightDirection);
			lightDirection/=lightDistance;
			
			float attenuation = 1.0 / (lights[i].constantAttenuation + lights[i].linearAttenuation*lightDistance + lights[i].quadraticAttenuation*lightDistance*lightDistance);
			
			vec3 halfVector= normalize(lightDirection + eye_direction);
			
			float diffuse = max(0.0, dot(normal, lightDirection));
			float specular = max(0.0, dot(normal, halfVector));
			
			if(diffuse == 0.0){
				specular=0;
			}else{
				specular = pow(specular, materials[material_index].shininess)*strength;
			}
			scatteredLight += lights[i].ambient*materials[material_index].ambient*attenuation + lights[i].color* materials[material_index].diffuse*diffuse*attenuation;
			reflectedLight += lights[i].color*materials[material_index].specular*specular*attenuation;
		}
	}
	
	
	
	
	vec3 rgb = min(materials[material_index].emission+color.rgb*scatteredLight+reflectedLight, vec3(1.0));
	FragColor =vec4(rgb, color.a);
	
	
	
}