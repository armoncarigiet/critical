#version 330 core

struct LightProperties {
	bool isEnabled;
	bool isLocal;
	bool isSpot;
	vec3 ambient;
	vec3 color;
	vec3 position;
	vec3 halfVector;
	vec3 coneDirection;
	float spotCosCutoff;
	float spotExponent;
	float constantAttenuation;
	float linearAttenuation;
	float quadraticAttenuation;
};

struct MaterialProperties{
	vec3 ambient;
	vec3 emission;
	vec3 diffuse;
	vec3 specular;
	float shininess;
};

const int MAX_LIGHTS = 10;
const int MAX_MATERIALS = 10;
uniform MaterialProperties materials[MAX_MATERIALS];
uniform LightProperties lights[MAX_LIGHTS];

uniform vec3 eye_direction;
uniform float strength;
uniform int material_index;

//flat in int material_index;				// after for per vertex material
in vec4 position;
in vec3 normal;
in vec4 color;

out vec4 FragColor;

void main(){
//	FragColor=color;
//	//FragColor=vec4(materials[material].ambient, 1.0);
//	
//	}
	vec3 scatteredLight = vec3(0.0);
	vec3 reflectedLight = vec3(0.0);
	
	for(int i=0; i<MAX_LIGHTS; i++){
		if(!lights[i].isEnabled){
			continue;
		}
		vec3 halfVector;
		vec3 lightDirection = lights[i].position;
		float attenuation= 1.0;
		
		if(lights[i].isLocal){
			lightDirection = lightDirection - vec3(position);
			float lightDistance = length(lightDirection);
			lightDirection = lightDirection / lightDistance;
			
			attenuation /= (lights[i].constantAttenuation + lights[i].linearAttenuation*lightDistance + lights[i].quadraticAttenuation*lightDistance*lightDistance);
			
			// spotlights need additional calculations
			if(lights[i].isSpot){
				float spotCos = dot(lightDirection, -lights[i].coneDirection);
				if(spotCos < lights[i].spotCosCutoff){
					attenuation = 0.0;
				}else{
					attenuation *= pow(spotCos, lights[i].spotExponent);
				}
			}
			halfVector = normalize(lightDirection + eye_direction);
		}else{
			halfVector = lights[i].halfVector;
		}
		float diffuse = max(0.0, dot(normal, lightDirection));
		float specular = max(0.0, dot(normal, halfVector));
			
		if(diffuse == 0){
			specular = 0;
		}else{
			specular = pow(specular, materials[material_index].shininess) * strength;
		}
		
		scatteredLight += lights[i].ambient * materials[material_index].ambient * attenuation
						+ lights[i].color * materials[material_index].diffuse * diffuse * attenuation;
		reflectedLight += lights[i].color * materials[material_index].specular * specular * attenuation;
	}
	
	vec3 c = min(materials[material_index].emission + color.rgb * scatteredLight + reflectedLight, vec3(1.0));
	FragColor = vec4(c, color.a);
}