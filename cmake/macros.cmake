include(CMakeParseArguments)

macro(critical_add_library target)
	cmake_parse_arguments(LIB "" "TYPE" "DEPENDS;SOURCES;EXT_LIBS" ${ARGN})
	
	message("library: ${LIB_TYPE}")
	
	if(CRITICAL_OS_WINDOWS)
		set(LIB_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/extlib/win/x86)	#to change
	elseif(CRITICAL_OS_MACOSX)
		set(LIB_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/extlib/osx/)
	elseif(CRITICAL_OS_LINUX)
		set(LIB_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/extlib/linux/)
	elseif(CRITICAL_OS_FREEBSD)
		set(LIB_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/extlib/freebsd/)
	endif()

#message("target: ${target}")
#message("dependencies: ${LIB_DEPENDS}")
#message("sources: ${LIB_SOURCES}")
#message("externlibraries: ${LIB_EXT_LIBS}")
	
	if((LIB_TYPE STREQUAL "FRAMEWORK"))
		add_library(${target} SHARED ${LIB_SOURCES})
	else()
		add_library(${target} ${LIB_TYPE} ${LIB_SOURCES})
	endif()

		
	if((LIB_TYPE STREQUAL "FRAMEWORK") AND NOT CRITICAL_OS_MACOSX)
		message(FATAL_ERROR "Can only build frameworks for macosx!")
	endif()
	
	if(LIB_TYPE STREQUAL "SHARED")
		#set_target_properties(${target} PROPERTIES DEBUG_POSTFIX -d)

#set_target_properties(${target} PROPERTIES
#                          BUILD_WITH_INSTALL_RPATH 1
#                          INSTALL_NAME_DIR "@rpath")

set_target_properties(${target} PROPERTIES XCODE_ATTRIBUTE_LD_DYLIB_INSTALL_NAME "${CMAKE_SOURCE_PATH}/lib/lib${target}.dylib")

#set_target_properties(${target} PROPERTIES INSTALL_NAME_DIR "/usr/local/lib")

		if(CRITICAL_OS_WINDOWS)
            set_target_properties(${target} PROPERTIES SUFFIX "-${VERSION_MAJOR}${CMAKE_SHARED_LIBRARY_SUFFIX}")
		endif()
	else()
		#set_target_properties(${target} PROPERTIES DEBUG_POSTFIX -s-d)
        #set_target_properties(${target} PROPERTIES RELEASE_POSTFIX -s)
        #set_target_properties(${target} PROPERTIES MINSIZEREL_POSTFIX -s)
	endif()
	
	if(LIB_DEPENDS)
        target_link_libraries(${target} ${LIB_DEPENDS})
    endif()
	
	if((LIB_TYPE STREQUAL "FRAMEWORK") AND CRITICAL_OS_MACOSX)
		 set_target_properties(${target} PROPERTIES FRAMEWORK TRUE FRAMEWORK_VERSION ${CRITICAL_VERSION_MAJOR}.${CRITICAL_VERSION_MINOR}.${CRITICAL_VERSION_PATCH} MACOSX_FRAMEWORK_IDENTIFIER com.armoncarigiet.${target}
                                  MACOSX_FRAMEWORK_SHORT_VERSION_STRING ${CRITICAL_VERSION_MAJOR}.${CRITICAL_VERSION_MINOR}.${CRITICAL_VERSION_PATCH} MACOSX_FRAMEWORK_BUNDLE_VERSION ${CRITICAL_VERSION_MAJOR}.${CRITICAL_VERSION_MINOR}.${CRITICAL_VERSION_PATCH})
		set_target_properties(${target} PROPERTIES BUILD_WITH_INSTALL_RPATH 1 INSTALL_NAME_DIR "@executable_path/../Frameworks")
	endif()
	
#	if(THIS_EXT_LIBS)
#        target_link_libraries(${target} ${THIS_EXT_LIBS})
#    endif()
	
	if(LIB_EXT_LIBS)
		foreach(lib ${LIB_EXT_LIBS})
			message("lib: ${lib}")
			find_library(tmplib ${lib} ${LIB_INCLUDE_DIR})
			if(NOT tmplib)
				message(FATAL_ERROR "${lib} not found!")
			else()
				target_link_libraries(${target} ${tmplib})
			endif()
			message("l: ${tmplib}")
			unset(tmplib CACHE)
		endforeach()
	endif()
	
	target_link_libraries(${target} ${CRITICAL_SYSTEM_LIBRARIES})

endmacro()
	
