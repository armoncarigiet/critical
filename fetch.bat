@echo off
git clone https://armoncarigiet@bitbucket.org/armoncarigiet/glmathlib.git

cd include

mklink /D glmath ..\glmathlib\include\glmath
mklink /D unit ..\glmathlib\tests\unit
@echo on

