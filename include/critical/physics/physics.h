//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file physics.h
 *	@date 11.11.2014.
 *  @author Armon Carigiet
 *
 *  @brief Main include file for the physics module.
 */
//===========================================================================

//===========================================================================
/**
 *  @defgroup physics Physics unit
 *
 *  Provides different classes to calculate the properties and the movement of
 *  physical objects for 3d-realtime rendering. It supports a simple forms of
 *  pointmass and rigidbody simulations, whereas a collison detection and
 *  resolution system between these objects is not availabel at the moment.
 */
//===========================================================================

#ifndef _CRITICAL_PHYSICS_PHYSICS_H
#define _CRITICAL_PHYSICS_PHYSICS_H

namespace critical {
    //! @ingroup physics
    namespace physics {
        //! @ingroup physics
        namespace constants {
            
        }
        //! @ingroup physics
        namespace values {
            
        }
    }
}

//===========================================================================
// Headers
//===========================================================================

#include <critical/physics/Constants.h>
#include <critical/physics/ForceObject.h>
#include <critical/physics/ForceGeneratorList.h>
#include <critical/physics/PhysicsVector.h>


#include <critical/physics/Particle.h>
#include <critical/physics/ParticleForceGenerator.h>
#include <critical/physics/ParticleList.h>
#include <critical/physics/ParticleSpace.h>
#include <critical/physics/ParticleGravityGenerator.h>
#include <critical/physics/SimpleParticleGravityGenerator.h>
#include <critical/physics/ParticleDragGenerator.h>
#include <critical/physics/FixedParticleBungeeRope.h>
#include <critical/physics/FixedParticleSpring.h>
#include <critical/physics/ParticleBungeeRope.h>
#include <critical/physics/ParticleLink.h>
#include <critical/physics/ParticleSpring.h>


#include <critical/physics/PointMass.h>
#include <critical/physics/PointMassSystem.h>
#include <critical/physics/RigidBody.h>
#include <critical/physics/RigidBodyForceGenerator.h>
#include <critical/physics/RigidBodyList.h>
#include <critical/physics/RigidBodySpace.h>
#include <critical/physics/RigidBodySpinGenerator.h>

//#include <critical/physics/ParticleSystem.h>
//#include <critical/physics/PhysicsRenderWindow.h>

#endif /* _CRITICAL_PHYSICS_PHYSICS_H */