//
//  PointMassSystem.h
//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file PointMassSystem.h
 *	@date 30.07.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a Class representing a sytem of point masses used for the
 *  process of calculating a rigidbody's properties.
 *  @ingroup physics
 */
//===========================================================================

#ifndef CRITICAL_PHYSICS_POINTMASSSYSTEM_H_
#define CRITICAL_PHYSICS_POINTMASSSYSTEM_H_


#include <critical/physics/PointMass.h>
#include <critical/core/List.h>
#include <critical/physics/ConvexHull.h>

#include <glmath/glmath_vec3.h>
#include <glmath/glmath_mat3.h>

namespace critical { namespace physics {
 
//! Class representing a system of poinmasses, used for the calculation of inertia tensors.
//! @ingroup physics
class PointMassSystem{
private:
    List<PointMass> pm;
public:
    //! Adds new pointmass to the system.
    //! @param [in] m Pointmass to add.
    void addObject(PointMass &m);
    //! Adds new pointmass to the system.
    //! @param [in] m Pointmass to add.
    void addObject(PointMass &&m);
    
    //! Calculates the system's center of masss.
    //! @return Coordinates of the center of mass.
    glmath_vec3 calculateCenterOfMass();
    //! Calculates the system's inertia tensor.
    //! @return Calculated inertia tensor as 3x3 matrix.
    glmath_mat3 calculateInertiaTensor();
    
    // @note Not in use for now!
    MemoryManager::ConnectedPointer<ConvexHull> calculateApproximateHull();
};
    
}}

#endif /* defined(__critical__PointMassSystem__) */