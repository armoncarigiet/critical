//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file ParticleForceGenerator.h
 *	@date 16.04.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a Template for creating force generators for particles.
 *  @ingroup physics
 */
//===========================================================================

#ifndef CRITICAL_PHYSICS_PARTICLEFORCEGENERATOR_H_
#define CRITICAL_PHYSICS_PARTICLEFORCEGENERATOR_H_

#include <critical/core/Vector.h>
#include <critical/physics/ForceGenerator.h>
#include <critical/physics/Particle.h>
#include <critical/physics/ParticleList.h>

namespace critical { namespace physics {

//! Template class for creating particleforcgenerators.
//! @note This class won't work on it's own.
class ParticleForceGenerator{
protected:
	Vector<Particle*> _p;
    bool active;
public:
    ParticleForceGenerator(bool a): active(a){}
    ParticleForceGenerator(ParticleForceGenerator &f): active(f.active){}
    ParticleForceGenerator(const ParticleForceGenerator &f): active(f.active){}
    
    ParticleForceGenerator& operator=(ParticleForceGenerator &f){
        active=f.active;
        return *this;
    }
    
    ParticleForceGenerator& operator=(ParticleForceGenerator &&f){
        active=f.active;
        return *this;
    }
    
    void setActive(bool a){
        active=a;
    }

	Vector<Particle*>::Iterator addObject(Particle *particle);
	Vector<Particle*>::Iterator addObjects(ParticleList<> *list);
	void applyForce(){}
};

}}
#endif /* defined(CRITICAL_PHYSICS_PARTICLEFORCEGENERATOR_H_) */