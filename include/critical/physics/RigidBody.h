//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file RigidBody.h
 *	@date 15.04.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a class representing a rigidbody.
 *  @ingroup physics
 */
//===========================================================================

#ifndef CRITICAL_PHYSICS_RIGIDBODY_H_
#define CRITICAL_PHYSICS_RIGIDBODY_H_

#include <critical/physics/ForceObject.h>
#include <glmath/glmath_mat3.h>
#include <glmath/glmath_mat4.h>
#include <glmath/glmath_quat.h>

namespace critical { namespace physics {

//! Class representing a rigidbody
class RigidBody: public ForceObject{
public:
    
    //glmath_mat4 morientation;
    
    glmath_quat orientation;
    
	glmath_vec3 angularvelocity;
	//glmath_mat3 InertiaTensor;
    
    glmath_vec3 torqueAccumulator;
    
    glmath_mat3 InverseInertiaTensor;
    glmath_mat3 WorldInverseInertiaTensor; // World coordinates;
    
    glmath_vec3 centerOfMass;
    
 
   
    RigidBody(String &n, glmath_real mass=INFINITY, glmath_vec3 p0={{0,0,0}}, glmath_vec3 v0={{0,0,0}}, glmath_vec3 phi0={{0,0,0}}, glmath_vec3 omega0={{0,0,0}}): ForceObject(n,mass,p0,v0), orientation(glmath_quat_create_rotation_euler_angles({{phi0.x,phi0.y,phi0.z}})), angularvelocity(omega0){}
    RigidBody(String &&n, glmath_real mass=INFINITY, glmath_vec3 p0={{0,0,0}}, glmath_vec3 v0={{0,0,0}}, glmath_vec3 phi0={{0,0,0}}, glmath_vec3 omega0={{0,0,0}}): ForceObject(n,mass,p0,v0), orientation(glmath_quat_create_rotation_euler_angles({{phi0.x,phi0.y,phi0.z}})), angularvelocity(omega0){}
	
	//! Clears the torque accumulator.
	void clearTorqueAccumulator();
    
    //! Clears the torque and the force accumulator.
	void clearAccumulators();
    
    //! Applies a force in respect to the local coordiatesystem to the body.
    //! @param force Force to apply
    //! @param point Attac point in local coordinates.
    void addForceAtPoint(glmath_vec3 force, glmath_vec3 point);
    //! Applies a force on the body in world coordinates.
    //! @param force Force to apply
    //! @param point Attac point in world coordinates.
    void addForceAtBodyPoint(glmath_vec3 force, glmath_vec3 point);
	
    //! Sets an inertia tenor for the rigidbody
    //! @param t Inertia tensor to set.
    bool setInertiaTensor(glmath_mat3 t);
    //bool calculateUniformInertiaTensor();
    
    //! not needed yet
    void setCeneterOfMass(glmath_vec3 cm);
    
    //! Calculates a transformation for the render engine containing the objects position and orientation.
    void calculateTransformationMatrix();
    
    //! updates the position, orientation, velocity and angular velocity of the rigidbody.
    //! @param Time interval to iterate forward.
    //! @note Clears all akkumulators.
	void integrate(glmath_real dt);
    
    
	//glmath_mat3 calculateInertiaTensorWithRespectToPoint(glmath_vec3 p);
    
    //! Calculates a new intertia tensor with respect to a new rotation point.
    //! @param rotationPoint Rotation point.
    //! @return New Inertia tensor.
    glmath_mat3 calculateInertiaTensorWithRotationPoint(glmath_vec3 rotationPoint);
    
    //! Replaces the inertia tensor with a new one calculated with respect to a new rotation point.
    //! @param rotationPoint Rotation point.
    void recalculateInertiaTensorWithRotationPoint(glmath_vec3 rotationPoint);
    
    
    //! Calculates the inertia tensor in world coordinates.
    void calculateWorldInertiaTensor();
    
};
	
}}

#endif /* defined(CRITICAL_PHYSICS_RIGIDBODY_H_) */