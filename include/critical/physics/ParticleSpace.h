//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file ParticleSpace.h
 *	@date 08.05.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a Class for simulating the movent of and the interactions
 *  between Particles.
 *  @ingroup physics
 */
//===========================================================================



#ifndef CRITICAL_PHYSICS_PARTICLESPACE_H_
#define CRITICAL_PHYSICS_PARTICLESPACE_H_


#include <critical/core/MemoryManager.h>
#include <critical/physics/Particle.h>
#include <critical/physics/ParticleList.h>
#include <critical/physics/ForceGeneratorList.h>
#include <critical/physics/pforcegen.h>
#include <critical/core/Chrono.h>

namespace critical { namespace physics {
	
//enum ParticleForceGeneratorCode{
//	CRNormalForceGenerator = 0,
//	CRSimpleGravityForceGenerator,
//	CRGravityForceGenerator,
//	CRDragForceGenerator,
//};
	
//! Class representing a simulation space for particles.
//! @ingroup physics
class ParticleSpace{
private:
	
//	struct _forcegenEntry{
//		bool activated;
//		ParticleForceGeneratorCode identifier;
//		_forcegenEntry *next;
//		critical::MemoryManager::ConnectedPointer<_forcegenEntry> self;
//		critical::MemoryManager::ConnectedPointer<ParticleForceGenerator> fgen;
//	};
	
	ParticleList<> plist;
    
    ForceGeneratorList<ParticleForceGenerator> fgenList;
    
	//ForceGeneratorList<SimpleParticleGravityGenerator> sGravGenList;
	//ForceGeneratorList<ParticleGravityGenerator> gravGenList;
	//ForceGeneratorList<ParticleDragGenerator> dragGenList;
	
	bool simulationActive;
	
	//_forcegenEntry *fgenfirst;
	//_forcegenEntry *fgenlast;
	//size_t fgencount;
	
	
//	template <class T> constexpr ParticleForceGeneratorCode getForcegenCode(){
//		if(typeid(T)==typeid(SimpleParticleGravityGenerator)){
//			return CRSimpleGravityForceGenerator;
//		}else if(typeid(T)==typeid(ParticleGravityGenerator)){
//			return CRGravityForceGenerator;
//		}else if(typeid(T)==typeid(ParticleDragGenerator)){
//			return CRDragForceGenerator;
//		}else{
//			return CRNormalForceGenerator;
//		}
//	}
	
public:
	//ParticleSpace(): fgenfirst(nullptr), fgenlast(nullptr), fgencount(0){}
	ParticleSpace(): simulationActive(false){}
	
	template <class T> T* addForceGenerator(T &gen, bool activated=true){
        
        T* ptr=&fgenList.pushFront(gen);
        ptr->setActive(activated);
        return ptr;
        
//		if(typeid(T)==typeid(SimpleParticleGravityGenerator)){
//			return &(sGravGenList.pushBack(*gen));
//		}else if(typeid(T)==typeid(ParticleGravityGenerator)){
//			return &(gravGenList.pushBack(*gen));
//		}else if(typeid(T)==typeid(ParticleDragGenerator)){
//			return &(dragGenList.pushBack(*gen));
//		}
	}
	
	template <class T> T* addForceGenerator(T &&gen, bool activated=true){
        
        T* ptr=&fgenList.pushFront(gen);
        ptr->setActive(activated);
        return ptr;
        
//		ParticleForceGeneratorCode c=getForcegenCode<T>();
//		
//		if(c==CRSimpleGravityForceGenerator){
//			return reinterpret_cast<T*>(&(sGravGenList.pushBack(*reinterpret_cast<SimpleParticleGravityGenerator*>(&gen))));
//		}else if(c==CRGravityForceGenerator){
//			return reinterpret_cast<T*>(&(gravGenList.pushBack(*reinterpret_cast<ParticleGravityGenerator*>(&gen))));
//		}else if(CRDragForceGenerator){
//			return reinterpret_cast<T*>(&(dragGenList.pushBack(*reinterpret_cast<ParticleDragGenerator*>(&gen))));
//		}
	}
	
	//		MemoryManager::ConnectedPointer<_forcegenEntry> e=MemoryManager::salloc<_forcegenEntry>(sizeof(_forcegenEntry));
	//		e->self=e;
	//		e->activated=activated;
	//		e->identifier=getForcegenCode<T>();
	//		e->fgen=MemoryManager::salloc<ParticleForceGenerator>(sizeof(T));
	//		//T* ptr=static_cast<T*>(e->fgen.getPtr());
	//		*(e->fgen.getPtr())=T(*gen);
	//
	//		if(!fgenfirst){
	//			fgenfirst=&e;
	//			fgenlast=&e;
	//		}else{
	//			fgenlast->next=&e;
	//			fgenlast=&e;
	//		}
	//		fgencount++;
	//		return static_cast<T*>(e->fgen.getPtr());
	
//	template <class T> void popForceGenerator(){
//		MemoryManager::sfree(fgenlast->self);
//		fgencount--;
//	}
	
	Particle* addObject(Particle &p);
	Particle* addObject(Particle &&p);
	bool removeObject(Particle *p);
	bool removeObject(String &n);
	bool removeObject(String &&n);
	
    //!
	void initFrame();
    
    //! Makes all forcegenerators apply their forces to the registered particles.
	void applyForces();
    
    //! Updates the position and veloctiy of all registered particles.
    //! @param dt Time interval to iterate forward.
	void integrate(glmath_real dt);
    
    //! not in use
	void writeData(void *mem);
	
    //! Starts the simulation if it was previously stopped.
	void startSimulation(){
		simulationActive=true;
	}
    
    //! Prevents the Particles from beeing integrated.
	void stopSimulation(){
		simulationActive=false;
	}
    
    //! Checks if the simulation is running.
    //! @return [true, false] True if it is running, false if not.
    bool simulationIsRunning(){
        return simulationActive;
    }
	
    //! not in use
	glmath_real getKineticEnergy();
    //! not in use
	glmath_real getPotentialEnergy();   // not possible!!!
	
};
	
}}

#endif /* defined(CRITICAL_PHYSICS_PARTICLESPACE_H_) */