//
//  SParticle.h
//  critical
//
//  Created by Armon Carigiet on 06.05.15.
//
//

#ifndef CRITICAL_PHYSICS_SPARTICLE_H_
#define CRITICAL_PHYSICS_SPARTICLE_H_

#include <critical/physics/Particle.h>

namespace critical { namespace physics {

class SParticle: Particle{
private:
	glmath_real age;
};

}}
#endif /* defined(CRITICAL_PHYSICS_SPARTICLE_H_) */
