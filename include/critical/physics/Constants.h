//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Constants.h
 *	@date 16.04.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides various physical constants.
 *  @ingroup physics
 */
//===========================================================================

#ifndef CRITICAL_PHYSICS_CONSTANTS_H_
#define CRITICAL_PHYSICS_CONSTANTS_H_

#include <glmath/glmath_types.h>
#include <glmath/glmath_constants.h>

// Data from "Formelsammlung OrelFüssli"

namespace critical { namespace physics {
namespace constants{
	//! Gravitational constant.
	template <typename T=glmath_real> constexpr T G(){ return 6.6738480E-17; }
    //! Lightspeed.
	template <typename T=glmath_real> constexpr T c(){ return 299792458; }
    //! Magnetic constant.
	template <typename T=glmath_real> constexpr T magnetic_constant(){ return GLMATH_4PI*1.0E-7; }
    //! Electric constant.
	template <typename T=glmath_real> constexpr T electric_constant(){ return 1/(magnetic_constant<T>()*c<T>*c<T>); }
    //! Coulomb constant.
	template <typename T=glmath_real> constexpr T coulomb_constant(){ return 1/(GLMATH_4PI*electric_constant<T>()); }
    //! Elementary charge.
	template <typename T=glmath_real> constexpr T elementary_charge(){ return 1.60217656535E-19; }
    //! Atomic mass unit.
	template <typename T=glmath_real> constexpr T atomic_mass(){ return 1.66053892173E-27; }
    //! Gas constant.
	template <typename T=glmath_real> constexpr T gas_constant(){ return 8.314462175; }
    //! Avogadro constant.
	template <typename T=glmath_real> constexpr T avogadro_constant(){ return 6.0221412927E+23; }
	
}
    //! Useful physical values.
	namespace values{
        //! Planet codes to identiy the gravitational acceleration.
		enum gravityCode{
			CREarth = 0,
			CRMoon,
			CRMars,
			CRJupiter,
			CRSun
		};
		
        //! Returns the gravitaional acceleration for a given planet.
        //! @return gravitational acceleration in m/s^2
		template <gravityCode C=CREarth, typename T=glmath_real> constexpr T gravitational_acceleration(){
			if(C==CREarth){
				return 9.807;
			}else if(C==CRMoon){
				return 1.622;
			}else if(C==CRMars){
				return 3.711;
			}else if(C==CRJupiter){
				return 24.79;
			}else if(C==CRSun){
				return 274;
			}else{
				return 9.807;
			}
		}
		
        //! Identifiers for different substances.
		enum materialCode{
			CRHelium = 0,
			CRHydrogen,
			CRNeon,
			CRXenon,
			CRKrypton,
			CRArgon,
			CRMethane,
			CREthane,
			CRPropane,
			CRButhane,
			CRAir,
			CRStyrofoam,
			CRCork,
			CRLithium,
			CRWood,
			CRSodium,
			CRIce,
			CRWater,
			CRPlastic,
			CRMagnesium,
			CRAluminium,
			CRDiamond,
			CRTitanium,
			CRZinc,
			CRChromium,
			CRTin,
			CRGranite,
			CRIron,
			CRCobalt,
			CRNickel,
			CRCopper,
			CRSilver,
			CRLead,
			CRMercury,
			CRUranium,
			CRGold,
			CRPlatinum,
			CRIridium,
			CROsmium,
			CRWhiteDwarf,
			CRAtomicNuclei,
			CRNeutronStar
		};
		
        //! Returns the molar mass for a given substance.
        //! @return Molar mass in kg/mol.
		template <materialCode m, typename T=glmath_real> constexpr T molar_mass(){
			if(m==CRHelium){
				return 4.003;
			}else if(m==CRHydrogen){
				return 2.016; 		//H2
			}else if(m==CRNeon){
				return 20.18;
			}else if(m==CRXenon){
				return 131.3;
			}else if(m==CRKrypton){
				return 83.80;
			}else if(m==CRArgon){
				return 39.95;
			}else if(m==CRMethane){
				return 16.042;
			}else if(m==CREthane){
				return 30.068;
			}else if(m==CRPropane){
				return 44.094;
			}else if(m==CRButhane){
				return 58.12;
			}else if(m==CRAir){
				return 29;
			}else if(m==CRStyrofoam){
				return NAN;
			}else if(m==CRCork){
				return NAN;
			}else if(m==CRLithium){
				return 6.941;
			}else if(m==CRWood){
				return NAN;
			}else if(m==CRSodium){
				return 32.07;
			}else if(m==CRIce){
				return 18.016;
			}else if(m==CRWater){
				return 18.016;
			}else if(m==CRPlastic){
				return NAN;
			}else if(m==CRMagnesium){
				return 24.31;
			}else if(m==CRAluminium){
				return 27;
			}else if(m==CRDiamond){
				return 12.01;
			}else if(m==CRTitanium){
				return 47.87;
			}else if(m==CRZinc){
				return 65.38;
			}else if(m==CRChromium){
				return 52.00;
			}else if(m==CRTin){
				return 118.7;
			}else if(m==CRGranite){
				return NAN;
			}else if(m==CRIron){
				return 55.85;
			}else if(m==CRCobalt){
				return 58.93;
			}else if(m==CRNickel){
				return 58.69;
			}else if(m==CRCopper){
				return 63.55;
			}else if(m==CRSilver){
				return 107.9;
			}else if(m==CRLead){
				return 207.2;
			}else if(m==CRMercury){
				return 200.6;
			}else if(m==CRUranium){
				return 238.0;
			}else if(m==CRGold){
				return 197.0;
			}else if(m==CRPlatinum){
				return 195.1;
			}else if(m==CRIridium){
				return 192.2;
			}else if(m==CROsmium){
				return 190.2;
			}else if(m==CRWhiteDwarf){
				return NAN;
			}else if(m==CRAtomicNuclei){
				return NAN;
			}else if(m==CRNeutronStar){
				return NAN;
			}
		}
		
        //! Returns the thermal expansion coefficeint for a given substance.
        //! @not if the coefficient is not defined for a substance, NAN will be returned.
		template <materialCode m, typename T=glmath_real> constexpr T thermal_expansion_coefficient(){
			if(m==CRHelium){
				return NAN;
			}else if(m==CRHydrogen){
				return NAN;
			}else if(m==CRNeon){
				return NAN;
			}else if(m==CRXenon){
				return NAN;
			}else if(m==CRKrypton){
				return NAN;
			}else if(m==CRArgon){
				return NAN;
			}else if(m==CRMethane){
				return NAN;
			}else if(m==CREthane){
				return NAN;
			}else if(m==CRPropane){
				return NAN;
			}else if(m==CRButhane){
				return NAN;
			}else if(m==CRAir){
				return NAN;
			}else if(m==CRStyrofoam){
				return NAN;
			}else if(m==CRCork){
				return NAN;
			}else if(m==CRLithium){
				return NAN;
			}else if(m==CRWood){
				return NAN;
			}else if(m==CRSodium){
				return NAN;
			}else if(m==CRIce){
				return 37.0E-6;
			}else if(m==CRWater){
				return 0.207E-3;
			}else if(m==CRPlastic){
				return NAN;
			}else if(m==CRMagnesium){
				return NAN;
			}else if(m==CRAluminium){
				return 71.4E-6;
			}else if(m==CRDiamond){
				return 3.54E-6;
			}else if(m==CRTitanium){
				return NAN;
			}else if(m==CRZinc){
				return 78.9E-6;
			}else if(m==CRChromium){
				return NAN;
			}else if(m==CRTin){
				return NAN;
			}else if(m==CRGranite){
				return NAN;
			}else if(m==CRIron){
				return 12.0E-6;
			}else if(m==CRCobalt){
				return NAN;
			}else if(m==CRNickel){
				return 38.7E-6;
			}else if(m==CRCopper){
				return NAN;
			}else if(m==CRSilver){
				return NAN;
			}else if(m==CRLead){
				return 93.9E-6;
			}else if(m==CRMercury){
				return 0.182E-3;
			}else if(m==CRUranium){
				return NAN;
			}else if(m==CRGold){
				return 42.9;
			}else if(m==CRPlatinum){
				return 27.0E-6;
			}else if(m==CRIridium){
				return NAN;
			}else if(m==CROsmium){
				return NAN;
			}else if(m==CRWhiteDwarf){
				return NAN;
			}else if(m==CRAtomicNuclei){
				return NAN;
			}else if(m==CRNeutronStar){
				return NAN;
			}

		}
		
		// in Kelvin and Pascal
        //! Returns the density of a given substance under given conditions.
        //! @param t Temperature.
        //! @param p Pressure.
        //! @return Density in kg/m^3.
		template <materialCode m, typename T=glmath_real, T t=2.7315E+2, T p=1.013E+5> constexpr T density(){
			
			// at 0K matter would collapse into itself.
			if(t==0){
				return INFINITY;
			}
			
			T nt=2.7315E+2;
			T np=1.013E+5;
			T dt=t-nt;
			T dp=p-np;
		
			T M=molar_mass<m>();
			
			T d=0;
			
			// ideal gas
			if(m==CRHelium || m==CRHydrogen || m==CRNeon || m==CRXenon || m==CRKrypton || m==CRArgon || m==CRMethane || m==CREthane || m==CREthane || m==CRPropane || m==CRButhane || m==CRAir){
				d=(M*p)/(critical::physics::constants::gas_constant()*t);
			}else{
				if(m==CRStyrofoam){
					d=75;
				}else if(m==CRCork){
					d=240;
				}else if(m==CRLithium){
					d=535;
				}else if(m==CRWood){
					d=700;
				}else if(m==CRSodium){
					d=970;
				}else if(m==CRIce){
					d=916.7;
				}else if(m==CRWater){
					d=1000;
				}else if(m==CRPlastic){
					d=1175;
				}else if(m==CRMagnesium){
					d=1740;
				}else if(m==CRAluminium){
					d=2700;
				}else if(m==CRDiamond){
					d=3500;
				}else if(m==CRTitanium){
					d=4540;
				}else if(m==CRZinc){
					d=7000;
				}else if(m==CRChromium){
					d=7200;
				}else if(m==CRTin){
					d=7325;
				}else if(m==CRGranite){
					d=2691;
				}else if(m==CRIron){
					d=7870;
				}else if(m==CRCobalt){
					d=8900;
				}else if(m==CRNickel){
					d=8900;
				}else if(m==CRCopper){
					d=8940;
				}else if(m==CRSilver){
					d=10500;
				}else if(m==CRLead){
					d=11340;
				}else if(m==CRMercury){
					d=13546;
				}else if(m==CRUranium){
					d=18800;
				}else if(m==CRGold){
					d=19320;
				}else if(m==CRPlatinum){
					d=21450;
				}else if(m==CRIridium){
					d=22420;
				}else if(m==CROsmium){
					d=22570;
				}else if(m==CRWhiteDwarf){
					d=2.1E+9;
				}else if(m==CRAtomicNuclei){
					d=2.3E+17;
				}else if(m==CRNeutronStar){
					d=1.0E+18;
				}

				T alpha=thermal_expansion_coefficient<m>();
				if(alpha!=NAN){
					d=d/(1+alpha*dt);
				}
				
			}
			
		}
		
        enum InertiaTensorType{
            CRCube = 0,
            CRCylinder,
            CRSphere,
            CRCone,
        };
		
	}
}}


#endif /* defined(CRITICAL_PHYSICS_CONSTANTS_H_) */