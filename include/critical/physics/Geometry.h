//
//  Geometry.h
//  critical
//
//  Created by Armon Carigiet on 08.05.15.
//
//

#ifndef CRITICAL_PHYSICS_GEOMETRY_H_
#define CRITICAL_PHYSICS_GEOMETRY_H_

#include <glmath/glmath_vec3.h>

namespace critical { namespace math {
	
class Line;
class Plane;
	
	
class Line{
private:
	glmath_vec3 position;
	glmath_vec3 direction;
public:
	Line(glmath_vec3 p, glmath_vec3 d);
	
	glmath_vec3 getPosition();
	glmath_vec3 getDirection();
	
	void setPosition(glmath_vec3 &p);
	void setDirection(glmath_vec3 &d);
	
	bool isElement(glmath_vec3 &point);
	bool intersects(Line &line, glmath_vec3 *intersection_point, glmath_real *intersection_angle);
	bool intersects(Plane &plane, Line *intersection_line, glmath_real *intersection_angle);
	
	glmath_real distance(glmath_vec3 &point);
	glmath_real distance(Line &line);
	glmath_real distance(Plane &plane);
};
	
class Plane{
private:
	// HNF
	glmath_vec3 normal;
	glmath_real d;
public:
	Plane(glmath_vec3 &A, glmath_vec3 &B, glmath_vec3 &C);
	Plane(glmath_vec3 n, glmath_vec3 p);
	Plane(glmath_vec3 n, glmath_real dist);
	
	glmath_vec3 getNormal();
	glmath_vec3 getD();
	
	void setNormal(glmath_vec3 &p);
	void setD(glmath_vec3 &d);
	
	bool isElement(glmath_vec3 &point);
	bool intersects(Line &line, glmath_vec3 *intersection_point, glmath_real *intersection_angle);
	bool intersects(Plane &plane, Line *intersection_line, glmath_real *intersection_angle);
	
	glmath_real distance(glmath_vec3 &point);
	glmath_real distance(Line &line);
	glmath_real distance(Plane &plane);
	
	void reflectPoint(glmath_vec3 &point);
};
	
}}

#endif /* defined(CRITICAL_PHYSICS_GEOMETRY_H_) */
