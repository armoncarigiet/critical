//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file SimpleParticleGravityGenerator.h
 *	@date 16.04.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a class simulating the gravitational force approximated
 *  by a homgenous field.
 *  @ingroup physics
 */
//===========================================================================

#ifndef CRITICAL_PHYSICS_SIMPLEPARTICLEGRAVITYGENERATOR_H_
#define CRITICAL_PHYSICS_SIMPLEPARTICLEGRAVITYGENERATOR_H_

#include <critical/physics/ParticleForceGenerator.h>

namespace critical { namespace physics {

//! Forcegenerator class for Particles simulating the gravitational pull in one straight direction.
//! @ingroup physics
class SimpleParticleGravityGenerator: public ParticleForceGenerator{
protected:
	//glmath_vec3 g;
public:
    glmath_vec3 g;
    
    //! Constructor
    //! @param acceleration Gravitational acceleration used to callculate the forces.
    //! @param a activation status of the force generator.
    SimpleParticleGravityGenerator(glmath_vec3 acceleration, bool a=true): ParticleForceGenerator(a), g(acceleration){}
    SimpleParticleGravityGenerator(SimpleParticleGravityGenerator &f): ParticleForceGenerator(f.active), g(f.g){}
    SimpleParticleGravityGenerator(const SimpleParticleGravityGenerator &f): ParticleForceGenerator(f.active), g(f.g){}
    
    SimpleParticleGravityGenerator& operator=(SimpleParticleGravityGenerator &f){
        ParticleForceGenerator::operator=(f);
        g=f.g;
        return *this;
    }
    
    SimpleParticleGravityGenerator& operator=(SimpleParticleGravityGenerator &&f){
        ParticleForceGenerator::operator=(f);
        g=f.g;
        return *this;
    }
    
    //! Applies the resulting forces to the registered particles.
	void applyForce();

};
	
}}

#endif /* defined(CRITICAL_PHYSICS_SIMPLEPARTICLEGRAVITYGENERATOR_H_) */