//
//  PhysicsVector.h
//  critical
//
//  Created by Armon Carigiet on 14.04.15.
//
//

#ifndef CRITICAL_PHYSICS_PHYSICSVECTOR_H_
#define CRITICAL_PHYSICS_PHYSICSVECTOR_H_

#include <critical/core/Vector.h>
#include <critical/core/Chrono.h>

namespace critical { namespace physics {

template <class T>
class PhysicsVector: public Vector<T>{
public:
	
};
	
}}

#endif /* defined(CRITICAL_PHYSICS_PHYSICSVECTOR_H_) */
