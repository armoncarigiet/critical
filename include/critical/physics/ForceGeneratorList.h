//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file ForceGeneratorList.h
 *	@date 31.05.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a Class for storing different force generators.
 *  @ingroup physics
 */
//===========================================================================

#ifndef CRITICAL_PHYSICS_FORCEGENERATORLIST_H_
#define CRITICAL_PHYSICS_FORCEGENERATORLIST_H_

#include <critical/core/List.h>

#include <critical/physics/ParticleForceGenerator.h>
#include <critical/physics/SimpleParticleGravityGenerator.h>
#include <critical/physics/ParticleGravityGenerator.h>
#include <critical/physics/ParticleDragGenerator.h>

#include <critical/physics/RigidBodySpinGenerator.h>

namespace critical { namespace physics{

//template <class T=ParticleForceGenerator>
//	class ForceGeneratorList: public List<T>{
//private:
//	
//public:
//	void applyForce(){
//		for(typename List<T>::Iterator it=this->begin(); it.isOutOfRange(); it++){
//			it->applyForce();
//		}
//	}
//		
//	template <class U> void addObject(U* object){
//		for(typename List<T>::Iterator it=this->begin(); it.isOutOfRange(); it++){
//			it->addObject(object);
//		}
//	}
//};
    
//! Identifiers for the different forcegenerators.
enum ForceGeneratorType{
    CRUnknowen = -1,
    CRSimpleParticleGravityGenerator = 0,
    CRParticleDragGenerator,
    CRParticleGravityGenerator,
    CRSimpleRigidBodyGravityGenerator,
    CRRigidBodyGravityGenerator,
    CRRigidBodyDragGenerator,
    CRRigidBodySpinGenerator
};
    
template <typename T> ForceGeneratorType getForceGenCode(){
    if(typeid(T)==typeid(SimpleParticleGravityGenerator)){
        return CRSimpleParticleGravityGenerator;
    }else if(typeid(T)==typeid(ParticleGravityGenerator)){
        return CRParticleGravityGenerator;
    }else if(typeid(T)==typeid(ParticleDragGenerator)){
        return CRParticleDragGenerator;
    }else if(typeid(T)==typeid(RigidBodySpinGenerator)){
        return CRRigidBodySpinGenerator;
    }
    return CRUnknowen;
}
    
//! Class storing and managing different Forcegenerators.
//! @note Be carefull with this class. Adding of not registered or wrong forcegenerators can lead to messy bugs.
//! @ingroup physics
template <class T=ParticleForceGenerator>
class ForceGeneratorList{
private:
    static void __throw_out_of_range(){
    #if defined(CRITICAL_DEBUG)
        printf("\nList out of Range!\n");
    #endif
        throw MemoryException(CRMemoryExceptionContainerOutOfRange);
    }

protected:
    //! Ignore.
    template <class U=ForceGenerator>
    struct _list_block{
        _list_block *last;
        _list_block *next;
        MemoryManager::ConnectedPointer<void> p;
        ForceGeneratorType type;
        U data;
    };
    
    _list_block<T> *_front;
    _list_block<T> *_back;
    MemoryManager::ConnectedPointer<uint32_t> _refcounter;
    size_t _s;
    bool __alloc;

public:
    template <class U> class Iterator;
    
    //! Standart constructor.
    ForceGeneratorList(){
        _front=NULL;
        _back=NULL;
        _s=0;
        __alloc=false;
    }
    
    //! @note The Memory is only released if all Objects pointing to it are destroyed.
    ~ForceGeneratorList(){
        if(&_refcounter){
            --(*_refcounter);
            if((*_refcounter)==0){
                _refcounter.reset();
                _list_block<T> *block=_front;
                for(int i=0; i<_s; i++){
                    _list_block<T> *tmp=block->next;
                    block->p.reset();
                    block=tmp;
                }
            }
        }
    }
    
    //! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
    ForceGeneratorList(ForceGeneratorList &l) : _front(l._front), _back(l._back), _s(l._s), _refcounter(l._refcounter), __alloc(l.__alloc){ if(_refcounter.getVoidPtr()){ ++(*_refcounter); } }
    
    //! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
    ForceGeneratorList(ForceGeneratorList &&l) : _front(l._front), _back(l._back),_s(l._s), _refcounter(l._refcounter), __alloc(l.__alloc){ if(_refcounter.getVoidPtr()){ ++(*_refcounter); } }
    
    //! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
    ForceGeneratorList<T>& operator=(ForceGeneratorList<T> &l){
        __alloc=l.__alloc;
        _s=l._s;
        _front=l._front;
        _back=l._back;
        _refcounter=l._refcounter;
        if(_refcounter.getVoidPtr()){
            ++(*_refcounter);
        }
        return *this;
    }
    
    //! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
    ForceGeneratorList<T>& operator=(ForceGeneratorList<T> &&l){
        __alloc=l.__alloc;
        _s=l._s;
        _front=l._front;
        _back=l._back;
        _refcounter=l._refcounter;
        if(_refcounter.getVoidPtr()){
            ++(*_refcounter);
        }
        return *this;
    }
    
    //! Allocates new referencecounter.
    void allocate(){
        _refcounter=MemoryManager::salloc<uint32_t>(sizeof(uint32_t));
        *(_refcounter)=1;
        __alloc=true;
    }
    
    
    //! @return Iterator pointing to the list's first object.
    template <class U> Iterator<U> begin(){
        return Iterator<U>(static_cast<_list_block<U>*>(_front));
    }
    
    //! @return Iterator pointing to the list's last object.
    template <class U> Iterator<U> end(){
        return Iterator<U>(static_cast<_list_block<U>*>(_back));
    }
    
    //! @return Size of the list.
    size_t size(){
        return _s;
    }
    
    //! @return Reference to the list's first object.
    template <class U> U& front(){
        return static_cast<U*>(_front)->data;
    }
    
    //! @return Reference to the list's first object.
    template <class U> const U& front() const{
        return static_cast<U*>(_front)->data;
    }
    
    //! @return Reference to the list's last object.
    template <class U> U& back(){
        return static_cast<U*>(_back)->data;
    }
    
    //! @return Reference to the list's last object.
    template <class U> const U& back() const{
        return static_cast<U*>(_back)->data;
    }
    
    //! Inserts a new entry at the begin of the list.
    //! @param __ref Object to copy.
    //! @return Iterator pointing to the inserted object.
    template <class U>
    Iterator<U> pushFront(const U& __ref){
        if(!__alloc){
            allocate();
        }
        MemoryManager::ConnectedPointer<void> pointer=MemoryManager::salloc<void>(sizeof(_list_block<U>));
        _list_block<U> *block=static_cast<_list_block<U>*>(&pointer);
        block->p=MemoryManager::ConnectedPointer<void>(pointer);
        block->data=U(__ref);
        block->last=nullptr;
        block->next=reinterpret_cast<_list_block<U>*>(_front);
        block->type=getForceGenCode<U>();
        _list_block<T> *b=reinterpret_cast<_list_block<T>*>(block);
        //_list_block<U> *test=reinterpret_cast<_list_block<U>*>(b);
        
        if(_front){
            _front->last=b;
        }
        
        _front=b;
        if(_s==0){
            _back=b;
        }
        ++_s;
        return Iterator<U>(block);
    }
    
    //! Deletes the first element of the list.
    void popFront(){
        if(_front){
            _list_block<T> *tmp=_front;
            _front=_front->next;
            tmp->p.reset();
            --_s;
        }
    }

    //! Applies the regeistered forcegenerators' Forces to their registered particles.
    void applyForce(){
        for(typename ForceGeneratorList<T>::Iterator<T> it=this->begin(); it.isOutOfRange(); it++){
            it->applyForce();
        }
    }
    
    //! Registers a new forcegenerator.
    template <class U> void addObject(U* object){
        for(typename ForceGeneratorList<T>::Iterator<T> it=this->begin<T>(); it.isOutOfRange(); it++){
            it->addObject(object);
        }
    }
    
    template <class U>
    class Iterator{
        friend class ForceGeneratorList;
    private:
        _list_block<U>* _b;
        Iterator(_list_block<U> *b) : _b(b) {}
    public:
        Iterator() : _b(nullptr) {}
        ~Iterator() {}
        
        // slow!
        //! @return Pointer to the List's object, which the Iterator is holding.
        U* operator&(){
            if(!_b){
                return NULL;
            }
            return &_b->data;
        }
        
        //! @note Exception is thrown if the Iterator is pointing to an invalid object.
        //! @return Reference to the List's object, which the Iterator is holding.
        U& operator*(){
            if(!_b){
                __throw_out_of_range();
            }
            return _b->data;
        }
        
        //! Access object the iterator is holding.
        //! @return Pointer to the List's object, which the Iterator is holding.
        U* operator->() const{
            if(!_b){
                return NULL;
            }
            return &_b->data;
        }
        
        ForceGeneratorType getForceGeneratorType(){
            return _b->type;
        }
        
        //! Checks if the iterator is pointing to an invalid object/ if the iterator is out of the List's range.
        //! @return true if iterator is out of range, else false.
        bool isOutOfRange(){
            if(!_b){
                return true;
            }
            return false;
        }
        
        //		Iterator& operator=(Iterator &i){
        //			_b=i._b;
        //			return *this;
        //		}
        //		Iterator& operator=(Iterator &&i){
        //			_b=i._b;
        //			return *this;
        //		}
        
        //! Creates an iterator pointing to an object in front of the object the current iterator is holding.
        //! @param offset Offset between the object the current iterator is holding and the aimed one.
        //! @return Iterator pointing to the new object.
        Iterator operator+(uint32_t offset){
            _list_block<U> *tmp=_b;
            for(int i=0; i<offset; i++) {
                if(tmp){
                    tmp=tmp->next;
                }else{
                    __throw_out_of_range();
                    break;
                }
            }
            return Iterator(tmp);
        }
        
        //! Creates an iterator pointing to an object behind the object the current iterator is holding.
        //! @param offset Offset between the object the current iterator is holding and the aimed one.
        //! @return Iterator pointing to the new object.
        Iterator operator-(ptrdiff_t offset){
            _list_block<U> *tmp=_b;
            for(int i=0; i<offset; i++) {
                if(tmp){
                    tmp=tmp->last;
                }else{
                    __throw_out_of_range();
                    break;
                }
            }
            return Iterator(tmp);
        }
        
        //! Moves the iterator forward.
        //! @param offset Number of objects to move forward.
        //! @return Reference to the modified iterator.
        Iterator& operator+=(ptrdiff_t offset){
            for(int i=0; i<offset; i++) {
                if(_b){
                    _b=_b->next;
                }else{
                    __throw_out_of_range();
                    break;
                }
            }
            return *this;
        }
        
        //! Moves the iterator back.
        //! @param offset Number of objects to move forward.
        //! @return Reference to the modified iterator.
        Iterator& operator-=(ptrdiff_t offset){
            for(int i=0; i<offset; i++) {
                if(_b){
                    _b=_b->last;
                }else{
                    __throw_out_of_range();
                    break;
                }
            }
            return *this;
        }
        
        //! Moves the iterator forward by one.
        //! @return Reference to the modified iterator.
        Iterator& operator++(){
            if(_b){
                _b=_b->next;
            }else{
                __throw_out_of_range();
            }
           return *this;
        }
        
        //! Moves the iterator back by one.
        //! @return Reference to the modified iterator.
        Iterator& operator--(){
            if(_b){
                _b=_b->last;
            }else{
                __throw_out_of_range();
            }
            return *this;
        }
        
        //! Creates an iterator pointing to the object in front of the object the current iterator is holding.
        //! @return New iterator.
        Iterator operator++(int){
            Iterator i(*this);
            ++(*this);
            return i;
        }
        
        //! Creates an iterator pointing to the object behind the object the current iterator is holding.
        //! @return New iterator.
        Iterator operator--(int){
            Iterator i(*this);
            --(*this);
            return i;
        }
        
        template <class K> Iterator<K> getIterator(){
            return Iterator<K>(static_cast<K*>(_b));
        }
        
        template <class K> K* getForceGenerator(){
            return static_cast<K*>(&_b->data);
        }
        
    };
};
	
}}

#endif /* defined(CRITICAL_PHYSICS_FORCEGENERATORLIST_H_) */