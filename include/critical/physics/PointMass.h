//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file PointMass.h
 *	@date 30.07.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a Class representing a point mass used for the process
 *  of calculating a rigidbody's properties.
 *  @ingroup physics
 */
//===========================================================================

#ifndef CRITICAL_PHYSICS_POINTMASS_H_
#define CRITICAL_PHYSICS_POINTMASS_H_

#include <glmath/glmath_types.h>
#include <critical/physics/Particle.h>

namespace critical{ namespace physics {

//! Class representing a inmoveable pointmass.
//! @note This class is mainly used for the calculation of insertia tensors. 
//! @ingroup physics
class PointMass{
private:
    
public:
    glmath_real mass;
    glmath_vec3 position;
    
    PointMass(Particle p): mass(1/p.inverseMass), position(p.position){}
    PointMass(glmath_vec3 p, glmath_real m): mass(m), position(p){}
};
    
}}

#endif /* defined(CRITICAL_PHYSICS_POINTMASS_H_) */