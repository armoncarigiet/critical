//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file RigidBodyList.h
 *	@date 25.07.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a Class for storing different RigidBodies.
 *  @ingroup physics
 */
//===========================================================================

#ifndef CRITICAL_PHYSICS_RIGIDBODYLIST_H_
#define CRITICAL_PHYSICS_RIGIDBODYLIST_H_

#include <critical/core/List.h>
#include <critical/physics/RigidBody.h>

namespace critical { namespace physics {

//! List class adjustet to hold rigidbodies.
//! @ingroup physics
template <class T=RigidBody>
class RigidBodyList: public List<T>{

public:
    void integrate(glmath_real dt){
        for(typename List<T>::Iterator it=this->begin(); !it.isOutOfRange(); it++){
            it->integrate(dt);
        }
    }
    
    void addForce(glmath_vec3 f){
        for(typename List<T>::Iterator it=this->begin(); !it.isOutOfRange(); it++){
            it->addForce(f);
        }
    }
    void clearForceAccumulator(){
        for(typename List<T>::Iterator it=this->begin(); !it.isOutOfRange(); it++){
            it->clearForceAccumulator();
        }
    }
    void clearTorqueAccumulator(){
        for(typename List<T>::Iterator it=this->begin(); !it.isOutOfRange(); it++){
            it->clearTorqueAccumulator();
        }
    }
    void clearAccumulators(){
        for(typename List<T>::Iterator it=this->begin(); !it.isOutOfRange(); it++){
            it->clearAccumulators();
        }
    }

};
    
}}

#endif /* defined(__critical__RigidBodyList__) */