//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file ParticleSpring.h
 *	@date 18.04.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a Class representing a spring attached between two
 *  Particles.
 *  @ingroup physics
 */
//===========================================================================

#ifndef CRITICAL_PHYSICS_PARTICLESPRING_H_
#define CRITICAL_PHYSICS_PARTICLESPRING_H_

#include <critical/physics/Particle.h>

namespace critical { namespace physics {

//! Spring between two particles
//! @ingroup physics
class ParticleSpring{
	Particle *_p1;
	Particle *_p2;
	
	glmath_real slength;
	glmath_real sconstant;
	
	ParticleSpring(Particle *p1, Particle *p2, glmath_real y, glmath_real D): _p1(p1), _p2(p2), slength(y), sconstant(D){}
	
	void applyForce();
};
	
}}

#endif /* defined(CRITICAL_PHYSICS_PARTICLESPRING_H_) */