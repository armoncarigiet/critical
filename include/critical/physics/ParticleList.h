//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file ParticleList.h
 *	@date 16.04.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a Class for storing different Particles.
 *  @ingroup physics
 */
//===========================================================================

#ifndef CRITICAL_PHYSICS_PARTICLELIST_H_
#define CRITICAL_PHYSICS_PARTICLELIST_H_

#include <critical/core/List.h>
#include <critical/physics/Particle.h>

namespace critical { namespace physics {

//! List class adjustet to hold particles.
//! @ingroup physics
template <class T=Particle>
class ParticleList: public List<T>{
public:
	void integrate(glmath_real dt){
		for(typename List<T>::Iterator it=this->begin(); !it.isOutOfRange(); it++){
			it->integrate(dt);
		}
	}
	
	void addForce(glmath_vec3 f){
		for(typename List<T>::Iterator it=this->begin(); !it.isOutOfRange(); it++){
			it->addForce(f);
		}
	}
	void clearForceAccumulator(){
		for(typename List<T>::Iterator it=this->begin(); !it.isOutOfRange(); it++){
			it->clearForceAccumulator();
		}
	}
};
	
}}

#endif /* defined(CRITICAL_PHYSICS_PARTICLELIST_H_) */