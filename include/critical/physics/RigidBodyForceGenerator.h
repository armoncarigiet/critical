//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file RigidBodyForceGenerator.h
 *	@date 09.08.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a Template for creating force generators for rigidbodies.
 *  @ingroup physics
 */
//===========================================================================

#ifndef CRITICAL_PHYSICS_RIGIDBODYFORCEGENERATOR_H_
#define CRITICAL_PHYSICS_RIGIDBODYFORCEGENERATOR_H_

#include <critical/core/Vector.h>
#include <critical/physics/RigidBody.h>
#include <critical/physics/RigidBodyList.h>

//! Simple template class to create forcegenerators for rigidbodies.
//! @ingroup physics
namespace critical { namespace physics {
    
    class RigidBodyForceGenerator{
    protected:
        Vector<RigidBody*> _rb;
        bool active;
    public:
        RigidBodyForceGenerator(bool a): active(a){}
        RigidBodyForceGenerator(RigidBodyForceGenerator &f): active(f.active){}
        RigidBodyForceGenerator(const RigidBodyForceGenerator &f): active(f.active){}
        
        RigidBodyForceGenerator& operator=(RigidBodyForceGenerator &f){
            active=f.active;
            return *this;
        }
        
        RigidBodyForceGenerator& operator=(RigidBodyForceGenerator &&f){
            active=f.active;
            return *this;
        }
        
        //! Changes the status of the forcegenerator
        //! @param [true, false] True if the forcegenerator is to be activated, flas if it has to be deactivated.
        void setActive(bool a){
            active=a;
        }
        
        Vector<RigidBody*>::Iterator addObject(RigidBody *rigidBody);
        Vector<RigidBody*>::Iterator addObjects(RigidBodyList<> *list);
        void applyForce(){ /* What force? */ } // can't maek virtual
    };
    
}}

#endif /* defined(CRITICAL_PHYSICS_RIGIDBODYFORCEGENERATOR_H_) */