//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file RigidBodySpinGenerator.h
 *	@date 09.08.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a class applying a Force to several rigidbodys to create a
 *  spin movement.
 *  @ingroup physics
 */
//===========================================================================

#ifndef CRITICAL_PHYSICS_RIGIDBODYSPINGENERATOR_H_
#define CRITICAL_PHYSICS_RIGIDBODYSPINGENERATOR_H_

#include <critical/physics/RigidBodyForceGenerator.h>
#include <critical/physics/Constants.h>

namespace critical { namespace physics {
    
//! Forcegenerator able to simulate a spin-creating force.
//! @ingroup physics.
class RigidBodySpinGenerator: public RigidBodyForceGenerator{
private:
    glmath_vec3 force;
    glmath_vec3 attackLocation;
    bool world;
public:
    RigidBodySpinGenerator(glmath_vec3 f, glmath_vec3 p, bool a=true): RigidBodyForceGenerator(a), force(f), attackLocation(p), world(false){}
    RigidBodySpinGenerator(RigidBodySpinGenerator &f): RigidBodyForceGenerator(f.active), force(f.force), attackLocation(f.attackLocation), world(f.world){}
    RigidBodySpinGenerator(const RigidBodySpinGenerator &f): RigidBodyForceGenerator(f.active), force(f.force), attackLocation(f.attackLocation), world(f.world){}
        
    RigidBodySpinGenerator& operator=(RigidBodySpinGenerator &f){
        RigidBodyForceGenerator::operator=(f);
        force=f.force;
        attackLocation=f.attackLocation;
        world=f.world;
        return *this;
    }
    
    RigidBodySpinGenerator& operator=(RigidBodySpinGenerator &&f){
        RigidBodyForceGenerator::operator=(f);
        force=f.force;
        attackLocation=f.attackLocation;
        world=f.world;
        return *this;
    }
    
    //! Apply the force to the registered rigidbodies.
    void applyForce();
};
    
}}

#endif /* defined(CRITICAL_PHYSICS_RIGIDBODYSPINGENERATOR_H_) */