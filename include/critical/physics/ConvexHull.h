//
//  ConvexHull.h
//  critical
//
//  Created by Armon Carigiet on 30.07.15.
//
//

#ifndef CRITICAL_PHYSICS_CONVEXHULL_H_
#define CRITICAL_PHYSICS_CONVEXHULL_H_

#include <critical/core/Vector.h>
#include <glmath/glmath_types.h>

namespace critical { namespace physics {
class ConvexHull{
private:
    Vector<glmath_vec3> p;
public:
    
};
}}

#endif /* defined(CRITICAL_PHYSICS_CONVEXHULL_H_) */