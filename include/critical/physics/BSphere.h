//
//  Sphere.h
//  critical
//
//  Created by Armon Carigiet on 02.05.15.
//
//

#ifndef CRITICAL_PHYSICS_BSPHERE_H_
#define CRITICAL_PHYSICS_BSPHERE_H_

#include <critical/physics/RigidBody.h>
#include <critical/graphics/TriangulatedMesh.h>

class BSphere{
private:
    glmath_real radius;
    glmath_vec3 position;
public:
    
    bool isIntersecting(BSphere &s){
        glmath_vec3 v=glmath_vec3_sub(this->position, s.position);
        glmath_real ds=glmath_vec3_dot(v, v);
        
        glmath_real r=radius+s.radius;
        if(ds<r*r){
            return true;
        }else{
            return false;
        }
    }
    
    glmath_vec3 resolveIntersection(BSphere &s){
        glmath_vec3 v=glmath_vec3_sub(this->position, s.position);
        v=glmath_vec3_add(s.position, glmath_vec3_mul_s(v, 0.5));
        return v;
    }
    
    void setRadius(glmath_real r){
        radius=r;
    }
    
    glmath_real getRadius(){
        return radius;
    }
    
    void setPosition(glmath_vec3 p){
        position=p;
    }
    
    glmath_vec3 getPosition(){
        return position;
    }
    
    void fitMesh(critical::TriangulatedMesh mesh){
        // center the two most far away positions in mesh to get Sphere
    }
    
    

    
};

#endif /* defined(CRITICAL_PHYSICS_SPHERE_H_) */
