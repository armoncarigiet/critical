//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Particle.h
 *	@date 23.03.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a class representing an object with all mass concentratet into
 *  an infinitely small point.
 *  @ingroup physics
 */
//===========================================================================

#ifndef CRITICAL_PHYSICS_PARTICLE_H_
#define CRITICAL_PHYSICS_PARTICLE_H_

#include <glmath/glmath_vec3.h>
#include <critical/physics/ForceObject.h>

namespace critical { namespace physics {

//! Class representing a pointmass.
//! @ingroup physics
class Particle: public ForceObject{
private:	

public:
    //! Contructor
    //! @param n The Particle's name.
    //! @param mass The Particle's mass.
    //! @param p0 The initial position.
    //! @param p0 The initial velocity.
    //! @param d Damping factor (not in use).
	Particle(String &n, glmath_real mass=INFINITY, glmath_vec3 p0={{0,0,0}}, glmath_vec3 v0={{0,0,0}}, glmath_real d=0): ForceObject(n,mass,p0,v0) {}
    //! Contructor
    //! @param n The Particle's name.
    //! @param mass The Particle's mass.
    //! @param p0 The initial position.
    //! @param p0 The initial velocity.
    //! @param d Damping factor (not in use).
	Particle(String &&n, glmath_real mass=INFINITY, glmath_vec3 p0={{0,0,0}}, glmath_vec3 v0={{0,0,0}}, glmath_real d=0): ForceObject(n,mass,p0,v0) {}
    
    //! Updates the position and velocity using th applied forces.
    //! @param Time interval to iterate forward.
	void integrate(glmath_real dt){
		position=glmath_vec3_add(position, glmath_vec3_mul_s(velocity, dt));
		//glmath_real speed=glmath_vec3_mag(velocity);	// expensive!
		glmath_vec3 force_acceleration=glmath_vec3_mul_s(ForceAccumulator, inverseMass);
		velocity=glmath_vec3_add(velocity, glmath_vec3_mul_s(force_acceleration, dt));
		this->clearForceAccumulator();
	}
};

}}

#endif /* defined(CRITICAL_PHYSICS_PARTICLE_H_) */