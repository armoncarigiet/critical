//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file FixedParticleBungeeRope.h
 *	@date 18.04.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a Class representing a Pined up bungee rope attached to a Particle.
 *  @ingroup physics
 */
//===========================================================================

#ifndef CRITICAL_PHYSICS_FIXEDPARTICLEBUNGEEROPE_H_
#define CRITICAL_PHYSICS_FIXEDPARTICLEBUNGEEROPE_H_

#include <critical/physics/Particle.h>

namespace critical { namespace physics {
	
    //! Simulates a pinned up bungee rope.
    //! @ingroup physics
	class FixedParticleBungeeRope{
		Particle *_p;
		glmath_vec3 fixed_point;
		
		glmath_real slength;
		glmath_real sconstant;
		
		FixedParticleBungeeRope(Particle *p, glmath_vec3 fp, glmath_real y, glmath_real D): _p(p), fixed_point(fp), slength(y), sconstant(D){}
		
		void applyForce();
	};
	
}}

#endif /* defined(CRITICAL_PHYSICS_FIXEDPARTICLEBUNGEEROPE_H_) */