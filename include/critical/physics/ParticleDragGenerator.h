//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file ParticleDragGenerator.h
 *	@date 17.04.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a Class creating a drag force for pointmasses.
 *  @ingroup physics
 */
//===========================================================================

#ifndef CRITICAL_PHYSICS_PARTICLEDRAGGENERATOR_H_
#define CRITICAL_PHYSICS_PARTICLEDRAGGENERATOR_H_

#include <critical/physics/ParticleForceGenerator.h>

namespace critical { namespace physics{

//! A forcegenerator for particles, which simulates the imposed dragforce.
//! @note It calculates just a simple form of drag.
//! @todo move the drag constant and the half-section area to the Particle's class.
class ParticleDragGenerator: public ParticleForceGenerator{
	glmath_real d;
	glmath_real cw;
	glmath_real a;
public:
	ParticleDragGenerator(glmath_real density, glmath_real drag_coefficient, glmath_real surface, bool act=true): ParticleForceGenerator(act), d(density), cw(drag_coefficient), a(surface){}
    ParticleDragGenerator(ParticleDragGenerator &f): ParticleForceGenerator(f.active), d(f.d), cw(f.cw), a(f.a){}
    ParticleDragGenerator(const ParticleDragGenerator &f): ParticleForceGenerator(f.active), d(f.d), cw(f.cw), a(f.a){}
    
    ParticleDragGenerator& operator=(ParticleDragGenerator &f){
        ParticleForceGenerator::operator=(f);
        d=f.d;
        cw=f.cw;
        a=f.a;
        return *this;
    }
    
    ParticleDragGenerator& operator=(ParticleDragGenerator &&f){
        ParticleForceGenerator::operator=(f);
        d=f.d;
        cw=f.cw;
        a=f.a;
        return *this;
    }
    
	void setDensity(glmath_real density);
	glmath_real getDensity();
	void setDragCoefficient(glmath_real drag_coefficient);
	glmath_real getDragCoefficient();
	void setSurface(glmath_real surface);
	glmath_real getSurface();
	
	void applyForce();
};
	
}}

#endif /* defined(CRITICAL_PHYSICS_PARTICLEDRAGGENERATOR_H_) */