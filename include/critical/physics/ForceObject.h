//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file ForceObject.h
 *	@date 14.04.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a Class representing an Object which can be influenced by 
 *  newtonian Forces.
 *  @ingroup physics
 */
//===========================================================================

#ifndef CRITICAL_PHYSICS_FORCEOBJECT_H_
#define CRITICAL_PHYSICS_FORCEOBJECT_H_

#include <glmath/glmath_vec3.h>
#include <critical/physics/pherror.h>
#include <critical/core/String.h>

namespace critical { namespace physics {

//! Simple template for objects influenced by newtonian forces.
//! @note This template can't be used to creates objects from it.
//! @ingroup physics
class ForceObject{
private:
	static void __throw_forceobject_has_no_mass(ForceObject *p){
	#if defined(CRITICAL_DEBUG)
		printf("\nForceObject: %p has no mass", p);
	#endif
		throw CRHasNoMass;
	}
public:
	
	String identifier;
	
	glmath_vec3 position;
	glmath_vec3 velocity;
	
	// D’ALEMBERT’S PRINCIPLE
	glmath_vec3 ForceAccumulator;
	
	glmath_real inverseMass;
	
	ForceObject(String &name, glmath_real mass=INFINITY, glmath_vec3 p0={{0,0,0}}, glmath_vec3 v0={{0,0,0}}): identifier(name), position(p0), velocity(v0), ForceAccumulator({{0,0,0}}){
		if(mass==INFINITY){
			inverseMass=0;
		}else if(mass==0){
			__throw_forceobject_has_no_mass(this);
		}else{
			inverseMass=1/mass;
		}
	}
	
	ForceObject(String &&name, glmath_real mass=INFINITY, glmath_vec3 p0={{0,0,0}}, glmath_vec3 v0={{0,0,0}}): identifier(name), position(p0), velocity(v0), ForceAccumulator({{0,0,0}}){
		if(mass==INFINITY){
			inverseMass=0;
		}else if(mass==0){
			__throw_forceobject_has_no_mass(this);
		}else{
			inverseMass=1/mass;
		}
	}
	
	glmath_real getMass();
	glmath_vec3 getForce();
	glmath_vec3 getPosition();
	glmath_vec3 getVelocity();
	
	void addForce(glmath_vec3 f);
	void clearForceAccumulator(){
		ForceAccumulator={{0,0,0}};
	}
	
	// Numerical integrator
	void integrate(glmath_real t){
		position=glmath_vec3_add(position, glmath_vec3_mul_s(velocity, t));
		velocity=glmath_vec3_add(velocity, glmath_vec3_add_s(glmath_vec3_add_s(ForceAccumulator,inverseMass), t));
		clearForceAccumulator();
	}
	
};
	
}}

#endif /* defined(CRITICAL_PHYSICS_FORCEOBJECT_H_) */