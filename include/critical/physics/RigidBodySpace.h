//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file RigidBodySpace.h
 *	@date 14.05.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a Class for simulating the movent of and the interactions
 *  between RigidBodies.
 *  @ingroup physics
 */
//===========================================================================

#ifndef CRITICAL_PHYSICS_RIGIDBODYSPACE_H_
#define CRITICAL_PHYSICS_RIGIDBODYSPACE_H_

#include <critical/physics/RigidBody.h>
#include <critical/physics/RigidBodyList.h>
#include <critical/physics/ForceGeneratorList.h>
#include <critical/physics/RigidBodyForceGenerator.h>
#include <critical/physics/RigidBodySpinGenerator.h>

namespace critical { namespace physics {

//! Class representing a simulation space for rigidbodies.
//! @ingroup physics
class RigidBodySpace{
private:
    RigidBodyList<> rblist;
    ForceGeneratorList<RigidBodyForceGenerator> fgenList;
    //ForceGeneratorList<SimpleParticleGravityGenerator> sGravGenList;
    //ForceGeneratorList<ParticleGravityGenerator> gravGenList;
    //ForceGeneratorList<ParticleDragGenerator> dragGenList;
    
    bool simulationActive;

public:
    
    template <class T> T* addForceGenerator(T &gen, bool activated=true){
        T* ptr=&fgenList.pushFront(gen);
        ptr->setActive(activated);
        return ptr;
    }
    
    template <class T> T* addForceGenerator(T &&gen, bool activated=true){
        T* ptr=&fgenList.pushFront(gen);
        ptr->setActive(activated);
        return ptr;
    }
    
    RigidBody* addObject(RigidBody &rb);
    RigidBody* addObject(RigidBody &&rb);
    bool removeObject(RigidBody *rb);
    bool removeObject(String &n);
    bool removeObject(String &&n);
    
    void initFrame();
    
    //! Makes all forcegenerators apply their forces and torques to the registered rigidbodies.
    void applyForces();
    
    //! Updates the position and veloctiy of all registered particles.
    //! @param dt Time interval to iterate forward.
    void integrate(glmath_real dt);
	//void writeData(void *mem);
    
     //! Starts the simulation if it was previously stopped.
    void startSimulation(){
        simulationActive=true;
    }
    
    //! Prevents the Particles from beeing integrated.
    void stopSimulation(){
        simulationActive=false;
    }
    
    //! Checks if the simulation is running.
    //! @return [true, false] True if it is running, false if not.
    bool simulationIsRunning(){
        return simulationActive;
    }
    
};
	
}}

#endif /* defined(CRITICAL_PHYSICS_RIGIDBODYSPACE_H_) */