//
//  ForceGenerator.h
//  critical
//
//  Created by Armon Carigiet on 14.04.15.
//
//

#ifndef CRITICAL_PHYSICS_FORCEGENERATOR_H_
#define CRITICAL_PHYSICS_FORCEGENERATOR_H_

#include <critical/core/Vector.h>
#include <critical/physics/ForceObject.h>

namespace critical{ namespace physics {

class ForceGenerator{
private:
	//Vector<Force>
public:
	
	//virtual void updateForce(ForceObject %obj) = 0;
    //virtual uint32_t addObject(ForceObject *obj) = 0;
    virtual void applyForce(){};
};
	
}}

#endif /* defined(CRITICAL_PHYSICS_FORCEGENERATOR_H_) */