//
//  MemoryTable.h
//  critical
//
//  Created by Armon Carigiet on 07.12.14.
//
//

#ifndef CRITICAL_CORE_MEMORYSTACK_H_
#define CRITICAL_CORE_MEMORYSTACK_H_


#include <vector>
#include "MemoryManager.h"

namespace critical {
	
class MemoryTable{
	friend class memoryTableAnalyser;
private:
	
	struct _mem_tbl_block{
		void* address;
		bool allocated;
	};
	MemoryManager memoryManager;
	
	MemoryManager::ConnectedPointer<void> ptr;
	
	MemoryManager::ConnectedPointer<void> reg;
	
	int tableEntryCount;
	size_t tableSize;
	size_t usedTableSize;
	size_t blockSize;
	
//	MemoryManager::_mem_table_entry* getMemTableEntry(){
//		return ptr.tableEntry;
//	}
public:
	MemoryTable(size_t blocksize);
	MemoryTable(size_t blocksize, int capacity);
	
	~MemoryTable();
	
	class tb_ptr{
	private:
		void* mem_ptr;
		uint32_t index;
		bool alloc;
	public:
		tb_ptr(): mem_ptr(NULL), index(0), alloc(false){}
		tb_ptr(uint32_t i, void* mem): mem_ptr(mem), index(i), alloc(true){}
		
		~tb_ptr();
		
		tb_ptr& operator=( const tb_ptr& other );
		tb_ptr& operator=( tb_ptr& other );
		void operator[](const uint32_t &index);
		void* operator&();
		void operator*();
		void* operator->() const;
	};
	
	void expand(size_t bytes);
	void retract(size_t bytes);
	void expandc(int capacity);
	void retractc(int capacity);
	
	tb_ptr addBlock();
	bool deleteBlock(tb_ptr &ptr);
	bool eraseBlock(tb_ptr &ptr);
	void* getAddressAtIndex(int index);
	
};
	
	
}

#endif /* defined(CRITICAL_CORE_MEMORYSTACK_H_) */
