//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file RenderInformation.h
 *	@date 23.03.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides identifiers and classes for the render engines.
 *  @ingroup core
 */
//===========================================================================

#ifndef CRITICAL_CORE_RENDERINFORMATION_H_
#define CRITICAL_CORE_RENDERINFORMATION_H_

#include "Map.h"
#include "String.h"

namespace critical {

//! Indentifiers for the render engines.
enum RenderType{
	CRUndefinedRenderer = 0,
	CROpenGL,
	CRBuiltIn,			// maybe for the future :)
};

//! Identifier for the different shader types.
enum ShaderType{
	CRVertexShader = 0,
	CRFragmentShader,
	CRGeometryShader,
	CRTesselationControlShader,
	CRTesselationEvaluationShader
};
	
enum RenderAttributes{
	CRWindowHeight=0,
	CRWindowWidth,
};
    
//! Class holding information to initialize the render engine.
//! @ingroup core
class RenderInformation{
public:
	RenderType type;
	Map<String> attributes;
	
	// to add more!
	
};
	
}

#endif /* defined(CRITICAL_CORE_RENDERINFORMATION_H_) */
