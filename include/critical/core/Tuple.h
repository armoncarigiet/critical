//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Tuple.h
 *	@date 17.05.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a data class for pair value sorage.
 *  @ingroup core
 */
//===========================================================================

#ifndef CRITICAL_CORE_TUPLE_H_
#define CRITICAL_CORE_TUPLE_H_

namespace critical {
	
//! @ingroup core
template <class T1, class T2>
class Tuple{
public:
	T1 _t1;
	T2 _t2;
	Tuple(T1 val1, T2 val2): _t1(val1), _t2(val2){}
	Tuple(Tuple<T1, T2> &t): _t1(t._t1), _t2(t._t2){}
	Tuple(Tuple<T1, T2> &&t): _t1(t._t1), _t2(t._t2){}
    
    Tuple<T1,T2>& operator=(Tuple<T1,T2> &p){
        _t1=p._t1;
        _t2=p._t2;
        return *this;
    }
    
    Tuple<T1,T2>& operator=(Tuple<T1,T2> &&p){
        _t1=p._t1;
        _t2=p._t2;
        return *this;
    }
};
	
}

#endif /* defined(CRITICAL_CORE_TUPLE_H_) */