//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Chrono.h
 *	@date 08.04.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides helper-functions for cross platform CPU-timing.
 *  @ingroup core
 *  @see Chrono.cpp
 */
//===========================================================================

#ifndef CRITICAL_CORE_CHRONO
#define CRITICAL_CORE_CHRONO

#include <stdint.h>
#include <glmath/glmath_types.h>
#include <glmath/glmath_constants.h>

#if defined(CRITICAL_POSIX)
	#include <unistd.h>
	#include <time.h>
	#include <sys/time.h>
	#include <sys/types.h>
	#if defined(__MACH__)
		#include <sys/_types/_timespec.h>
		#include <mach/mach.h>
		#include <mach/clock.h>
		#include <mach/mach_time.h>

		typedef int clockid_t;
		#define CLOCK_REALTIME CALENDAR_CLOCK
		#define CLOCK_MONOTONIC SYSTEM_CLOCK
	#endif
#elif defined(CRITICAL_WINAPI)
	typedef int clockid_t;
#endif

#define CRITICAL_IDEAL_FRAME	(1.0f/30.0f)
#define CRITICAL_FAST_FRAME		(1.0f/60.0f)
#define CRITICAL_IDEAL_FRAME_NANOSECONDS	CRITICAL_IDEAL_FRAME/GLMATH_NANO
#define CRITICAL_FAST_FRAME_NANOSECONDS		CRITICAL_FAST_FRAME/GLMATH_NANO

#if defined(__MACH__) || defined(CRITICAL_WINDOWS)
	int clock_gettime(clockid_t clock_id, struct timespec *tp);
#endif

namespace critical {

enum timeUnit{
	CRNanoseconds = 0,
	CRMicroseconds,
	CRMilliseconds,
	CRSeconds
};
	
enum clockType{
	CRRealtimeClock = 0,
	CRMonotonicClock,
	CRProcessClock,
	CRThreadClock
};
	
}

#endif