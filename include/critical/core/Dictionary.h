//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Dictionary.h
 *	@date 25.02.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a dictionary container, storring key-value pairs.
 *  @ingroup core
 */
//===========================================================================

#ifndef CRITICAL_CORE_DICTIONARY_H_
#define CRITICAL_CORE_DICTIONARY_H_

#include "Vector.h"
#include "String.h"


namespace critical {

template <class T>
struct _dictionary_entry{
	String _k;
	T _v;
};

//! Container that stores string-value pairs.
//! @tparam T Type of values to store.
//! @ingroup core
template <class T>
class Dictionary: private Vector<_dictionary_entry<T>>{
	friend class dictionaryAnalyser;
public:
private:
	//! Ignore.
	void __throw_no_value_for_key(){
	#if defined(CRITICAL_DEBUG)
		printf("\nNo Value Found for Key!\n");
	#endif
		throw 1;
	}
public:
	Dictionary(){}
	Dictionary(size_t size){}
	
	~Dictionary(){
	}
	
	using Vector<_dictionary_entry<T>>::allocate;
	using Vector<_dictionary_entry<T>>::size;
	using Vector<_dictionary_entry<T>>::capacity;
	using Vector<_dictionary_entry<T>>::popBack;
	
	
	
//	bool allocate(size_t size){
//		if(!__alloc){
//			_v=Vector<_dictionary_entry>(size);
//			__alloc=true;
//			return true;
//		}
//		return false;
//	}
	
//	size_t size(){
//		return _v.size();
//	}
	
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	Dictionary(Dictionary &d): Vector<_dictionary_entry<T> >(d) {}
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	Dictionary(Dictionary &&d): Vector<_dictionary_entry<T> >(d) {}
	
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	Dictionary<T>& operator=(Dictionary<T> &d){
		Vector<_dictionary_entry<T>>::operator=(d);
		return *this;
	}
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	Dictionary<T>& operator=(Dictionary<T> &&d){
		Vector<_dictionary_entry<T>>::operator=(d);
		return *this;
	}
	
//	void expand(size_t size){
//		_v.expand(size);
//	}
	
	//! Adds new key-value pair.
	//! @param [in] key String key.
	//! @param [in] __ref Object to copy.
	void addElement(String &key, T& __ref){
		_dictionary_entry<T> e={String(key), T(__ref)};
		pushBack(e);
	}
	//! Adds new key-value pair.
	//! @param [in] key String key.
	//! @param [in] __ref Object to copy.
	void addElement(String &key, T&& __ref){
		_dictionary_entry<T> e={String(key), T(__ref)};
		pushBack(e);
	}
	
	//! Adds new key-value pair.
	//! @param [in] key String key.
	//! @param [in] __ref Object to copy.
	void addElement(const char* key, T& __ref){
		_dictionary_entry<T> e={String(key), T(__ref)};
		Vector<_dictionary_entry<T>>::pushBack(e);
	}
	//! Adds new key-value pair.
	//! @param [in] key String key.
	//! @param [in] __ref Object to copy.
	void addElement(const char* key, T&& __ref){
		_dictionary_entry<T> e={String(key), T(__ref)};
		Vector<_dictionary_entry<T>>::pushBack(e);
	}
	
	//! Checks if a specific key is registered in the dictionary.
	//! @param [in] key String key.
	//! @return True if key exists, else false.
	bool isKey(String &key){
		for(int i=0; i<size(); i++){
			if(strcmp((*this)[i]._k.c_str(),key.c_str())==0){
				return true;
			}
		}
		return false;
	}
	
	//! Checks if a specific key is registered in the dictionary.
	//! @param [in] key String key.
	//! @return True if key exists, else false.
	bool isKey(String &&key){
		for(int i=0; i<size(); i++){
			if(strcmp((*this)[i]._k.c_str(),key.c_str())==0){
				return true;
			}
		}
		return false;
	}
	
	//! Checks if a specific key is registered in the dictionary.
	//! @param [in] key String key.
	//! @return True if key exists, else false.
	bool isKey(const char* key){
		for(int i=0; i<size(); i++){
			if(strcmp((*this)[i]._k.c_str(),key)==0){
				return true;
			}
		}
		return false;
	}
	
	//! Array like random access.
	//! index Index.
	//! @return Value of the key-value pair at the given index.
	T& elementAtIndex(uint32_t index){
		return Vector<_dictionary_entry<T>>::operator[](index)._v;
	}
	
	//! @note If no match is found for the given key, an exception will be thrown.
	//! @param [in] key String key.
	//! @return Value of the key-value pair matching the given key.
	T& elementForKey(String &key){
		for(int i=0; i<size(); i++){
			if(strcmp((*this)[i]._k.c_str(),key.c_str())==0){
				return (*this)[i]._v;
			}
		}
		__throw_no_value_for_key();
		return (*this)[size()-1]._v;
	}
	//! @note If no match is found for the given key, an exception will be thrown.
	//! @param [in] key String key.
	//! @return Value of the key-value pair matching the given key.
	T& elementForKey(String &&key){
		for(int i=0; i<size(); i++){
			if(strcmp((*this)[i]._k.c_str(),key.c_str())==0){
				return (*this)[i]._v;
			}
		}
		__throw_no_value_for_key();
		return (*this)[size()-1]._v;
	}
	//! @note If no match is found for the given key, an exception will be thrown.
	//! @param [in] key String key.
	//! @return Value of the key-value pair matching the given key.
	T& elementForKey(const char* key){
		for(int i=0; i<size(); i++){
			if(strcmp((*this)[i]._k.c_str(),key)==0){
				return (*this)[i]._v;
			}
		}
		__throw_no_value_for_key();
		return (*this)[size()-1]._v;
	}
	
	// Vector like random access.
	T& operator[](const char* key){
		return elementForKey(key);
	}
	
	using Vector<_dictionary_entry<T>>::operator[];
	
	
	//! Removes a key value-pair maching the given key.
	//! @return True if a maching key-value pair was found and erased, else false.
	bool eraseElementForKey(String &key){
		for(int i=0; i<size(); i++){
			if(strcmp((*this)[i]._k.c_str(),key.c_str())==0){
				this->erase(i);
				return true;
			}
		}
		return false;
	}
	//! Removes a key value-pair maching the given key.
	//! @return True if a maching key-value pair was found and erased, else false.
	bool eraseElementForKey(String &&key){
		for(int i=0; i<size(); i++){
			if(strcmp((*this)[i]._k.c_str(),key.c_str())==0){
				this->erase(i);
				return true;
			}
		}
		return false;
	}
	//! Removes a key value-pair maching the given key.
	//! @return True if a maching key-value pair was found and erased, else false.
	bool eraseElementForKey(const char*key){
		for(int i=0; i<size(); i++){
			if(strcmp((*this)[i]._k.c_str(),key)==0){
				this->erase(i);
				return true;
			}
		}
		return false;
	}

};
	
}

#endif /* defined(CRITICAL_CORE_DICTIONARY_H_) */