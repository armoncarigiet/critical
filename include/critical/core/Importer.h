//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Importer.h
 *	@date 21.01.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a Importing interface to load scenes into memory.
 *  @ingroup core
 *  @see Importer.cpp
 */
//===========================================================================

#ifndef CRITICAL_CORE_IMPORTER_H_
#define CRITICAL_CORE_IMPORTER_H_

#include <critical/graphics/Color.h>
#include <critical/graphics/Mesh.h>
#include <critical/graphics/Camera.h>
#include <critical/graphics/LightSource.h>
#include <critical/graphics/Scene.h>
#include <critical/graphics/SceneNode.h>

namespace critical {

//! Class handling the scene import.
//! @ingroup core
class Importer{
private:
	
public:
    //! Imports a scene from a given file.
    //! @param [in] path Path to the file containing the scene.
    //! @return Pointer to the loaded Scene object.
	MemoryManager::ConnectedPointer<Scene> loadSceneFromFile(const char* path);
	//Scene* loadSceneFromMemory();
};

}

#endif /* defined(CRITICAL_CORE_IMPORTER_H_) */