//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Vector.h
 *	@date 22.12.2014.
 *  @author Armon Carigiet
 *
 *  @brief Provides a dynamic array container class.
 *  @ingroup core
 */
//===========================================================================

#ifndef CRITICAL_CORE_VECTOR_H_
#define CRITICAL_CORE_VECTOR_H_

#include "MemoryManager.h"
#include "SharedPointer.h"
#include "Array.h"

//#include <vector>

namespace critical{

//! Dynamic Array
//! @ingroup core
template <class T>
class Vector{
	friend class vectorAnalyser;
	
	//! Ignore.
	static void __throw_out_of_range(){
	#if defined(CRITICAL_DEBUG)
		printf("\nVector out of Range!\n");
	#endif
		throw MemoryException(CRMemoryExceptionContainerOutOfRange);
	}
	//! Ignore.
	static void __throw_invalid_operation_on_empty_vector(){
	#if defined(CRITICAL_DEBUG)
		printf("\nCalled invalid operation on empty vector!\n");
	#endif
		throw MemoryException(CRMemoryExceptionUnknowenError);
	}
public:
	class Iterator;
private:
	size_t _s;
	size_t _c;
	SharedPointer<T> _p;
	bool __alloc;
	
public:
	
	//! Standart constructor.
	Vector() : _s(0), _c(0), __alloc(false) {}
	
	//! Initialises vector.
	//! @param size Number of elements to reserve.
	Vector(size_t capacity): _s(0), _c(capacity), _p(SharedPointer<T>(MemoryManager::salloc<T>(capacity*sizeof(T)))), __alloc(true) {}
	
	//! @note The Memory is only released if all Objects pointing to it are destroyed.
	~Vector(){
		if(__alloc){
			__alloc=false;
			_p.reset();
		}
	}
	
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	Vector(Vector &p) : _s(p._s), _c(p._c), _p(p._p), __alloc(p.__alloc) {}
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	Vector(Vector &&p) : _s(p._s), _c(p._c), _p(p._p),  __alloc(p.__alloc) { p.__alloc=false; }
	
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	Vector<T>& operator=(Vector<T> &p){
		_p=p._p;
		_s=p._s;
		_c=p._c;
		__alloc=p.__alloc;
		return *this;
	}
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	Vector<T>& operator=(Vector<T> &&p){
		_p=p._p;
		_s=p._s;
		_c=p._c;
		__alloc=p.__alloc;
		
		p.__alloc=false;
		
		return *this;
	}
	
	//! Checks if the cector is allocated.
	bool isAllocated(){ return __alloc; }
	
	//! Allocates new referencecounter.
	bool allocate(size_t s){
		if(!__alloc){
			_p=SharedPointer<T>(MemoryManager::salloc<T>(s*sizeof(T)));
			__alloc=true;
			_s=0;
			_c=s;
			return true;
		}
		return false;
	};
	
	//! Converts th vector into an array.
	//! @note The data is not copied into the new array. The array only acesses the vector's data.
	//! @return Array.
	Array<T> getArray(){
		Array<T> a;
		a._p=this->_p;
		a._s=this->_c;
		a.__alloc=this->__alloc;
		return a;
	}
	
	
	
	//! Ignore.
	void printref(){ _p.printref(); }
	//! Ignore
	void printAlloc(){ printf("Alloc: %d", __alloc); }
	//! Ignore
	void setSize(size_t s){
		_s=s;
	}
	
	//! @return Size of vector.
	size_t size(){
		return _s;
	}
	
	//! @return Capacity of vector.
	size_t capacity(){
		return _c;
	}
	
	//! Checks if vector is empty.
	//! @return true if empty, else false.
	bool empty(){
		return _s==0;
	}
	
	//! Inserts a new entry at the end of the vector.
	//! @param __ref Object to copy.
	//! @return Iterator pointing to the inserted object.
	Iterator pushBack(T& __ref){
		allocate(1);
		if(_s==_c){
			this->expand(1);
		}
		_p[_s]=__ref;
		_s+=1;
		return Iterator(&_p[_s-1], _s-1);
	}
	//! Inserts a new entry at the end of the vector.
	//! @param __ref Object to copy.
	//! @return Iterator pointing to the inserted object.
	Iterator pushBack(T&& __ref){
		allocate(1);
		if(_s==_c){
			this->expand(1);
		}
		_p[_s]=__ref;
		_s+=1;
		return Iterator(&_p[_s-1], _s-1);
	}
	//! Deletes the last element of the vector.
	void popBack(){
		_s-=1;
	}
	
	//! Insterts a new entry in front of another.
	//! @param p Iterator pointing to the other object.
	//! @param __ref Object to copy.
	//! @note All entries will be reallocated if the object is not inserted at the end of the vector.
	//! @return Iterator pointing to the inserted object.
	Iterator insert(Iterator itr, T& __ref){
		if(_s==_c){
			this->expand(1);
		}
		T* ptr=&_p;
		for(int k=_s; k>=itr._i; k--){
			ptr[k+1]=ptr[k];
		}
		ptr[itr._i]=__ref;
		_s+=1;
		return Iterator(ptr+itr._i, itr._i);
	}
	Iterator insert(Iterator itr, T&& __ref){
		if(_s==_c){
			this->expand(1);
		}
		T* ptr=&_p;
		for(int k=_s; k>=itr._i; k--){
			ptr[k+1]=ptr[k];
		}
		ptr[itr._i]=__ref;
		_s+=1;
		return Iterator(ptr+itr._i, itr._i);
	}
	
	//! @return Iterator pointing to the vectors's first object.
	virtual Iterator begin(){
		if(this->empty())
			__throw_invalid_operation_on_empty_vector();
		return Iterator(&_p, 0);
	}
	//! @return Iterator pointing to the vectors's last object.
	virtual Iterator end(){
		if(this->empty())
			__throw_invalid_operation_on_empty_vector();
		uint32_t i=_s-1;
		return Iterator(&_p+i, i);
	}
	
	//! Resizes vector.
	//! @param size New size.
	//! @note If the new size is bigger than the current size, the vector will be realocated
	void resize(size_t size){
		if(size>_c){
			_p.realloc(size*(sizeof(T)));
			
		}
		_c=size;
	}
	//! Expands vector.
	//! @param size Number of objects that should fit in the vector additionaly.
	//! @note Vector will be reallocated.
	void expand(size_t size){
		if(!__alloc){
			allocate(size);
		}
		resize(_s+size);
	}
	
	//! Retracts vector.
	//! @param Number of objects to retract.
	void retract(size_t size){
		resize(_s-size);
	}
	
	
	uint32_t erase(uint32_t index){
		if(index>(_s-1)){
			__throw_out_of_range();
		}
		T* ptr=&_p;
		for(int i=index; i<_s; ++i) {
			ptr[i]=ptr[i+1];
		}
		_s-=1;
	}
	//! Deletes entry.
	//! @param p Iterator pointing to the entry to delete.
	//! @note Objects infront of the deleted entry will be reallocated to close up the other entrys.
	//! @return Iterator pointing to the entry located behind the deleted entry.
	Iterator erase(Iterator &itr){
		if(itr._i>(_s-1)){
			__throw_out_of_range();
		}
		T* ptr=&_p;
		for(int i=itr._i; i<_s; ++i) {
			ptr[i]=ptr[i+1];
		}
		_s-=1;
		return Iterator(ptr+itr._i, itr._i);
	}
	
	//! Deletes a range of entries.
	//! @param begin Iterator pointing to the first entry to delete.
	///! @param end Iterator pointing to the last entry to delete.
	//! @note Objects infront of the deleted entries will be reallocated to close up the other entrys.
	//! @return Iterator pointing to the entry located behind the deleted entries.
	Iterator erase(Iterator &begin, Iterator &end){
		if(end._i>(_s-1)){
			__throw_out_of_range();
		}
		T* ptr=&_p;
		ptrdiff_t diff=begin._i-end._i+1;
		for(int i=begin._i; i<_s; ++i) {
			ptr[i]=ptr[i+diff];
		}
		_s-=1;
		return Iterator(ptr+begin._i, begin._i);
	}
	
	//! Deletes content of the vector.
	void clear(){
		for(int i=0; i<_s; i++){
			_p[i]=0;
		}
		_s=0;
	}
	
	//! Destroys vector and releases used memory
	virtual void destroy(){
		this->~Vector();
 	}
	
	//! @return Pointer to memory used by the vector.
	T* data(){
		return &_p;
	}
	
	//! Random access.
	//! @return Reference to acessed object.
	T& objectAtIndex(const uint32_t &index){
		if(index>(_s-1)){
			__throw_out_of_range();
		}
		return _p[index];
	}

	//! Random access operator.
	//! @return Reference to acessed object.
	T& operator[](const uint32_t &index){
		if(index>(_s-1)){
			__throw_out_of_range();
		}
		return _p[index];
	}
	
	//! Iterator class for Lists.
	class Iterator{
		friend class Vector;
	private:
			T *_p;
		uint32_t _i;
		Iterator(T *ptr, uint32_t i) : _p(ptr), _i(i) {}
	public:
		Iterator() : _p(nullptr) {}
		~Iterator() {}
		
		//! Overloaded operator.
		T& operator[](const uint32_t &index){
			return _p[index];
		}
		//! Overloaded operator.
		T* operator&(){
			return _p;
		}
		//! Overloaded operator.
		T& operator*(){
			*_p;
		}
		//! Overloaded operator.
		T* operator->() const{
			return _p;
		}

		//! Creates an iterator pointing to an object in front of the object the current iterator is holding.
		//! @param offset Offset between the object the current iterator is holding and the aimed one.
		//! @return Iterator pointing to the new object.
		Iterator operator+(ptrdiff_t offset){
			return Iterator(_p+offset, _i+offset);
		}
		//! Creates an iterator pointing to an object behind the object the current iterator is holding.
		//! @param offset Offset between the object the current iterator is holding and the aimed one.
		//! @return Iterator pointing to the new object.

		Iterator operator-(ptrdiff_t offset){
			return Iterator(_p-offset, _i-offset);
		}
		
		//! @param p Second iterator.
		//! @return Arithmetic pointer difference between the two iterators.
		ptrdiff_t operator-(Iterator &p){
			return _p-p.ptr;
		}
		
		//! Moves the iterator forward.
		//! @param offset Number of objects to move forward.
		//! @return Reference to the modified iterator.
		Iterator& operator+=(ptrdiff_t offset){
			_p+=offset;
			_i+=offset;
			return *this;
		}
		//! Moves the iterator back.
		//! @param offset Number of objects to move forward.
		//! @return Reference to the modified iterator.
		Iterator& operator-=(ptrdiff_t offset){
			_p-=offset;
			_i-=offset;
			return *this;
		}
		
		//! Moves the iterator forward by one.
		//! @return Reference to the modified iterator.
		Iterator& operator++(){
			++_p;
			++_i;
			return *this;
		}
		//! Moves the iterator back by one.
		//! @return Reference to the modified iterator.
		Iterator& operator--(){
			--_p;
			--_i;
			return *this;
		}
		
		//! Creates an iterator pointing to the object in front of the object the current iterator is holding.
		//! @return New iterator.
		Iterator operator++(int){
			Iterator i(*this);
			++(*this);
			return i;
		}
		//! Creates an iterator pointing to the object behind the object the current iterator is holding.
		//! @return New iterator.
		Iterator operator--(int){
			Iterator i(*this);
			--(*this);
			return i;
		}
		
	};

};

}

#endif /* defined(CRITICAL_CORE_VECTOR_H_) */
