//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Pointer.h
 *	@date 17.12.2014.
 *  @author Armon Carigiet
 *
 *  @brief Provides a simple pointer class.
 *  @ingroup core
 */
//===========================================================================

#ifndef CRITICAL_CORE_POINTER_H_
#define CRITICAL_CORE_POINTER_H_

#include "Memory.h"


namespace critical{
	
//======================================================================================
//	declaration
//======================================================================================

//! @ingroup core
template <typename T> class Pointer;
template <typename T> Pointer<T> operator+(const Pointer<T> &p, ptrdiff_t offset);
template <typename T> Pointer<T> operator-(const Pointer<T> &p, ptrdiff_t offset);
	
//======================================================================================
//	definition
//======================================================================================
	
//! @ingroup core
template <class T>
class Pointer{
protected:
	//void* ptr;
public:
	void *ptr;
	Pointer(): ptr(nullptr){}
	Pointer(void* ptr): ptr(ptr){}
	Pointer(void* ptr, ptrdiff_t offset): ptr(static_cast<void*>(static_cast<T*>(ptr)+offset)){}
	Pointer(ptrdiff_t byteoffset, void* ptr): ptr(static_cast<void*>(static_cast<char*>(ptr)+byteoffset)){}
	~Pointer() {}
	
	void shift(ptrdiff_t offset);
	void shiftBytes(ptrdiff_t bytes);
	
	//! Returns the void pointer this object is handling.
	//! @return Pointer to memory section.
	void* getVoidPtr();
	
	//! Returns the pointer this object is handling.
	//! @return Pointer to memory section.
	T* getPtr();
	
	//! Resets Pointer.
	virtual void reset();
    
	//! Resets Pointer to a new pointer.
	void reset(T *ptr);
    
	//! Resets Pointer to a new void pointer.
	void reset(void *ptr);
    
	//! Resets Pointer to the pointer, which p is handling.
	void reset(Pointer<T> &p);
	
	T& operator[](const uint32_t &index);
	T* operator&();
	T& operator*();
	T* operator->() const;
	
	//friend Pointer<T> operator+ <>(const Pointer<T> &p, ptrdiff_t offset);
	//friend Pointer<T> operator- <>(const Pointer<T> &p, ptrdiff_t offset);

	Pointer<T> operator+(ptrdiff_t offset);
	Pointer<T> operator-(ptrdiff_t offset);
	
	ptrdiff_t operator-(Pointer<T> &p);
	
	Pointer<T>& operator+=(ptrdiff_t offset);
	Pointer<T>& operator-=(ptrdiff_t offset);
	
	Pointer<T>& operator++();
	Pointer<T>& operator--();
	Pointer<T> operator++(int);
	Pointer<T> operator--(int);
};
	

	
//======================================================================================
//	specialisation
//======================================================================================

template<>
class Pointer<void>{
protected:
	void* ptr;
public:
	Pointer(): ptr(nullptr){}
	Pointer(void* ptr): ptr(ptr){}
	Pointer(void* ptr, ptrdiff_t offset): ptr(static_cast<void*>(static_cast<char*>(ptr)+offset)){}
	~Pointer() {}
	
	void shift(ptrdiff_t bytes);
	void* getVoidPtr();
	void* getPtr();
	
	virtual void reset();
	void reset(void *ptr);
	void reset(Pointer<void> &p);
	
	void* operator&();
	
	//friend Pointer<void> operator+ (const Pointer<void> &p, ptrdiff_t offset);
	//friend Pointer<void> operator- (const Pointer<void> &p, ptrdiff_t offset);
	
	Pointer<void> operator+(ptrdiff_t offset);
	Pointer<void> operator-(ptrdiff_t offset);
	
	Pointer<void>& operator+=(ptrdiff_t offset);
	Pointer<void>& operator-=(ptrdiff_t offset);
	
	Pointer<void>& operator++();
	Pointer<void>& operator--();
	Pointer<void> operator++(int);
	Pointer<void> operator--(int);
};

//======================================================================================
//	implementation
//======================================================================================

template <class T>
void Pointer<T>::shift(ptrdiff_t offset){
	this->ptr=static_cast<void*>(static_cast<T*>(this->ptr)+offset);
}
inline void Pointer<void>::shift(ptrdiff_t bytes){
	this->ptr=static_cast<void*>(static_cast<char*>(this->ptr)+bytes);
}
	
template <class T>
void Pointer<T>::shiftBytes(ptrdiff_t bytes){
	this->ptr=static_cast<void*>(static_cast<char*>(this->ptr)+bytes);
}
	
template <class T>
T* Pointer<T>::getPtr(){
	return static_cast<T*>(this->ptr);
}
inline void* Pointer<void>::getPtr(){
	return ptr;
}
	
template <class T>
void* Pointer<T>::getVoidPtr(){
	return this->ptr;
}
inline void* Pointer<void>::getVoidPtr(){
	return getPtr();
}

template <class T>
void Pointer<T>::reset(){
	this->ptr=nullptr;
}
inline void Pointer<void>::reset(){
	this->ptr=nullptr;
}

template <class T>
void Pointer<T>::reset(T *ptr){
	this->ptr=static_cast<void*>(ptr);
}

template <class T>
void Pointer<T>::reset(void *ptr){
	this->ptr=ptr;
}
	
inline void Pointer<void>::reset(void *ptr){
	this->ptr=ptr;
}
	
template <class T>
void Pointer<T>::reset(Pointer<T> &p){
	this->ptr=p.getVoidPtr();
}
	
inline void Pointer<void>::reset(Pointer<void> &p){
	this->ptr=p.getPtr();
}
	
template <class T>
T& Pointer<T>::operator[](const uint32_t &index){
	return static_cast<T*>(ptr)[index];
}
	
template <class T>
T* Pointer<T>::operator&(){
	return static_cast<T*>(ptr);
}
inline void* Pointer<void>::operator&(){
	return ptr;
}
	
template <class T>
T& Pointer<T>::operator*(){
	return *static_cast<T*>(ptr);
}
	
template <class T>
T* Pointer<T>::operator->() const{
	return static_cast<T*>(ptr);
}
	
template<class T>
Pointer<T> Pointer<T>::operator+( ptrdiff_t offset){
	return Pointer<T>(this->ptr, offset);
}

inline Pointer<void> Pointer<void>::operator+( ptrdiff_t offset){
	return Pointer<void>(this->ptr, offset);
}
	
template<class T>
Pointer<T> Pointer<T>::operator-( ptrdiff_t offset){
	return Pointer<T>(this->ptr, -offset);
}
	
inline Pointer<void> Pointer<void>::operator-( ptrdiff_t offset){
	return Pointer<void>(this->ptr, -offset);
}

template <class T>
ptrdiff_t Pointer<T>::operator-(Pointer<T> &p){
	return static_cast<T*>(this->ptr)-static_cast<T*>(p.ptr);
}

template <class T>
Pointer<T>& Pointer<T>::operator+=(ptrdiff_t offset){
	this->shift(offset);
	return *this;
}

inline Pointer<void>& Pointer<void>::operator+=(ptrdiff_t offset){
	this->shift(offset);
	return *this;
}
	
template <class T>
Pointer<T>& Pointer<T>::operator-=(ptrdiff_t offset){
	this->shift(-offset);
	return *this;
}
	
inline Pointer<void>& Pointer<void>::operator-=(ptrdiff_t offset){
	this->shift(-offset);
	return *this;
}

	
template <class T>
Pointer<T>& Pointer<T>::operator++(){
	this->shift(1);
	return *this;
}
inline Pointer<void>& Pointer<void>::operator++(){
	this->shift(1);
	return *this;
}

template <class T>
Pointer<T>& Pointer<T>::operator--(){
	this->shift(-1);
	return *this;
}
inline Pointer<void>& Pointer<void>::operator--(){
	this->shift(-1);
	return *this;
}
	
template <class T>
Pointer<T> Pointer<T>::operator++(int){
	Pointer<T> tmp=Pointer<T>(*this);
	++(*this);
	return tmp;
}
inline Pointer<void> Pointer<void>::operator++(int){
	Pointer<void> tmp(*this);
	++(*this);
	return tmp;
}
	
template <class T>
Pointer<T> Pointer<T>::operator--(int){
	Pointer<T> tmp=Pointer<T>(*this);
	--(*this);
	return tmp;
}
inline Pointer<void> Pointer<void>::operator--(int){
	Pointer<void> tmp(*this);
	--(*this);
	return tmp;
}
	
}

#endif /* defined(CRITICAL_CORE_POINTER_H_) */