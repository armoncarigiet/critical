//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file AlignedList.h
 *	@date 21.03.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a linked-list container with aligned memory support.
 *  @ingroup core
 *  @see List.h
 */
//===========================================================================

#ifndef CRITICAL_CORE_ALIGNEDLIST_H_
#define CRITICAL_CORE_ALIGNEDLIST_H_

#include "List.h"

namespace critical {

//! @ingroup core
template <class T>
class AlignedList{
private:
	//! Ignore.
	static void __throw_out_of_range(){
	#if defined(CRITICAL_DEBUG)
		printf("\nList out of Range!\n");
	#endif
		throw MemoryException(CRMemoryExceptionContainerOutOfRange);
	}
protected:
	//! Ignore.
	struct _list_block{
		_list_block *last;
		_list_block *next;
		MemoryManager::ConnectedPointer<void> p;
		T data;
	};
	
	_list_block *_front;
	_list_block *_back;
	MemoryManager::ConnectedPointer<uint32_t> _refcounter;
	size_t _s;
	bool __alloc;
	uint32_t _alignment;
public:
	class Iterator;
	
	//! Standart constructor. Does not allocate memory.
	AlignedList(){
		_front=NULL;
		_back=NULL;
		_s=0;
		__alloc=false;
		_alignment=0;
	}
    
	//! @note The Memory is only released if all Objects pointing to it are destroyed.
	~AlignedList(){
		if(&_refcounter && __alloc){
			--(*_refcounter);
			if((*_refcounter)==0){
				//printf("\ndestroy list!");
				_refcounter.reset();
				_list_block *block=_front;
				for(int i=0; i<_s; i++){
					_list_block *tmp=block->next;
					block->p.reset();
					block=tmp;
				}
			}
		}
	}
    
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	AlignedList(AlignedList &l) : _front(l._front), _back(l._back), _refcounter(l._refcounter), _s(l._s), __alloc(l.__alloc), _alignment(l._alignment){ if(_refcounter.getVoidPtr()){ ++(*_refcounter); } }
	
    //! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	AlignedList(AlignedList &&l) : _front(l._front), _back(l._back), _refcounter(l._refcounter), _s(l._s), __alloc(l.__alloc), _alignment(l._alignment){ if(_refcounter.getVoidPtr()){ ++(*_refcounter); } }
	
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	AlignedList<T>& operator=(AlignedList<T> &l){
		__alloc=l.__alloc;
		_s=l._s;
		_front=l._front;
		_back=l._back;
		_refcounter=l._refcounter;
		if(_refcounter.getVoidPtr()){
			++(*_refcounter);
		}
		_alignment=l._alignment;
		return *this;
	}
    
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	AlignedList<T>& operator=(AlignedList<T> &&l){
		__alloc=l.__alloc;
		_s=l._s;
		_front=l._front;
		_back=l._back;
		_refcounter=l._refcounter;
		if(_refcounter.getVoidPtr()){
			++(*_refcounter);
		}
		_alignment=l._alignment;
		return *this;
	}

	//! Allocates new referencecounter.
	//! @param align Memory alignment.
	void allocate(uint32_t align=16){
		_refcounter=MemoryManager::salloc<uint32_t>(sizeof(uint32_t));
		*(_refcounter)=1;
		__alloc=true;
		_alignment=align;
	}
	
	//! @return Iterator pointing to the list's first object.
	Iterator begin(){
		return Iterator(_front);
	}
    
	//! @return Iterator pointing to the list's last object.
	Iterator end(){
		return Iterator(_back);
	}
	
	//! @return Size of the list.
	size_t size(){
		return _s;
	}
	
	//! @return Reference to the list's first object.
	T& front(){
		return _front->data;
	}
	
	//! @return Reference to the list's first object.
	const T& front() const{
		return _front->data;
	}
    
	//! @return Reference to the list's last object.
	T& back(){
		return _back->data;
	}
    
	//! @return Reference to the list's last object.
	const T& back() const{
		return _back->data;
	}

	//! Inserts a new entry at the begin of the list.
	//! @param [in] __ref Object to copy.
	//! @return Iterator pointing to the inserted object.
	Iterator pushFront(const T& __ref){
		if(!__alloc){
			allocate();
		}
		MemoryManager::ConnectedPointer<void> pointer=MemoryManager::alignAlloc<void>(sizeof(_list_block), _alignment);
		_list_block *block=static_cast<_list_block*>(&pointer);
		block->p=MemoryManager::ConnectedPointer<void>(pointer);
		block->data=T(__ref);
		block->last=nullptr;
		block->next=_front;
		if(_front){
			_front->last=block;
		}
		_front=block;
		if(_s==0){
			_back=block;
		}
		++_s;
		return Iterator(block);
	}
    
	//! Inserts a new entry at the begin of the list.
	//! @param [in] __ref Object to copy.
	//! @return Iterator pointing to the inserted object.
	Iterator pushFront(T&& __ref){
		if(!__alloc){
			allocate();
		}
		MemoryManager::ConnectedPointer<void> pointer=MemoryManager::alignAlloc<void>(sizeof(_list_block), _alignment);
		_list_block *block=static_cast<_list_block*>(&pointer);
		block->p=MemoryManager::ConnectedPointer<void>(pointer);
		block->data=T(__ref);
		block->last=nullptr;
		block->next=_front;
		if(_front){
			_front->last=block;
		}
		_front=block;
		if(_s==0){
			_back=block;
		}
		++_s;
		return Iterator(block);
	}
	
	//! Deletes the first element of the list.
	void popFront(){
		if(_front){
			_list_block *tmp=_front;
			_front=_front->next;
			tmp->p.reset();
			--_s;
		}
	}
	
	//! Insterts a new entry at the end of the list.
	//! @param __ref Object to copy.
	//! @return Iterator pointing to the inserted object.
	Iterator pushBack(T& __ref){
		if(!__alloc){
			allocate();
		}
		MemoryManager::ConnectedPointer<void> pointer=MemoryManager::alignAlloc<void>(sizeof(_list_block), _alignment);
		_list_block *block=static_cast<_list_block*>(&pointer);
		block->p=MemoryManager::ConnectedPointer<void>(pointer);
		block->data=T(__ref);
		block->last=_back;
		block->next=nullptr;
		if(_back){
			_back->next=block;
		}
		_back=block;
		if(_s==0){
			_front=block;
		}
		++_s;
		return Iterator(block);
	}
    
	//! Insterts a new entry at the end of the list.
	//! @param [in] __ref Object to copy.
	//! @return Iterator pointing to the inserted object.
	Iterator pushBack(T&& __ref){
		if(!__alloc){
			allocate();
		}
		MemoryManager::ConnectedPointer<void> pointer=MemoryManager::alignAlloc<void>(sizeof(_list_block), _alignment);
		_list_block *block=static_cast<_list_block*>(&pointer);
		block->p=MemoryManager::ConnectedPointer<void>(pointer);
		block->data=T(__ref);
		block->last=_back;
		block->next=nullptr;
		if(_back){
			_back->next=block;
		}
		_back=block;
		if(_s==0){
			_front=block;
		}
		++_s;
		return Iterator(block);
	}
    
	//! Deletes the first element of the list.
	void popBack(){
		if(_back){
			_list_block *tmp=_back;
			_back=_back->last;
			tmp->p.reset();
			--_s;
		}
	}
	
	//! Insterts a new entry in front of another.
	//! @param [in] p Iterator pointing to the other object.
	//! @param [in] __ref Object to copy.
	//! @return Iterator pointing to the inserted object.
	Iterator insert(const Iterator &p, const T& __ref){
		MemoryManager::ConnectedPointer<void> pointer=MemoryManager::alignAlloc<void>(sizeof(_list_block), _alignment);
		_list_block *block=static_cast<_list_block*>(&pointer);
		block->p=MemoryManager::ConnectedPointer<void>(pointer);
		if(p._b){
			block->next=p._b->next;
			p._b->next=block;
		}else{
			block->next=nullptr;
		}
		block->data=T(__ref);
		++_s;
	}
	//! Insterts a new entry in front of another.
	//! @param [in] p Iterator pointing to the other object.
	//! @param [in] __ref Object to copy.
	//! @return Iterator pointing to the inserted object.
	Iterator insert(const Iterator &p, T&& __ref){
		MemoryManager::ConnectedPointer<void> pointer=MemoryManager::alignAlloc<void>(sizeof(_list_block), _alignment);
		_list_block *block=static_cast<_list_block*>(&pointer);
		block->p=MemoryManager::ConnectedPointer<void>(pointer);
		block->last=p._b;
		if(p._b){
			block->next=p._b->next;
			p._b->next=block;
		}else{
			block->next=nullptr;
		}
		block->data=T(__ref);
		++_s;
	}
	
	//! Deletes entry.
	//! @param [in] p Iterator pointing to the entry.
	//! @return Iterator pointing to the entry located behind the deleted entry.
	Iterator& erase(const Iterator &p){
		_list_block *tmp=p._b->last;
		if(p._b->next){
			p._b->next->last=p._b->last;
		}
		if(p._b->last){
			p._b->last->next=p._b->next;
		}
		p._b->p.reset();
		return Iterator(tmp);
	}

	//! Iterator for AlignedLists
	class Iterator{
		friend class AlignedList;
	private:
		_list_block* _b;
		Iterator(_list_block *b) : _b(b) {}
	public:
		Iterator() : _b(nullptr) {}
        ~Iterator() {}
        
		//! @return Pointer to the List's object, which the Iterator is holding.
		T* operator&(){
			return &_b->data;
		}
        
		//! @note Exception is thrown if the Iterator is pointing to an invalid object.
		//! @return Reference to the List's object, which the Iterator is holding.
		T& operator*(){
			return _b->data;
		}
        
		//! Access object the iterator is holding.
		//! @return Pointer to the
		T* operator->() const{
			return &_b->data;
		}
		
		//! Checks if the iterator is pointing to an invalid object/ if the iterator is out of the List's range.
		//! @return true if iterator is out of range, else false.
		bool isOutOfRange(){
			if(!_b){
				return true;
			}
			return false;
		}

		//! Creates an iterator pointing to an object in front of the object the current iterator is holding.
		//! @param offset Offset between the object the current iterator is holding and the aimed one.
		//! @return Iterator pointing to the new object.
		Iterator operator+(uint32_t offset){
			_list_block *tmp=_b;
			for(int i=0; i<offset; i++) {
				if(tmp->next){
					tmp=tmp->next;
				}else{
					__throw_out_of_range();
					break;
				}
			}
			return Iterator(tmp);
		}
        
		//! Creates an iterator pointing to an object behind the object the current iterator is holding.
		//! @param offset Offset between the object the current iterator is holding and the aimed one.
		//! @return Iterator pointing to the new object.
		Iterator operator-(ptrdiff_t offset){
			_list_block *tmp=_b;
			for(int i=0; i<offset; i++) {
				if(tmp->last){
					tmp=tmp->last;
				}else{
					__throw_out_of_range();
					break;
				}
			}
			return Iterator(tmp);
		}
		
		//! Moves the iterator forward.
		//! @param offset Number of objects to move forward.
		//! @return Reference to the modified iterator.
		Iterator& operator+=(ptrdiff_t offset){
			for(int i=0; i<offset; i++) {
				if(_b->next){
					_b=_b->next;
				}else{
					__throw_out_of_range();
					break;
				}
			}
			return *this;
		}
        
		//! Moves the iterator back.
		//! @param offset Number of objects to move forward.
		//! @return Reference to the modified iterator.
		Iterator& operator-=(ptrdiff_t offset){
			for(int i=0; i<offset; i++) {
				if(_b->last){
					_b=_b->last;
				}else{
					__throw_out_of_range();
					break;
				}
			}
			return *this;
		}
        
		//! Moves the iterator forward by one.
		//! @return Reference to the modified iterator.
		Iterator& operator++(){
			if(_b->next){
				_b=_b->next;
			}else{
				__throw_out_of_range();
			}
			return *this;
		}
        
		//! Moves the iterator back by one.
		//! @return Reference to the modified iterator.
		Iterator& operator--(){
			if(_b->last){
				_b=_b->last;
			}else{
				__throw_out_of_range();
			}
			return *this;
		}
		
		//! Creates an iterator pointing to the object in front of the object the current iterator is holding.
		//! @return New iterator.
		Iterator operator++(int){
			Iterator i(*this);
			++(*this);
			return i;
		}
        
		//! Creates an iterator pointing to the object behind the object the current iterator is holding.
		//! @return New iterator.
		Iterator operator--(int){
			Iterator i(*this);
			--(*this);
			return i;
		}
		
		
	};


};
	
}

#endif /* defined(CRITICAL_CORE_ALIGNEDLIST_H_) */