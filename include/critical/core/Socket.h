//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Socket.h
 *	@date 22.07.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides networking classes.
 *  @ingroup core
 *  @note Work in progress!
 */
//===========================================================================

#ifndef CRITICAL_CORE_SOCKET_H_
#define CRITICAL_CORE_SOCKET_H_


#include <critical/core/MemoryManager.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

namespace critical{

enum TCPPort{
    CRAnyPort = -1,
    CRFTPDataPort = 20,
    CRFTPControlPort = 21,
    CRSSHPort = 22,
    CRTelnetPort = 23,
    CRSMTPPort = 25,
    CRWHOISPort = 43,
    CRDNSPort = 53,
    CRGopherPort = 70,
    CRHTTPPort = 80,
    
};
    
enum NetworkProtocol{
    CRIPv4Protocol = 0,
    CRIPv6Protocol
};

enum TransmissionProtocol{
    CRTCPProtocol = 0,
    CRUDPProtocol,
    CRDCCPProtocol,
};

template <NetworkProtocol np>
class NetworkAddress{
    MemoryManager::ConnectedPointer<uint32_t> addr;
    int16_t port;
};

template <NetworkProtocol np, TransmissionProtocol tp>
class Socket{
    
};
    
}

#endif /* defined(CRITICAL_CORE_SOCKET_H_) */

