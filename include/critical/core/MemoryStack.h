//
//  MemoryStack.h
//  
//
//  Created by Armon Carigiet on 02.07.14.
//
//

#ifndef CRITICAL_CORE_MEMORYSTACK_H_
#define CRITICAL_CORE_MEMORYSTACK_H_


#include <vector>
#include <cstdlib>
#include <cstddef>
#include "types.h"
#include "MemoryManager.h"

#include <stdio.h>




namespace critical{

	//template <class T = void*>
class MemoryStack{
private:
	bool allocated, connected;
	void *start_pointer;
	void *top_pointer;
	std::vector<void*> block_index;
	size_t size;
	size_t free_size;
	size_t block_count;
	_mem_table_entry *tableEntry;
	
public:
	MemoryStack(size_t bytes){
		this->start_pointer=malloc(bytes);
		this->top_pointer=this->start_pointer;
		this->size=bytes;
		this->free_size=this->size;
		this->block_count=0;
		this->allocated=true;
		this->connected=false;
	}
	template <typename T> MemoryStack(MemoryManager::ConnectedPointer<T> &ptr, size_t bytes, off_t offset=0){
		if(ptr.allocated && (ptr.tableEntry->size<=(bytes+offset))){
			tableEntry=ptr.tableEntry;
			this->start_pointer=this->tableEntry->memoryPointer;
			this->top_pointer=this->start_pointer;
			this->size=bytes;
			this->free_size=this->size;
			this->block_count=0;
			this->allocated=true;
			this->connected=false;
		}else{
			this->allocated=false;
		}
	};
	virtual ~MemoryStack();
	
	
	
	void* allocateBlock(size_t bytes);
	
	bool freeLastBlock();
	bool freeLastBlocks(size_t blocks);
	
	void freeStack();
};
	
	
}


#endif /* defined(CRITICAL_CORE_MEMORYSTACK_H_) */
