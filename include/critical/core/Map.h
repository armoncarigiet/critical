//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Map.h
 *	@date 24.03.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a container, storring custom key-value pairs.
 *  @ingroup core
 */
//===========================================================================

#ifndef CRITICAL_CORE_MAP_H_
#define CRITICAL_CORE_MAP_H_

#include "Vector.h"

namespace critical {
	
template <class T, class K>
struct _map_entry{
	K _k;
	T _v;
};
	
//! Container that stores key-value pairs.
//! @tparam T Type of values to store.
//! @tparam T Type of key.
//! @ingroup core
template <class T, typename K=int>
class Map: private Vector<_map_entry<T,K>>{
	//friend class dictionaryAnalyser;
public:
private:
	void __throw_no_value_for_key(){
	#if defined(CRITICAL_DEBUG)
		printf("\nNo Value Found for Key!\n");
	#endif
		throw 1;
	}
public:
	Map(){}
	Map(size_t size){}
		
	~Map(){
		
	}
		
	using Vector<_map_entry<T,K>>::allocate;
	using Vector<_map_entry<T,K>>::size;
	using Vector<_map_entry<T,K>>::capacity;
	using Vector<_map_entry<T,K>>::popBack;
		
	//	bool allocate(size_t size){
	//		if(!__alloc){
	//			_v=Vector<_dictionary_entry>(size);
	//			__alloc=true;
	//			return true;
	//		}
	//		return false;
	//	}
	
	//	size_t size(){
	//		return _v.size();
	//	}
		
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	Map(Map &d): Vector<_map_entry<T,K> >(d) {}
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	Map(Map &&d): Vector<_map_entry<T,K> >(d) {}
	
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	Map<T,K>& operator=(Map<T,K> &m){
		Vector<_map_entry<T,K>>::operator=(m);
		return *this;
	}
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	Map<T,K>& operator=(Map<T,K> &&m){
		Vector<_map_entry<T,K>>::operator=(m);
		return *this;
	}
	
	//	void expand(size_t size){
	//		_v.expand(size);
	//	}
	
	//! Adds new key-value pair.
	//! @param key Key.
	//! @param [in] __ref Object to copy.
	void addElement(K key, T& __ref){
		_map_entry<T,K> e={key, T(__ref)};
		this->pushBack(e);
	}
	//! Adds new key-value pair.
	//! @param key Key.
	//! @param [in] __ref Object to copy.
	void addElement(K key, T&& __ref){
		_map_entry<T,K> e={key, T(__ref)};
		this->pushBack(e);
	}
	
	//! Adds new key-value pair.
	//! @param key Key.
	//! @param [in] __ref Object to copy.
	void addElement(K *key, T& __ref){
		_map_entry<T,K> e={*key, T(__ref)};
		this->pushBack(e);
	}
	//! Adds new key-value pair.
	//! @param key Key.
	//! @param [in] __ref Object to copy.
	void addElement(K *key, T&& __ref){
		_map_entry<T,K> e={*key, T(__ref)};
		this->pushBack(e);
	}
	
	//! Checks if a specific key is registered in the map.
	//! @param key String key.
	//! @return True if key exists, else false.
	bool isKey(K key){
		for(int i=0; i<size(); i++){
			if((*this)[i]._k==key){
				return true;
			}
		}
		return false;
	}
	
	//! Array like random access.
	//! index Index.
	//! @return Value of the key-value pair at the given index.
	T& elementAtIndex(uint32_t index){
		return Vector<_map_entry<T,K>>::operator[](index)._v;
	}
		
	//! @note If no match is found for the given key, an exception will be thrown.
	//! @param key Key.
	//! @return Value of the key-value pair matching the given key.
	T& elementForKey(K key){
		for(int i=0; i<size(); i++){
			if(Vector<_map_entry<T,K>>::objectAtIndex(i)._k==key){
				return Vector<_map_entry<T,K>>::objectAtIndex(i)._v;
			}
		}
		__throw_no_value_for_key();
		return elementAtIndex(size()-1);
	}
	
	// Vector like random access.
	T& operator[](K key){
		return elementForKey(key);
	}
		
	//using Vector<_map_entry<T,K>>::operator[];
	
	//! Removes a key value-pair maching the given key.
	//! @return True if a maching key-value pair was found and erased, else false.
	bool eraseElementForKey(K key){
		for(int i=0; i<size(); i++){
			if((*this)[i]._k==key){
				this->erase(i);
				return true;
			}
		}
		return false;
	}
};
	
}

#endif /* defined(CRITICAL_CORE_MAP_H_) */