//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file SystemClock.h
 *	@date 08.04.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides class for measuring the CPU-time.
 *  @ingroup core
 */
//===========================================================================

#ifndef CRITICAL_CORE_SYSTEMCLOCK
#define  CRITICAL_CORE_SYSTEMCLOCK

#include "Chrono.h"

namespace critical {
	
//! @ingroup core
template <clockType T=CRMonotonicClock>
class SystemClock{
	uint64_t nanoseconds;
public:
	uint64_t getTime(){
		
		#if defined(__MACH__)
			#define CLOCK_PROCESS_CPUTIME_ID CLOCK_MONOTONIC
			#define CLOCK_THREAD_CPUTIME_ID CLOCK_MONOTONIC
		#endif
		
		#if defined(CRITICAL_WINDOWS)
			#define CLOCK_REALTIME 1
			#define CLOCK_MONOTONIC 1
			#define CLOCK_PROCESS_CPUTIME_ID 1
			#define CLOCK_THREAD_CPUTIME_ID 1
		#endif
		
		timespec s;
		clockid_t c=0;
		if(T==CRRealtimeClock){
			c=CLOCK_REALTIME;
		}else if(T==CRMonotonicClock){
			c=CLOCK_MONOTONIC;
		}else if(T==CRProcessClock){
			c=CLOCK_PROCESS_CPUTIME_ID;
		}else if(T==CRThreadClock){
			c=CLOCK_THREAD_CPUTIME_ID;
		}
		
		clock_gettime(c, &s);
		return s.tv_sec/GLMATH_NANO+s.tv_nsec;
	}
	
	void start(){
		nanoseconds=this->getTime();
	}
	
	uint64_t stop(){
		return this->getTime()-nanoseconds;
	}
	
};
	
}

#endif /* defined(CRITICAL_CORE_SYSTEMCLOCK) */