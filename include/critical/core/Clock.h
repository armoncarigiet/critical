//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Clock.h
 *	@date 06.04.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a helper class for CPU time handeling.
 *  @ingroup core
 *  @see Clock.cpp
 */
//===========================================================================

#include "Chrono.h"

#ifndef CRITICAL_CORE_CLOCK_H_
#define CRITICAL_CORE_CLOCK_H_

namespace critical {

//! Clock class for managing and modifying time intervals.
//! @ingroup core
template <timeUnit U=CRNanoseconds>
class Clock{
public:
	
	
private:
	uint64_t _elapsed;
	float _scale;
	bool _paused;
	
public:
	
    //! @param scale Time interval scale factor.
	Clock(float scale=1.0){
		_elapsed=0;
		_scale=scale;
		_paused=false;
	}
	
    //! Resets the clock.
	void clear(){
		_elapsed=0;
	}
	
    //! Sets the scale factor to a specific value.
    //! @param scale Time interval scale factor.
	void setScale(float scale){
		_scale=scale;
	}
	
    //! Changes the value indicating if the clock is running or paused.
    //! @param paused Value to set.
	void setPause(bool paused){
		_paused=paused;
	}
	
    //! Starts the clock if it was paused.
    //! This affects that the clock continue updating the elapsed time.
	void start(){
		_paused=false;
	}
	
    //! Pauses the clock.
    //! This will make the clock not updating anymore if the update method is called.
	void pause(){
		_paused=true;
	}
	
    //! Calculates the time difference between two clocks.
    //! @param c Clock to subtract.
	uint64_t difference(Clock &c){
		return llabs(_elapsed-c._elapsed);
	}
	
    //! Update the clock. Adds a time interval to the already elapsed clock time.
    //! @param dt Time interval to add in CPU-cycles.
	void update(uint64_t dt){
		if(!_paused){
			_elapsed+=dt;
		}
		
	}
	
    //! Update the clock. Adds a time interval to the already elapsed clock time.
    //! @param dt Time interval to add in seconds.
	void update(float seconds){
		if(!_paused){
			if(U==CRNanoseconds){
				_elapsed+=seconds/GLMATH_NANO;
			}else if(U==CRMicroseconds){
				_elapsed+=seconds*GLMATH_MICRO;
			}else if(U==CRMilliseconds){
				_elapsed+=seconds*GLMATH_MILLI;
			}else if(U==CRSeconds){
				_elapsed+=seconds;
			}
		}
	}

     //! Adds the time interval for one 30fps simulation's ideal frame to the elapsed time (~33.3ms).
	void step(){
		if(_paused){
			if(U==CRNanoseconds){
				_elapsed+=CRITICAL_IDEAL_FRAME*GLMATH_NANO;
			}else if(U==CRMicroseconds){
				_elapsed+=CRITICAL_IDEAL_FRAME*GLMATH_MICRO;
			}else if(U==CRMilliseconds){
				_elapsed+=CRITICAL_IDEAL_FRAME*GLMATH_MILLI;
			}else if(U==CRSeconds){
				_elapsed+=CRITICAL_IDEAL_FRAME;
			}
		}
	}
	
     //! Adds the time interval for one 60fps simulation's ideal frame to the elapsed time (~16.6ms).
	void step60(){
		if(_paused){
			if(U==CRNanoseconds){
				_elapsed+=CRITICAL_FAST_FRAME*GLMATH_NANO;
			}else if(U==CRMicroseconds){
				_elapsed+=CRITICAL_FAST_FRAME*GLMATH_MICRO;
			}else if(U==CRMilliseconds){
				_elapsed+=CRITICAL_FAST_FRAME*GLMATH_MILLI;
			}else if(U==CRSeconds){
				_elapsed+=CRITICAL_FAST_FRAME;
			}
		}
	}
	
    //! Doesn't make any sense. Why did I put this here.
	void step(uint64_t step){
		if(_paused){
			_elapsed+=step;
		}
	}
	
};

}

#endif /* defined(CRITICAL_CORE_CLOCK_H_) */