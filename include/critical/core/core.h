//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file core.h
 *	@date 27.06.2014.
 *  @author Armon Carigiet
 *
 *  @brief Main include file for the core module.
 */
//===========================================================================

//===========================================================================
/**
 *  @defgroup core Core unit
 *
 *  Provides classes for memory management, file IO, time measurement and
 *  different container classes. All other modules base upon the core unit and 
 *  won't be able to work without it.
 */
//===========================================================================

#ifndef CRITICAL_CORE_CORE_H_
#define CRITICAL_CORE_CORE_H_

//===========================================================================
// Headers
//===========================================================================

#include <critical/core/Memory.h>
#include <critical/core/Mutex.h>
#include <critical/core/Thread.h>

#include <critical/core/Pointer.h>
#include <critical/core/WeakPointer.h>
#include <critical/core/SharedPointer.h>

#include <critical/core/MemoryManager.h>
#include <critical/core/FileManager.h>

#include <critical/core/Array.h>
#include <critical/core/Vector.h>
#include <critical/core/Tuple.h>
#include <critical/core/List.h>
#include <critical/core/AlignedList.h>
#include <critical/core/Dictionary.h>
#include <critical/core/Map.h>
#include <critical/core/String.h>

#include <critical/core/Chrono.h>
#include <critical/core/SystemClock.h>
#include <critical/core/Clock.h>

#include <critical/core/Importer.h>
#include <critical/core/ColladaHelper.h>

#include <critical/core/RenderInformation.h>
//#include <critical/core/Socket.h>








#endif /* CRITICAL_CORE_CORE_H_ */


