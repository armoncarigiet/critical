//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Thread.h
 *	@date 11.07.2014.
 *  @author Armon Carigiet
 *
 *  @brief Provides different classes for thread management.
 *  @ingroup core
 */
//===========================================================================

#ifndef CRITICAL_CORE_THREAD_H_
#define CRITICAL_CORE_THREAD_H_
#include <stddef.h>

#if defined(CRITICAL_POSIX)
	#include <pthread.h>
#elif defined(CRITICAL_WINAPI)
	#include <windows.h>
	//#include <Processthreadsapi.h>
#endif

namespace critical {

//! @ingroup core
class Thread{
public:
#if defined(CRITICAL_POSIX)
	typedef void* (*ThreadFunc)(void*);
#elif defined(CRITICAL_WINAPI)
	typedef DWORD WINAPI (*ThreadFunc)(LPVOID);
#endif
	
private:
#if defined(CRITICAL_POSIX)
	pthread_t _t;
#elif defined(CRITICAL_WINAPI)
	HANDLE _t;
#endif
	//ThreadFunc _f;
	bool allocated;
public:
	Thread();
	Thread(ThreadFunc f, void *arg);
	~Thread();
	
	bool init(ThreadFunc f, void *arg);
	bool terminate();
	
	void detach();
	void join();
	
};
	
}

#endif