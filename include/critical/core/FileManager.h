//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file FileManager.h
 *	@date 07.12.2014.
 *  @author Armon Carigiet
 *
 *  @brief Management class, handling the file reading and writing processes.
 *  @ingroup core
 *  @see FileManager.cpp
 */
//===========================================================================

#ifndef CRITICAL_CORE_FILEMANAGER_H_
#define CRITICAL_CORE_FILEMANAGER_H_

#include <critical/config.h>

#include "MemoryManager.h"
#include "WeakPointer.h"
#include "Vector.h"

#include <fstream>
#include <cerrno>
#include <sys/stat.h>

#if defined(CRITICAL_MACOSX)
struct __sFILEX {
	unsigned char	*up;	/* saved _p when _p is doing ungetc data */
	pthread_mutex_t	fl_mutex;	/* used for MT-safety */
	int		orientation:2;	/* orientation for fwide() */
	int		counted:1;	/* stream counted against STREAM_MAX */
	mbstate_t	mbstate;	/* multibyte conversion state */
};
#endif

namespace critical{
	
int __sread(void *cookie, char *buf, int n);
int __swrite(void *cookie, const char *buf, int n);
fpos_t __sseek(void *cookie, fpos_t offset, int whence);
int __sclose(void *cookie);
	
//! Class handeling file input and output.
//! @note This class is build up static, so there is no need to crate objects.
//! @ingroup core
class FileManager{
	friend class FileBuffer;
	template <class T> friend class FileStream;
	struct _file_table_entry{
		int allocated;
		FILE _fp;
		__sFILEX ex;
	};
	
	struct _file_dec{
		FILE* _fp;
		uint32_t _i;
	};
	
public:
	
	enum fileExceptionCode{
		CRFileExceptionUnknowenError = 1,
		CRFileExceptionNoSuchFile,
		CRFileExceptionPermissionDenied,
		CRFileExceptionFileLimitExceeded,
		CRFileExceptionTooLongPath,
		CRFileExceptionNoMemoryLeft,
	};
	
    //! Exception class for file IO.
	class FileException{
	private:
		int c;
		fileExceptionCode e;
	public:
		FileException(int cerror){
			c=cerror;
			if(cerror==ENOENT){
				e=CRFileExceptionNoSuchFile;
			}else if(cerror==EACCES || cerror==EPERM){
				e=CRFileExceptionPermissionDenied;
			}else if(cerror==ENFILE){
				e=CRFileExceptionFileLimitExceeded;
			}else if(cerror==ENAMETOOLONG){
				e=CRFileExceptionTooLongPath;
			}else if(cerror==ENOMEM){
				e=CRFileExceptionNoMemoryLeft;
			}else{
				e=CRFileExceptionUnknowenError;
			}
		}
		FileException(fileExceptionCode except){
			e=except;
		};
		
		int getCError(){ return c; };
		fileExceptionCode getExceptionCode(){ return e; };
	};
	
    //! Types of seeking methods
	enum seekMode{
		CRSeekSet = 0,
		CRSeekCurrent,
		CRSeekEnd
	};
	
	typedef unsigned int openMode;
	static const openMode append = 		0x01;
	static const openMode ate = 		0x02;
	static const openMode binary = 		0x04;
	static const openMode in = 			0x08;
	static const openMode out = 		0x10;
	static const openMode truncate = 	0x20;
	
	static Vector<_file_table_entry> _v;
private:
	//static Vector<_file_table_entry> _v;
	
	static _file_dec __sfp();
	static int __sflags(char *mode, int *optr);
	static _file_dec __fopen(const char* p, const char* m);
	static int __fclose(uint32_t i);

public:
	
    //! File stream class for reading from and writing t a file.
	template <typename T>
	class __BasicFileStream{
		friend class FileManager;
	private:
		FILE* _fp;
		uint32_t index;
	public:
		
		__BasicFileStream(){
			_fp=NULL;
			index=0;
		}
        
		__BasicFileStream(_file_dec d){
			if(d._fp && d._fp->_file!=-1){
				_fp=d._fp;
				index=d._i;
				
			}
			
		}
		
        //! Reads the next value available in the stream and moves the cursor forward.
        //! @return Value read by the file stream.
		T get(){
			T c;
			fread(&c, sizeof(T), 1, _fp);
			return c;
		}
        
        //! Reads the next value available in the stream and moves the cursor forward.
        //! @param [out] c Location to put the read value.
		void get(T& c){
			fread(c, sizeof(T), 1, _fp);
		}
        
        //! Writes a given value available in the stream and moves the cursor forward.
        //! @param [in] c Location of the value to be written.
		void put(T& c){
			fwrite(&c, sizeof(T), 1, _fp);
		}
		
//		void getLine(T* s, ){
//			fgetln
//		}
		
        //! Reads values from the file stream.
        //! @param [out] s Memory location to put the read values.
        //! @param n Numer of values to read.
		void read(T* s, size_t n){
			fread(s, sizeof(T), n, _fp);
		}
        //! writes values to the file stream.
        //! @param [in] s Memory location to read the values from.
        //! @param n Numer of values to write.
		void write(const T* s, size_t n){
			fwrite(s, sizeof(T), n, _fp);
		}
		
        //! Flushes the file stream.
		void flush(){
			fflush(_fp);
		}
        
		//! Moves the file cursor.
        //! @param off Offset to move the cursor.
        //! @param m Way of moving the cursor.
		void seek(fpos_t off, seekMode m){
			if(fseek(_fp, off, m)!=0){
				throw MemoryException(errno);
			}
		}
		
        //! Gets the location of the file cursor.
        //! @return Position of the file cursor.
		size_t tell(){
			size_t s=ftell(_fp);
			return s;
		}
		
        //! Gets the offset of the file cursor.
        //! @return Offset of the file cursor.
		fpos_t getFilePosition(){
			fpos_t pos;
			if(fgetpos(_fp, &pos)==-1){
				throw MemoryException(errno);
			}
			return pos;
		}
		
        //! Sets the file cursor to a given offset.
        //! @param off Offset to set the curser to.
		void setFilePosition(fpos_t off){
			if(fsetpos(_fp, &off)==-1){
				throw MemoryException(errno);
			}
		}
		
        //! Checks if the stream has reached the end of the file.
		bool endOfFile(){
			return feof(_fp);
		}
		
        //! Loads the content of a file to the memory.
        //! @param [out] s Location where the size of the file will be saved.
        //! @return Pointer to the memory holding the loaded file.
		MemoryManager::ConnectedPointer<T> loadFile(size_t *s=nullptr){
			this->seek(0, CRSeekEnd);
			size_t size=this->tell();
			this->seek(0, CRSeekSet);
			MemoryManager::ConnectedPointer<T> p=MemoryManager::salloc<T>(size);
			read(&p, size);
			if(s){
				*s=size;
			}
			return p;
			
		}
		
	};

	typedef __BasicFileStream<char> FileStream;
	
    //! Initializes the FileManager.
    //! @note This method has to be called before using the Manager.
	static void init(uint32_t capacity=4096/sizeof(_file_table_entry)){
		//printf("\nFM Vector Capacity: %d\n", capacity);
		_v=Vector<_file_table_entry>(capacity);
		//_v.printref();
	}
	
    //! Terminates the FileManager.
	static void terminate(){
		_v.destroy();
	}
	
	//! Opens a File and creates a stream to perform operations on it.
    //! @param [in] path Path to the file.
    //! @param [in] m File opening options in form of a string literal.
    //! @return Created FileStream.
	static FileStream openStream(const char* path, const char* m){
		// to be carefull!!
		_file_dec dec=__fopen(path, m);
		//FILE* fp=fopen("/Users/armoncarigiet/Desktop/testfile.dae", "r");
		//printf("bf: %d", dec._fp->);
		//printf("");
		//FILE* f=;
		return FileStream(dec);
	}
	
    //! Closes a open file and its associated FileStream object.
    //! @param [in, out] b FileStream managing th file to be closed.
    //! @return [true, false] True if the process was successful, False if an error occured.
	static bool closeStream(FileStream &_b){
		int a=__fclose(_b.index);
		if(a==-1){
			return false;
		}
		return true;
	}
	
};
	
}

#endif /* defined(CRITICAL_CORE_FILEMANAGER_H_) */