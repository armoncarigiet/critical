//
//  MemoryPool.h
//  
//
//  Created by Armon Carigiet on 04.07.14.
//
//

#ifndef CRITICAL_CORE_MEMORYPOOL_H_
#define CRITICAL_CORE_MEMORYPOOL_H_

#include <cstdlib>
#include <cstddef>
#include <vector>
#include "types.h"
#include "MemoryManager.h"
#include "Pointer.h"


namespace critical {

class MemoryPool{
private:
	struct _mem_block{
		bool allocated;
		char data[1];
	};
	static size_t _mem_block_size;
public:
	class PoolPointer: public Pointer<void>{
		friend class MemoryPool;
	private:
		uint32_t pindex;
	public:
		//! Resets Pointer.
		void reset();
		//! Resets Pointer to block _i.
		void reset(uint32_t _i);
		//! Resets Pointer to the pointer, which p is handling.
		void reset(PoolPointer &p);
		
		PoolPointer operator+(ptrdiff_t offset);
		PoolPointer operator-(ptrdiff_t offset);
		PoolPointer& operator+=(ptrdiff_t offset);
		PoolPointer& operator-=(ptrdiff_t offset);
		
		PoolPointer& operator++();
		PoolPointer& operator--();
		PoolPointer operator++(int);
		PoolPointer operator--(int);
	};
private:

	MemoryManager::ConnectedPointer<void> _p;
	size_t blockSize;
	size_t size;
	
	uint32_t limit;
public:
	MemoryPool(size_t blocksize, size_t count);
	virtual ~MemoryPool();
	
	PoolPointer allocateBlock();
	PoolPointer allocateBlocks(size_t count);
	void freeBlock(PoolPointer& p);
	//bool freeBlocks(PoolPointer& p, size_t count);
	//bool freeBlocksRange(size_t low, size_t high);
	
	void expand(size_t count);
	
	void freePool();
	
};
	
	
	
}

#endif /* defined(CRITICAL_CORE_MEMORYPOOL_H_) */
