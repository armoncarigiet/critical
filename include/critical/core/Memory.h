//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Memory.h
 *	@date 30.12.2014.
 *  @author Armon Carigiet
 *
 *  @brief Provides different macros, functions and classes for the memory-management.
 *  @ingroup core
 */
//===========================================================================

#ifndef CRITICAL_CORE_MEMORY_H_
#define CRITICAL_CORE_MEMORY_H_

#if defined(CRITICAL_POSIX)
	#include <sys/mman.h>
	#include <unistd.h>
#elif defined(CRITICAL_WINAPI)
	#include <windows.h>
#endif
#include <cstdlib>
#include <cstddef>
#include <cerrno>
#include <cstring>
#include "Mutex.h"

#if defined(CRITICAL_DEBUG)
	#include <cstdio>
#endif

#define CRITICAL_CEIL(n, to) 	((int)(n/to)+1)*to
#define CRITICAL_FLOOR(n, to) 	(int)(n/to)*to

#define CRITICAL_MEM_ALIGN	8
#define CRITICAL_ALIGN2(x)	(((x-1)&(~0x1))+2)
#define CRITICAL_ALIGN4(x)	(((x-1)&(~0x3))+4)
#define CRITICAL_ALIGN8(x)	(((x-1)&(~0x7))+8)

#define CRITICAL_ALIGN(x,a) (((x-1)&(~(a-1)))+a)

#define CRITICAL_ALIGN_PTR(x,a) reinterpret_cast<void*>(((reinterpret_cast<size_t>(p.ptr)-1)&(~(align-1)))+align)

#define CRITICAL_HEAP_SIZE 409600	// 400 KiB	==> TODO define by ammount of memory available

#define CRITICAL_MEMORY_HEAPMETABLOCK_SIZE (2*sizeof(_heap_meta_block*)+sizeof(size_t)+3*sizeof(int))

namespace critical {

//======================================================================================
// declaration
//======================================================================================
	class MemoryManager;
	class MemoryException;
	
//! memoryLocation codes
enum memoryLocationCode{
	CRMemoryLocationUndefined = 0,
	CRMemoryLocationHeap,
	CRMemoryLocationPageTable,
};
	
//! memoryException codes
enum memoryExceptionCode{
	CRMemoryExceptionUnknowenError = 0,
	CRMemoryExceptionRunOutOfMemory,
	CRMemoryExceptionBadAlloc,
	CRMemoryExceptionHeapNotEnoughtSpace,
	CRMemoryExceptionFullTable,
	CRMemoryExceptionOutOfRange,
	CRMemoryExceptionContainerOutOfRange,
	CRMemoryExceptionNoSpaceLeft
};
	
struct _mem_table_entry{
	void* memoryPointer;
	size_t size;
	int references;
	int alignment;
	bool allocated;
};
	
struct _heap_meta_block{
	_heap_meta_block* next;
	_heap_meta_block* last;
	size_t size;
	int allocated;
	int references;
	int alignment;
	char data[1];
};
	
//! Class for handling exceptions created by MemoryManager.
class MemoryException{
private:
	int cerror;
	memoryExceptionCode exceptionCode;
public:
	MemoryException(int except): cerror(except){
		if(except==ENOMEM){
			exceptionCode=CRMemoryExceptionRunOutOfMemory;
		}else{
			exceptionCode=CRMemoryExceptionBadAlloc;
		}
	}
	MemoryException(memoryExceptionCode code){
		exceptionCode=code;
		cerror=0;
	}
	//! Returns exception code stored in class.
	//! @return exceptionCode;
	int getExceptionCode(){
		return exceptionCode;
	}
	int getCError(){
		return cerror;
	}
    
};

}

#endif