//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file SharedPointer.h
 *	@date 30.12.2014.
 *  @author Armon Carigiet
 *
 *  @brief Provides a pointer class with reference counting abilities.
 *  @ingroup core
 */
//===========================================================================

#ifndef  CRITICAL_CORE_SHAREDPOINTER_H_
#define  CRITICAL_CORE_SHAREDPOINTER_H_

#include "MemoryManager.h"
#include <cstdio>

namespace critical {

//======================================================================================
// declaration
//======================================================================================
	
template <typename T> class SharedPointer;
	
//======================================================================================
// definition
//======================================================================================

//! Pointer class with reference counting.
template <typename T>
class SharedPointer : public MemoryManager::ConnectedPointer<T>{
	friend class memoryManagerAnalyser;
protected:
	int *refcounter;
public:
	SharedPointer() : MemoryManager::ConnectedPointer<T>(), refcounter(nullptr) {}
	SharedPointer(MemoryManager::ConnectedPointer<T> &p) : MemoryManager::ConnectedPointer<T>(p){
		if(p.location==CRMemoryLocationHeap){
			this->refcounter=&p.block->references;
			++(*refcounter);
		}else if(p.location==CRMemoryLocationPageTable){
			this->refcounter=&MemoryManager::pointerTable[p.eindex].references;
			++(*refcounter);
		}
		
	}
	SharedPointer(MemoryManager::ConnectedPointer<T> &&p) : MemoryManager::ConnectedPointer<T>(p){
		if(p.location==CRMemoryLocationHeap){
			this->refcounter=&p.block->references;
			++(*refcounter);
			//printf("");
		}else if(p.location==CRMemoryLocationPageTable){
			this->refcounter=&MemoryManager::pointerTable[p.eindex].references;
			++(*refcounter);
		}
		
		//p.ptr=nullptr; p.eindex=0; p.allocated=false; p.location=CRMemoryLocationUndefined; p.block=nullptr;
	}
	
	//SharedPointer(SharedPointer<void> &p) {}	virtual ~SharedPointer();
	virtual ~SharedPointer() { reset(); }
	
	virtual void sync();
	
	void realloc(size_t size);
	
	virtual void reset();
	void reset(SharedPointer<T> &ptr);
	
	void printref(){
		printf("Refcounter: %d", *refcounter);
	}
	
	SharedPointer<T> operator+(ptrdiff_t offset);
	SharedPointer<T> operator-(ptrdiff_t offset);
	SharedPointer<T>& operator+=(ptrdiff_t offset);
	SharedPointer<T>& operator-=(ptrdiff_t offset);
	
	SharedPointer<T>& operator++();
	SharedPointer<T>& operator--();
	SharedPointer<T> operator++(int);
	SharedPointer<T> operator--(int);

	// Custom move and copy constructors to fix move assignment
	SharedPointer(SharedPointer<T> &p) : MemoryManager::ConnectedPointer<T>(p){
		if(p.location==CRMemoryLocationHeap){
			MemoryManager::ConnectedPointer<T>(p.block);
			this->refcounter=&(p.block->references);
			++(*refcounter);
		}else if(p.location==CRMemoryLocationPageTable){
			MemoryManager::ConnectedPointer<T>(p.eindex);
			this->refcounter=&(MemoryManager::pointerTable[p.eindex].references);
			++(*refcounter);
		}
	}
	SharedPointer(SharedPointer<T> &&p) : MemoryManager::ConnectedPointer<T>(p){
		if(p.location==CRMemoryLocationHeap){
			MemoryManager::ConnectedPointer<T>(p.block);
			this->refcounter=&(p.block->references);
			++(*refcounter);
		}else if(p.location==CRMemoryLocationPageTable){
			MemoryManager::ConnectedPointer<T>(p.eindex);
			this->refcounter=&(MemoryManager::pointerTable[p.eindex].references);
			++(*refcounter);
		}
		//p.ptr=nullptr; p.eindex=0; p.allocated=false; p.location=CRMemoryLocationUndefined; p.block=nullptr; p.refcounter=nullptr;
	}

	SharedPointer<T>& operator=(SharedPointer<T> &p);
	SharedPointer<T>& operator=(SharedPointer<T> &&p);
	
};
//======================================================================================
// specialisation
//======================================================================================

template<> class SharedPointer<void> : public MemoryManager::ConnectedPointer<void>{
	friend class memoryManagerAnalyser;
protected:
	int *refcounter;
public:
	SharedPointer() : MemoryManager::ConnectedPointer<void>(), refcounter(nullptr) {}
	SharedPointer(MemoryManager::ConnectedPointer<void> &p) : MemoryManager::ConnectedPointer<void>(p){
		if(p.location==CRMemoryLocationHeap){
			this->refcounter=&p.block->references;
			++(*refcounter);
		}else if(p.location==CRMemoryLocationPageTable){
			this->refcounter=&MemoryManager::pointerTable[p.eindex].references;
			++(*refcounter);
		}
	}
	SharedPointer(MemoryManager::ConnectedPointer<void> &&p) : MemoryManager::ConnectedPointer<void>(p){
		if(p.location==CRMemoryLocationHeap){
			this->refcounter=&p.block->references;
			++(*refcounter);
		}else if(p.location==CRMemoryLocationPageTable){
			this->refcounter=&MemoryManager::pointerTable[p.eindex].references;
			++(*refcounter);
		}
	};
	virtual ~SharedPointer() { reset(); }
	
	virtual void sync();
	
	void realloc(size_t size);
	
	virtual void reset();
	void reset(SharedPointer<void> &ptr);
	
	SharedPointer<void> operator+(ptrdiff_t offset);
	SharedPointer<void> operator-(ptrdiff_t offset);
	SharedPointer<void>& operator+=(ptrdiff_t offset);
	SharedPointer<void>& operator-=(ptrdiff_t offset);
	
	SharedPointer<void>& operator++();
	SharedPointer<void>& operator--();
	SharedPointer<void> operator++(int);
	SharedPointer<void> operator--(int);
	
	SharedPointer(SharedPointer<void> &p) : MemoryManager::ConnectedPointer<void>(p){
		if(p.location==CRMemoryLocationHeap){
			MemoryManager::ConnectedPointer<void>(p.block);
			this->refcounter=&(p.block->references);
			++(*refcounter);
		}else if(p.location==CRMemoryLocationPageTable){
			MemoryManager::ConnectedPointer<void>(p.eindex);
			this->refcounter=&(MemoryManager::pointerTable[p.eindex].references);
			++(*refcounter);
		}
	}
	SharedPointer(SharedPointer<void> &&p) : MemoryManager::ConnectedPointer<void>(p){
		if(p.location==CRMemoryLocationHeap){
			MemoryManager::ConnectedPointer<void>(p.block);
			this->refcounter=&(p.block->references);
			++(*refcounter);
		}else if(p.location==CRMemoryLocationPageTable){
			MemoryManager::ConnectedPointer<void>(p.eindex);
			this->refcounter=&(MemoryManager::pointerTable[p.eindex].references);
			++(*refcounter);
		}
		//p.ptr=nullptr; p.eindex=0; p.allocated=false; p.location=CRMemoryLocationUndefined; p.block=nullptr; p.refcounter=nullptr;
	}

	
	SharedPointer<void>& operator=(SharedPointer<void> &p);
	SharedPointer<void>& operator=(SharedPointer<void> &&p);
};

//======================================================================================
// implementation
//======================================================================================

template <class T>
SharedPointer<T>& SharedPointer<T>::operator=(SharedPointer<T> &p){
	MemoryManager::ConnectedPointer<T>::operator=(p);
	if(p.allocated){
		if(p.location==CRMemoryLocationHeap){
			this->refcounter=&p.block->references;
			//printf("\nRefcounter1: %d",*refcounter);
			//printf("");
			++(*refcounter);
		}else if(p.location==CRMemoryLocationPageTable){
			this->refcounter=&MemoryManager::pointerTable[p.eindex].references;
			++(*refcounter);
		}
		//printf("\nRefcounter2: %d",*refcounter);
		//printf("");
	}
	return *this;
}
inline SharedPointer<void>& SharedPointer<void>::operator=(SharedPointer<void> &p){
	MemoryManager::ConnectedPointer<void>::operator=(p);
	if(p.allocated){
		if(p.location==CRMemoryLocationHeap){
			this->refcounter=&p.block->references;
			++(*refcounter);
		}else if(p.location==CRMemoryLocationPageTable){
			this->refcounter=&MemoryManager::pointerTable[p.eindex].references;
			++(*refcounter);
		}
	}
	return *this;
}

template <class T>
SharedPointer<T>& SharedPointer<T>::operator=(SharedPointer<T> &&p){
	MemoryManager::ConnectedPointer<T>::operator=(p);
	if(p.allocated){
		if(p.location==CRMemoryLocationHeap){
			this->refcounter=&p.block->references;
			++(*refcounter);
		}else if(p.location==CRMemoryLocationPageTable){
			this->refcounter=&MemoryManager::pointerTable[p.eindex].references;
			++(*refcounter);
		}
	}
	return *this;

}
inline SharedPointer<void>& SharedPointer<void>::operator=(SharedPointer<void> &&p){
	MemoryManager::ConnectedPointer<void>::operator=(p);
	if(p.allocated){
		if(p.location==CRMemoryLocationHeap){
			this->refcounter=&p.block->references;
			++(*refcounter);
		}else if(p.location==CRMemoryLocationPageTable){
			this->refcounter=&MemoryManager::pointerTable[p.eindex].references;
			++(*refcounter);
		}
	}
	return *this;
}
	
template <class T>
void SharedPointer<T>::sync(){
	MemoryManager::ConnectedPointer<T>::sync();
	if(this->location==CRMemoryLocationPageTable){
		this->refcounter=&MemoryManager::pointerTable[this->eindex].references;
	}else if(this->location==CRMemoryLocationHeap){
		this->refcounter=&this->block->references;
	}
}
inline void SharedPointer<void>::sync(){
	MemoryManager::ConnectedPointer<void>::sync();
	if(this->location==CRMemoryLocationPageTable){
		this->refcounter=&MemoryManager::pointerTable[this->eindex].references;
	}else if(this->location==CRMemoryLocationHeap){
		this->refcounter=&this->block->references;
	}
}
template <class T>
void SharedPointer<T>::realloc(size_t size){
	MemoryManager::srealloc(*this, size);
	refcounter=&this->block->references;
}
	
template <class T>
void SharedPointer<T>::reset(){
	if(this->allocated){
		--(*refcounter);
		if((*refcounter)==0){
			MemoryManager::sfree(*this);
			this->allocated=false;
		}
	}
}
inline void SharedPointer<void>::reset(){
	if(this->allocated){
		--(*refcounter);
		if((*refcounter)==0){
			MemoryManager::sfree(*this);
			this->allocated=false;
		}
	}
}
	
template <class T>
void SharedPointer<T>::reset(SharedPointer<T> &ptr){
	if(this->allocated){
		MemoryManager::ConnectedPointer<T>::reset(ptr);
		if(this->location==CRMemoryLocationHeap){
			this->refcounter=&ptr.block->references;
		}else if(this->location==CRMemoryLocationPageTable){
			this->refcounter=&MemoryManager::pointerTable[ptr.eindex].references;
		}
		(*refcounter)+=1;
	}
}
inline void SharedPointer<void>::reset(SharedPointer<void> &ptr){
	if(this->allocated){
		MemoryManager::ConnectedPointer<void>::reset(ptr);
		if(this->location==CRMemoryLocationHeap){
			this->refcounter=&ptr.block->references;
		}else if(this->location==CRMemoryLocationPageTable){
			this->refcounter=&MemoryManager::pointerTable[ptr.eindex].references;
		}
		(*refcounter)+=1;
	}
}
	
template <class T>
SharedPointer<T> SharedPointer<T>::operator+(ptrdiff_t offset){
	SharedPointer<T> tmp(*this);
	tmp.shift(offset);
	return tmp;
}
inline SharedPointer<void> SharedPointer<void>::operator+(ptrdiff_t offset){
	SharedPointer<void> tmp(*this);
	tmp.shift(offset);
	return tmp;
}
	
template <class T>
SharedPointer<T> SharedPointer<T>::operator-(ptrdiff_t offset){
	SharedPointer<T> tmp(*this);
	tmp.shift(-offset);
	return tmp;
}
inline SharedPointer<void> SharedPointer<void>::operator-(ptrdiff_t offset){
	SharedPointer<void> tmp(*this);
	tmp.shift(-offset);
	return tmp;
}
	
template <class T>
SharedPointer<T>& SharedPointer<T>::operator+=(ptrdiff_t offset){
	this->shift(offset);
	return *this;
}
inline SharedPointer<void>& SharedPointer<void>::operator+=(ptrdiff_t offset){
	this->shift(offset);
	return *this;
}
	
template <class T>
SharedPointer<T>& SharedPointer<T>::operator-=(ptrdiff_t offset){
	this->shift(-offset);
	return *this;
}
inline SharedPointer<void>& SharedPointer<void>::operator-=(ptrdiff_t offset){
	this->shift(offset);
	return *this;
}
	
template <class T>
SharedPointer<T>& SharedPointer<T>::operator++(){
	this->shift(1);
	return *this;
}
inline SharedPointer<void>& SharedPointer<void>::operator++(){
	this->shift(1);
	return *this;
}
	
template <class T>
SharedPointer<T>& SharedPointer<T>::operator--(){
	this->shift(-1);
	return *this;
}
inline SharedPointer<void>& SharedPointer<void>::operator--(){
	this->shift(-1);
	return *this;
}
	
template <class T>
SharedPointer<T> SharedPointer<T>::operator++(int){
	SharedPointer<T> tmp(*this);
	++(*this);
	return tmp;
}
inline SharedPointer<void> SharedPointer<void>::operator++(int){
	SharedPointer<void> tmp(*this);
	++(*this);
	return tmp;
}
	
template <class T>
SharedPointer<T> SharedPointer<T>::operator--(int){
	SharedPointer<T> tmp(*this);
	--(*this);
	return tmp;
}
inline SharedPointer<void> SharedPointer<void>::operator--(int){
	SharedPointer<void> tmp(*this);
	--(*this);
	return tmp;
}

}
#endif /* defined(CRITICAL_CORE_SHAREDPOINTER_H_) */
