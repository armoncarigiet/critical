//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file ColladaHelper.h
 *	@date 31.01.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a helper class for loading and processing COLLADA files.
 *  @ingroup core
 *  @see ColladaHelper.cpp
 */
//===========================================================================

#ifndef CRITICAL_CORE_COLLADAHELPER_H_
#define CRITICAL_CORE_COLLADAHELPER_H_

#include "Vector.h"
#include "String.h"
#include <critical/graphics/Camera.h>
#include <critical/graphics/Mesh.h>
#include <critical/graphics/Scene.h>

#include <libxml/parser.h>

namespace critical {
	
//! @ingroup core
class ColladaHelper{
public:
	
//===========================================================================
// COLLADA Tags
//===========================================================================
	
	static const char mainTag[];
	
	//-------------------------------------
	// Extensibility
	//-------------------------------------
	
	static const char techniqueCommonTag[];
	static const char extraTag[];
	static const char techniqueTag[];
	
	//-------------------------------------
	// Asset
	//-------------------------------------
	
	static const char assetTag[];
	
	static const char contributorTag[];
		static const char authorTag[];
		static const char authoringToolTag[];
	static const char createdTag[];
	static const char modifiedTag[];
	static const char unitTag[];
	static const char upAxisTag[];
	
	//-------------------------------------
	// Camera
	//-------------------------------------
	
	static const char libraryCamerasTag[];
	static const char instanceCameraTag[];
	
	static const char cameraTag[];
	static const char imagerTag[];
	static const char opticsTag[];
	static const char orthographic[];
	static const char perspectiveTag[];
		static const char xfovTag[];
		static const char yfovTag[];
		static const char xmagTag[];
		static const char ymagTag[];
		static const char aspectRatioTag[];
		static const char znearTag[];
		static const char zfarTag[];
	
	
	//-------------------------------------
	// Lighting
	//-------------------------------------
	
	static const char libraryLightsTag[];
	static const char instanceLightTag[];
	
	static const char lightTag[];
	static const char ambientTag[];
	static const char directionalTag[];
	static const char pointTag[];
	static const char spotTag[];
		static const char colorTag[];
		static const char constantAttenuationTag[];
		static const char linearAttenuationTag[];
		static const char quadraticAttenuationTag[];
		static const char fallofAngleTag[];
		static const char fallofExponentTag[];
	
	
	//-------------------------------------
	// Geometry
	//-------------------------------------
	
	static const char libraryGeometriesTag[];
	static const char instanceGeometryTag[];
	
	static const char meshTag[];
	static const char splineTag[];
	static const char convexMeshTag[];
	static const char linesTag[];
	static const char linestripsTag[];
	static const char polygonsTag[];
	static const char polylistTag[];
	
	static const char geometryTag[];
	static const char trianglesTag[];
	static const char trifansTag[];
	static const char tristripsTag[];
	static const char verticesTag[];
	
	static const char pTag[];
	static const char hTag[];
	static const char phTag[];
	static const char vcountTag[];
	
	
	//-------------------------------------
	// Data Flow
	//-------------------------------------
	
	static const char accesssorTag[];
	static const char boolArrayTag[];
	static const char floatArrayTag[];
	static const char IDREFArrayTag[];
	static const char intArrayTag[];
	static const char nameArrayTag[];
	static const char paramTag[];
	static const char sourceTag[];
	static const char inputTag[];
	
	
	//-------------------------------------
	// Scene
	//-------------------------------------
	
	static const char libraryVisualScenesTag[];
	static const char libraryNodesTag[];
	
	static const char instanceNodeTag[];
	static const char instanceVisualSceneTag[];
	static const char nodeTag[];
	static const char sceneTag[];
	static const char visualSceneTag[];
	
	
	//-------------------------------------
	// Transform
	//-------------------------------------
	
	static const char lookatTag[];
	static const char matrixTag[];
	static const char rotateTag[];
	static const char scaleTag[];
	static const char skewTag[];
	static const char translateTag[];
	
	//-------------------------------------
	// Material
	//-------------------------------------
	
	static const char libraryMaterialsTag[];
	static const char materialTag[];
	
	//-------------------------------------
	// Effect
	//-------------------------------------
	
	static const char libraryEffectsTag[];
	static const char effectTag[];
	static const char instanceEffectTag[];
	static const char phongTag[];
	static const char profile_COMMONTag[];
	static const char shininessTag[];
	static const char emissionTag[];
	static const char diffuseTag[];
	static const char specularTag[];
	static const char reflectiveTag[];
	static const char reflecitvityTag[];
	static const char transparentTag[];
	static const char transparencyTag[];
	static const char indexOfRefractionTag[];
	
	
	
	//-------------------------------------
	// Physics
	//-------------------------------------
    // Mybe in the future.
	
	
	//-------------------------------------
	// Semantics
	//-------------------------------------
	
	static const char PositionSemantic[];
	
	static const char VertexSemantic[];
	static const char NormalSemantic[];
	static const char TangentSemantic[];
	static const char BitangentSemantic[];
	static const char TexturecoordSemantic[];
	
	
//===========================================================================
// Data Structs
//===========================================================================
	
	//! Ignore.
	class metadata{
	public:
		metadata(){}
		metadata(uint32_t pairs){ v=Vector<Tuple<String, String>>(pairs); }
		Vector<Tuple<String, String>> v;
		
		bool isKey(const char* key){
			for(int i=0; i<v.size(); i++) {
				if(strcmp((char*)v[i]._t1.c_str(),key)==0){
					return true;
				}
			}
			return false;
		}
		
		String& getValueForKey(const char* key){
			String* s=nullptr;
			for(int i=0; i<v.size(); i++) {
				if(strcmp((char*)v[i]._t1.c_str(),key)==0){
					s=&v[i]._t2;
					break;
				}
			}
			if(!s){
				throw 1;
			}
			return *s;
		}
		
		void print(){
			printf("\n\n");
			for (int i=0; i<v.size(); ++i) {
				printf("%s\t=>\t%s\n", v[i]._t1.c_str(), v[i]._t2.c_str());
			}
			printf("\n");
		}
		
		metadata(metadata& m): v(m.v){  }
		metadata(metadata&& m): v(m.v){  }

	};

	
private:
	xmlNode* findNodeWithId(xmlNode *node, const char* i);
	xmlNode* findeNodeWithName(xmlNode* node, const char* name);
	int getNodeOffsetWithId(xmlNode *node, const char* i);

	Array<float> resolveFloatArray(xmlNode *node);
	Array<glmath_vec2> resolveFloat2Array(xmlNode *node);
	Array<glmath_vec3> resolveFloat3Array(xmlNode *node, Scene::SceneAttributes upaxis);
	Array<glmath_vec4> resolveFloat4Array(xmlNode *node);
	Array<int> resolveIntArray(xmlNode *node);
	Array<int> resolvePArray(xmlNode *node, int count);
	Array<int> resolveArray(xmlNode *node);
	Array<float> resolvefArray(xmlNode *node);
	
	glmath_vec3 resolveVec3(xmlNode *node);
	glmath_vec4 resolveVec4(xmlNode *node);
	
	void resolveMat4(glmath_mat4 *mat, xmlNode *node, Scene::SceneAttributes attr);
	
	bool resolveNode(SceneNode &node, xmlNode *n, Scene *scene, xmlNode *sceneNode, bool root);
public:
    
    //! Extracts the metadata stored in the asset child of a given collada Tag.
    //! @param [in] node XML node to get the metadata from.
	//! @return Metadata of the given node.
	metadata getMetaData(xmlNode *node);
	
	//! Ignore.
	Camera getMainCamera(xmlNode* node);
	//! Ignore.
	Camera getMainLight(xmlNode* node);
	
	//! Loads COLLADA scene.
    //! @note This function will result in undefined behaviour if the given node is not holding a valid collada main tag.
	//! @param [in] node Main node of the fetched file.
	//! @param offset Index of the scene to be loaded.
	//! @return Pointer to the loaded scene.
	MemoryManager::ConnectedPointer<Scene> getScene(xmlNode* node, uint32_t offset=0);
	
	//! Loads cameras from a scene into an array.
	//! @param [in, out] cameras Reference to the Vector to store the values in.
	//! @param [in] node Camera node.
	//! @return True if the loading was successfull, else false.
	bool loadCameras(Vector<Camera> &cameras, xmlNode *node);
	//! Loads light sources from a scene into an array.
	//! @param [in, out] lights Reference to the Vector to store the values in.
	//! @param [in] node LightSource node.
	//! @return True if the loading was successful, else false.
	bool loadLightSources(Vector<LightSource> &lights, xmlNode *node);
	//! Loads materials from a scene into an array.
	//! @param [in, out] materials Reference to the Vector to store the values in.
	//! @param [in] node Material node.
	//! @return True if the loading was successful, else false.
	bool loadMaterials(Vector<Material> &materials, xmlNode* node);
	//! Loads one mesh from a scene.
	//! @param [out] mesh Location to store the mesh in.
	//! @param [in] materials Reference to the loaded materials.
	//! @param [in] attr Information of the scene.
	//! @return True if the loading was successful, else false.
	bool loadMesh(Mesh &mesh, xmlNode* node, Vector<Material> &materials, Dictionary<Scene::SceneAttributes> &attr);
	
};
	
}

#endif /* defined(CRITICAL_CORE_COLLADAHELPER_H_) */