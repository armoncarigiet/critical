//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file MemoryManager.h
 *	@date 04.07.2014.
 *  @author Armon Carigiet
 *
 *  @brief Provides a memory-management class.
 *  @ingroup core
 *  @see MemoryManager.cpp
 */
//===========================================================================

#ifndef CRITICAL_CORE_MEMORYMANAGER_H_
#define CRITICAL_CORE_MEMORYMANAGER_H_

#include <critical/config.h>

#include "Memory.h"
#include "Pointer.h"
#include <cstdio>

namespace critical {
	
//======================================================================================
// definition
//======================================================================================

//! Main memory managing Unit.
//! When working with critical, memory should be allocated using MemoryManager.
//! @note MemoryManager stays not in conflict with C/C++'s native memory allocation functions, so they can be used simultainously with the MemoryManager.
//! @ingroup core
class MemoryManager{
private:
	// friendships
	friend class memoryManagerAnalyser;
	template <class T> friend class Pointer;
	template <class T> friend class ConnectedPointer;
	template <class T> friend class SharedPointer;
	template <class T> friend class List;
	
	static Mutex tableMutex;
	static Mutex heapMutex;
	static long int pageSize;
	static bool initialized;
	static size_t tableEntryCount;
	
	static size_t table_size;
	static size_t used_table_size;
	static size_t table_pages;
	//static bool defragmenting_heap;
	//static bool defragmenting_heap_seq;
	//static uint32 lastDefragmentedBlock;
	
	static void exp_tbl();
	
	// important!
	static size_t heap_meta_block_size;
	
	static void* heap_base_address;
	static void* heap_break;
	static void* heap_limit;
	static void* heap_last_page_bound;
	
	static _heap_meta_block* heap_used_limit;
	
	static _heap_meta_block* allocHeapBlock(size_t size);
	static bool freeHeapBlock(_heap_meta_block *b);
	
	static bool freeTableEntry(uint32_t index);
	
	static _heap_meta_block* findHeapBlock(_heap_meta_block* start, size_t size);
	static _heap_meta_block* splitHeapBlock(_heap_meta_block *b, size_t size);
	static _heap_meta_block* expandHeap(size_t size);
public:
	
	static struct _mem_table_entry* pointerTable;
	
	//! Gets page size used by the system.
	//! @return System page size.
	static size_t getPageSize(){
		return MemoryManager::pageSize;
	}
	
	//! Class for handling Memory allocated by MemoryManager
    //! @ingroup core
	template <typename T>
	class ConnectedPointer : public Pointer<T>{
		friend class MemoryManager;
		friend class MemoryStack;
		friend class MemoryPool;
		friend class MemoryTable;
		friend class memoryManagerAnalyser;
		template <class U> friend class SharedPointer;
		template <class U> friend class WeakPointer;
	protected:
		//size_t eindex;	// constant!
		//bool allocated;
		//int location;
		//_heap_meta_block *block;
	public:
		size_t eindex;	// constant!
		bool allocated;
		int location;
		_heap_meta_block *block;
		
		ConnectedPointer() : Pointer<T>(), eindex(0), allocated(false), location(CRMemoryLocationUndefined), block(nullptr){}
        
		ConnectedPointer(uint32_t index) : Pointer<T>(MemoryManager::pointerTable[index].memoryPointer), eindex(index), allocated(true), location(CRMemoryLocationPageTable), block(nullptr) {}
        
		ConnectedPointer(_heap_meta_block* b) : Pointer<T>(b->data), eindex(0), allocated(true), location(CRMemoryLocationHeap), block(b) {}
        
		~ConnectedPointer(){}
		
		//! Synchronizes the current Pointer object with the class Memorymanager.
		//! @note Function needs to be called after Memorymanager's table has been expanded to revalidate the pointer.
		virtual void sync();
		
		//! Resets the Pointer.
		virtual void reset();
        
		//! Resets the Pointer to the pointer, which Memorymanager's pointerTable is holding at index i.
		//! @param i pointerTable index
		void reset(uint32_t i);
        
		//! Resets the Pointer to the pointer, which b is holding.
		//! @param b pointer to heap block
		void reset(_heap_meta_block* b);
        
		//! Resets the Pointer to the pointer, which p is handling.
		//! @param p ConnectedPointer
		void reset(ConnectedPointer<T> &p);

		//! Gets size of the memory section, which the current Pointer is handling.
		//! @return Size of the memory section.
		size_t size();
		
        //! Checks if the pointer an/or the handled memory section is allocated.
        //! @return [true, false] True if the pointer is working, flase if not.
		bool isAllocated();
	
		ConnectedPointer<T> operator+(ptrdiff_t offset);
		ConnectedPointer<T> operator-(ptrdiff_t offset);
		ConnectedPointer<T>& operator+=(ptrdiff_t offset);
		ConnectedPointer<T>& operator-=(ptrdiff_t offset);
		
		ConnectedPointer<T>& operator++();
		ConnectedPointer<T>& operator--();
		ConnectedPointer<T> operator++(int);
		ConnectedPointer<T> operator--(int);

		// Custom move and copy constructors to fix move assignment
		ConnectedPointer(ConnectedPointer<T> &p) : Pointer<T>(p.ptr), eindex(p.eindex), allocated(p.allocated), location(p.location), block(p.block) {}
        
		ConnectedPointer(ConnectedPointer<T> &&p): Pointer<T>(p.ptr), eindex(p.eindex), allocated(p.allocated), location(p.location), block(p.block){
			//p.ptr=nullptr; p.eindex=0; p.allocated=false; p.location=CRMemoryLocationUndefined; p.block=nullptr;
		}
		
		ConnectedPointer<T>& operator=(ConnectedPointer<T> &p);
		ConnectedPointer<T>& operator=(ConnectedPointer<T> &&p);
		
		//friend ConnectedPointer<T> operator+ <>(const ConnectedPointer<T> &p, ptrdiff_t offset);
		//friend ConnectedPointer<T> operator- <>(const ConnectedPointer<T> &p, ptrdiff_t offset);
	};
	//	connected_pointer end
	
	MemoryManager();
	~MemoryManager();
	
	//! Expands the pointer table.
	//! @note Needs to be called if the memoryException CRMemoryExceptionFullTable has risen.
	static void expandTable();
	
	// unique initialisation/termination -> for all class objects
	// init should be called as fast as possible after programm start
	// only call init once!
	
	//! Intitializes the MemoryManager.
	//! @note If Memorymanager is already initialised the functioncall will be ignored.
	//! @note Before using any of MemoryManager's functions, this method needs to be called.
	//! @note This function only needs to be called once.
	static void init(size_t heap_size=CRITICAL_HEAP_SIZE);
	
	//! Terminates the MemoryManager.
	//! @note All memory allocated by MemoryManager will be freed if this method is called
	//! @note If this method is called, calls on any other member function of MemoryManager (except init()) will end in undefined behaviour.
	static void terminate();
	
	
	//! Allocates memory on the heap or memorypages in the virtual address space.
	//! @param s Memory size to allocate.
	//! @note If size is greater than the system page size, whole memory pages will be allocated. Else memory is allocated on the heap.
	//! @note Size will be aligned to page boundary if it is bigger than the system page size.
	//! @return On success connected_pointer pointing to the begin of the allocated memory section. On failure empty connected_pointer.
	template<class T>
	static ConnectedPointer<T> salloc(size_t s);
	
	//! Allocates memory on the heap.
	//! @param s Memory size to allocate.
	//! @return On success connected_pointer pointing to the begin of the allocated memory section. On failure empty connected_pointer.
	template<class T>
	static ConnectedPointer<T> heapAlloc(size_t s);
	
	//! Allocates memory pages in the virtual address space.
	//! @param s Memory size to allocate.
	//! @note Size will be aligned to page boundary.
	//! @return On success connected_pointer pointing to the begin of the allocated memory section. On failure empty connected_pointer.
	template<class T>
	static ConnectedPointer<T> pageAlloc(size_t s);
	
	//! Allocates aligned memory on the heap or memorypages in the virtual address space.
	//! @param s Memory size to allocate.
	//! @param align memory alignment
	//! @note If size is greater than the system page size, whole memory pages will be allocated. Else memory is allocated on the heap.
	//! @note Size will be aligned to page boundary if it is bigger than the system page size.
	//! @return On success connected_pointer pointing to the begin of the allocated memory section. On failure empty connected_pointer.
	template<class T>
	static ConnectedPointer<T> alignAlloc(size_t s, size_t align);
	
	//! Reallocates memory on the heap or memorypages in the virtual address space.
	//! @param p Connected_pointer pointing to memory section.
	//! @param s New memory section size.
	//! @return True on success, false on failure.
	template<class T>
	static bool srealloc(ConnectedPointer<T> &p, size_t s, size_t align=0);
	
	//! Frees memory on the heap or memorypages in the virtual address space.
	//! @param p Connected_pointer pointing to memory section.
	//! @return True on success, false on failure.
	template<class T>
	static bool sfree(ConnectedPointer<T> &p);
	
//	bool defragmentHeap();
//	bool defragmentHeapSequential(const uint32 blocks);
	
};
//======================================================================================
// specialisation
//======================================================================================

//! @ingroup core
template<>
class MemoryManager::ConnectedPointer<void> : public Pointer<void>{
	friend class MemoryManager;
	friend class MemoryStack;
	friend class MemoryPool;
	friend class MemoryTable;
	friend class memoryManagerAnalyser;
	template <class U> friend class SharedPointer;
	template <class U> friend class WeakPointer;
protected:
	size_t eindex;	// constant!
	bool allocated;
	int location;
	_heap_meta_block *block;
public:

	ConnectedPointer() : Pointer<void>(), eindex(0), allocated(false), location(CRMemoryLocationUndefined), block(nullptr) {}
    
	ConnectedPointer(uint32_t index) : Pointer<void>(MemoryManager::pointerTable[index].memoryPointer), eindex(index), allocated(true), location(CRMemoryLocationPageTable), block(nullptr) {}
    
	ConnectedPointer(_heap_meta_block* b) : Pointer<void>(b->data), eindex(0), allocated(true), location(CRMemoryLocationHeap), block(b) {}
    
	~ConnectedPointer() {}
	
	virtual void sync();
	
	virtual void reset();
	void reset(uint32_t i);
	void reset(_heap_meta_block* b);
	void reset(ConnectedPointer<void> &p);
	
	size_t size();
	
	bool isAllocated();

	ConnectedPointer<void> operator+(ptrdiff_t offset);
	ConnectedPointer<void> operator-(ptrdiff_t offset);
	ConnectedPointer<void>& operator+=(ptrdiff_t offset);
	ConnectedPointer<void>& operator-=(ptrdiff_t offset);
	
	ConnectedPointer<void>& operator++();
	ConnectedPointer<void>& operator--();
	ConnectedPointer<void> operator++(int);
	ConnectedPointer<void> operator--(int);
	
	ConnectedPointer(ConnectedPointer<void> &p) : Pointer<void>(p.ptr), eindex(p.eindex), allocated(p.allocated), location(p.location), block(p.block) {}
    
	ConnectedPointer(ConnectedPointer<void> &&p): Pointer<void>(p.ptr), eindex(p.eindex), allocated(p.allocated), location(p.location), block(p.block){
		//p.ptr=nullptr; p.eindex=0; p.allocated=false; p.location=CRMemoryLocationUndefined; p.block=nullptr;
	}
	
	ConnectedPointer<void>& operator=(ConnectedPointer<void> &p);
	ConnectedPointer<void>& operator=(ConnectedPointer<void> &&p);
	
	//friend ConnectedPointer<void> operator+ (const ConnectedPointer<void> &p, ptrdiff_t offset);
	//friend ConnectedPointer<void> operator- (const ConnectedPointer<void> &p, ptrdiff_t offset);
};


//======================================================================================
//	implementation
//======================================================================================
	
//---------------------------------------------
// MemoryManager
//---------------------------------------------
	
template<class T>
MemoryManager::ConnectedPointer<T> MemoryManager::salloc(size_t s){
	if(s<MemoryManager::pageSize){
		heapMutex.lock();
		_heap_meta_block *b=allocHeapBlock(s);
		heapMutex.unlock();
		ConnectedPointer<T> p(b);
		return p;
		
	}else{
		return pageAlloc<T>(s);
	}
}
	
template <class T>
MemoryManager::ConnectedPointer<T> MemoryManager::heapAlloc(size_t s){
	heapMutex.lock();
	_heap_meta_block *b=allocHeapBlock(s);
	heapMutex.unlock();
	return ConnectedPointer<T>(b);
}

template <class T>
MemoryManager::ConnectedPointer<T> MemoryManager::pageAlloc(size_t s){
	size_t size=CRITICAL_CEIL(s, pageSize);
	if(tableEntryCount>=table_size){ // full table
		throw MemoryException(CRMemoryExceptionFullTable);
		return ConnectedPointer<T>();
	}
	int index=0;
	bool d=false;
	tableMutex.lock();
	for(int i=0; i<table_size; i++){
		if(pointerTable[i].allocated==false){
			index=i;
			d=true;
			pointerTable[i].allocated=true;
		#if defined(CRITICAL_POSIX)
			#if defined(__APPLE__) && defined(__MACH__)
			pointerTable[i].memoryPointer=mmap(0, size, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, -1, 0);
			#else
			pointerTable[i].memoryPointer=mmap(0, size, PROT_READ|PROT_WRITE, MAP_ANONYMOUS|MAP_PRIVATE, -1, 0);
			#endif
			if(pointerTable[i].memoryPointer==MAP_FAILED){
				int error=errno;
				d=false;
				throw MemoryException(error);
			}
		#elif defined(CRITICAL_WINAPI)
			pointerTable[i].memoryPointer=VirtualAlloc(0, size, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);
			if(pointerTable[i].memoryPointer==NULL){
				d=false;
				throw MemoryException(-1);
			}
		#endif
			pointerTable[i].size=size;
			pointerTable[i].references=0;
			pointerTable[i].alignment=0;
			tableEntryCount+=1;
			if(tableEntryCount>0 && i==used_table_size){		//because blockindex starts from zero!
				used_table_size++;
			}
			break;
		}
	}
	tableMutex.unlock();
	ConnectedPointer<T> pointer(index);
	
	if(!d){
		// redundant -> remove in future
		pointer.ptr=nullptr;
		pointer.eindex=0;
		pointer.allocated=false;
		pointer.location=CRMemoryLocationUndefined;
	}
	return pointer;
}
	
template<class T>
MemoryManager::ConnectedPointer<T> MemoryManager::alignAlloc(size_t s, size_t align){
	ConnectedPointer<T> p=salloc<T>(s+align);
	if(align!=0){
		p.ptr=CRITICAL_ALIGN_PTR(p.ptr, align);
	}
	if(p.location==CRMemoryLocationPageTable){
		pointerTable[p.eindex].alignment=align;
	}else if(p.location==CRMemoryLocationHeap){
		p.block->alignment=align;
	}
	return p;
}
		
template <class T>
bool MemoryManager::srealloc(ConnectedPointer<T> &p, size_t s, size_t align){
	if(p.location==CRMemoryLocationHeap){
		heapMutex.lock();
		_heap_meta_block *b=allocHeapBlock(s+align);
		//_heap_meta_block *old=reinterpret_cast<_heap_meta_block*>(static_cast<char*>(p.getVoidPtr())-heap_meta_block_size);
		_heap_meta_block* old=p.block;
		b->references=old->references;
		if(!b){
			return false;
		}
		size_t cpysize;
		if(b->size<old->size){
			cpysize=s;
		}else{
			cpysize=old->size;
		}
		memcpy(&b->data, &old->data, cpysize);
		heapMutex.unlock();
		freeHeapBlock(p.block);
		p.block=b;
		if(align){
			p.ptr=CRITICAL_ALIGN_PTR(&b->data, align);
		}else{
			p.ptr=&b->data;
		}
	}else if(p.location==CRMemoryLocationPageTable){
		if(p.allocated && pointerTable[p.eindex].allocated && s>pointerTable[p.eindex].size){
			tableMutex.lock();
			size_t size=CRITICAL_CEIL(s, pageSize);
			int ret;
			void* tmppointer;
		#if defined(CRITICAL_POSIX)
			#if defined(__APPLE__) && defined(__MACH__)
			tmppointer=mmap(0, size, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, -1, 0);
			#else
			tmppointer=mmap(0, size, PROT_READ|PROT_WRITE, MAP_ANONYMOUS|MAP_PRIVATE, -1, 0);
			#endif
			if(tmppointer==MAP_FAILED){
				int error=errno;
				tableMutex.unlock();
				throw MemoryException(error);
			}
		#elif defined(CRITICAL_WINAPI)
			tmppointer=VirtualAlloc(0, size, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);
			if(tmppointer==NULL){
				tableMutex.unlock();
				throw MemoryException(-1);
			}
		#endif
			size_t cpysize;
			if(size>=pointerTable[p.eindex].size){
				cpysize=pointerTable[p.eindex].size;
			}else{
				cpysize=size;
			}
			memcpy(tmppointer, pointerTable[p.eindex].memoryPointer, cpysize);
		#if defined(CRITICAL_POSIX)
			ret=munmap(pointerTable[p.eindex].memoryPointer, pointerTable[p.eindex].size);
			if(ret==-1){
				int error=errno;
				tableMutex.unlock();
				throw MemoryException(error);
			}
		#elif defined(CRITICAL_WINAPI)
			ret=VirtualFree(pointerTable[p.eindex].memoryPointer, 0, MEM_RELEASE);
			if(ret==NULL){
				tableMutex.unlock();
				throw MemoryException(-1);
			}
		#endif
			p.ptr=tmppointer;
			pointerTable[p.eindex].memoryPointer=tmppointer;
			pointerTable[p.eindex].size=size;
			tableMutex.unlock();
			return true;
		}
	}
	return false;
}

template <class T>
bool MemoryManager::sfree(ConnectedPointer<T> &p){
	if(p.allocated){
		if(p.location==CRMemoryLocationHeap){
			return freeHeapBlock(p.block);
		}else if(p.location==CRMemoryLocationPageTable){
			return freeTableEntry(p.eindex);
		}
		p.ptr=nullptr;
		p.eindex=0;
		p.allocated=false;
		p.location=CRMemoryLocationUndefined;
	}
	return false;
}
	
//---------------------------------------------
// ConnectedPointer
//---------------------------------------------

template <class T>
void MemoryManager::ConnectedPointer<T>::sync(){
	if(this->location==CRMemoryLocationPageTable){
		this->ptr=MemoryManager::pointerTable[eindex].memoryPointer;
	}else if(this->location==CRMemoryLocationHeap){
		this->ptr=&this->block->data;
	}
}
inline void MemoryManager::ConnectedPointer<void>::sync(){
	if(this->location==CRMemoryLocationPageTable){
		this->ptr=MemoryManager::pointerTable[eindex].memoryPointer;
	}else if(this->location==CRMemoryLocationHeap){
		this->ptr=&this->block->data;
	}
}
	
template<class T>
void MemoryManager::ConnectedPointer<T>::reset(){
	if(this->allocated){
		sfree(*this);
		eindex=0;
		block=nullptr;
		allocated=false;
	}
}
inline void MemoryManager::ConnectedPointer<void>::reset(){
	if(this->allocated){
		sfree(*this);
		eindex=0;
		block=nullptr;
		allocated=false;
	}
}
	
template<class T>
void MemoryManager::ConnectedPointer<T>::reset(uint32_t i){
	if(MemoryManager::pointerTable[i].allocated){
		this->allocated=true;
		this->eindex=i;
		this->location=CRMemoryLocationPageTable;
		this->ptr=MemoryManager::pointerTable[i].memoryPointer;
		this->block=nullptr;
	}else{
		this->reset();
	}
}
inline void MemoryManager::ConnectedPointer<void>::reset(uint32_t i){
	if(MemoryManager::pointerTable[i].allocated){
		this->allocated=true;
		this->eindex=i;
		this->location=CRMemoryLocationPageTable;
		this->ptr=MemoryManager::pointerTable[i].memoryPointer;
		this->block=nullptr;
	}else{
		this->reset();
	}
}
	
template<class T>
void MemoryManager::ConnectedPointer<T>::reset(_heap_meta_block* b){
	if(b->allocated){
		this->allocated=true;
		this->eindex=0;
		this->location=CRMemoryLocationHeap;
		this->ptr=&(b->data);
		this->block=b;
	}else{
		reset();
	}
}
inline void MemoryManager::ConnectedPointer<void>::reset(_heap_meta_block* b){
	if(b->allocated){
		this->allocated=true;
		this->eindex=0;
		this->location=CRMemoryLocationHeap;
		this->ptr=&(b->data);
		this->block=b;
	}else{
		reset();
	}
}
	
template<class T>
void MemoryManager::ConnectedPointer<T>::reset(MemoryManager::ConnectedPointer<T> &p){
	if(p.allocated){
		_heap_meta_block *b=reinterpret_cast<_heap_meta_block*>(static_cast<char*>(p.ptr)-heap_meta_block_size);
		if((p.location==CRMemoryLocationPageTable && MemoryManager::pointerTable[p.eindex].allocated) || (p.location==CRMemoryLocationHeap && b->allocated)){
			this->allocated=true;
			this->eindex=p.eindex;
			this->ptr=p.ptr;
			this->location=p.location;
			this->block=p.block;
		}else{
			reset();
		}
	}
}
inline void MemoryManager::ConnectedPointer<void>::reset(MemoryManager::ConnectedPointer<void> &p){
	if(p.allocated){
		_heap_meta_block *b=reinterpret_cast<_heap_meta_block*>(static_cast<char*>(p.ptr)-heap_meta_block_size);
		if((p.location==CRMemoryLocationPageTable && MemoryManager::pointerTable[p.eindex].allocated) || (p.location==CRMemoryLocationHeap && b->allocated)){
			this->allocated=true;
			this->eindex=p.eindex;
			this->ptr=p.ptr;
			this->location=p.location;
			this->block=p.block;
		}else{
			reset();
		}
	}
}

template <class T>
size_t MemoryManager::ConnectedPointer<T>::size(){
	if(this->location==CRMemoryLocationHeap){
		return this->block->size;
	}else if(this->location==CRMemoryLocationPageTable){
		return MemoryManager::pointerTable[this->eindex].size;
	}
	return 0;
}
	
inline size_t MemoryManager::ConnectedPointer<void>::size(){
	if(this->location==CRMemoryLocationHeap){
		return this->block->size;
	}else if(this->location==CRMemoryLocationPageTable){
		return MemoryManager::pointerTable[this->eindex].size;
	}
	return 0;
}
	
template <class T>
bool MemoryManager::ConnectedPointer<T>::isAllocated(){
	return MemoryManager::pointerTable[this->eindex].allocated;
}
	
inline bool MemoryManager::ConnectedPointer<void>::isAllocated(){
	return MemoryManager::pointerTable[this->eindex].allocated;
}
	
	
template <class T>
MemoryManager::ConnectedPointer<T> MemoryManager::ConnectedPointer<T>::operator+(ptrdiff_t offset){
	ConnectedPointer<T> tmp=(*this);
	tmp.shift(offset);
	return tmp;
}
inline MemoryManager::ConnectedPointer<void> MemoryManager::ConnectedPointer<void>::operator+(ptrdiff_t offset){
	ConnectedPointer<void> tmp(*this);
	tmp.shift(offset);
	return tmp;
}
	
template <class T>
MemoryManager::ConnectedPointer<T> MemoryManager::ConnectedPointer<T>::operator-(ptrdiff_t offset){
	ConnectedPointer<T> tmp(*this);
	tmp.shift(-offset);
	return tmp;
}
inline MemoryManager::ConnectedPointer<void> MemoryManager::ConnectedPointer<void>::operator-(ptrdiff_t offset){
	ConnectedPointer<void> tmp(*this);
	tmp.shift(-offset);
	return tmp;
}
	
template <class T>
MemoryManager::ConnectedPointer<T>& MemoryManager::ConnectedPointer<T>::operator+=(ptrdiff_t offset){
	this->shift(offset);
	return *this;
}
inline MemoryManager::ConnectedPointer<void>& MemoryManager::ConnectedPointer<void>::operator+=(ptrdiff_t offset){
	this->shift(offset);
	return *this;
}
	
template <class T>
MemoryManager::ConnectedPointer<T>& MemoryManager::ConnectedPointer<T>::operator-=(ptrdiff_t offset){
	this->shift(-offset);
	return *this;
}
inline MemoryManager::ConnectedPointer<void>& MemoryManager::ConnectedPointer<void>::operator-=(ptrdiff_t offset){
	this->shift(offset);
	return *this;
}
	
template <class T>
MemoryManager::ConnectedPointer<T>& MemoryManager::ConnectedPointer<T>::operator++(){
	this->shift(1);
	return *this;
}
inline MemoryManager::ConnectedPointer<void>& MemoryManager::ConnectedPointer<void>::operator++(){
	this->shift(1);
	return *this;
}
	
template <class T>
MemoryManager::ConnectedPointer<T>& MemoryManager::ConnectedPointer<T>::operator--(){
	this->shift(-1);
	return *this;
}
inline MemoryManager::ConnectedPointer<void>& MemoryManager::ConnectedPointer<void>::operator--(){
	this->shift(-1);
	return *this;
}
	
template <class T>
MemoryManager::ConnectedPointer<T> MemoryManager::ConnectedPointer<T>::operator++(int){
	ConnectedPointer<T> tmp(*this);
	++(*this);
	return tmp;
}
inline MemoryManager::ConnectedPointer<void> MemoryManager::ConnectedPointer<void>::operator++(int){
	ConnectedPointer<void> tmp(*this);
	++(*this);
	return tmp;
}

template <class T>
MemoryManager::ConnectedPointer<T> MemoryManager::ConnectedPointer<T>::operator--(int){
	ConnectedPointer<T> tmp(*this);
	--(*this);
	return tmp;
}
inline MemoryManager::ConnectedPointer<void> MemoryManager::ConnectedPointer<void>::operator--(int){
	ConnectedPointer<void> tmp(*this);
	--(*this);
	return tmp;
}

template <class T>
MemoryManager::ConnectedPointer<T>& MemoryManager::ConnectedPointer<T>::operator=(MemoryManager::ConnectedPointer<T> &p){
	this->ptr=p.ptr;
	this->allocated=p.allocated;
	this->eindex=p.eindex;
	this->location=p.location;
	this->block=p.block;
	return *this;
}
inline MemoryManager::ConnectedPointer<void>& MemoryManager::ConnectedPointer<void>::operator=(MemoryManager::ConnectedPointer<void> &p){
	this->ptr=p.ptr;
	this->allocated=p.allocated;
	this->eindex=p.eindex;
	this->location=p.location;
	this->block=p.block;
	return *this;
}
	
template <class T>
MemoryManager::ConnectedPointer<T>& MemoryManager::ConnectedPointer<T>::operator=(MemoryManager::ConnectedPointer<T> &&p){
	this->ptr=p.ptr;
	this->allocated=p.allocated;
	this->eindex=p.eindex;
	this->location=p.location;
	this->block=p.block;
	return *this;
}
inline MemoryManager::ConnectedPointer<void>& MemoryManager::ConnectedPointer<void>::operator=(MemoryManager::ConnectedPointer<void> &&p){
	this->ptr=p.ptr;
	this->allocated=p.allocated;
	this->eindex=p.eindex;
	this->location=p.location;
	this->block=p.block;
	return *this;
}
	
}

#endif /* defined(CRITICAL_CORE_MEMORYMANAGER_H_) */