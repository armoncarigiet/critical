//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Array.h
 *	@date 20.02.2015.
 *  @author Armon Carigiet
 *
 *  @ingroup core
 *  @brief Provides a simple array container.
 */
//===========================================================================

#ifndef CRITICAL_CORE_ARRAY_H_
#define CRITICAL_CORE_ARRAY_H_

#include <stdio.h>
#include "MemoryManager.h"
#include "SharedPointer.h"

namespace critical{

//! Simple array with some additional features.
//! @ingroup core
template <class T>
class Array{
	template <class U> friend class Vector;
private:
	//! Ignore.
	static void __throw_out_of_range(){
		#if defined(CRITICAL_DEBUG)
		printf("\nArray out of Range!\n");
		#endif
		throw MemoryException(CRMemoryExceptionContainerOutOfRange);
	}
	
	size_t _s;
	SharedPointer<T> _p;
	bool __alloc;
public:
	Array(): _s(0), __alloc(false){}
	Array(size_t size): _s(size), _p(SharedPointer<T>(MemoryManager::salloc<T>(size*sizeof(T)))), __alloc(true){}
	
	~Array(){
		if(__alloc){
			__alloc=false;
			_p.reset();
		}
	}
	
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	Array(Array &p) : _s(p._s), _p(p._p), __alloc(p.__alloc) {}
    
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	Array(Array &&p) : _s(p._s), _p(p._p),  __alloc(p.__alloc) { p.__alloc=false; }
	
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	Array<T>& operator=(Array<T> &p){
		_p=p._p;
		_s=p._s;
		__alloc=p.__alloc;
		return *this;
	}
    
	//! @note The new Object is just a reference to the data used by the old one. Both Objects are using the same memory.
	Array<T>& operator=(Array<T> &&p){
		_p=p._p;
		//_p.printref();
		_s=p._s;
		__alloc=p.__alloc;
		p.__alloc=false;
		return *this;
	}

	//! @return Size of array.
	size_t size(){
		return _s;
	}
	
	//! Destroys array and frees used memory.
	void destroy(){
		this->~Array();
	}
	
	//! @return Pointer to memory used by the array.
	T* data(){
		return &_p;
	}
	
	//! Random access operator.
	//! @return Reference to acessed object.
	T& operator[](const uint32_t &index){
		if(index>(_s-1)){
			__throw_out_of_range();
		}
		return _p[index];
	}
};

}

#endif /* defined(CRITICAL_CORE_ARRAY_H_) */