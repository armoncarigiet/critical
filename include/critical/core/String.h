//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file String.h
 *	@date 15.01.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a String class.
 *  @ingroup core
 */
//===========================================================================

#ifndef CRITICAL_CORE_STRING_H_
#define CRITICAL_CORE_STRING_H_

#include <cassert>
#include <cstring>
#include "MemoryManager.h"

namespace critical{

//! @ingroup core
class String{
	enum StringEncoding{
		CRASCIIEncoding = 0,
		CRWideCharacterUnicodeEncoding,
		CRUTF8Encoding,
		CRUTF16Encoding,
		CRUTF32Encoding
	};
private:
	MemoryManager::ConnectedPointer<void> _p;
	StringEncoding _e;

public:
	
	String(){}
	
	String(const void* s, StringEncoding e=CRASCIIEncoding){
		_e=e;
		if(e==CRASCIIEncoding){
			size_t size=strlen((char*)s)+1;
			_p=MemoryManager::salloc<void>(size);
			memcpy(&_p, s, size);
		}else if(e==CRWideCharacterUnicodeEncoding){
			//printf("\nWchar Unicode hasn't been implemented yet!\n");
			assert(0);
		}else if(e==CRUTF8Encoding){
			//printf("\nUTF8 hasn't been implemented yet!\n");
			assert(0);
		}else if(e==CRUTF16Encoding){
			//printf("\nUTF16 hasn't been implemented yet!\n");
			assert(0);
		}else if(e==CRWideCharacterUnicodeEncoding){
			//printf("\nUTF32 hasn't been implemented yet!\n");
			assert(0);
		}
	}
	
	bool isAllocated(){
		return _p.isAllocated();
	}
	
	void print(){
		printf("%s", (char*)&_p);
	}
	
	bool isEqual(String &s){
		const char *s1=static_cast<char*>(&_p);
		const char *s2=static_cast<char*>(&s._p);
		if(strcmp(s1, s2)==0){
			return true;
		}
		return false;
	}
	
	char* c_str(){ return (char*)&_p; }
	//String(StringEncoding e=CRASCIIEncoding);
};

}
#endif /* defined(CRITICAL_CORE_STRING_H_) */