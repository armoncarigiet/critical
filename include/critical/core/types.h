//
//  types.h
//  
//
//  Created by Armon Carigiet on 02.07.14.
//
//

#ifndef CRITICAL_CORE_TYPES_H_
#define CRITICAL_CORE_TYPES_H_

#if defined(CRITICAL_SINGLE_PRECISION)
	#define GLMATH_SINGLE_PRECISION 1
#elif defined(CRITICAL_DOUBLE_PRECISION)
	#define GLMATH_DOUBLE_PRECISION 1
#elif defined(CRITICAL_HIGH_PRECISION)
	#define GLMATH_QUADRUPLE_PRECISION 1
#else
	#define GLMATH_SINGLE_PRECISION 1
#endif

#include <glmath/glmath_types.h>

//namespace critical {
	typedef char int8;
	typedef short int16;
	typedef int int32;
	typedef long int int64;
	
	typedef unsigned char uint8;
	typedef unsigned short uint16;
	typedef unsigned int uint32;
	typedef unsigned long int uint64;
	
	typedef unsigned char uchar;
	typedef unsigned short ushort;
	typedef unsigned int uint;
	typedef unsigned long int ulong;
	
//math
//#if defined(CRITICAL_SINGLE_PRECISION)
//	typedef float real;
//	static inline real real_sqrt(real x){ return sqrtf(x); }
//	static inline real real_sin(real x){ return sinf(x); }
//	static inline real real_cos(real x){ return cosf(x); }
//	static inline real real_tan(real x){ return tanf(x); }
//	static inline real real_asin(real x){ return asinf(x); }
//	static inline real real_acos(real x){ return acosf(x); }
//	static inline real real_atan(real x){ return atanf(x); }
//#elif defined(CRITICAL_DOUBLE_PRECISION)
//	typedef double real;
//	static inline real real_sqrt(real x){ return sqrt(x); }
//	static inline real real_sin(real x){ return sin(x); }
//	static inline real real_cos(real x){ return cos(x); }
//	static inline real real_tan(real x){ return tan(x); }
//	static inline real real_asin(real x){ return asin(x); }
//	static inline real real_acos(real x){ return acos(x); }
//	static inline real real_atan(real x){ return atan(x); }
//#elif defined(CRITICAL_HIGH_PRECISION)
//	typedef long double real;
//	static inline real real_sqrt(real x){ return sqrtl(x); }
//	static inline real real_sin(real x){ return sinl(x); }
//	static inline real real_cos(real x){ return cosl(x); }
//	static inline real real_tan(real x){ return tanl(x); }
//	static inline real real_asin(real x){ return asinl(x); }
//	static inline real real_acos(real x){ return acosl(x); }
//	static inline real real_atan(real x){ return atanl(x); }
//#else
//	// default is single precision
//	typedef float real;
//	static inline real real_sqrt(real x){ return sqrtf(x); }
//	static inline real real_sin(real x){ return sinf(x); }
//	static inline real real_cos(real x){ return cosf(x); }
//	static inline real real_tan(real x){ return tanf(x); }
//	static inline real real_asin(real x){ return asinf(x); }
//	static inline real real_acos(real x){ return acosf(x); }
//	static inline real real_atan(real x){ return atanf(x); }
//#endif
	

//}


#endif
