//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Mutex.h
 *	@date 15.11.2014.
 *  @author Armon Carigiet
 *
 *  @brief Provides different mutex classes for mutal exclusion procedures.
 *  @ingroup core
 *  @see Mutex.cpp
 */
//===========================================================================

#ifndef CRITICAL_CORE_MUTEX_H_
#define CRITICAL_CORE_MUTEX_H_

#if defined(CRITICAL_POSIX)
#include <pthread.h>
#elif defined(CRITICAL_WINAPI)
#include <windows.h>
//#include <Processthreadsapi.h>
#endif

namespace critical{

//! Simple mutex class.
//! @ingroup core
class Mutex{
private:
#if defined(CRITICAL_POSIX)
	pthread_mutex_t _m;
#elif defined(CRITICAL_WINAPI)
	CRITICAL_SECTION _m;
#endif
public:
	Mutex();
	~Mutex();
    
    //! Tryes to look the mutex until the procedure was sucessfull.
	void lock();
    
    //! Tryes to lock the mutex and returns if it was sucesssful or unsucessful.
    //! @return [true, false] True if the mutex was locked sucessfully, flase if the procedure failed.
	bool trylock();
    
    //! Unlocks the mutex.
	void unlock();
};
	
//! Mutex class that is not working at the moment.
class FastMutex{
private:
	int _m;
public:
	FastMutex();
	~FastMutex();
	bool trylock();
	void unlock();
};



}



#endif /* defined(CRITICAL_CORE_MUTEX_H_) */
