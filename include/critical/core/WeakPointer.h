//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file WeakPointer.h
 *	@date 07.01.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a pointer as counterpart to the SharedPointer class.
 *  @ingroup core
 */
//===========================================================================

#ifndef CRITICAL_CORE_WEAKPOINTER_H_
#define CRITICAL_CORE_WEAKPOINTER_H_

#include "MemoryManager.h"


namespace critical{

//======================================================================================
// declaration
//======================================================================================
//! @ingroup core
template <class T> class WeakPointer;

//======================================================================================
// definition
//======================================================================================

template <typename T>
class WeakPointer : public MemoryManager::ConnectedPointer<T>{
protected:
	int *refcounter;
public:
	WeakPointer() : MemoryManager::ConnectedPointer<T>(), refcounter(nullptr) {}
	WeakPointer(MemoryManager::ConnectedPointer<T> &p) : MemoryManager::ConnectedPointer<T>(p), refcounter(&MemoryManager::pointerTable[p.eindex].references) {}
	WeakPointer(MemoryManager::ConnectedPointer<T> &&p) : MemoryManager::ConnectedPointer<T>(p), refcounter(&MemoryManager::pointerTable[p.eindex].references) {}
	
	~WeakPointer(){}
	
	void sync();
	
	void reset();
	void reset(WeakPointer<T> ptr);
	
	WeakPointer<T> operator+(ptrdiff_t offset);
	WeakPointer<T> operator-(ptrdiff_t offset);
	WeakPointer<T>& operator+=(ptrdiff_t offset);
	WeakPointer<T>& operator-=(ptrdiff_t offset);
	
	WeakPointer<T>& operator++();
	WeakPointer<T>& operator--();
	WeakPointer<T> operator++(int);
	WeakPointer<T> operator--(int);
	
	// Custom move and copy constructors to fix move assignment
	WeakPointer(WeakPointer<T> &p) : MemoryManager::ConnectedPointer<T>(p){}
	WeakPointer(WeakPointer<T> &&p) : MemoryManager::ConnectedPointer<T>(p){}
	
	WeakPointer<T>& operator=(WeakPointer<T> &p);
	WeakPointer<T>& operator=(WeakPointer<T> &&p);
};
	
	
//======================================================================================
// specialisation
//======================================================================================

template<> class WeakPointer<void> : public MemoryManager::ConnectedPointer<void>{
protected:
	int *refcounter;
public:
	WeakPointer() : MemoryManager::ConnectedPointer<void>(), refcounter(nullptr) {}
	WeakPointer(MemoryManager::ConnectedPointer<void> &p) : MemoryManager::ConnectedPointer<void>(p), refcounter(&MemoryManager::pointerTable[p.eindex].references) {}
	WeakPointer(MemoryManager::ConnectedPointer<void> &&p) : MemoryManager::ConnectedPointer<void>(p), refcounter(&MemoryManager::pointerTable[p.eindex].references) {}
		
	~WeakPointer(){}
	
	void sync();
	
	void reset();
	void reset(WeakPointer<void> ptr);
	
	WeakPointer<void> operator+(ptrdiff_t offset);
	WeakPointer<void> operator-(ptrdiff_t offset);
	WeakPointer<void>& operator+=(ptrdiff_t offset);
	WeakPointer<void>& operator-=(ptrdiff_t offset);
	
	WeakPointer<void>& operator++();
	WeakPointer<void>& operator--();
	WeakPointer<void> operator++(int);
	WeakPointer<void> operator--(int);
	
	// Custom move and copy constructors to fix move assignment
	WeakPointer(WeakPointer<void> &p) : MemoryManager::ConnectedPointer<void>(p){}
	WeakPointer(WeakPointer<void> &&p) : MemoryManager::ConnectedPointer<void>(p){}
		
	WeakPointer<void>& operator=(WeakPointer<void> &p);
	WeakPointer<void>& operator=(WeakPointer<void> &&p);
};


//======================================================================================
// implementation
//======================================================================================

	
	
	


template <class T>
WeakPointer<T>& WeakPointer<T>::operator=(WeakPointer<T> &p){
	MemoryManager::ConnectedPointer<T>::operator=(p);
	this->refcounter=p.refcounter;
	return *this;
}
inline WeakPointer<void>& WeakPointer<void>::operator=(WeakPointer<void> &p){
	MemoryManager::ConnectedPointer<void>::operator=(p);
	this->refcounter=p.refcounter;
	return *this;
}

template <class T>
WeakPointer<T>& WeakPointer<T>::operator=(WeakPointer<T> &&p){
	MemoryManager::ConnectedPointer<void>::operator=(p);
	this->refcounter=p.refcounter;
	p.refcounter=nullptr;
	return *this;
}
inline WeakPointer<void>& WeakPointer<void>::operator=(WeakPointer<void> &&p){
	MemoryManager::ConnectedPointer<void>::operator=(p);
	this->refcounter=p.refcounter;
	p.refcounter=nullptr;
	return *this;
}

template <class T>
void WeakPointer<T>::sync(){
	MemoryManager::ConnectedPointer<T>::sync();
	if(this->location==CRMemoryLocationPageTable){
		this->refcounter=&MemoryManager::pointerTable[this->eindex].references;
	}else if(this->location==CRMemoryLocationHeap){
		this->refcounter=&this->block->references;
	}
}
inline void WeakPointer<void>::sync(){
	MemoryManager::ConnectedPointer<void>::sync();
	if(this->location==CRMemoryLocationPageTable){
		this->refcounter=&MemoryManager::pointerTable[this->eindex].references;
	}else if(this->location==CRMemoryLocationHeap){
		this->refcounter=&this->block->references;
	}
}



template <class T>
void WeakPointer<T>::reset(){
	if(this->allocated){
		this->allocated=false;
		this->eindex=0;
		this->block=nullptr;
		this->location=CRMemoryLocationUndefined;
	}
}
inline void WeakPointer<void>::reset(){
	if(this->allocated){
		this->allocated=false;
		this->ptr=nullptr;
		this->eindex=0;
		this->block=nullptr;
		this->location=CRMemoryLocationUndefined;
	}
}

template <class T>
void WeakPointer<T>::reset(WeakPointer<T> ptr){
	if(ptr.allocated){
		this->allocated=true;
		this->ptr=ptr.ptr;
		this->eindex=ptr.eindex;
		this->block=ptr.block;
		this->location=ptr.location;
	}
}
inline void WeakPointer<void>::reset(WeakPointer<void> ptr){
	if(ptr.allocated){
		this->allocated=true;
		this->ptr=ptr.ptr;
		this->eindex=ptr.eindex;
		this->block=ptr.block;
		this->location=ptr.location;
	}
}

template <class T>
WeakPointer<T> WeakPointer<T>::operator+(ptrdiff_t offset){
	WeakPointer<T> tmp(*this);
	tmp.shift(offset);
	return tmp;
}
inline WeakPointer<void> WeakPointer<void>::operator+(ptrdiff_t offset){
	WeakPointer<void> tmp(*this);
	tmp.shift(offset);
	return tmp;
}
	
template <class T>
WeakPointer<T> WeakPointer<T>::operator-(ptrdiff_t offset){
	WeakPointer<T> tmp(*this);
	tmp.shift(-offset);
	return tmp;
}
inline WeakPointer<void> WeakPointer<void>::operator-(ptrdiff_t offset){
	WeakPointer<void> tmp(*this);
	tmp.shift(-offset);
	return tmp;
}
	
template <class T>
WeakPointer<T>& WeakPointer<T>::operator+=(ptrdiff_t offset){
	this->shift(offset);
	return *this;
}
inline WeakPointer<void>& WeakPointer<void>::operator+=(ptrdiff_t offset){
	this->shift(offset);
	return *this;
}
	
template <class T>
WeakPointer<T>& WeakPointer<T>::operator-=(ptrdiff_t offset){
	this->shift(-offset);
	return *this;
}
inline WeakPointer<void>& WeakPointer<void>::operator-=(ptrdiff_t offset){
	this->shift(offset);
	return *this;
}
	
template <class T>
WeakPointer<T>& WeakPointer<T>::operator++(){
	this->shift(1);
	return *this;
}
inline WeakPointer<void>& WeakPointer<void>::operator++(){
	this->shift(1);
	return *this;
}
	
template <class T>
WeakPointer<T>& WeakPointer<T>::operator--(){
	this->shift(-1);
	return *this;
}
inline WeakPointer<void>& WeakPointer<void>::operator--(){
	this->shift(-1);
	return *this;
}
	
template <class T>
WeakPointer<T> WeakPointer<T>::operator++(int){
	WeakPointer<T> tmp(*this);
	++(*this);
	return tmp;
}
inline WeakPointer<void> WeakPointer<void>::operator++(int){
	WeakPointer<void> tmp(*this);
	++(*this);
	return tmp;
}
	
template <class T>
WeakPointer<T> WeakPointer<T>::operator--(int){
	WeakPointer<T> tmp(*this);
	--(*this);
	return tmp;
}
inline WeakPointer<void> WeakPointer<void>::operator--(int){
	WeakPointer<void> tmp(*this);
	--(*this);
	return tmp;
}
	
}
#endif /* defined(CRITICAL_CORE_WEAKPOINTER_H_) */
