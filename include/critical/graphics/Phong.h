//
//  Phong.h
//  critical
//
//  Created by Armon Carigiet on 24.02.15.
//
//

#ifndef CRITICAL_GRAPHICS_PHONG_H
#define CRITICAL_GRAPHICS_PHONG_H

#include <critical/graphics/Color.h>

namespace critical {

class Phong{
public:
	Color emission;
	Color ambient;
	Color diffuse;
	Color specular;
	glmath_real shininess;
	Color reflective;
	glmath_real reflectivity;
	Color transparent;
	glmath_real transparency;
	
	Phong(){
		Color c=Color(0,0,0,1);
		emission=c;
		ambient=c;
		diffuse=c;
		specular=c;
		shininess=0.0f;
		reflective=c;
		reflectivity=0.0f;
		transparent=c;
		transparency=0.0f;
	}
};
	
	
}

#endif /* defined(CRITICAL_GRAPHICS_PHONG_H) */
