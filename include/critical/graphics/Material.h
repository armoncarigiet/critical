//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Material.h
 *	@date 27.02.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a class for storing information of a surface material.
 *  @ingroup graphics
 */
//===========================================================================

#ifndef CRITICAL_GRAPHICS_MATERIAL_H
#define CRITICAL_GRAPHICS_MATERIAL_H

#include <critical/core/Dictionary.h>
#include <critical/core/Array.h>
#include <critical/graphics/Phong.h>
#include <glmath/glmath_types.h>
#include <critical/core/Tuple.h>
#include <critical/core/Map.h>

namespace critical {

//! Identifiers for the different atrributes of a material.
//! @ingroup graphics
enum MaterialAttribute{
	CRMaterialEmission = 0,
	CRMaterialAmbient,
	CRMaterialDiffuse,
	CRMaterialSpecular,
	CRMaterialShininess,
	CRMaterialIndexOfRefraction,
	CRMaterialIndex
};

//! Class representing a material.
//! @note It isn't advisable to modify the attributes manually. If you're not sure how the class works, you shouldn't touch the object's members.
//! @ingroup graphics
class Material {
public:
	
	//static const char* emissionKey;
	//static const char* ambientKey;
	
	enum materialType{
		CRUndefinedMaterial = 0,
		CRPhongMaterial,
		CRLambertMaterial,
	};

	materialType t;
	//Dictionary<Array<glmath_real>> d;
	
	Map<Array<glmath_real>, MaterialAttribute> _attributes;
	
	//Phong getPhong();
};

}
#endif /* defined(CRITICAL_GRAPHICS_MATERIAL_H) */