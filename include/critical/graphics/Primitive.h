//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Primitive.h
 *	@date 20.02.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a class for storing geometrical primitives.
 *  @ingroup graphics
 */
//===========================================================================

#ifndef CRITICAL_GRAPHICS_PRIMITIVE_H
#define CRITICAL_GRAPHICS_PRIMITIVE_H

#include <glmath/glmath_vec3.h>
#include "Graphic.h"
#include <critical/core/Array.h>
#include <critical/graphics/Material.h>

namespace critical {

//! Identifiers for the different primitive types.
//! @ingroup graphics
enum primitiveType{
	CRLines = 0,
	CRLinesStrips,
	CRTriangles,
	CRTriangleStrips,
	CRTriangleFans,
	CRPolygons,
	CRPolylist
};

//! Class representing a geometric primitive.
//! @note It isn't advisable to modify the attributes manually. If you're not sure how the class works, you shouldn't touch the object's members.
//! @ingroup graphics
class Primitive{
public:
	
	bool _hasPositions;
	bool _hasNormals;
	bool _hasTangents;
	bool _hasBitangents;
	bool _hasTextureCoords;
	
	InputSource *i;
	primitiveType type;
	int count;
	short vertexCount;

	Array<int> vindex;
	
	// only needed for Polygons and Polylists, Linestrips etc.
	Array<int> vcount;
	
	Material *material;
	uint32_t material_index;
	
	Primitive(): i(nullptr), material(NULL){}
	
	Primitive(InputSource *s, primitiveType t, size_t indexes): i(s), type(t), vindex(Array<int>(indexes)){}
	Primitive(InputSource *s, primitiveType t, size_t indexes, size_t polygoncount): i(s), type(t), vindex(Array<int>(indexes)), vcount(Array<int>(polygoncount)){}
	
	// copy/move constructor/operator
	Primitive(Primitive &p) : _hasPositions(p._hasPositions),_hasNormals(p._hasNormals), _hasTangents(p._hasTangents), _hasBitangents(p._hasBitangents), _hasTextureCoords(p._hasTextureCoords), i(p.i), type(p.type), count(p.count), vindex(p.vindex), vcount(p.vcount){}
	
    Primitive(Primitive &&p) : _hasPositions(p._hasPositions),_hasNormals(p._hasNormals), _hasTangents(p._hasTangents), _hasBitangents(p._hasBitangents), _hasTextureCoords(p._hasTextureCoords), i(p.i), type(p.type), count(p.count), vindex(p.vindex), vcount(p.vcount){}
	
	Primitive& operator=(Primitive &p){
		_hasPositions=p._hasPositions;
		_hasNormals=p._hasNormals;
		_hasTangents=p._hasTangents;
		_hasBitangents=p._hasBitangents;
		_hasTextureCoords=p._hasTextureCoords;
		i=p.i;
		type=p.type;
		count=p.count;
		vindex=p.vindex;
		vcount=p.vcount;
		return *this;
	}
    
	Primitive& operator=(Primitive &&p){
		_hasPositions=p._hasPositions;
		_hasNormals=p._hasNormals;
		_hasTangents=p._hasTangents;
		_hasBitangents=p._hasBitangents;
		_hasTextureCoords=p._hasTextureCoords;
		i=p.i;
		type=p.type;
		count=p.count;
		vindex=p.vindex;
		vcount=p.vcount;
		return *this;
	}

};
	
}

#endif /* defined(CRITICAL_GRAPHICS_PRIMITIVE_H) */