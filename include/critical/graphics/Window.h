//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Window.h
 *	@date 14.05.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a Windowing class handling a OpenGL context.
 *  @ingroup graphics
 */
//===========================================================================

#ifndef CRITICAL_GRAPHICS_WINDOW_H
#define CRITICAL_GRAPHICS_WINDOW_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <critical/core/RenderInformation.h>
#include <critical/core/MemoryManager.h>

#define CRITICAL_OPENGL_MAJOR_VERSION 3
#define CRITICAL_OPENGL_MINOR_VERSION 3

#define CRITICAL_DEF_WINDOW_WIDTH 640
#define CRITICAL_DEF_WINDOW_HEIGHT 480

namespace critical {

//! Simple Window class wrapping around the glfw window object.
//! @ingroup graphics
template <RenderType type=CROpenGL>
class Window{
private:
	static bool _initialized;
	//GLFWwindow *_w;
	RenderInformation info;
	
public:
	GLFWwindow *_w;
	
	static void init(){
		if(!_initialized) {
			if(!glfwInit()){
				throw 1;
			}
			_initialized=true;
		}
	}
	
	void terminate(){
		if(_initialized){
			glfwTerminate();
			_initialized=false;
		}
	}
	
	Window(){}
	
	Window(RenderInformation &information){
		info=information;
	}
	
	void openWindow(String &title, uint32_t width=CRITICAL_DEF_WINDOW_WIDTH, uint32_t height=CRITICAL_DEF_WINDOW_HEIGHT){
		if (type==CROpenGL) {
		#if defined(CRITICAL_MACOSX)
			glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, CRITICAL_OPENGL_MAJOR_VERSION);
			glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, CRITICAL_OPENGL_MAJOR_VERSION);
			glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
			glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		#endif
			_w=glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
			if(!_w){
				throw 1;
				return;
			}
			glfwMakeContextCurrent(_w);
			
			glewExperimental=true;
			glewInit();
		}else{
			throw 1;
		}
	}
	
	void swapBuffers(){
		glfwSwapBuffers(_w);
	}
	
	void pollEvents(){
		glfwPollEvents();
	}

};
template<RenderType _t>
bool Window<_t>::_initialized=false;
	
}

#endif /* defined(CRITICAL_GRAPHICS_WINDOW_H) */