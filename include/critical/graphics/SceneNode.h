//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file SceneNode.h
 *	@date 23.01.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a data structure to organise a virtual scene.
 *  @ingroup graphics
 */
//===========================================================================

#ifndef CRITICAL_GRAPHICS_SCENENODE_H
#define CRITICAL_GRAPHICS_SCENENODE_H

#include <glmath/glmath_mat4.h>
#include <critical/core/Vector.h>
#include <critical/core/List.h>

namespace critical {
	
//! Class helping to organise a virtual scene.
//! @ingroup graphics
class SceneNode{
public:
	SceneNode *_parent;
	SceneNode *_next;
	SceneNode *_last;
	List<SceneNode> _children;
	Vector<uint32_t> mindex;
	
	glmath_mat4 *_tmat;
	
	//glmath_mat4 _tmat;
	
	
	SceneNode(): _parent(nullptr), _next(nullptr), _last(nullptr), _children(List<SceneNode>()), mindex(Vector<uint32_t>()), _tmat(nullptr){
	}
	
	~SceneNode() {}
	
	SceneNode(SceneNode &n) : _parent(n._parent), _next(n._next), _last(n._last), _children(n._children), mindex(n.mindex), _tmat(n._tmat) { }
	SceneNode(SceneNode &&n) : _parent(n._parent), _next(n._next), _last(n._last), _children(n._children), mindex(n.mindex), _tmat(n._tmat) {  }
	
	SceneNode& operator=(SceneNode &n){
		_parent=n._parent;
		_next=n._next;
		_last=n._last;
		_children=n._children;
		mindex=n.mindex;
		_tmat=n._tmat;
		return *this;
	}
	SceneNode& operator=(SceneNode &&n){
		_parent=n._parent;
		_next=n._next;
		_last=n._last;
		_children=n._children;
		mindex=n.mindex;
		_tmat=n._tmat;
		return *this;
	}
	
	glmath_mat4 getMatrix(){
		return *_tmat;
	}
};
	
}

#endif /* defined(_CRITICAL_GRAPHICS_SCENEOBJECT_H) */