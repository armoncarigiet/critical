//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Mesh.h
 *	@date 15.01.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a class for storing geometrical meshes.
 *  @ingroup graphics
 */
//===========================================================================

#ifndef CRITICAL_GRAPHICS_MESH_H
#define CRITICAL_GRAPHICS_MESH_H

#include <critical/graphics/Graphic.h>
#include <glmath/glmath_vec4.h>
#include <critical/core/MemoryManager.h>
#include <critical/core/Array.h>
#include <critical/graphics/Primitive.h>

namespace critical {

//! Class representing a mesh.
//! @note It isn't advisable to modify the attributes manually. If you're not sure how the class works, you shouldn't touch the object's members.
//! @ingroup graphics
class Mesh{
private:
	//
	bool noIndices;
	bool triangulated;
public:
	Array<glmath_vec3> _vertices;
	Array<glmath_vec3> _normals;
	Array<glmath_vec3> _tangents;
	Array<glmath_vec3> _bitangents;
	Array<glmath_vec3> _texturecoords;
	
	InputSource s;
	
	Array<Primitive> _primitives;
	
	//uint32_t _material_index;
	
	Mesh(): s({NULL, NULL, NULL, NULL, NULL}){}
	
	void allocVectices(size_t count){ _vertices=Array<glmath_vec3>(count); s.vertices=_vertices.data(); }
	void allocNormals(size_t count){ _normals=Array<glmath_vec3>(count); s.normals=_normals.data(); }
	void allocTangents(size_t count){ _tangents=Array<glmath_vec3>(count); s.tangents=_tangents.data(); }
	void allocBitangents(size_t count){ _bitangents=Array<glmath_vec3>(count); s.bitangents=_bitangents.data(); }
	void allocTextureCoords(size_t count){ _texturecoords=Array<glmath_vec3>(count); s.texturecoords=_texturecoords.data(); }
	
	void allocPrimitives(size_t count){ _primitives=Array<Primitive>(count); }
	
     //! @note Not working. Do not use!
	bool resolveIndices();
    
    //! @note Not working. Do not use!
	bool triangulate();
	
};
	
}

#endif /* defined(#define _CRITICAL_GRAPHICS_MESH_H) */