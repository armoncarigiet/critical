//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Camera.h
 *	@date 21.01.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a class for storing information of virtual cameras.
 *  @ingroup graphics
 */
//===========================================================================

#ifndef CRITICAL_GRAPHICS_CAMERA_H
#define CRITICAL_GRAPHICS_CAMERA_H

#include <glmath/glmath_mat4.h>

namespace critical {
    
enum CameraAttribute{
	CREyeDirection = 0,
};

//! Class representing a virtual camera.
//! @ingroup graphics
class Camera{
public:
	
	enum cameraType{
		CRPerspective = 0,
		CROrthographic
	};
	
	cameraType _type;
	
	glmath_real _aspect;
	glmath_real _zfar;
	glmath_real _znear;
	bool _fovType;
	glmath_real _fov;
	
	glmath_vec3 _forward;
	glmath_vec3 _position;
	glmath_vec3 _up;

    //! Creates a camera-perspective matrix from the camera's attributes.
    //! @param Location to store the created matrix.
	void getMatrix(glmath_mat4 &m){
		m=glmath_mat4_mul(glmath_mat4_create_view(_position, _forward, _up), glmath_mat4_create_perspective(_fov, _aspect, _znear, _zfar));
	}
	
    //! Sets the Aspect attribute.
    //! @param a Aspect to set.
	void setAspect(glmath_real a){
		_aspect=a;
	}
	
    //! Sets the clipping planes of the camera.
    //! @param far Far clipping plane.
    //! @param near Near clipping plane.
	void setPlanes(glmath_real far, glmath_real near){
		_zfar=far;
		_znear=near;
	}
	
    //! Sets the far clipping planes of the camera.
    //! @param far Clipping plane.
	void setFarPlane(glmath_real far){
		_zfar=far;
	}
	
    //! Sets the near clipping planes of the camera.
    //! @param near Clipping plane.
	void setNearPlane(glmath_real near){
		_znear=near;
	}
	
    //! Sets the horizontal field of view
    //! @param fov [0, \pi] Field of view in radiants.
	void setHorizontalFOV(glmath_real fov){
		_fov=fov;
	}
	
    //! Rotates the camera around the up-axis.
    //! @param rad [0, 2\pi] Rotation angle.
	void yaw(glmath_real rad){
		glmath_mat3 r=glmath_mat3_create_rotation(rad, _up);
		_forward=glmath_mat3_mul_vec3(r, _forward);
	}
	
    //! Rotates the camera around the right-axis.
    //! @param rad [0, 2\pi] Rotation angle.
	void pitch(glmath_real rad){
		glmath_vec3 right=glmath_vec3_cross(_forward, _up);
		glmath_mat3 r=glmath_mat3_create_rotation(rad, right);
		_forward=glmath_mat3_mul_vec3(r, _forward);
		_up=glmath_vec3_cross(right, _forward);
	}
	
    //! Rotates the camera around the forward-axis.
    //! @param rad [0, 2\pi] Rotation angle.
	void roll(glmath_real rad){
		glmath_mat3 r=glmath_mat3_create_rotation(rad, _forward);
		_up=glmath_mat3_mul_vec3(r, _up);
	}
	
    //! Sets the camera to look at a specific location.
    //! @param eye New camera location.
    //! @param center Location to look at.
    //! @param up New up-axis.
	void lookAt(glmath_vec3 eye, glmath_vec3 center, glmath_vec3 up){
		_position=eye;
		_forward=glmath_vec3_norm(glmath_vec3_sub(center, eye));
		glmath_vec3 right=glmath_vec3_norm(glmath_vec3_cross(_forward, up));
		_up=glmath_vec3_cross(right, _forward);
	}
};

}

#endif /* defined(CRITICAL_GRAPHICS_CAMERA_H) */