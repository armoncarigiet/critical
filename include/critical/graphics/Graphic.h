//
//  Graphic.h
//  critical
//
//  Created by Armon Carigiet on 20.02.15.
//
//

#ifndef CRITICAL_GRAPHICS_GRAPHIC_H
#define CRITICAL_GRAPHICS_GRAPHIC_H

#include <glmath/glmath_vec3.h>

class InputSource{
public:
	glmath_vec3 *vertices;
	glmath_vec3 *normals;
	glmath_vec3 *tangents;
	glmath_vec3 *bitangents;
	glmath_vec3 *texturecoords;
};

#endif
