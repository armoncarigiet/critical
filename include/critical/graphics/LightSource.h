//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file LightSource.h
 *	@date 21.01.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a class for storing information of virtual light sources.
 *  @ingroup graphics
 */
//===========================================================================

#ifndef CRITICAL_GRAPHICS_LIGHTSOURCE_H
#define CRITICAL_GRAPHICS_LIGHTSOURCE_H

#include "Color.h"
#include <critical/core/Dictionary.h>
#include <glmath/glmath_types.h>
#include <critical/core/Tuple.h>
#include <critical/core/Map.h>

namespace critical {

//! Identifiers for the different atrributes of a light source.
//! @ingroup graphics
enum LightSourceAttribute{
	CREnabled = 0,
	CRLocal,
	CRSpot,
	CRAmbient,
	CRColor,
	CRPosition,
	CRHalfVector,
	CRConeDirection,
	CRConstantAttenuation,
	CRLinearAttenuation,
	CRQuadraticAttenuation,
	CRFallofAngle,
	CRFallofExponent
};

//! Class representing a light source.
//! @note It isn't advisable to modify the attributes manually. If you're not sure how the class works, you shouldn't touch the object's members.
//! @ingroup graphics
class LightSource{
public:
	enum LightSourceType{
		CRUndefinedLight = 0,
		CRAmbientLight,
		CRDirectionalLight,
		CRPointLight,
		CRSpotLight
	};

	Color _color;
	glmath_vec3 position;
	glmath_vec3 direction;
	
	LightSourceType _type;
	
	bool isEnabled;
	
	//Dictionary<glmath_real> _attr;
	
	Map<glmath_real, LightSourceAttribute> _attributes;
	
	// evtl. Dictionary

	LightSource& operator=(LightSource& l){
		_color=l._color;
		position=l.position;
		direction=l.direction;
		_type=l._type;
		_attributes=l._attributes;
        return *this;
	}
	
};
	
}

#endif /* defined(CRITICAL_GRAPHICS_LIGHTSOURCE_H) */