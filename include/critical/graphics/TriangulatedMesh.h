//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file TriangulatedMesh.h
 *	@date 15.03.2015.
 *  @author Armon Carigiet
 *
 *  @brief Privides a data structure to store a virtual scene.
 *  @ingroup graphics
 */
//===========================================================================

#ifndef CRITICAL_GRAPHICS_TRIANGULATEDMESH_H
#define CRITICAL_GRAPHICS_TRIANGULATEDMESH_H

#include <critical/core/Vector.h>

#include "Mesh.h"

namespace critical{
	
//! Class representing a triangulated mesh, ready for getting processed by the render engine.
//! @ingroup graphics
class TriangulatedMesh{
private:
	
public:
	glmath_mat4 *_mmat;
	glmath_mat4 *_phymat;
	Material *_material;
	uint64_t _m_index;
	
	// TODO
	// Vector<Material*> materials;
	// Vector<uint32_t> primitivelimits;
	
	Vector<glmath_vec3> positions;
	Vector<glmath_vec3> normals;
	Vector<glmath_vec3> tangents;
	Vector<glmath_vec3> bitangents;
	Vector<glmath_vec3> textureCoords;
	
	TriangulatedMesh(){}
	
	~TriangulatedMesh(){
		
	}
	
	TriangulatedMesh(TriangulatedMesh &m): positions(m.positions), normals(m.normals), tangents(m.tangents), bitangents(m.bitangents), textureCoords(m.textureCoords) {}
	TriangulatedMesh(TriangulatedMesh &&m): positions(m.positions), normals(m.normals), tangents(m.tangents), bitangents(m.bitangents), textureCoords(m.textureCoords){ }
	
		TriangulatedMesh& operator=(TriangulatedMesh &m){
			positions=m.positions;
			normals=m.normals;
			tangents=m.tangents;
			bitangents=m.bitangents;
			textureCoords=m.textureCoords;
			return *this;
		}
		TriangulatedMesh& operator=(TriangulatedMesh &&m){
			positions=m.positions;
			normals=m.normals;
			tangents=m.tangents;
			bitangents=m.bitangents;
			textureCoords=m.textureCoords;
			return *this;
		}
	
	bool triangulateMesh(Mesh &mesh, glmath_mat4 *mat, glmath_mat4 *phymat=NULL);
};

}

#endif /* defined(CRITICAL_GRAPHICS_TRIANGULATEDMESH_H) */
