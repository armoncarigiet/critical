//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Color.h
 *	@date 15.01.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a class for storing rgba-colors.
 *  @ingroup graphics
 */
//===========================================================================

#ifndef CRITICAL_GRAPHICS_COLOR_H
#define CRITICAL_GRAPHICS_COLOR_H

#include <glmath/glmath_vec4.h>

namespace critical {

//! Class storing a rgba-color.
//! @ingroup graphics
class Color{
private:
	
public:
	glmath_real _r;
	glmath_real _g;
	glmath_real _b;
	glmath_real _a;
	Color(): _r(0), _g(0), _b(0), _a(0){}
	Color(glmath_real r, glmath_real g, glmath_real b, glmath_real a=255): _r(r), _g(g), _b(b), _a(a){}
	Color(glmath_vec4 c): _r(c.r), _g(c.g), _b(c.b), _a(c.a){}
	
    //! Sets the color to an rgba-value.
    //! @param r [0.0f, 1.0f] Red color value.
    //! @param g [0.0f, 1.0f] Green color value.
    //! @param b [0.0f, 1.0f] Blue color value.
    //! @param a [0.0f, 1.0f] Alpha value (transparency).
	void set(glmath_real r, glmath_real g, glmath_real b, glmath_real a=255);
    
    //! Sets the color to an rgba-value.
    //! @param c rgba-vector
	void set(glmath_vec4 c);
    
    //! Creates a vector storing the rgba-color.
    //! @return rgba-vector
	glmath_vec4 get();
    
    //! Interpolates the current color with another rgba-color.
    //! @param c rgba-vector
	void mix(glmath_vec4 c);
    
    //! Interpolates the current color with another rgba-color.
    //! @param r [0.0f, 1.0f] Red color value.
    //! @param g [0.0f, 1.0f] Green color value.
    //! @param b [0.0f, 1.0f] Blue color value.
    //! @param a [0.0f, 1.0f] Alpha value (transparency).
	void mix(glmath_real r, glmath_real g, glmath_real b, glmath_real a=255);
};
	
}

#endif /* defined(_CRITICAL_GRAPHICS_COLOR_H) */