//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Primitive.h
 *	@date 20.02.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a class for storing geometrical primitives.
 *  @ingroup graphics
 */
//===========================================================================

#ifndef CRITICAL_GRAPHICS_RENDERWINDOW_H
#define CRITICAL_GRAPHICS_RENDERWINDOW_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <critical/core/Thread.h>

#include <critical/core/String.h>
#include <critical/core/RenderInformation.h>
#include <critical/graphics/Scene.h>
#include <critical/graphics/TriangulatedMesh.h>

namespace critical{
	
    struct LightProperties {
		bool isEnabled;
		bool isLocal;
		bool isSpot;
		glmath_vec3 ambient;
		glmath_vec3 color;
		glmath_vec3 position;
		glmath_vec3 halfVector;
		glmath_vec3 coneDirection;
		float spotCosCutoff;
		float spotExponent;
		float constantAttenuation;
		float linearAttenuation;
		float quadraticAttenuation;
	};
    
	struct LightPropertyIdentifiers{
		String enabled;
		String local;
		String spot;
		String ambient;
		String color;
		String position;
		String halfVector;
		String coned;
		String spotCos;
		String spotExp;
		String cAtt;
		String lAtt;
		String qAttr;
	};
	
	struct MaterialProperties{
		glmath_vec3 ambient;
		glmath_vec3 emission;
		glmath_vec3 diffuse;
		glmath_vec3 specular;
		float shininess;
	};
    
	struct MaterialPropertyIdentifiers{
		String ambient;
		String emission;
		String diffuse;
		String specular;
		String shininess;
	};

//! Window capable of rendering a simple scene
//! @deprecated This class became obsolete with the creaton of the framework.
//! @ingroup graphics
class RenderWindow{
	static uint32_t defWindowHeight;
	static uint32_t defWindowWidth;
	static bool _initialized;
	static GLuint shader_programme;
	
public:
	GLFWwindow *_w;
	//Thread rthread;
	
	static void init();
	static void terminate();
	
	bool openWindow(RenderInformation* information, String title);
	
	bool renderScene(Scene *scene);
	
}__attribute__((deprecated));
	
}

#endif /* defined(CRITICAL_GRAPHICS_RENDERWINDOW_H) */