//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Scene.h
 *	@date 23.01.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a data structure to store a virtual scene.
 *  @ingroup graphics
 */
//===========================================================================

#ifndef CRITICAL_GRAPHICS_SCENE_H
#define CRITICAL_GRAPHICS_SCENE_H

#include "SceneNode.h"
#include "LightSource.h"
#include "Camera.h"
#include "Mesh.h"
#include "Material.h"
#include <critical/core/List.h>
#include <critical/core/AlignedList.h>
#include <critical/core/Dictionary.h>

namespace critical {

//! Class representing a virtual scene.
//! @ingroup graphics
class Scene{
public:
	
    //! Attributes for the render scene.
	enum SceneAttributes{
		CRUnitMeters = 0,
		CRUnitCentimeters,
		CRUnitKilometers,
		CRUnitMiles,
		CRUpAxisX,
		CRUpAxisY,
		CRUpAxisZ,
	};
	
	~Scene(){}
	
	void free(){
		rootNode.~SceneNode();
		cameras.destroy();
		lights.destroy();
		meshes.destroy();
		materials.destroy();
	}
	
//	Scene(Scene &p) : rootNode(rootNode)) {}
//	Scene(Scene &&p) : _s(p._s), _c(p._c), _p(p._p),  __alloc(p.__alloc) { p.__alloc=false; }
	
//	Scene& operator=(Scene &p){
//		_p=p._p;
//		_s=p._s;
//		_c=p._c;
//		__alloc=p.__alloc;
//		return *this;
//	}
//	Scene& operator=(Scene &&p){
//		_p=p._p;
//		_s=p._s;
//		_c=p._c;
//		__alloc=p.__alloc;
//		
//		p.__alloc=false;
//		
//		return *this;
//	}
	
	SceneNode rootNode;
	Vector<Camera> cameras;
	Vector<LightSource> lights;
	Vector<Mesh> meshes;
	Vector<Material> materials;
	
	AlignedList<glmath_mat4> matrices;
	
	Dictionary<SceneAttributes> attr;
	
	bool hasCameras(){
		return cameras.size();
	}
	bool hasLightSources(){
		return lights.size();
	}
	bool hasMeshes(){
		return meshes.size();
	}
	bool hasMaterials(){
		return materials.size();
	}
};
	
}

#endif /* defined(_CRITICAL_GRAPHICS_SCENE_H) */