//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file graphics.h
 *	@date 11.11.2014.
 *  @author Armon Carigiet
 *
 *  @brief Main include file for the graphics module.
 */
//===========================================================================

//===========================================================================
/**
 *  @defgroup graphics Graphics unit
 *
 *  Provides several data structures and classes for representing and 
 *  modifing virtual scenes. It also helps generating render windows and
 *  initialising OpenGL ontexts. The functionallity is limited to simple 
 *  modification possibilities. With help of the core module's Importer class,
 *  it's possible to load a virtual scen from a file into these data structures
 */
//===========================================================================

#ifndef CRITICAL_GRAPHICS_GRAPHICS_H_
#define CRITICAL_GRAPHICS_GRAPHICS_H_

//===========================================================================
// Headers
//===========================================================================

#include <critical/graphics/Color.h>
#include <critical/graphics/Camera.h>
#include <critical/graphics/LightSource.h>
#include <critical/graphics/Material.h>
#include <critical/graphics/Primitive.h>
#include <critical/graphics/Mesh.h>
#include <critical/graphics/TriangulatedMesh.h>
#include <critical/graphics/SceneNode.h>
#include <critical/graphics/Scene.h>

#include <critical/graphics/RenderWindow.h>
#include <critical/graphics/Window.h>


#endif /* CRITICAL_GRAPHICS_GRAPHICS_H_ */