//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file physics.h
 *	@date 31.08.2015.
 *  @author Armon Carigiet
 *
 *  @brief Main include file for the physics module.
 */
//===========================================================================

//===========================================================================
/**
 *  @defgroup framework Framework
 *
 *  The framework provides an user friendly Application Programming Interface 
 *  (API) for creating custom Simulations and simplifies the library setup and
 *  the collaboration between the three library modules. The basic
 *  implementation is only able to create simple Simulations, but will be updated
 *  in the future.
 */
//===========================================================================

#ifndef CRITICAL_FRAMEWORK_FRAMEWORK_H_
#define CRITICAL_FRAMEWORK_FRAMEWORK_H_

//===========================================================================
// Headers
//===========================================================================

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <critical/framework/CoreManager.h>
#include <critical/framework/SceneManager.h>
#include <critical/framework/RenderManager.h>
#include <critical/framework/PhysicsManager.h>
#include <critical/framework/EventManager.h>


#endif /* CRITICAL_FRAMEWORK_FRAMEWORK_H_ */