//
//  programpattern.cpp
//  critical
//
//  Created by Armon Carigiet on 06.09.15.
//
//

#include <critical/core/core.h>
#include <critical/graphics/graphics.h>
#include <critical/physics/physics.h>
#include <critical/framework/framework.h>

using namespace critical;

int main(){
    
    CoreManager<CROpenGL>::init();
    CoreManager<CROpenGL> manager;
    Importer importer;
    RenderInformation info;
    
    //========================================
    // Settings
    //========================================
    
    // Path to the scene to load.
    char *scenePath = (char*) "/Users/user/Documents/test/scene.dae";
    // Path to the vertex shader.
    String vertexShaderPath("/Users/user/Documents/test/vertex.glsl");
    // Path to the fragment shader.
    String fragmentShaderPath("/Users/user/Documents/test/fragment.glsl");
    // Window title
    String windowTitle("Window");
    // Display the fps on the Window title?
    bool displayFPS = true;
    // The index of the camera of the virtual scene to be used
    int cameraIndex=0;
    // Vertex shader's model matrix identifier.
    String mmatrixIdentifier("modelmatrix");
    // Vertex shader's camera matrix identifier.
    String cmatrixIdentifier("cameramatrix");
    // Vertex shader's perspective matrix identifier.
    String pmatrixIdentifier("perspectivematrix");
    // Vertex shader's normal matrix identifier.
    String nmatrixIdentifier("normalmatrix");
    
    
    //========================================
    // Code
    //========================================
    
    // Load the scene.
    MemoryManager::ConnectedPointer<Scene> scene=importer.loadSceneFromFile(scenePath);
    
    // Generate all managers.
    SceneManager *sceneManager=manager.generateScenmanager(&(*scene));
    PhysicsManager *physicsManager=manager.generatePhysicsManager();
    Window<CROpenGL> *Window=manager.generateRenderWindow(windowTitle, info);
    RenderManager<CROpenGL> *renderManager=manager.generateRenderManager();
    SystemClock<CRMonotonicClock> *clock=manager.generateClock();
    EventManager<CROpenGL> *eventManager=manager.generateEventmanager();
    
    // Load the shaders. Other opengl shaders can be added in the same way.
    renderManager->loadShader(CRVertexShader, vertexShaderPath);
    renderManager->loadShader(CRFragmentShader, fragmentShaderPath);
    renderManager->linkShaders();
    
    renderManager->setCamera(cameraIndex);
    
    renderManager->registerUniformMatrix(CRModelMatrix, mmatrixIdentifier);
    renderManager->registerUniformMatrix(CRCameraMatrix, cmatrixIdentifier);
    renderManager->registerUniformMatrix(CRProjectionMatix, pmatrixIdentifier);
    renderManager->registerUniformMatrix(CRNormalMatrix, nmatrixIdentifier);
    
    
    
    // Material identifiers
    String str("materials");
    String ambientIdentifier("ambient");
    String emissionIdentifier("emission");
    String diffuseIdentifier("diffuse");
    String specularIdentifier("specular");
    String shininessIdentifier("shininess");
    String materialindexIdentifier("material_index");
    
    
    renderManager->registerUniformMaterialAttributeVector(CRMaterialAmbient, str,ambientIdentifier);
    renderManager->registerUniformMaterialAttributeVector(CRMaterialEmission, str, emissionIdentifier);
    renderManager->registerUniformMaterialAttributeVector(CRMaterialDiffuse, str, diffuseIdentifier);
    renderManager->registerUniformMaterialAttributeVector(CRMaterialSpecular, str, specularIdentifier);
    renderManager->registerUniformMaterialAttributeVector(CRMaterialShininess, str, shininessIdentifier);
    renderManager->registerMaterialIndexLocation(materialindexIdentifier);
    
    
    // light properties
    
    String l("lights");
    String ambientIdentifier("isEnabled");
    String localIdentifier("isLocal");
    String spotIdentifier("isSpot");
    String ambientIdentifier("ambient");
    String colorIdentifier("color");
    String positionIdentifier("position");
    String halfVectorIdentifier("halfVector");
    String coneDirectionIdentifier("coneDirection");
    String spotCosIdentifier("spotCosCutoff");
    String spotExponentIdentifier("spotExponent");
    String constantAttenuationIdentifier("constantAttenuation");
    String linearAttenuationIdentifier("linearAttenuation");
    String quadraticAttenuationIdentifier("quadraticAttenuation");
    
    renderManager->registerUniformLightAttributeVector(CREnabled, l, ambientIdentifier);
    renderManager->registerUniformLightAttributeVector(CRLocal, l, localIdentifier);
    renderManager->registerUniformLightAttributeVector(CRSpot, l, spotIdentifier);
    renderManager->registerUniformLightAttributeVector(CRAmbient, l, ambientIdentifier);
    renderManager->registerUniformLightAttributeVector(CRColor, l, colorIdentifier);
    renderManager->registerUniformLightAttributeVector(CRPosition, l, positionIdentifier);
    renderManager->registerUniformLightAttributeVector(CRHalfVector, l, halfVectorIdentifier);
    renderManager->registerUniformLightAttributeVector(CRConeDirection, l, coneDirectionIdentifier);
    //renderManager->registerUniformLightAttributeVector(CRFallofAngle, l, spotCosIdentifier);		// will pass cosinus to shader
    //renderManager->registerUniformLightAttributeVector(CRFallofExponent, l, spotExponentIdentifier);
    renderManager->registerUniformLightAttributeVector(CRConstantAttenuation, l, constantAttenuationIdentifier);
    renderManager->registerUniformLightAttributeVector(CRLinearAttenuation, l, linearAttenuationIdentifier);
    renderManager->registerUniformLightAttributeVector(CRQuadraticAttenuation, l, quadraticAttenuationIdentifier);
    
    // other variables
    
    // strength of the lighting...
    glmath_real strength=1;
    
    renderManager->registerUniformVariable("strength", &strength);
    
    renderManager->resolveUniformLocations();
    
    
    
    //=====================
    // Physics
    //=====================
    
    physics::ParticleSpace* particleSpace=physicsManager->createParticleSpace();
    
    glmath_real mass=10;
    glmath_vec3 position=glmath_vec3_create(0, 0, 5);
    glmath_vec3 velocity=glmath_vec3_create(2, 0, 0);
    
    physics::Particle *p1=particleSpace->addObject(physics::Particle(String("Particle 1"), mass, position, velocity));

    // Gravity force generator
    physics::SimpleParticleGravityGenerator* ggen=particleSpace->addForceGenerator(physics::SimpleParticleGravityGenerator(glmath_vec3_create(0, 0, -physics::values::gravitational_acceleration<physics::values::CREarth>())));
    ggen->addObject(p1);
    
    
    // Drag force generator
    physics::ParticleDragGenerator* dgen1=particleSpace->addForceGenerator(physics::ParticleDragGenerator(1.4, 1.15, GLMATH_PI*0.25*0.25));
    dgen1->addObject(p1);
    
        // Link phyiscal object with a scenen mesh.
    physicsManager->addLink(&sceneManager->meshes[0], p1);


    
    uint64_t dt=CRITICAL_IDEAL_FRAME_NANOSECONDS;
    
    long framecounter=0;
    uint64_t timeaccumulator=0;
    
    
    // render loop
    while(true){
        clock->start();
        manager.renderOneFrame(dt);
        
        particleSpace->applyForces();
        
        glmath_real t=dt/GLMATH_GIGA;
        particleSpace->integrate(t);
        
        physicsManager->updateModelMatrices();
        
        Window->swapBuffers();
        Window->pollEvents();
        
        if(glfwGetKey(Window->_w, GLFW_KEY_SPACE)==GLFW_PRESS){
            // Simulation starts if space is pressed.
            particleSpace->startSimulation();
        }
        
        // Simple camera constroll with ASWD and up,down,left,right
        if (glfwGetKey(Window->_w, GLFW_KEY_UP)==GLFW_PRESS) {
            glmath_vec3 right=glmath_vec3_cross(sceneManager->cameras[0]._forward, sceneManager->cameras[0]._up);
            sceneManager->cameras[0]._up=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(M_PI/180, right), sceneManager->cameras[0]._up));
            sceneManager->cameras[0]._forward=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(M_PI/180, right), sceneManager->cameras[0]._forward));
        }
        if(glfwGetKey(Window->_w, GLFW_KEY_DOWN)==GLFW_PRESS){
            glmath_vec3 right=glmath_vec3_cross(sceneManager->cameras[0]._forward, sceneManager->cameras[0]._up);
            sceneManager->cameras[0]._up=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(-M_PI/180, right), sceneManager->cameras[0]._up));
            sceneManager->cameras[0]._forward=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(-M_PI/180, right), sceneManager->cameras[0]._forward));
        }
        if(glfwGetKey(Window->_w, GLFW_KEY_RIGHT)==GLFW_PRESS){
            sceneManager->cameras[0]._forward=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(M_PI/180, sceneManager->cameras[0]._up), sceneManager->cameras[0]._forward));
        }
        if(glfwGetKey(Window->_w, GLFW_KEY_LEFT)==GLFW_PRESS){
            sceneManager->cameras[0]._forward=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(-M_PI/180, sceneManager->cameras[0]._up), sceneManager->cameras[0]._forward));
        }
        if(glfwGetKey(Window->_w, GLFW_KEY_S)==GLFW_PRESS){
            sceneManager->cameras[0]._position.x+=0.1;
        }
        if(glfwGetKey(Window->_w, GLFW_KEY_A)==GLFW_PRESS){
            sceneManager->cameras[0]._position.x-=0.1;
        }
        if(glfwGetKey(Window->_w, GLFW_KEY_W)==GLFW_PRESS){
            sceneManager->cameras[0]._position.y+=0.1;
        }
        if(glfwGetKey(Window->_w, GLFW_KEY_S)==GLFW_PRESS){
            sceneManager->cameras[0]._position.y-=0.1;
        }
        
        
        if (glfwGetKey(Window->_w, GLFW_KEY_ESCAPE)==GLFW_PRESS){
            glfwSetWindowShouldClose (Window->_w, 1);
        }
        
        
        dt=clock->stop();
        
        timeaccumulator+=dt;
        
        framecounter++;
        
        
        // calculate fps
        if((glmath_real)timeaccumulator*GLMATH_NANO>=1.0f){
            glmath_real fps=framecounter/((glmath_real)timeaccumulator*GLMATH_NANO);
            char buffer[50];
            sprintf(buffer, "Window - fps: %f", fps);
            glfwSetWindowTitle(manager._rWindow._w, buffer);
            framecounter=0;
            timeaccumulator=0;
        };
        
    }


}