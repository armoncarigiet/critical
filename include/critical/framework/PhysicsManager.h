//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file PhysicsManager.h
 *	@date 14.05.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a class to manage the physics module.
 *  @ingroup framework
 */
//===========================================================================

#ifndef CRITICAL_FRAMEWORK_PHYSICSMANAGER_H_
#define CRITICAL_FRAMEWORK_PHYSICSMANAGER_H_

#include <critical/core/List.h>
#include <critical/core/AlignedList.h>
#include <critical/core/Map.h>
#include <critical/core/Tuple.h>

#include <critical/physics/Particle.h>
#include <critical/physics/Rigidbody.h>
#include <critical/physics/ParticleSpace.h>
#include <critical/physics/RigidbodySpace.h>
#include <critical/physics/ParticleSystem.h>
#include <critical/physics/PointMassSystem.h>
#include <critical/graphics/TriangulatedMesh.h>

namespace critical {
	
//! Class handling the simulation's physical calculations.
//! @ingroup framework
class PhysicsManager{
private:
	
	typedef Tuple<physics::Particle*, TriangulatedMesh*> _plistEntry;
	typedef Tuple<physics::RigidBody*, TriangulatedMesh*> _rblistEntry;
	
//	class _plistEntry{
//	public:
//		physics::Particle *p;
//		TriangulatedMesh *m;
//		AlignedList<glmath_mat4>::Iterator mat;
//		
//		_plistEntry(physics::Particle *particle, TriangulatedMesh* mesh, AlignedList<glmath_mat4>::Iterator& it): p(particle), m(mesh), mat(it){}
//	};
//	
//	class _rblistEntry{
//	public:
//		physics::RigidBody *rb;
//		TriangulatedMesh *m;
//		AlignedList<glmath_mat4>::Iterator mat;
//		
//		_rblistEntry(physics::RigidBody *particle, TriangulatedMesh* mesh, AlignedList<glmath_mat4>::Iterator& it): rb(particle), m(mesh), mat(it){}
//	};
	
	physics::ParticleSpace _pSpace;
	physics::RigidBodySpace _rbSpace;
	List<physics::ParticleSystem> _pSystems;
    
	
	List<_plistEntry> _plist;
	List<_rblistEntry> _rblist;
	
   
    
public:
	
	//! Creates a Space for pointmass simulation.
    //! @return Pointer to the created ParticleSpace object.
	physics::ParticleSpace* createParticleSpace();
    
    //! Creates a Space for rigid body simulation.
    //! @return Pointer to the created RigidBodySpace object.
	physics::RigidBodySpace* createRigidBodySpace();
    
    //! Creates a particle system
    //! @note Not in use. Does nothing.
    //! @return Pointer to the created ParticleSystem object.
	physics::ParticleSystem* createParticleSystem();

		
	//! Adds a particle to the simulation.
    //! @param particle Particle template.
    //! @param mesh TriangulatedMesh object to link the particle with.
    //! @return Pointer to the created Particle object.
	physics::Particle* addPhyisicalObject(physics::Particle &particle, TriangulatedMesh *mesh=NULL);
    
    //! Adds a rigidbody to the simulation.
    //! @param particle RigidBody template.
    //! @param mesh TriangulatedMesh object to link the rigidbody with.
    //! @return Pointer to the created RigidBody object.
	physics::RigidBody* addPhyisicalObject(physics::RigidBody &rigidBody, TriangulatedMesh *mesh=NULL);
	
    
    //! Removes a particle from the simulation.
    //! @param Particle object to be deleted.
    //! @return True if the object was found and deleted, false if the object wasn't found.
	bool removePhyisicalObject(physics::Particle *particle);
    //! Removes a rigidbody from the simulation.
    //! @param RigidBody object to be deleted.
    //! @return True if the object was found and deleted, false if the object wasn't found.
	bool removePhyisicalObject(physics::RigidBody *rigidBody);
    //! Removes a physical from the simulation.
    //! @param identifier String identifier.
    //! @note If two objects posses the given identifier, then only the first added will be removed.
    //! @return True if the object was found and deleted, false if the object wasn't found.
	bool removePhyisicalObject(String &identifier);
    
    
	//bool removePhyisicalObject(uint64_t identifier);
	
	
    //! Adds a link between a particle and a mesh from the rendering scene.
    //! @param mesh Scene mesh object.
    //! @param particel Particle object.
    //! @note To work properly the mesh object and the particle object have to be registered by the SceneManager/PhysicsManager.
	void addLink(TriangulatedMesh* mesh, physics::Particle* particle);
    //! Adds a link between a rigidbody and a mesh from the rendering scene.
    //! @param mesh Scene mesh object.
    //! @param rigidBody RigidBody object.
    //! @note To work properly the mesh object and the rigidbody object have to be registered by the SceneManager/PhysicsManager.
	void addLink(TriangulatedMesh* mesh, physics::RigidBody* rigidBody);
    
    //! Removes a link between a physical objct and a mesh from the rendering scene.
    //! @param mesh Scene mesh object.
    //! @note Nothing will be done if no matching link was found.
	void removeLink(TriangulatedMesh *mesh);
    //! Removes a link between a particle and a mesh from the rendering scene.
    //! @param particle Particle object.
    //! @note Nothing will be done if no matching link was found.
	void removeLink(physics::Particle *particle);
    //! Removes a link between a rigidbody and a mesh from the rendering scene.
    //! @param particle RigidBody object.
    //! @note Nothing will be done if no matching link was found.
	void removeLink(physics::RigidBody *rigidBody);
	
    //! Updates the modelmatrices of all linked meshes based on the current data the linked physical objects are holding.
	void updateModelMatrices();
};
	
}

#endif /* defined(CRITICAL_FRAMEWORK_PHYSICSMANAGER_H_) */