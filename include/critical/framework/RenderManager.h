//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file RenderManager.h
 *	@date 17.05.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a class to manage the render process.
 *  @ingroup framework
 */
//===========================================================================

#ifndef CRITICAL_FRAMEWORK_RENDERMANAGER_H_
#define CRITICAL_FRAMEWORK_RENDERMANAGER_H_


#include <GL/glew.h>
#include <critical/core/RenderInformation.h>
#include <critical/framework/SceneManager.h>
#include <critical/core/FileManager.h>
#include <critical/core/Tuple.h>

namespace critical {
	
static inline void startFrameOpenGL(){
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
}
	
static inline void endFrameOpenGL(){
		
}
	
constexpr void startFramec(RenderType t){
	if(t==CROpenGL){
		startFrameOpenGL();
	}else if(t==CRBuiltIn){
		// ...
	}
}

constexpr void endFramec(RenderType t){
	if(t==CROpenGL){
		endFrameOpenGL();
	}else if(t==CRBuiltIn){
		// ...
	}
}

//! Struct for holding the generated Opengl Buffer-indices.
struct _glData{
	GLuint p_vbo;
	GLuint n_vbo;
	GLuint t_vbo;
	GLuint bt_vbo;
	GLuint tex_vbo;
	GLuint vao;
};

//! Describing the type of an uniform variable
enum uniformType{
	CRUndefinedType = 0,
	CRFloat,
	CRDouble,
	CRInteger,
	CRUnsignedInteger,
	CRReal,
	CRVec2,
	CRVec3,
	CRVec4,
	CRMat2,
	CRMat3,
	CRMat4,
};
	
//! Matrix identifiers.
enum matrixType{
	CRModelMatrix = 0,
	CRCameraMatrix,
	CRProjectionMatix,
	CRNormalMatrix
};

//! Assigns an uniform identifier to a given variable type.
//! @tparam T uniform Type.
//! @return Assigned uniformType.
template <class T>
constexpr uniformType getUniformIdentifier(){
	if(typeid(T)==typeid(float)) {
		return CRFloat;
	}else if(typeid(T)==typeid(double)) {
		return CRDouble;
	}else if(typeid(T)==typeid(int)) {
		return CRInteger;
	}else if(typeid(T)==typeid(unsigned int)) {
		return CRUnsignedInteger;
	}else if(typeid(T)==typeid(glmath_real)) {
		return CRReal;
	}else if(typeid(T)==typeid(glmath_vec2)) {
		return CRVec2;
	}else if(typeid(T)==typeid(glmath_vec3)) {
		return CRVec3;
	}else if(typeid(T)==typeid(glmath_vec4)) {
		return CRVec4;
	}else if(typeid(T)==typeid(glmath_mat2)) {
		return CRMat2;
	}else if(typeid(T)==typeid(glmath_mat3)) {
		return CRMat3;
	}else if(typeid(T)==typeid(glmath_mat4)) {
		return CRMat4;
	}else{
		return CRUndefinedType;
	}
}

//! Class managing the rendering enviorment for one simulation.
//! @ingroup framework
template <RenderType _type=CROpenGL>
class RenderManager{
public:
    //! Function pointer for framelisteners.
	typedef void (*FrameFunc)();
    
	glmath_mat4 cam;
	glmath_mat4 proj;
	glmath_mat4 mmat;
	glmath_mat3 norm;
	glmath_vec3 eye_direction;
	
	String materialString;
	GLuint materialIndexLocation;
	
	Camera *camera;
private:
	typedef Tuple<ShaderType, unsigned int> _shaderRegistration;
	
	class _uniformRegistration{
	public:
		void *variable;
		String identifier;
		uniformType type;
		GLuint location;
		_uniformRegistration(void* var, String &s, uniformType t): variable(var), identifier(s), type(t), location(0){}
	};
	
	class _matrixRegistaration{
	public:
		String identifier;
		matrixType type;
		GLuint location;
		_matrixRegistaration(matrixType t, String &s): identifier(s), type(t), location(0){}
	};
	
	class _materialRegistration{
	public:
		String vec;
		String identifier;
		MaterialAttribute attr;
		Vector<GLuint> location;
		_materialRegistration(MaterialAttribute a, String &v, String &ident): vec(v), identifier(ident), attr(a){}
		_materialRegistration(MaterialAttribute a, String &ident): vec(String()), identifier(ident), attr(a){}
	};
	
	class _lightSourceRegistraition{
	public:
		String vec;
		String identifier;
		LightSourceAttribute attr;
		Vector<GLuint> location;
		_lightSourceRegistraition(LightSourceAttribute a, String &v, String &ident): vec(v), identifier(ident), attr(a){}
		_lightSourceRegistraition(LightSourceAttribute a, String &ident): vec(String()), identifier(ident), attr(a){}
	};
	
	class _cameraRegistration{
	public:
		String identifier;
		CameraAttribute attr;
		GLuint location;
		_cameraRegistration(CameraAttribute a, String &ident): identifier(ident), attr(a){}
	};
	
	FrameFunc _startFrameFunction;
	FrameFunc _endFrameFunction;
	SceneManager *_sManager;
	Vector<_glData> _data;
	Vector<_shaderRegistration> _shaders;
	
	List<_uniformRegistration> _uniforms;
	List<_matrixRegistaration> _matrices;
	List<_materialRegistration> _materials;
	List<_lightSourceRegistraition> _lights;
	List<_cameraRegistration> _cameras;
	
	GLuint _shaderProgramme;
	
public:
	RenderManager(): _startFrameFunction(nullptr), _endFrameFunction(nullptr){}
	//! Adds a framelistener at the begin of the frame.
    //! @param f Function to execute.
	void addStartFrameFunction(FrameFunc f){ _startFrameFunction=f; }
    //! Adds a framelistener at the end of the frame.
    //! @param f Function to execute.
	void addEndFrameFunction(FrameFunc f){ _endFrameFunction=f; }
	

	void startFrame(){
		startFramec(_type);
		if(_startFrameFunction){
			_startFrameFunction();
		}
	}
	
	void endFrame(){
		endFramec(_type);
		if(_endFrameFunction){
			_endFrameFunction();
		}
	}
	
    //! Links a SceneManager providing a scene to render.
    //! @param sManager Pointer to the SceneManager.
	void linkSceneManager(SceneManager *sManager){
		if(sManager){
			_sManager=sManager;
			
			size_t s=sManager->meshes.size();
			
			_data.allocate(s);
			
			for(int i=0; i<sManager->meshes.size(); i++) {
				
				Vector<_glData>::Iterator it=_data.pushBack(_glData());
				
				bool hasPoints=false;
				bool hasNormals=false;
				bool hasTangents=false;
				bool hasBitangents=false;
				bool hasTextureCoords=false;
				
				if(sManager->meshes[i].positions.size()){
					glGenBuffers (1, &it->p_vbo);
					glBindBuffer (GL_ARRAY_BUFFER, it->p_vbo);
					glBufferData (GL_ARRAY_BUFFER, 3*sManager->meshes[i].positions.size()*sizeof(glmath_real), sManager->meshes[i].positions.data(), GL_STATIC_DRAW);
					hasPoints=true;
				}
				
				// normals
				if(sManager->meshes[i].normals.size()){
					glGenBuffers (1, &it->n_vbo);
					glBindBuffer (GL_ARRAY_BUFFER, it->n_vbo);
					glBufferData (GL_ARRAY_BUFFER, 3*sManager->meshes[i].normals.size()*sizeof(glmath_real), sManager->meshes[i].normals.data(), GL_STATIC_DRAW);
					hasNormals=true;
				}
				
				// tangents
				if(sManager->meshes[i].tangents.size()){
					glGenBuffers (1, &it->t_vbo);
					glBindBuffer (GL_ARRAY_BUFFER, it->t_vbo);
					glBufferData (GL_ARRAY_BUFFER, 3*sManager->meshes[i].tangents.size()*sizeof(glmath_real), sManager->meshes[i].tangents.data(), GL_STATIC_DRAW);
					hasTangents=true;
				}
				
				// bitangents
				if(sManager->meshes[i].bitangents.size()){
					glGenBuffers (1, &it->bt_vbo);
					glBindBuffer (GL_ARRAY_BUFFER, it->bt_vbo);
					glBufferData (GL_ARRAY_BUFFER, 3*sManager->meshes[i].bitangents.size()*sizeof(glmath_real), sManager->meshes[i].bitangents.data(), GL_STATIC_DRAW);
					hasBitangents=true;
				}
				
				// textureCoords
				if(sManager->meshes[i].bitangents.size()){
					glGenBuffers (1, &it->tex_vbo);
					glBindBuffer (GL_ARRAY_BUFFER, it->tex_vbo);
					glBufferData (GL_ARRAY_BUFFER, 3*sManager->meshes[i].bitangents.size()*sizeof(glmath_real), sManager->meshes[i].bitangents.data(), GL_STATIC_DRAW);
					hasTextureCoords=true;
				}
				
				
				glGenVertexArrays(1, &it->vao);
				glBindVertexArray(it->vao);
				
				
				if(hasPoints){
					glBindBuffer(GL_ARRAY_BUFFER, it->p_vbo);
					glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
					glEnableVertexAttribArray(0);
				}
				if(hasNormals){
					glBindBuffer(GL_ARRAY_BUFFER, it->n_vbo);
					glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
					glEnableVertexAttribArray(1);
				}
				if(hasTangents){
					glBindBuffer(GL_ARRAY_BUFFER, it->t_vbo);
					glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);
					glEnableVertexAttribArray(2);
				}
				if(hasBitangents){
					glBindBuffer(GL_ARRAY_BUFFER, it->bt_vbo);
					glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, NULL);
					glEnableVertexAttribArray(3);
				}
				if(hasTextureCoords){
					glBindBuffer(GL_ARRAY_BUFFER, it->tex_vbo);
					glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 0, NULL);
					glEnableVertexAttribArray(4);
				}
			}
		}
		
	}
	
    //! Loads an OpenGL shader from a specific file.
    //! @param s Type of shader.
    //! @param file Path to a shader resource.
	void loadShader(ShaderType s, String &file){
		FileManager::FileStream stream=FileManager::openStream(file.c_str(), "r");
		size_t size;
		MemoryManager::ConnectedPointer<char> p=stream.loadFile(&size);
		FileManager::closeStream(stream);
		const char* sp=&p;
		
		GLuint f;
		
		if(s==CRVertexShader){
			f=GL_VERTEX_SHADER;
		}else if(s==CRFragmentShader){
			f=GL_FRAGMENT_SHADER;
		}else if(s==CRGeometryShader){
			f=GL_GEOMETRY_SHADER;
		}else if(s==CRTesselationControlShader){
			f=GL_TESS_CONTROL_SHADER;
		}else if(s==CRTesselationEvaluationShader){
			f=GL_TESS_EVALUATION_SHADER;
        }else{
            throw 1;
            f=0;
        }
		
		unsigned int sh;
		
		if(_type==CROpenGL){
			sh=glCreateShader(f);
			glShaderSource(sh, 1, &sp, NULL);
			glCompileShader(sh);
			
			//char buffer[60000];
			//glGetShaderInfoLog(sh, 60000, NULL, buffer);
			//printf("\n\n%s\n\n", buffer);
			
		}

		Vector<_shaderRegistration>::Iterator it=_shaders.pushBack(_shaderRegistration(s, sh));
		
		
		MemoryManager::sfree(p);
		
	}
    
    //! Links the loaded shaders into a programm.
	void linkShaders(){
		if(_type==CROpenGL){
			_shaderProgramme = glCreateProgram();
			for(int i=0; i<_shaders.size(); i++){
				glAttachShader(_shaderProgramme, _shaders[i]._t2);
			}
			glLinkProgram(_shaderProgramme);
			
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LESS);
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			glFrontFace(GL_CCW);
		}
	}
	
    //! Sets the rendercamera to a specific camera the scenemanager is holding.
    //! @param i Camera index.
	void setCamera(uint64_t i){
		if(i<=_sManager->cameras.size()){
			//printf("aspect ratio: %f", _sManager->cameras[0]._aspect);
			camera=&_sManager->cameras[i];
			cam=glmath_mat4_create_view(camera->_position, camera->_forward, camera->_up);
			proj=glmath_mat4_create_perspective(camera->_fov/180*M_PI, camera->_aspect, camera->_znear, camera->_zfar);
		}else{
			throw 1;
		}
		
	}
    
    void setCustomCamera(Camera &c){
        camera=&c;
        cam=glmath_mat4_create_view(camera->_position, camera->_forward, camera->_up);
        proj=glmath_mat4_create_perspective(camera->_fov/180*M_PI, camera->_aspect, camera->_znear, camera->_zfar);
    }
	
    //! Links a variable with a shader-uniform
    //! @param identifier Uniform identifier, which is defined in the shader file.
    //! @param variable Variable to link to.
	template <class T>
	void registerUniformVariable(String &identifier, T* variable){
		uniformType t=getUniformIdentifier<T>();
		if(t==CRUndefinedType){
			throw 1;
		}else{
			_uniforms.pushBack(_uniformRegistration(static_cast<void*>(variable), identifier, t));
		}
		
	}
	
    //! Links a variable with a shader-uniform.
    //! @param identifier Uniform identifier, which is defined in the shader file.
    //! @param variable Variable to link to.
	template <class T>
	void registerUniformVariable(String &&identifier, T* variable){
		uniformType t=getUniformIdentifier<T>();
		if(t==CRUndefinedType){
			throw 1;
		}else{
			_uniforms.pushBack(_uniformRegistration(static_cast<void*>(variable), identifier, t));
		}
	}
	
    //! Links a matrix with a shader-uniform.
    //! @param type The matrix to be linked.
    //! @param identifier Uniform identifier, which is defined in the shader file.
	void registerUniformMatrix(matrixType type, String &identifier){
		_matrices.pushBack(_matrixRegistaration(type, identifier));
	}
    
    //! Links a matrix with a shader-uniform.
    //! @param type The matrix to be linked.
    //! @param identifier Uniform identifier, which is defined in the shader file.
	void registerUniformMatrix(matrixType type, String &&identifier){
		_matrices.pushBack(_matrixRegistaration(type, identifier));
	}

    //! Links a material attribute with a shader-uniform.
    //! @param attr The attribute to be linked.
    //! @param identifier Uniform identifier, which is defined in the shader file.
	void registerUniformMaterialAttribute(MaterialAttribute attr, String &identifier){
		_materials.pushBack(_materialRegistration(attr, identifier));
	}
    
    //! Links a material attribute with a shader-uniform.
    //! @param attr The attribute to be linked.
    //! @param identifier Uniform identifier, which is defined in the shader file.
	void registerUniformMaterialAttribute(MaterialAttribute attr, String &&identifier){
		_materials.pushBack(_materialRegistration(attr, identifier));
	}
	
    //! Links a material attribute with a shader-uniform inside an array.
    //! @param attr The attribute to be linked.
    //! @param vec Uniform vector identifier, which is defined in the shader file.
    //! @param identifier Uniform attribute identifier, which is defined in the shader file.
    //! @note The attributes should be arranged as a struct array, because there are mostly multiple materials available.
    //! @note It will be searched with the following OpenGL shader identifier: vec[n].identifier, where n ist the material-index.
	void registerUniformMaterialAttributeVector(MaterialAttribute attr, String &vec, String &identifier){
		_materials.pushBack(_materialRegistration(attr, vec, identifier));
	}
    //! Links a material attribute with a shader-uniform inside an array.
    //! @param attr The attribute to be linked.
    //! @param vec Uniform vector identifier, which is defined in the shader file.
    //! @param identifier Uniform attribute identifier, which is defined in the shader file.
    //! @note The attributes should be arranged as a struct array, because there are mostly multiple materials available.
    //! @note It will be searched with the following OpenGL shader identifier: vec[n].identifier, where n ist the material-index.

	void registerUniformMaterialAttributeVector(MaterialAttribute attr, String &&vec, String &&identifier){
		_materials.pushBack(_materialRegistration(attr, vec, identifier));
	}
    //! Links a material attribute with a shader-uniform inside an array.
    //! @param attr The attribute to be linked.
    //! @param vec Uniform vector identifier, which is defined in the shader file.
    //! @param identifier Uniform attribute identifier, which is defined in the shader file.
    //! @note The attributes should be arranged as a struct array, because there are mostly multiple materials available.
    //! @note It will be searched with the following OpenGL shader identifier: vec[n].identifier, where n ist the material-index.

	void registerUniformMaterialAttributeVector(MaterialAttribute attr, String &vec, String &&identifier){
		_materials.pushBack(_materialRegistration(attr, vec, identifier));
	}
    
    //! Links a material attribute with a shader-uniform inside an array.
    //! @param attr The attribute to be linked.
    //! @param vec Uniform vector identifier, which is defined in the shader file.
    //! @param identifier Uniform attribute identifier, which is defined in the shader file.
    //! @note The attributes should be arranged as a struct array, because there are mostly multiple materials available.
    //! @note It will be searched with the following OpenGL shader identifier: vec[n].identifier, where n ist the material-index.
	void registerUniformMaterialAttributeVector(MaterialAttribute attr, String &&vec, String &identifier){
		_materials.pushBack(_materialRegistration(attr, vec, identifier));
	}
	
    //! Links a lightning attribute with a shader-uniform.
    //! @param attr The attribute to be linked.
    //! @param identifier Uniform identifier, which is defined in the shader file.
	void registerUniformLightAttribute(MaterialAttribute attr, String &identifier){
		_lights.pushBack(_lightSourceRegistraition(attr, identifier));
	}
    
    //! Links a lightning attribute with a shader-uniform.
    //! @param attr The attribute to be linked.
    //! @param identifier Uniform identifier, which is defined in the shader file.
	void registerUniformLightAttribute(MaterialAttribute attr, String &&identifier){
		_lights.pushBack(_lightSourceRegistraition(attr, identifier));
	}
	
    //! Links a material attribute with a shader-uniform inside an array.
    //! @param attr The attribute to be linked.
    //! @param vec Uniform vector identifier, which is defined in the shader file.
    //! @param identifier Uniform attribute identifier, which is defined in the shader file.
    //! @note The attributes should be arranged as a struct array, because there are mostly multiple materials available.
    //! @note It will be searched with the following OpenGL shader identifier: vec[n].identifier, where n ist the material-index.

	void registerUniformLightAttributeVector(LightSourceAttribute attr, String &vec, String &identifier){
		_lights.pushBack(_lightSourceRegistraition(attr, vec ,identifier));
	}
	void registerUniformLightAttributeVector(LightSourceAttribute attr, String &&vec, String &&identifier){
		_lights.pushBack(_lightSourceRegistraition(attr, vec ,identifier));
	}
	void registerUniformLightAttributeVector(LightSourceAttribute attr, String &vec, String &&identifier){
		_lights.pushBack(_lightSourceRegistraition(attr, vec ,identifier));
	}
	void registerUniformLightAttributeVector(LightSourceAttribute attr, String &&vec, String &identifier){
		_lights.pushBack(_lightSourceRegistraition(attr, vec ,identifier));
	}
	
	void registerUniformCameraAttribute(CameraAttribute attr, String &identifier){
		_lights.pushBack(_cameraRegistration(attr, identifier));
	}
	void registerUniformCameraAttribute(CameraAttribute attr, String &&identifier){
		_lights.pushBack(_cameraRegistration(attr, identifier));
	}
	
	void registerMaterialIndexLocation(String &identifier){
		materialString=identifier;
	}
	void registerMaterialIndexLocation(String &&identifier){
		materialString=identifier;
	}
	
	//! resolves the registered uniform identifiers.
	void resolveUniformLocations(){
		if(_type==CROpenGL){
			
			materialIndexLocation=glGetUniformLocation(_shaderProgramme, materialString.c_str());
			
			for(typename List<_uniformRegistration>::Iterator it=_uniforms.begin(); !it.isOutOfRange() ;it++){
				it->location=glGetUniformLocation(_shaderProgramme, it->identifier.c_str());
			}
			
			for(typename List<_materialRegistration>::Iterator it=_materials.begin(); !it.isOutOfRange() ;it++){
				
				if(!it->vec.isAllocated()){
					GLuint t=glGetUniformLocation(_shaderProgramme, it->identifier.c_str());
					it->location.pushBack(t);
				}else{
					for(int i=0; i<_sManager->materials.size(); i++){
						char buffer[strlen(it->vec.c_str())+strlen(it->identifier.c_str())+6];
						sprintf(buffer, "%s[%d].%s", it->vec.c_str(), i, it->identifier.c_str());
						GLuint t=glGetUniformLocation(_shaderProgramme, buffer);
						it->location.pushBack(t);
					}
				}
			}
			
			for(typename List<_lightSourceRegistraition>::Iterator it=_lights.begin(); !it.isOutOfRange() ;it++){
				if(!it->vec.isAllocated()){
					GLuint t=glGetUniformLocation(_shaderProgramme, it->identifier.c_str());
					it->location.pushBack(t);
				}else{
					for(int i=0; i<_sManager->lightSources.size(); i++){
						char buffer[strlen(it->vec.c_str())+strlen(it->identifier.c_str())+6];
						sprintf(buffer, "%s[%d].%s", it->vec.c_str(), i, it->identifier.c_str());
						GLuint t=glGetUniformLocation(_shaderProgramme, buffer);
						it->location.pushBack(t);
					}
				}
			}

			for(typename List<_matrixRegistaration>::Iterator it=_matrices.begin(); !it.isOutOfRange() ;it++){
				it->location=glGetUniformLocation(_shaderProgramme, it->identifier.c_str());
			}
			
			for(typename List<_cameraRegistration>::Iterator it=_cameras.begin(); !it.isOutOfRange() ;it++){
				it->location=glGetUniformLocation(_shaderProgramme, it->identifier.c_str());
			}
			
		}
	}
	
    //! Not used at the moment.
    //! @note Throws an exception.
	void finalizeData(){
		if(_type==CROpenGL){
			throw 1;
		}
	}
	
    //! Draws resp. refreshes the rendering scene.
	void drawObjects(){
		glUseProgram(_shaderProgramme);
		cam=glmath_mat4_create_view(camera->_position, camera->_forward, camera->_up);
		proj=glmath_mat4_create_perspective(camera->_fov/180*M_PI, camera->_aspect, camera->_znear, camera->_zfar);
		eye_direction=glmath_vec3_neg(camera->_forward);
		
		for(typename List<_uniformRegistration>::Iterator it=_uniforms.begin(); !it.isOutOfRange() ;it++){
			if(it->type==CRFloat){
				glUniform1f(it->location, *reinterpret_cast<float*>(it->variable));
			}else if(it->type==CRDouble){
				glUniform1d(it->location, *reinterpret_cast<double*>(it->variable));
			}else if(it->type==CRInteger){
				glUniform1i(it->location, *reinterpret_cast<int*>(it->variable));
			}else if(it->type==CRUnsignedInteger){
				glUniform1ui(it->location, *reinterpret_cast<unsigned int*>(it->variable));
			}else if(it->type==CRReal){
				glUniform1f(it->location, *reinterpret_cast<glmath_real*>(it->variable));
			}else if(it->type==CRVec2){
				glUniform2fv(it->location, 1, reinterpret_cast<glmath_vec2*>(it->variable)->v);
			}else if(it->type==CRVec3){
				glUniform3fv(it->location, 1, reinterpret_cast<glmath_vec3*>(it->variable)->v);
			}else if(it->type==CRVec4){
				glUniform4fv(it->location, 1, reinterpret_cast<glmath_vec4*>(it->variable)->v);
			}else if(it->type==CRMat2){
				glUniformMatrix2fv(it->location, 1, GL_FALSE, reinterpret_cast<glmath_mat2*>(it->variable)->m);
			}else if(it->type==CRMat3){
				glUniformMatrix3fv(it->location, 1, GL_FALSE, reinterpret_cast<glmath_mat3*>(it->variable)->m);
			}else if(it->type==CRMat4){
				glUniformMatrix4fv(it->location, 1, GL_FALSE, reinterpret_cast<glmath_mat4*>(it->variable)->m);
			}
		}
		
		for(typename List<_materialRegistration>::Iterator it=_materials.begin(); !it.isOutOfRange() ;it++){
			MaterialAttribute a=it->attr;
			for(int i=0; i<it->location.size(); i++) {
				if(a==CRMaterialEmission || a==CRMaterialAmbient || a==CRMaterialDiffuse || a==CRMaterialSpecular){
					glmath_real *v=_sManager->materials[i]._attributes.elementForKey(a).data();
					glUniform3fv(it->location[i], 1, v);
				}else if(a==CRMaterialShininess || a==CRMaterialIndexOfRefraction){
					glmath_real *v=_sManager->materials[i]._attributes.elementForKey(a).data();
					glUniform1fv(it->location[i], 1, v);
				}
			}
		}
		
		for(typename List<_lightSourceRegistraition>::Iterator it=_lights.begin(); !it.isOutOfRange() ;it++){
			LightSourceAttribute a=it->attr;
			for(int i=0; i<it->location.size(); i++) {
				if(a==CREnabled){
					glUniform1i(it->location[i], _sManager->lightSources[i].isEnabled);
				}else if(a==CRLocal){
					bool inf=_sManager->lightSources[0]._type!=LightSource::CRAmbientLight && _sManager->lightSources[0]._type!=LightSource::CRDirectionalLight ? true : false;
					glUniform1i(it->location[i], inf);
				}else if(a==CRSpot){
					bool inf=_sManager->lightSources[0]._type==LightSource::CRSpotLight ? true : false;
					glUniform1i(it->location[i], inf);
				}else if(a==CRAmbient){
					glUniform3f(it->location[i], 0,0,0);
				}else if(a==CRColor){
					glUniform3f(it->location[i], _sManager->lightSources[i]._color._r, _sManager->lightSources[i]._color._g, _sManager->lightSources[i]._color._b);
				}else if(a==CRPosition){
					glUniform3fv(it->location[i], 1, _sManager->lightSources[i].position.v);
				}else if(a==CRHalfVector){
					glmath_vec3 hv=glmath_vec3_norm(glmath_vec3_add(glmath_vec3_norm(_sManager->lightSources[i].position), glmath_vec3_norm(_sManager->lightSources[i].direction)));
					glUniform3f(it->location[i], hv.x, hv.y, hv.z);
				}else if(a==CRConeDirection){
					glUniform3fv(it->location[i], 1, _sManager->lightSources[i].direction.v);
				}else if(a==CRConstantAttenuation || a==CRLinearAttenuation || a==CRQuadraticAttenuation || a==CRFallofAngle || a==CRFallofExponent){
					glUniform1f(it->location[i], _sManager->lightSources[i]._attributes[a]);
				}
			}
		}
		
		for(typename List<_matrixRegistaration>::Iterator it=_matrices.begin(); !it.isOutOfRange() ;it++){
			matrixType t=it->type;
			if(t==CRCameraMatrix){
				glUniformMatrix4fv(it->location, 1, GL_FALSE, cam.m);
			}else if(t==CRProjectionMatix){
				glUniformMatrix4fv(it->location, 1, GL_FALSE, proj.m);
			}
		}
		
		for(typename List<_cameraRegistration>::Iterator it=_cameras.begin(); !it.isOutOfRange() ;it++){
			CameraAttribute a=it->attr;
			if(a==CREyeDirection){
				glUniform3fv(it->location, 1, eye_direction.v);
			}
		}
		if(_type==CROpenGL){
			for(int i=0; i<_data.size(); i++) {
				for(typename List<_matrixRegistaration>::Iterator it=_matrices.begin(); !it.isOutOfRange() ;it++){
					matrixType t=it->type;
					if(t==CRNormalMatrix || t==CRNormalMatrix || t==CRModelMatrix){
                        mmat=glmath_mat4_mul(*_sManager->meshes[i]._mmat, *_sManager->meshes[i]._phymat);
                        //mmat=*_sManager->meshes[i]._phymat;
					}
					if(t==CRModelMatrix){
						glUniformMatrix4fv(it->location, 1, GL_FALSE, mmat.m);
					}else if(t==CRNormalMatrix){
						int invertable=0;
						norm=glmath_mat3_transpose(glmath_mat3_invert(glmath_mat3_mul(glmath_mat4_get_mat3(mmat), glmath_mat4_get_mat3(cam)), &invertable));
						if(!invertable){
							throw 1;
						}
						glUniformMatrix3fv(it->location, 1, GL_FALSE, norm.m);
					}
				}
				
				glUniform1i(materialIndexLocation, _sManager->meshes[i]._m_index);
			
				glBindVertexArray(_data[i].vao);
				glDrawArrays(GL_TRIANGLES, 0, _sManager->meshes[i].positions.size());
			}
		}else{
			throw 1;
		}
	}

};
	
}

#endif /* defined(CRITICAL_FRAMEWORK_RENDERMANAGER_H_) */
