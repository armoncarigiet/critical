//
//  StatisticalSimulation.h
//  critical
//
//  Created by Armon Carigiet on 07.06.15.
//
//

#ifndef CRITICAL_FRAMEWORK_STATISTICALSIMULATION_H_
#define CRITICAL_FRAMEWORK_STATISTICALSIMULATION_H_

#include <critical/physics/ParticleSpace.h>
#include <critical/physics/RigidBodySpace.h>
#include <critical/core/Tuple.h>

namespace critical{

enum dataIdentifier{
	CRPosition = 0,			//abs
	CRPositionX,
	CRPositionY,
	CRPositionZ,
	CRVelocity,
	CRVelocityX,			//abs
	CRVelocityY,
	CRVelocityZ,
	CRMomentum,				//abs
	CRMomentumX,
	CRMomentumY,
	CRMomentumZ,
	CRAcceleration,			//abs
	CRAccelerationX,
	CRAccelerationY,
	CRAccelerationZ,
	CRResultingForce,
	CRResultingForceX,
	CRResultingForceY,
	CRResultingForceZ,		//abs
	CROrientation,			//abs
	CROrientationX,
	CROrientationY,
	CROrientationZ,
	CRAngularVelocity,		//abs
	CRAngularVelocityX,
	CRAngularVelocityY,
	CRAngularVelocityZ,
	CRAngularMomentum,		//abs
	CRAngularMomentumX,
	CRAngularMomentumY,
	CRAngularMomentumZ,
	CRAngularAcceleration,	//abs
	CRAngularAccelerationX,
	CRAngularAccelerationY,
	CRAngularAccelerationZ,
	CRTorque,				//abs
	CRTorqueX,
	CRTorqueY,
	CRTorqueZ,
	CRTime
};
	
//enum dataFormIdentifier{
//	CRRaw = 0,
//	CRMean,
//	CRMedian,
//	CRLowerQuartile,
//	CRUpperQuartile,
//	CRQuadraticDeviation,
//	CRStandartDeviation
//};
	
enum plotType{
	CRPoints,
	CRLines,
	CRDottedLines,
	CRLinePairs
};
	
enum systemIdentifier{
	CRParticleSpace = 0,
	CRRigidBodySpace,
};
	
class StatisticlalSimulation{
private:
	

	class _ParticleCorelationEntry{
		physics::Particle *p;
		dataIdentifier d1;
		dataIdentifier d2;
		_ParticleCorelationEntry(physics::Particle *particle, dataIdentifier data1, dataIdentifier data2): p(particle), d1(data1), d2(data2){}
	};
	
	class _RigidBodyCorelationEntry{
		physics::RigidBody *rb;
		dataIdentifier d1;
		dataIdentifier d2;
		_RigidBodyCorelationEntry(physics::RigidBody *rigidBody, dataIdentifier data1, dataIdentifier data2): rb(rigidBody), d1(data1), d2(data2){}
	};
	
	
	
	physics::ParticleSpace pSpace;
	physics::RigidBodySpace rbSpace;
	
	Vector<_ParticleCorelationEntry> _p;
	Vector<_RigidBodyCorelationEntry> _rb;
	Vector<Vector<Tuple<glmath_real, glmath_real>>> _pData;
	Vector<Vector<Tuple<glmath_real, glmath_real>>> _rbData;
	
	String workingDirectory;
	
	static char data_location[];
	static char plot_location[];
	
public:
	
	physics::ParticleSpace* createParticleSpace(){
		pSpace=physics::ParticleSpace();
		return &pSpace;
	}
	physics::RigidBodySpace* createRigidBodySpace(){
		rbSpace=physics::RigidBodySpace();
		return &rbSpace;
	}
	
	template <class T>
	void addCorelation(T *object, dataIdentifier x, dataIdentifier y){
		if(typeid(T)==typeid(physics::Particle)){
			if(!_p.size()){
				_p.allocate(MemoryManager::getPageSize());
			}
			_p.pushBack(_ParticleCorelationEntry(object, x, y));
		}else if(typeid(T)==typeid(physics::RigidBody)){
			if(!_rb.size()){
				_rb.allocate(MemoryManager::getPageSize());
			}
			_rb.pushBack(_RigidBodyCorelationEntry(object, x, y));
		}
		
		
		
	}
	
	void setLocation(String& l){
		
	}
	void setLocation(String&& l);
	
	void simulate(glmath_real dt){
		while(true){
			
		}
	}
	
	void writeData(){
		
	}
	
	void plotData(){
		
	}
	
	
};

}
	
#endif /* defined(CRITICAL_FRAMEWORK_STATISTICALSIMULATION_H_) */
