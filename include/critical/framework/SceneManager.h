//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file RenderManager.h
 *	@date 17.05.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a class to hold a virtual scene and make it ready for the
                                                                           
 *  RenderManager.
 *  @ingroup framework
 */
//===========================================================================

//
//  SceneManager.h
//  critical
//
//  Created by Armon Carigiet on 16.05.15.
//
//

#ifndef CRITICAL_FRAMEWORK_SCENEMANAGER_H_
#define CRITICAL_FRAMEWORK_SCENEMANAGER_H_

#include <critical/graphics/Scene.h>
#include <critical/graphics/TriangulatedMesh.h>
#include <critical/core/AlignedList.h>
#include <critical/core/Vector.h>
#include <critical/physics/Particle.h>
#include <critical/physics/RigidBody.h>


namespace critical {
	
//! Class managing a virtual scene.
//! @ingroup framework
class SceneManager{
private:
	void resolveNode(SceneNode *n, Scene* scene);

public:
	AlignedList<glmath_mat4> matrices;
	AlignedList<glmath_mat4> phymatrices;
	
	Vector<TriangulatedMesh> meshes;
	Vector<Material> materials;
	Vector<LightSource> lightSources;
	Vector<Camera> cameras;
	
    //! loads a specific scene into the manager.
    //! @param scene Pointer to the Scene object, which has to be loaded.
    //! @note All the scene's data will be copyed into the manager
	void loadScene(Scene* scene);
	
    //! Gets a specific mesh from the scene.
    //! @param index Mesh index.
    //! @return Pointer to the selected Mesh.
	TriangulatedMesh* getMesh(uint64_t index);
    
	// TODO
	//TriangulatedMesh* getMesh(String identifier);
	
};
	
}

#endif /* defined(CRITICAL_FRAMEWORK_SCENEMANAGER_H_) */