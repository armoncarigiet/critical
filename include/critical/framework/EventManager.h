//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file EventManager.h
 *	@date 17.05.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a class for registering key-listeners.
 *  @ingroup framework
 */
//===========================================================================

#ifndef CRITICAL_FRAMEWORK_EVENTMANAGER_H_
#define CRITICAL_FRAMEWORK_EVENTMANAGER_H_

#include <critical/core/List.h>
#include <critical/core/Tuple.h>
#include <critical/graphics/Window.h>

#include <GLFW/glfw3.h>

namespace critical {
	
	enum KeyboardKeyCode{
		CRKeyCode_Unknowen = -1,
		CRKeyCode_Space = 32,
		CRKeyCode_Apostrophe = 39,
		CRKeyCode_Comma = 44,
		CRKeyCode_Minus = 45,
		CRKeyCode_Period = 46,
		CRKeyCode_Slash = 47,
		
		CRKeyCode_0 = 48,
		CRKeyCode_1 = 49,
		CRKeyCode_2 = 50,
		CRKeyCode_3 = 51,
		CRKeyCode_4 = 52,
		CRKeyCode_5 = 53,
		CRKeyCode_6 = 54,
		CRKeyCode_7 = 55,
		CRKeyCode_8 = 56,
		CRKeyCode_9 = 57,
		
		CRKeyCode_Semicolon = 59,
		CRKeyCode_equal = 61,
		
		CRKeyCode_A = 65,
		CRKeyCode_B = 66,
		CRKeyCode_C = 67,
		CRKeyCode_D = 68,
		CRKeyCode_E = 69,
		CRKeyCode_F = 70,
		CRKeyCode_G = 71,
		CRKeyCode_H = 72,
		CRKeyCode_I = 73,
		CRKeyCode_J = 74,
		CRKeyCode_K = 75,
		CRKeyCode_L = 76,
		CRKeyCode_M = 77,
		CRKeyCode_N = 78,
		CRKeyCode_O = 79,
		CRKeyCode_P = 80,
		CRKeyCode_Q = 81,
		CRKeyCode_R = 82,
		CRKeyCode_S = 83,
		CRKeyCode_T = 84,
		CRKeyCode_U = 85,
		CRKeyCode_V = 86,
		CRKeyCode_W = 87,
		CRKeyCode_X = 88,
		CRKeyCode_Y = 89,
		CRKeyCode_Z = 90,
		
		CRKeyCode_LeftBracket = 91,
		CRKeyCode_BackSlash = 92,
		CRKeyCode_RightBracket = 93,
		CRKeyCode_GraveAccent = 94,
		
		CRKeyCode_World1 = 161,
		CRKeyCode_World2 = 162,
		
		CRKeyCode_Escape = 256,
		CRKeyCode_Enter = 257,
		CRKeyCode_Tab = 258,
		CRKeyCode_BackSpace = 259,
		CRKeyCode_Insert = 260,
		CRKeyCode_Delete = 261,
		CRKeyCode_Right = 262,
		CRKeyCode_Left = 263,
		CRKeyCode_Down = 264,
		CRKeyCode_Up = 265,
		CRKeyCode_PageUp = 266,
		CRKeyCode_PageDown = 267,
		CRKeyCode_Home = 268,
		CRKeyCode_End = 269,
		CRKeyCode_CapsLock = 280,
		CRKeyCode_ScrollLock = 281,
		CRKeyCode_NumLock = 282,
		CRKeyCode_PrintScreen = 283,
		CRKeyCode_Pause = 284,
		
		CRKeyCode_F1 = 290,
		CRKeyCode_F2 = 291,
		CRKeyCode_F3 = 292,
		CRKeyCode_F4 = 293,
		CRKeyCode_F5 = 294,
		CRKeyCode_F6 = 295,
		CRKeyCode_F7 = 296,
		CRKeyCode_F8 = 297,
		CRKeyCode_F9 = 298,
		CRKeyCode_F10 = 299,
		CRKeyCode_F11 = 300,
		CRKeyCode_F12 = 301,
		CRKeyCode_F13 = 302,
		CRKeyCode_F14 = 303,
		CRKeyCode_F15 = 304,
		CRKeyCode_F16 = 305,
		CRKeyCode_F17 = 306,
		CRKeyCode_F18 = 307,
		CRKeyCode_F19 = 308,
		CRKeyCode_F20 = 309,
		CRKeyCode_F21 = 310,
		CRKeyCode_F22 = 311,
		CRKeyCode_F23 = 312,
		CRKeyCode_F24 = 313,
		CRKeyCode_F25 = 314,
		
		CRKeyCode_Kp0 = 320,
		CRKeyCode_Kp1 = 321,
		CRKeyCode_Kp2 = 322,
		CRKeyCode_Kp3 = 323,
		CRKeyCode_Kp4 = 324,
		CRKeyCode_Kp5 = 325,
		CRKeyCode_Kp6 = 326,
		CRKeyCode_Kp7 = 327,
		CRKeyCode_Kp8 = 328,
		CRKeyCode_Kp9 = 329,
		
		CRKeyCode_KpDecimal = 330,
		CRKeyCode_KpDivide = 331,
		CRKeyCode_KpMultiply = 332,
		CRKeyCode_KpSubtract = 333,
		CRKeyCode_KpAdd = 334,
		CRKeyCode_KpEnter = 335,
		CRKeyCode_KpEqual = 336,
		
		CRKeyCode_LeftShift = 340,
		CRKeyCode_LeftControl = 341,
		CRKeyCode_LeftAlt = 342,
		CRKeyCode_LeftSuper = 343,
		CRKeyCode_RightShift = 344,
		CRKeyCode_RightControl = 345,
		CRKeyCode_RightAlt = 346,
		CRKeyCode_RightSuper = 347,
		CRKeyCode_Menu = 348,
	};
	
	enum KeyboardKeyStatus{
		CRKeyRelease = 0,
		CRKeyPress,
		CRKeyRepeat
	};
	
//! Class Manageing the system periferials and processing they're events.
    //! @ingroup framework
template <RenderType _type=CROpenGL>
class EventManager{
public:
	typedef void (*KeyListenerCallback)(KeyboardKeyCode, KeyboardKeyStatus);
private:
	typedef Tuple<KeyboardKeyCode, KeyListenerCallback> KeyListenerRegistration;
	List<KeyListenerRegistration> _r;
	Window<_type> *_w;
public:
	EventManager(): _w(nullptr){}
	EventManager(Window<_type> *w): _w(w){}
	
    
//! Adds a key listener to a specific keycode.
//! @param code Keyboard code.
//! @param callbackFunc Callback function, which will be called if the key has been pressed.
void addKeyListener(KeyboardKeyCode code, KeyListenerCallback callbackFunc){
	_r.pushBack(KeyListenerRegistration(code,callbackFunc));
}
	
//! resolves unhandeled key events.
//! callbackfunctions will be executed directly
void resolveEvents(){
	_w->pollEvents();
	for(List<KeyListenerRegistration>::Iterator it=_r.begin(); !it.isOutOfRange(); it++) {
		KeyboardKeyStatus i;
		if(_type==CROpenGL){
			i=(KeyboardKeyStatus)glfwGetKey(_w->_w, it->_t1);
		}
		if(i==CRKeyRelease || i==CRKeyPress || i==CRKeyRepeat){
			it->_t2(it->_t1, i);
		}
        
    }
}
	
};
	
}

#endif /* defined(CRITICAL_FRAMEWORK_EVENTMANAGER_H_) */