//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file CoreManager.h
 *	@date 14.05.2015.
 *  @author Armon Carigiet
 *
 *  @brief Provides a interface to manage a simulation.
 *  @ingroup framework
 */
//===========================================================================

#ifndef CRITICAL_FRAMEWORK_COREMANAGER_H_
#define CRITICAL_FRAMEWORK_COREMANAGER_H_

#include <critical/core/MemoryManager.h>
#include <critical/core/FileManager.h>
#include <critical/graphics/Window.h>
#include <critical/core/SystemClock.h>

#include <critical/framework/PhysicsManager.h>
#include <critical/framework/SceneManager.h>
#include <critical/framework/RenderManager.h>
#include <critical/framework/EventManager.h>

#include <critical/physics/ParticleSpace.h>
#include <critical/physics/RigidBodySpace.h>
#include <critical/physics/ParticleSystem.h>

namespace critical {
	
//! Main helper class handling a simulation with Critical.
//! Creates, managers and initialises all needed objects to perform a simulation.
//! @ingroup framework
template <RenderType _type=CROpenGL>
class CoreManager{
private:
public:
	Window<_type> _rWindow;
	RenderInformation _rInfo;
	SystemClock<CRMonotonicClock> _clock;
	SceneManager _sManager;
	PhysicsManager _pManager;
	RenderManager<_type> _rManager;
	EventManager<_type> _eManager;
	
	//! Initialise the library's crucial parts to perform a simulation.
	static void init(){
		MemoryManager::init();
		FileManager::init();
		Window<_type>::init();
	}
	
    //! Terminates the simulation and all objects used by it.
	static void terminate(){
		Window<_type>::terminate();
		FileManager::terminate();
		MemoryManager::terminate();
		
	}
	
	CoreManager(): _rWindow(Window<_type>()), _eManager(EventManager<_type>()) {}
	
	//! Generates a Window to display the simulation.
    //! @param title Window title.
    //! @param info Settings and shader locations for the renderengine.
    //! @return Poiner to the Window object.
	Window<_type>* generateRenderWindow(String &title, RenderInformation &info){
		_rWindow=Window<_type>(info);
		_rWindow.openWindow(title);
		return &_rWindow;
	}
	
    //! Generates an Object to manage the physical calculations
    //! @return Poiner to the PhysicsManager object.
	PhysicsManager* generatePhysicsManager(){
		_pManager=PhysicsManager();
		return &_pManager;
	}
    
    //! Generates an Object to handle a loaded scene and it's objects.
    //! @param s Scene to load initially.
    //! @return Poiner to the SceneManager object.
	SceneManager* generateScenmanager(Scene* s){
		_sManager=SceneManager();
		_sManager.loadScene(s);
		//printf("aspect ratio: %f", _sManager.cameras[0]._aspect);
		return &_sManager;
	}
    
    //! Generates an Object managing the system periferials.
    //! @return Poiner to the EventManager object.
	EventManager<_type>* generateEventmanager(){
		_eManager=EventManager<_type>(&_rWindow);
		return &_eManager;
	}
    
    //! Generates an Object managing the renderengine.
    //! @return Poiner to the RenderManager object.
	RenderManager<_type>* generateRenderManager(){
		_rManager=RenderManager<_type>();
		_rManager.linkSceneManager(&_sManager);
		return&_rManager;
	}
    
    //! Generates an Object reading the sistemclock.
	SystemClock<CRMonotonicClock>* generateClock(){
		_clock=SystemClock<CRMonotonicClock>();
		return &_clock;
	}
	
	// control simulation
	void startSimulation(){
		
	}
    
    //! Makes the renderengine iterate forward one frame.
    //! @param dt Frame duration.
	void renderOneFrame(uint64_t dt){
		_rManager.startFrame();
		_rManager.drawObjects();
		_rManager.endFrame();
	}
	
};
	
}

#endif /* defined(CRITICAL_FRAMEWORK_COREMANAGER_H_) */