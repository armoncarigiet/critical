//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file config.h
 *	@date 07.03.2014.
 *  @author Armon Carigiet
 *
 *  @brief Platform detection.
 */
//===========================================================================

#ifndef CRITICAL_CONFIG_H
#define CRITICAL_CONFIG_H

#if defined(_WIN32)
	#ifndef CRITICAL_WINDOWS
		#define CRITICAL_WINDOWS 1
	#endif
	#ifndef CRITICAL_WINAPI
		#define CRITICAL_WINAPI 1
	#endif
#elif defined(__linux__)
	#ifndef CRITICAL_LINUX
		#define CRITICAL_LINUX 1
	#endif
#elif defined(__APPLE__) && defined(__MACH__)
	#ifndef CRITICAL_MACOSX
		#define CRITICAL_MACOSX 1
	#endif
#elif defined(__FreeBSD__)
	#ifndef CRITICAL_FREEBSD
		#define CRITICAL_FREEBSD 1
	#endif
#endif


#if defined(__unix__)
	#ifndef CRITICAL_UNIX
		#define CRITICAL_UNIX 1
	#endif
#endif

#if defined(__unix__) || (defined(__APPLE__) && defined(__MACH__))
	#ifndef CRITICAL_POSIX
		#define CRITICAL_POSIX 1
	#endif
#endif

#endif