//
//  main.cpp
//
//
//  Created by Armon Carigiet on 15.11.14.
//
//

#include <critical/framework/CoreManager.h>
#include <critical/core/Importer.h>
#include <critical/framework/PhysicsManager.h>
#include <critical/framework/SceneManager.h>
#include <critical/framework/RenderManager.h>
#include <critical/framework/EventManager.h>

using namespace critical;

void printInertiaTensor(glmath_mat3 it){
    printf("| %f %f %f |\n| %f %f %f |\n| %f %f %f |\n", it.m[0], it.m[1], it.m[2], it.m[3], it.m[4], it.m[5], it.m[6], it.m[7], it.m[8]);
}


int main(){
    
    CoreManager<CROpenGL>::init();
    CoreManager<CROpenGL> manager;
    
    
    //========================
    
    //    critical::physics::RigidBody rb("Rb1", 1, {{0,0,0}}, {{0,0,0}}, {{0,0,0}}, {{0,0,0}});
    //
    //    rb.setInertiaTensor(glmath_mat3_create(2.0/3.0,-1.0/4.0,-1.0/4.0,-1.0/4.0,2.0/3.0,-1.0/4.0,-1.0/4.0,-1.0/4.0,2.0/3.0));
    //
    //    printf("| %f %f %f |\n| %f %f %f |\n| %f %f %f |\n", rb.InertiaTensor.m[0], rb.InertiaTensor.m[1], rb.InertiaTensor.m[2], rb.InertiaTensor.m[3], rb.InertiaTensor.m[4], rb.InertiaTensor.m[5], rb.InertiaTensor.m[6], rb.InertiaTensor.m[7], rb.InertiaTensor.m[8]);
    //
    //    rb.recalculateInertiaTensor(glmath_vec3_create(0.5, 0.5, 0.5), glmath_quat_create_identity());
    //
    //    printf("| %f %f %f |\n| %f %f %f |\n| %f %f %f |\n", rb.InertiaTensor.m[0], rb.InertiaTensor.m[1], rb.InertiaTensor.m[2], rb.InertiaTensor.m[3], rb.InertiaTensor.m[4], rb.InertiaTensor.m[5], rb.InertiaTensor.m[6], rb.InertiaTensor.m[7], rb.InertiaTensor.m[8]);
    
    
    //========================
    
    
    
    
    Importer importer;
    MemoryManager::ConnectedPointer<Scene> p=importer.loadSceneFromFile("/Users/armoncarigiet/Desktop/test2.dae");
    
    RenderInformation info;
    
    SceneManager *sceneManager=manager.generateScenmanager(&(*p));
    PhysicsManager *physicsManager=manager.generatePhysicsManager();
    String s("Window");
    Window<CROpenGL> *Window=manager.generateRenderWindow(s, info);
    //Window->openWindow(s);
    RenderManager<CROpenGL> *renderManager=manager.generateRenderManager();
    SystemClock<CRMonotonicClock> *clock=manager.generateClock();
    
    
    EventManager<CROpenGL> *eventManager=manager.generateEventmanager();
    String vs("/Users/armoncarigiet/Documents/Maturaarbeit/critical/shaders/vertex/vertex_classic_lighting.glsl");
    String fs("/Users/armoncarigiet/Documents/Maturaarbeit/critical/shaders/fragment/fragment_point.glsl");
    renderManager->loadShader(CRVertexShader, vs);
    renderManager->loadShader(CRFragmentShader, fs);
    renderManager->linkShaders();
    
    
    TriangulatedMesh *t1=sceneManager->getMesh(0);
    TriangulatedMesh *t2=sceneManager->getMesh(1);
    TriangulatedMesh *t3=sceneManager->getMesh(2);
    TriangulatedMesh *t4=sceneManager->getMesh(3);
    
    //    Camera c;
    //    c.lookAt({{56, 56, 56}}, {{0,3,3}}, {{0,0,1}});
    //    c.setAspect(480/360);
    //    c.setFarPlane(100.0f);
    //    c.setNearPlane(1.0f);
    //    c.setHorizontalFOV(M_PI/18*11);
    //    renderManager->setCustomCamera(c);
    
    
    renderManager->setCamera(0);
    
    renderManager->registerUniformMatrix(CRModelMatrix, String("model_matrix"));
    renderManager->registerUniformMatrix(CRCameraMatrix, String("camera_matrix"));
    renderManager->registerUniformMatrix(CRProjectionMatix, String("perspective_matrix"));
    renderManager->registerUniformMatrix(CRNormalMatrix, String("normal_matrix"));
    
    
    // material attributes
    
    String str("materials");
    
    renderManager->registerUniformMaterialAttributeVector(CRMaterialAmbient, str, String("ambient"));
    renderManager->registerUniformMaterialAttributeVector(CRMaterialEmission, str, String("emission"));
    renderManager->registerUniformMaterialAttributeVector(CRMaterialDiffuse, str, String("diffuse"));
    renderManager->registerUniformMaterialAttributeVector(CRMaterialSpecular, str, String("specular"));
    renderManager->registerUniformMaterialAttributeVector(CRMaterialShininess, str, String("shininess"));
    renderManager->registerMaterialIndexLocation(String("material_index"));
    
    
    // light properties
    
    String l("lights");
    
    renderManager->registerUniformLightAttributeVector(CREnabled, l, String("isEnabled"));
    renderManager->registerUniformLightAttributeVector(CRLocal, l, String("isLocal"));
    renderManager->registerUniformLightAttributeVector(CRSpot, l, String("isSpot"));
    renderManager->registerUniformLightAttributeVector(CRAmbient, l, String("ambient"));
    renderManager->registerUniformLightAttributeVector(CRColor, l, String("color"));
    renderManager->registerUniformLightAttributeVector(CRPosition, l, String("position"));
    renderManager->registerUniformLightAttributeVector(CRHalfVector, l, String("halfVector"));
    renderManager->registerUniformLightAttributeVector(CRConeDirection, l, String("coneDirection"));
    //renderManager->registerUniformLightAttributeVector(CRFallofAngle, l, String("spotCosCutoff"));		// will pass cosinus to shader
    //renderManager->registerUniformLightAttributeVector(CRFallofExponent, l, String("spotExponent"));
    renderManager->registerUniformLightAttributeVector(CRConstantAttenuation, l, String("constantAttenuation"));
    renderManager->registerUniformLightAttributeVector(CRLinearAttenuation, l, String("linearAttenuation"));
    renderManager->registerUniformLightAttributeVector(CRQuadraticAttenuation, l, String("quadraticAttenuation"));
    
    // other variables
    
    glmath_real strength=1;
    
    renderManager->registerUniformVariable("strength", &strength);
    
    renderManager->resolveUniformLocations();
    
    
    //renderManager->finalizeData();
    
    
    
    // ==============================
    // physics
    // ==============================
    
    
    physics::RigidBodySpace* rigidBodySpace=physicsManager->createRigidBodySpace();
    
    physics::RigidBody *rb1=rigidBodySpace->addObject(physics::RigidBody("Rigid Body 1", 14, glmath_vec3_create(0,0,0), glmath_vec3_create(0, 0, 0), glmath_vec3_create(0, 0, 0), glmath_vec3_create(0, 0, 0)));
    
    physics::PointMassSystem sys;
    
    sys.addObject(physics::PointMass(glmath_vec3_create(0.5f, 0, 0), 1));
    sys.addObject(physics::PointMass(glmath_vec3_create(0, 0.5f, 0), 1));
    sys.addObject(physics::PointMass(glmath_vec3_create(0, 0, 0.5f), 1));
    
    sys.addObject(physics::PointMass(glmath_vec3_create(-0.5f, 0, 0), 1));
    sys.addObject(physics::PointMass(glmath_vec3_create(0, -0.5, 0), 1));
    sys.addObject(physics::PointMass(glmath_vec3_create(0, 0, -0.5f), 1));
    
    sys.addObject(physics::PointMass(glmath_vec3_create(0.5f, 0.5f, 0.5f), 1));
    sys.addObject(physics::PointMass(glmath_vec3_create(0.5f, 0.5f, -0.5f), 1));
    sys.addObject(physics::PointMass(glmath_vec3_create(0.5f, -0.5f, 0.5f), 1));
    sys.addObject(physics::PointMass(glmath_vec3_create(0.5f, -0.5f, -0.5f), 1));
    sys.addObject(physics::PointMass(glmath_vec3_create(-0.5f, 0.5f, 0.5f), 1));
    sys.addObject(physics::PointMass(glmath_vec3_create(-0.5f, 0.5f, -0.5f), 1));
    sys.addObject(physics::PointMass(glmath_vec3_create(-0.5f, -0.5f, 0.5f), 1));
    sys.addObject(physics::PointMass(glmath_vec3_create(-0.5f, -0.5f, -0.5f), 1));
    
    
    
    
    glmath_mat3 in=sys.calculateInertiaTensor();
    
    //printInertiaTensor(in);
    
    rb1->setInertiaTensor(in);
    rb1->setCeneterOfMass(sys.calculateCenterOfMass());
    
    physics::RigidBodySpinGenerator *spgen=rigidBodySpace->addForceGenerator(physics::RigidBodySpinGenerator(glmath_vec3_create(-4, 1, 1), glmath_vec3_create(1, 0, 0), false), true);
    
    
    
    
    spgen->addObject(rb1);
    
    physicsManager->addLink(&sceneManager->meshes[0], rb1);
    
    
    //rigidBodySpace->startSimulation();
    //particleSpace->startSimulation();
    
    //manager.startSimulation();
    
    uint64_t dt=CRITICAL_IDEAL_FRAME_NANOSECONDS;
    
    long framecounter=0;
    uint64_t timeaccumulator=0;
    
    while(true){
        clock->start();
        manager.renderOneFrame(dt);
        
        //particleSpace->applyForces();
        rigidBodySpace->applyForces();
        
        glmath_real t=dt/GLMATH_GIGA;
        //particleSpace->integrate(t);
        rigidBodySpace->integrate(t);
        
        physicsManager->updateModelMatrices();
        
        Window->swapBuffers();
        Window->pollEvents();
        
        if(glfwGetKey(Window->_w, GLFW_KEY_SPACE)==GLFW_PRESS){
            //particleSpace->startSimulation();
            rigidBodySpace->startSimulation();
        }
        
        if (glfwGetKey(Window->_w, GLFW_KEY_UP)==GLFW_PRESS) {
            glmath_vec3 right=glmath_vec3_cross(sceneManager->cameras[0]._forward, sceneManager->cameras[0]._up);
            sceneManager->cameras[0]._up=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(M_PI/180, right), sceneManager->cameras[0]._up));
            sceneManager->cameras[0]._forward=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(M_PI/180, right), sceneManager->cameras[0]._forward));
        }
        if(glfwGetKey(Window->_w, GLFW_KEY_DOWN)==GLFW_PRESS){
            glmath_vec3 right=glmath_vec3_cross(sceneManager->cameras[0]._forward, sceneManager->cameras[0]._up);
            sceneManager->cameras[0]._up=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(-M_PI/180, right), sceneManager->cameras[0]._up));
            sceneManager->cameras[0]._forward=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(-M_PI/180, right), sceneManager->cameras[0]._forward));
        }
        if(glfwGetKey(Window->_w, GLFW_KEY_RIGHT)==GLFW_PRESS){
            sceneManager->cameras[0]._forward=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(M_PI/180, sceneManager->cameras[0]._up), sceneManager->cameras[0]._forward));
        }
        if(glfwGetKey(Window->_w, GLFW_KEY_LEFT)==GLFW_PRESS){
            sceneManager->cameras[0]._forward=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(-M_PI/180, sceneManager->cameras[0]._up), sceneManager->cameras[0]._forward));
        }
        if(glfwGetKey(Window->_w, GLFW_KEY_S)==GLFW_PRESS){
            sceneManager->cameras[0]._position.x+=0.1;
        }
        if(glfwGetKey(Window->_w, GLFW_KEY_A)==GLFW_PRESS){
            sceneManager->cameras[0]._position.x-=0.1;
        }
        if(glfwGetKey(Window->_w, GLFW_KEY_W)==GLFW_PRESS){
            sceneManager->cameras[0]._position.y+=0.1;
        }
        if(glfwGetKey(Window->_w, GLFW_KEY_S)==GLFW_PRESS){
            sceneManager->cameras[0]._position.y-=0.1;
        }
        
        
        if (glfwGetKey(Window->_w, GLFW_KEY_ESCAPE)==GLFW_PRESS){
            glfwSetWindowShouldClose (Window->_w, 1);
        }
        
        
        dt=clock->stop();
        
        timeaccumulator+=dt;
        
        framecounter++;
        
        if((glmath_real)timeaccumulator*GLMATH_NANO>=1.0f){
            glmath_real fps=framecounter/((glmath_real)timeaccumulator*GLMATH_NANO);
            char buffer[50];
            sprintf(buffer, "Window - fps: %f", fps);
            glfwSetWindowTitle(manager._rWindow._w, buffer);
            framecounter=0;
            timeaccumulator=0;
        };
        
        //	MemoryManager::init();
        //	
        //	//physics::SimpleParticleGravityGenerator f1(glmath_vec3_create(0, 9.81, 0));
        //	
        //	physics::ForceGeneratorList<physics::SimpleParticleGravityGenerator> fgenlist;
        //	
        //	physics::ForceGeneratorList<physics::SimpleParticleGravityGenerator>::Iterator FIt=fgenlist.pushBack(physics::SimpleParticleGravityGenerator(glmath_vec3_create(0, 9.81, 0)));
        //	
        //	physics::Particle p1(String("Particle 1"), 10, glmath_vec3_create(0, 4, 0), glmath_vec3_create(0, 0, 0));
        //	
        //	
        //	physics::SimpleParticleGravityGenerator *ptr=&FIt;
        //	
        //	ptr->addObject(&p1);
        //	
        //	ptr->applyForce();
        //
        //	glmath_real t=CRITICAL_IDEAL_FRAME;
        //	
        //	p1.integrate(t);
        //	
        //	printf("");
        
        
        
    }
}

