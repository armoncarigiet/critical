//
//  main.cpp
//  Critical Test 1
//
//  Created by Armon Carigiet on 05.03.15.
//  Copyright (c) 2015 Armon Carigiet. All rights reserved.
//

#include <iostream>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <critical/core/FileManager.h>
//#include <critical/core/ColladaHelper.h>
#include <critical/core/Importer.h>
#include <critical/graphics/TriangulatedMesh.h>


#include <GL/glew.h>
#include <GLFW/glfw3.h>

struct LightProperties {
	bool isEnabled;
	bool isLocal;
	bool isSpot;
	glmath_vec3 ambient;
	glmath_vec3 color;
	glmath_vec3 position;
	glmath_vec3 halfVector;
	glmath_vec3 coneDirection;
	float spotCosCutoff;
	float spotExponent;
	float constantAttenuation;
	float linearAttenuation;
	float quadraticAttenuation;
};

struct MaterialProperties{
	glmath_vec3 ambient;
	glmath_vec3 emission;
	glmath_vec3 diffuse;
	glmath_vec3 specular;
	float shininess;
};



void parseError(void* ctx, const char *msg, ...){
	char string[256];
	va_list arg_ptr;
	va_start(arg_ptr, msg);
	vsnprintf(string, 256, msg, arg_ptr);
	va_end(arg_ptr);
	std::cout << string << std::endl;
	exit(-1);
}

void print_array3f(critical::Array<glmath_vec3> a){
	for(int i=0; i<a.size(); i++) {
		printf("%d:{%f,%f,%f}\n", i, a[i].x, a[i].y, a[i].z);
	}
}

void print_arrayi(critical::Array<int> a){
	for(int i=0; i<a.size(); i++) {
		printf("%d, ",a[i]);
	}
}

int main(int argc, const char * argv[]) {
	
	errno=0;
	//printf("\n\n\n\nerror: %s\n\n\n\n",strerror(errno));
	
	critical::MemoryManager::init();
	critical::FileManager::init();
	
	critical::Importer importer;
	
	critical::MemoryManager::ConnectedPointer<critical::Scene> scene=importer.loadSceneFromFile("/Users/armoncarigiet/Desktop/testfile.dae");
	
	//critical::FileManager::FileStream vs=critical::FileManager::openStream("/Users/armoncarigiet/Documents/Maturaarbeit/Renderbibliothek/shaders/vertex/vertex_basic.glsl", "r");
	critical::FileManager::FileStream vs=critical::FileManager::openStream("/Users/armoncarigiet/Documents/Maturaarbeit/Renderbibliothek/shaders/vertex/vertex_classic_lighting.glsl", "r");
	critical::MemoryManager::ConnectedPointer<char> vertex_shader_source=vs.loadFile();
	//critical::FileManager::FileStream fs=critical::FileManager::openStream("/Users/armoncarigiet/Documents/Maturaarbeit/Renderbibliothek/shaders/fragment/fragment_basic.glsl", "r");
	critical::FileManager::FileStream fs=critical::FileManager::openStream("/Users/armoncarigiet/Documents/Maturaarbeit/Renderbibliothek/shaders/fragment/fragment_classic_lighting.glsl", "r");
	critical::MemoryManager::ConnectedPointer<char> fragment_shader_source=fs.loadFile();
	char* vs_pointer=&vertex_shader_source;
	char* fs_pointer=&fragment_shader_source;
	
	critical::FileManager::closeStream(vs);
	critical::FileManager::closeStream(fs);
	
	
	
	
	critical::Scene *s=&scene;
	
	critical::SceneNode &n1=s->rootNode._children.front();
	critical::SceneNode &n2=s->rootNode._children.back();
	
	critical::TriangulatedMesh m1, m2;
	m1.triangulateMesh(s->meshes[0]);			// somewhere an error; TODO
	m2.triangulateMesh(s->meshes[1]);
	
	
//	for(int i; i<m1.positions.size(); i++) {
//		printf("%d: {%f,%f,%f}\n", i, m1.positions[i].x, m1.positions[i].y, m1.positions[i].z);
//	}
//	
//	printf("\n\n");
//	
//	for(int i; i<m2.positions.size(); i++) {
//		printf("%d: {%f,%f,%f}\n", i, m2.positions[i].x, m2.positions[i].y, m2.positions[i].z);
//	}
//	
	
	
	
	
	// Material Properties
	MaterialProperties mp[1];
	
	critical::Array<float> &ambient=s->materials[0].d["ambient"];
	critical::Array<float> &emission=s->materials[0].d["emission"];
	critical::Array<float> &diffuse=s->materials[0].d["diffuse"];
	critical::Array<float> &specular=s->materials[0].d["specular"];
	critical::Array<float> &shininess=s->materials[0].d["shininess"];
	
	mp[0].ambient={ambient[0], ambient[1], ambient[2]};
	mp[0].emission={emission[0], emission[1], emission[2]};
	mp[0].diffuse={diffuse[0], diffuse[1], diffuse[2]};
	mp[0].specular={specular[0], specular[1], specular[2]};
	mp[0].shininess=shininess[0];
	
	//mp[0].ambient={{0,0,1}};

	
	
	
	// Light Properties
	LightProperties lp[1];
	
	lp[0].isEnabled=true;
	//critical::LightSource* lightsource=&s->lights[0];
	//printf("\nsize: %zu align: %d\n", sizeof(critical::SceneNode), GLMATH_PACK);
	
	//n1._tmat=glmath_mat4_create_identity();
	
	//glmath_mat4 m=n1.getMatrix();
	
	glmath_vec4 c=s->lights[0]._color.get();
	lp[0].color={{c.r, c.g, c.b}};
	lp[0].ambient={{1.0f, 1.0f, 1.0f}};
	lp[0].position=s->lights[0].position;
	
	critical::LightSource::LightSourceType type=s->lights[0]._type;
	if(type!=critical::LightSource::CRAmbientLight && type!=critical::LightSource::CRDirectionalLight) {
		lp[0].isLocal=true;
		lp[0].constantAttenuation=s->lights[0]._attr["constant_attenuation"];
		lp[0].linearAttenuation=s->lights[0]._attr["linear_attenuation"];
		lp[0].quadraticAttenuation=s->lights[0]._attr["quadratic_attenuation"];
	}else{
		lp[0].isLocal=false;
		lp[0].halfVector=glmath_vec3_norm(glmath_vec3_add(s->lights[0].position, s->lights[0].direction));
	}
	if(type==critical::LightSource::CRSpotLight){
		lp[0].isSpot=true;
		lp[0].spotExponent=s->lights[0]._attr["falloff_exponent"];
		lp[0].spotCosCutoff=glmath_cos(s->lights[0]._attr["falloff_exponent"]/180*M_PI);
		lp[0].coneDirection=s->lights[0].direction;
	}

	
	Camera *camera=&scene->cameras[0];
	
	
	
	printf("");
//	printf("meshPositions:\n");
//	print_array3f(s->meshes[0]._vertices);
//	printf("\nindices:\n");
//	print_arrayi(s->meshes[0]._primitives[0].vindex);
//	printf("\n\n");
//	
//	critical::TriangulatedMesh tmesh;
//	
//	tmesh.triangulateMesh(s->meshes[0]);
//	
//	
//	
//	printf("Triangulated Mesh:\n");
//	print_array3f(tmesh.positions);
	
	
	
	if(!glfwInit()){
		std::cout<<"error initialising glfw";
		exit(-1);
	}
	
#if defined(__APPLE__) && defined(__MACH__)
	glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif
	
	GLFWwindow* window = glfwCreateWindow (640, 480, "Critical Test", NULL, NULL);
	if (!window) {
		fprintf (stderr, "ERROR: could not open window with GLFW3\n");
		glfwTerminate();
		return 1;
	}
	glfwMakeContextCurrent(window);
	
	glewExperimental = GL_TRUE;
	glewInit();
	
	const GLubyte* renderer = glGetString (GL_RENDERER);
	const GLubyte* version = glGetString (GL_VERSION);
	printf ("Renderer: %s\n", renderer);
	printf ("OpenGL version supported %s\n", version);
	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	
	
	//points
	unsigned int m1_p_vbo = 0;
	glGenBuffers (1, &m1_p_vbo);
	glBindBuffer (GL_ARRAY_BUFFER, m1_p_vbo);
	glBufferData (GL_ARRAY_BUFFER, 3*m1.positions.size()*sizeof(glmath_real), m1.positions.data(), GL_STATIC_DRAW);
	
	unsigned int m2_p_vbo = 0;
	glGenBuffers (1, &m2_p_vbo);
	glBindBuffer (GL_ARRAY_BUFFER, m2_p_vbo);
	glBufferData (GL_ARRAY_BUFFER, 3*m2.positions.size()*sizeof(glmath_real), m2.positions.data(), GL_STATIC_DRAW);
	
	//normals
	
	unsigned int m1_n_vbo = 0;
	glGenBuffers (1, &m1_n_vbo);
	glBindBuffer (GL_ARRAY_BUFFER, m1_n_vbo);
	glBufferData (GL_ARRAY_BUFFER, 3*m1.normals.size()*sizeof(glmath_real), m1.normals.data(), GL_STATIC_DRAW);
	
	unsigned int m2_n_vbo = 0;
	glGenBuffers (1, &m2_n_vbo);
	glBindBuffer (GL_ARRAY_BUFFER, m2_n_vbo);
	glBufferData (GL_ARRAY_BUFFER, 3*m2.normals.size()*sizeof(glmath_real), m2.normals.data(), GL_STATIC_DRAW);
	
	
	unsigned int vao1 = 0;
	glGenVertexArrays(1, &vao1);
	glBindVertexArray(vao1);
	
	glBindBuffer(GL_ARRAY_BUFFER, m1_p_vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glBindBuffer(GL_ARRAY_BUFFER, m1_n_vbo);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	
	unsigned int vao2 = 0;
	glGenVertexArrays(1, &vao2);
	glBindVertexArray(vao2);
	
	glBindBuffer(GL_ARRAY_BUFFER, m2_p_vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glBindBuffer(GL_ARRAY_BUFFER, m2_n_vbo);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	
	//glmath_vec3 eye_pos={{3,3,3}};
	
	//glmath_mat4 cam=glmath_mat4_create_lookat_vec3(eye_pos, {{0,0,0}}, {{0,0,1}});
	//glmath_mat4 proj=glmath_mat4_create_perspective(50.0f/180*M_PI, (float)640/(float)480, 0.1f, 100.0f);
	glmath_mat4 proj=glmath_mat4_create_perspective(camera->_fov/180*M_PI, camera->_aspect, camera->_znear, camera->_zfar);
	
//	unsigned int lights_buffer = 0;
//	glGenBuffers (1, &lights_buffer);
//	glBindBuffer (GL_UNIFORM_BUFFER, lights_buffer);
//	glBufferData (GL_UNIFORM_BUFFER, 3*m1.normals.size()*sizeof(glmath_real), m1.normals.data(), GL_STATIC_DRAW);
//	
//	
//	GLuint ubol = 0;
//	glGenBuffers(1, &ubol);
//	glBindBuffer(GL_UNIFORM_BUFFER, ubol);
//	glBufferData(GL_UNIFORM_BUFFER, sizeof(LightProperties), &(lp[0]), GL_DYNAMIC_DRAW);
//	glBindBuffer(GL_UNIFORM_BUFFER, 0);
//	
//	GLuint ubom = 0;
//	glGenBuffers(1, &ubom);
//	glBindBuffer(GL_UNIFORM_BUFFER, ubom);
//	glBufferData(GL_UNIFORM_BUFFER, sizeof(LightProperties), &(lp[0]), GL_DYNAMIC_DRAW);
//	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	
	unsigned int vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vs_pointer, NULL);
	glCompileShader(vertex_shader);
	unsigned int fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fs_pointer, NULL);
	glCompileShader(fragment_shader);
	
	unsigned int shader_programme = glCreateProgram();
	glAttachShader(shader_programme, vertex_shader);
	glAttachShader(shader_programme, fragment_shader);
	glLinkProgram(shader_programme);
	
	// debug
	char buffer[60000];
	glGetShaderInfoLog(vertex_shader, 60000, NULL, buffer);
	std::cout<<"\n"<<buffer<<"\n";
	glGetShaderInfoLog(fragment_shader, 60000, NULL, buffer);
	std::cout<<"\n"<<buffer<<"\n";
	// debug end
	
	
	//GLuint mvp_matrix_location = glGetUniformLocation(shader_programme, "mvp_matrix");
	//GLuint mv_matrix_location = glGetUniformLocation(shader_programme, "mv_matrix");
	//GLuint n_matrix_location = glGetUniformLocation(shader_programme, "normal_matrix");
	
	//GLuint lpindex=glGetUniformBlockIndex(shader_programme, "lights");
	//GLuint mpindex=glGetUniformBlockIndex(shader_programme, "materials");
	
	//GLint size;
	
	//glGetActiveUniformBlockiv(shader_programme, lpindex, GL_UNIFORM_BLOCK_DATA_SIZE, &size);
	
	//printf("host: %zu\tdevice: %d", sizeof(LightProperties),size);
	
	
	
	int strength=1;
	
	int material_index=0;
	
	
	
	int invertable=false;
	
	glmath_mat3 mat=glmath_mat3_create(n1._tmat->m00, n1._tmat->m01, n1._tmat->m02, n1._tmat->m10, n1._tmat->m11, n1._tmat->m12, n1._tmat->m20, n1._tmat->m21, n1._tmat->m22);
	
	glmath_mat3 nmatrix1=glmath_mat3_transpose(glmath_mat3_invert(mat, &invertable));
	
	if(!invertable){
		throw 1;
	}
	
	mat=glmath_mat3_create(n2._tmat->m00, n2._tmat->m01, n2._tmat->m02, n2._tmat->m10, n2._tmat->m11, n2._tmat->m12, n2._tmat->m20, n2._tmat->m21, n2._tmat->m22);

	
	glmath_mat3 nmatrix2=glmath_mat3_transpose(glmath_mat3_invert(mat, &invertable));
	
	if(!invertable){
		throw 1;
	}
	
	
	
	
	glmath_real camX=3;
	glmath_real camY=3;
	glmath_real camZ=3;
	
	//glmath_vec3 fr=glmath_vec3_sub( glmath_vec3_create(0, 0.5, 0), glmath_vec3_create(camX, camY, camZ));
	
	
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	//glFrontFace(GL_CCW);
	
	glmath_real v=glmath_vec3_dot(camera->_up, camera->_forward);
	
	
	
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	
	do{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		//glmath_mat4 cam1=glmath_mat4_create_view(glmath_vec3_create(camX, camY, camZ), fr, glmath_vec3_create(0, 1, 0));
		//glmath_mat4 cam4=glmath_mat4_create_lookat_vec3(glmath_vec3_create(camX, camY, camZ), glmath_vec3_create(0, 0, 0.5), glmath_vec3_create(0, 0, 1));
		
		//glmath_mat4 cam4=glmath_mat4_create_lookat_vec3(camera->_position, glmath_vec3_create(0, 0.5, 0), camera->_up);
		glmath_mat4 cam4=glmath_mat4_create_view(camera->_position, camera->_forward, camera->_up);

		glUseProgram (shader_programme);
		
		GLuint modelmatrix_location = glGetUniformLocation (shader_programme, "model_matrix");
		GLuint projmatrix_location = glGetUniformLocation (shader_programme, "perspective_matrix");
		GLuint cameramatrix_location = glGetUniformLocation (shader_programme, "camera_matrix");
		
		GLuint strength_location=glGetUniformLocation(shader_programme, "strength");
		GLuint eyed_location=glGetUniformLocation(shader_programme, "eye_direction");
		GLuint material_index_location=glGetUniformLocation(shader_programme, "material_index");
		GLuint nmatrix_location=glGetUniformLocation(shader_programme, "normal_matrix");
		
		
		// - properties -
		GLuint lock;
		// lights
		
		lock=glGetUniformLocation(shader_programme, "lights[0].isEnabled");
		glUniform1i(lock, lp[0].isEnabled);
		lock=glGetUniformLocation(shader_programme, "lights[0].isLocal");
		glUniform1i(lock, lp[0].isLocal);
		lock=glGetUniformLocation(shader_programme, "lights[0].isSpot");
		glUniform1i(lock, lp[0].isSpot);
		lock=glGetUniformLocation(shader_programme, "lights[0].ambient");
		glUniform3fv(lock, 1, lp[0].ambient.v);
		lock=glGetUniformLocation(shader_programme, "lights[0].color");
		glUniform3fv(lock, 1, lp[0].color.v);
		lock=glGetUniformLocation(shader_programme, "lights[0].position");
		glUniform3fv(lock, 1, lp[0].position.v);
		lock=glGetUniformLocation(shader_programme, "lights[0].halfVector");
		glUniform3fv(lock, 1, lp[0].halfVector.v);
		lock=glGetUniformLocation(shader_programme, "lights[0].coneDirection");
		glUniform3fv(lock, 1, lp[0].coneDirection.v);
		lock=glGetUniformLocation(shader_programme, "lights[0].spotCosCutoff");
		glUniform1f(lock, lp[0].spotCosCutoff);
		lock=glGetUniformLocation(shader_programme, "lights[0].spotExponent");
		glUniform1f(lock, lp[0].spotExponent);
		lock=glGetUniformLocation(shader_programme, "lights[0].constantAttenuation");
		glUniform1f(lock, lp[0].constantAttenuation);
		lock=glGetUniformLocation(shader_programme, "lights[0].linearAttenuation");
		glUniform1f(lock, lp[0].linearAttenuation);
		lock=glGetUniformLocation(shader_programme, "lights[0].quadraticAttenuation");
		glUniform1f(lock, lp[0].quadraticAttenuation);
		
		// materials
		
		lock=glGetUniformLocation(shader_programme, "materials[0].ambient");
		glUniform3fv(lock, 1, mp[0].ambient.v);
		lock=glGetUniformLocation(shader_programme, "materials[0].emission");
		glUniform3fv(lock, 1, mp[0].emission.v);
		lock=glGetUniformLocation(shader_programme, "materials[0].diffuse");
		glUniform3fv(lock, 1, mp[0].diffuse.v);
		lock=glGetUniformLocation(shader_programme, "materials[0].specular");
		glUniform3fv(lock, 1, mp[0].specular.v);
		lock=glGetUniformLocation(shader_programme, "materials[0].shininess");
		glUniform1f(lock, mp[0].shininess);
		
		
		
		// - properties end -
		
		//glBindBufferBase(GL_UNIFORM_BUFFER, lpindex, ubol);
		//glBindBufferBase(GL_UNIFORM_BUFFER, mpindex, ubom);
		
		
		glUniform1i(strength_location, strength);
		
		glUniform1i(material_index_location, material_index);
		//glmath_vec3 direction=glmath_vec3_sub(glmath_vec3_create(camX, camY, camZ), glmath_vec3_create(0, 0.5, 0));
		glmath_vec3 direction=glmath_vec3_sub(glmath_vec3_create(0, 0.5, 0), glmath_vec3_create(camX, camY, camZ));
		glUniform3fv(eyed_location, 1, direction.v);
		
		
		glUniformMatrix4fv(projmatrix_location, 1, GL_FALSE, proj.m);
		glUniformMatrix4fv(cameramatrix_location, 1, GL_FALSE, cam4.m);
		
		
		//glmath_mat4 mvp=glmath_mat4_mul(glmath_mat4_mul(proj, cam4), *n1._tmat);
		//glUniformMatrix4fv(mvp_matrix_location, 1, GL_FALSE, mvp.m);
		
		//glmath_mat4 mv=glmath_mat4_mul(cam4, *n1._tmat);
		//glUniformMatrix4fv(mv_matrix_location, 1, GL_FALSE, mv.m);
		
		//glmath_mat4 mmat=glmath_mat4_transpose(*n1._tmat);
		glmath_mat4 mmat=*n1._tmat;
		glUniformMatrix4fv(modelmatrix_location, 1, GL_FALSE, mmat.m);
		glUniformMatrix3fv(nmatrix_location, 1, GL_FALSE, nmatrix1.m);
		
//		glUniformMatrix4fv(modelmatrix_location, 1, GL_FALSE, n1._tmat.m);
//		glUniformMatrix4fv(projmatrix_location, 1, GL_FALSE, proj.m);
//		glUniformMatrix4fv(cameramatrix_location, 1, GL_FALSE, cam4.m);
		glBindVertexArray(vao1);
		glDrawArrays(GL_TRIANGLES, 0, (GLsizei)m1.positions.size()*3);
		
		//mvp=glmath_mat4_mul(glmath_mat4_mul(proj, cam4), *n2._tmat);
		//glUniformMatrix4fv(mvp_matrix_location, 1, GL_FALSE, mvp.m);

		//mv=glmath_mat4_mul(cam4, *n2._tmat);
		//glUniformMatrix4fv(mv_matrix_location, 1, GL_FALSE, mv.m);
		
		//mmat=glmath_mat4_transpose(*n2._tmat);
		mmat=*n2._tmat;
		glUniformMatrix4fv(modelmatrix_location, 1, GL_FALSE, mmat.m);
		glUniformMatrix3fv(nmatrix_location, 1, GL_FALSE, nmatrix2.m);
		
		glBindVertexArray(vao2);
		glDrawArrays(GL_TRIANGLES, 0, (GLsizei)m2.positions.size()*3);
		
		
		
		glfwSwapBuffers(window);
	
		glfwPollEvents();
		
		
		if (glfwGetKey(window, GLFW_KEY_UP)==GLFW_PRESS) {
			//mat=glmath_mat4_mul(mat, glmath_mat4_create_xrotation(angle));
			camZ+=0.1;
		}
		if(glfwGetKey(window, GLFW_KEY_DOWN)==GLFW_PRESS){
			//mat=glmath_mat4_mul(mat, glmath_mat4_create_xrotation(-angle));
			camZ-=0.1;
		}
		if(glfwGetKey(window, GLFW_KEY_RIGHT)==GLFW_PRESS){
			camX+=0.1;
			
		}
		if(glfwGetKey(window, GLFW_KEY_LEFT)==GLFW_PRESS){
			camX-=0.1;
			camera->_forward=glmath_mat3_mul_vec3(glmath_mat3_create_rotation(-M_PI/180, camera->_up), camera->_forward);
		}
		if(glfwGetKey(window, GLFW_KEY_S)==GLFW_PRESS){
			//mat=glmath_mat4_mul(mat, glmath_mat4_create_zrotation(angle));
		}
		if(glfwGetKey(window, GLFW_KEY_A)==GLFW_PRESS){
			//mat=glmath_mat4_mul(mat, glmath_mat4_create_zrotation(-angle));
		}
		if(glfwGetKey(window, GLFW_KEY_W)==GLFW_PRESS){
			camY+=0.1;
		}
		if(glfwGetKey(window, GLFW_KEY_S)==GLFW_PRESS){
			camY-=0.1;
		}
		
		if (glfwGetKey(window, GLFW_KEY_ESCAPE)==GLFW_PRESS){
			glfwSetWindowShouldClose (window, 1);
		}
		
		
	}while(!glfwWindowShouldClose(window));
	
	
	
	
	printf("");

	
	
//	for(int i=0; i<size; i++){
//		printf("%c",buf[i]);
//	}
	
	
	
	
	
	
	
    return 0;
}
