//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *  @file Color.cpp
 *	@date 15.01.2015.
 *  @author Armon Carigiet
 *  @see Color.h
 *  @ingroup graphics
 */
//===========================================================================

#include <critical/graphics/Color.h>
namespace critical{

void Color::set(glmath_real r, glmath_real g, glmath_real b, glmath_real a){
	_r=r;
	_g=g;
	_b=b;
	_a=a;
}
	
void Color::set(glmath_vec4 c){
	_r=c.r;
	_g=c.g;
	_b=c.b;
	_a=c.a;
}
	
glmath_vec4 Color::get(){
	return glmath_vec4_create(_r, _g, _b, _a);
}
	
void Color::mix(glmath_vec4 c){
	_r=(_r+c.r)/2;
	_g=(_g+c.g)/2;
	_b=(_b+c.b)/2;
	_a=(_a+c.a)/2;
}
	
void Color::mix(glmath_real r, glmath_real g, glmath_real b, glmath_real a){
	_r=(_r+r)/2;
	_g=(_g+g)/2;
	_b=(_b+b)/2;
	_a=(_a+a)/2;
}

}