//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *  @file TriangulatedMesh.cpp
 *	@date 15.03.2015.
 *  @author Armon Carigiet
 *  @see TriangulatedMesh.cpp
 *  @ingroup graphics
 */
//===========================================================================

#include <critical/graphics/TriangulatedMesh.h>

namespace critical{
	bool TriangulatedMesh::triangulateMesh(Mesh &mesh, glmath_mat4 *mat, glmath_mat4 *phymat){
		
		_mmat=mat;
		_phymat=phymat;
		
		bool meshpos=mesh._vertices.size()!=0;
		bool meshnorm=mesh._normals.size()!=0;
		bool meshtan=mesh._tangents.size()!=0;
		bool meshbitan=mesh._bitangents.size()!=0;
		bool meshtcord=mesh._texturecoords.size()!=0;
		
		int pcount=mesh._primitives.size();
		for(int i=0; i<pcount; i++){
			size_t indicesCount=mesh._primitives[i].vindex.size()/mesh._primitives[i].vertexCount;
			Primitive *primitive=&mesh._primitives[i];
			if(primitive->_hasPositions && !meshpos){
				throw 1;
			}
			if(primitive->_hasNormals && !meshnorm){
				throw 1;
			}
			if(primitive->_hasTangents && !meshtan){
				throw 1;
			}
			if(primitive->_hasBitangents && !meshbitan){
				throw 1;
			}
			if(primitive->_hasTextureCoords && !meshtcord){
				throw 1;
			}
			
			if(primitive->type==CRTriangles){
				if(primitive->_hasPositions){
					this->positions.expand(indicesCount);
				}
				if(primitive->_hasNormals){
					this->normals.expand(indicesCount);
				}
				if(primitive->_hasTangents){
					this->tangents.expand(indicesCount);
				}
				if(primitive->_hasBitangents){
					this->bitangents.expand(indicesCount);
				}
				if(primitive->_hasTextureCoords){
					this->textureCoords.expand(indicesCount);
				}
				
				for(int w=0; w<indicesCount; w+=primitive->vertexCount){
					//int in=p;
					if(primitive->_hasPositions){
						this->positions.pushBack(mesh.s.vertices[primitive->vindex[w]]);
					}
					if(primitive->_hasNormals){
						this->normals.pushBack(mesh.s.normals[primitive->vindex[w+1]]);
					}
					if(primitive->_hasTangents){
						this->tangents.pushBack(mesh.s.tangents[primitive->vindex[w+2]]);
					}
					if(primitive->_hasBitangents){
						this->bitangents.pushBack(mesh.s.bitangents[primitive->vindex[w+3]]);
					}
					if(primitive->_hasTextureCoords){
						this->textureCoords.pushBack(mesh.s.texturecoords[primitive->vindex[w+4]]);
					}
				}
			}else if(primitive->type==CRPolylist){
				
				size_t calcTriangles=0;
				for(int k=0; k<primitive->vcount.size(); k++) {
					calcTriangles+=(primitive->vcount[k]-2);
				}
				if(primitive->_hasPositions){
					this->positions.expand(3*calcTriangles);
					//_pos.setSize(_pos.capacity());
				}
				if(primitive->_hasNormals){
					this->normals.expand(3*calcTriangles);
					//_norm.setSize(_norm.capacity());
				}
				if(primitive->_hasTangents){
					this->tangents.expand(3*calcTriangles);
					//_tan.setSize(_tan.capacity());
				}
				if(primitive->_hasBitangents){
					this->bitangents.expand(3*calcTriangles);
					//_bitan.setSize(_bitan.capacity());
				}
				if(primitive->_hasTextureCoords){
					this->textureCoords.expand(3*calcTriangles);
					//_texc.setSize(_texc.capacity());
				}
				
//				printf("\n");
//				size_t s=primitive->vindex[463];
//				printf("index: %d",s);
//				printf("\n");
//				printf("normal: {%f,%f,%f}", mesh._normals[primitive->vindex[462+1]].x, mesh._normals[primitive->vindex[462+1]].y, mesh._normals[primitive->vindex[462+1]].z);
//				printf("\n");
//				for(int l=0; l<primitive->vindex.size(); l++) {
//					printf("%d ", primitive->vindex[l]);
//				}
				
				size_t off=0;
				for(int k=0; k<primitive->vcount.size(); k++){
					if (primitive->vcount[k]<3) {
						return false;
					}else if(primitive->vcount[k]==3){
						for(int w=off; w<off+(3*primitive->vertexCount); w+=primitive->vertexCount){
							if(primitive->_hasPositions){
								this->positions.pushBack(mesh._vertices[primitive->vindex[w]]);
							}
							if(primitive->_hasNormals){
								this->normals.pushBack(mesh._normals[primitive->vindex[w+1]]);
							}
							if(primitive->_hasTangents){
								this->tangents.pushBack(mesh._tangents[primitive->vindex[w+2]]);
							}
							if(primitive->_hasBitangents){
								this->bitangents.pushBack(mesh._bitangents[primitive->vindex[w+3]]);
							}
							if(primitive->_hasTextureCoords){
								this->textureCoords.pushBack(mesh._texturecoords[primitive->vindex[w+4]]);
							}
						}
						off+=(3*primitive->vertexCount);
						
					}else if(primitive->vcount[k]>3){
//						for(int w=off; w<off+(primitive->vcount[k]*primitive->vertexCount); w+=primitive->vertexCount){
						
						for(int r=0; r<(primitive->vcount[k]-2); r++) {
							if(primitive->_hasPositions){
								this->positions.pushBack(mesh.s.vertices[primitive->vindex[off]]);		// simple triangulation
								this->positions.pushBack(mesh.s.vertices[primitive->vindex[off+(3*r*primitive->vertexCount)]]);
								this->positions.pushBack(mesh.s.vertices[primitive->vindex[off+(3*r*primitive->vertexCount)]]);
							}
							if(primitive->_hasNormals){
								this->normals.pushBack(mesh.s.normals[primitive->vindex[off+1]]);
								this->normals.pushBack(mesh.s.normals[primitive->vindex[off+(3*r*primitive->vertexCount)+1]]);
								this->normals.pushBack(mesh.s.normals[primitive->vindex[off+(3*r*primitive->vertexCount)+1]]);
							}
							if(primitive->_hasTangents){
								this->tangents.pushBack(mesh.s.tangents[primitive->vindex[off+2]]);
								this->tangents.pushBack(mesh.s.tangents[primitive->vindex[off+(3*r*primitive->vertexCount)+2]]);
								this->tangents.pushBack(mesh.s.tangents[primitive->vindex[off+(3*r*primitive->vertexCount)+2]]);
							}
							if(primitive->_hasBitangents){
								this->bitangents.pushBack(mesh.s.bitangents[primitive->vindex[off+3]]);
								this->bitangents.pushBack(mesh.s.bitangents[primitive->vindex[off+(3*r*primitive->vertexCount)+3]]);
								this->bitangents.pushBack(mesh.s.bitangents[primitive->vindex[off+(3*r*primitive->vertexCount)+3]]);
							}
							if(primitive->_hasTextureCoords){
								this->textureCoords.pushBack(mesh.s.texturecoords[primitive->vindex[off+4]]);
								this->textureCoords.pushBack(mesh.s.texturecoords[primitive->vindex[off+(3*r*primitive->vertexCount)+4]]);
								this->textureCoords.pushBack(mesh.s.texturecoords[primitive->vindex[off+(3*r*primitive->vertexCount)+4]]);
							}
						}
						off+=3*(primitive->vcount[k]-2)*primitive->vertexCount;

					}
				}
				
			}else{
				throw 1;
			}
		}
		
//		this->positions=_pos.getArray();
//		this->normals=_norm.getArray();
//		this->tangents=_tan.getArray();
//		this->bitangents=_bitan.getArray();
//		this->textureCoords=_texc.getArray();
		
		return true;
	}
}