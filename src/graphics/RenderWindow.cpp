//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *  @file RenderWindow.cpp
 *	@date 22.03.2015.
 *  @author Armon Carigiet
 *  @see RenderWindow.h
 *  @ingroup graphics
 */
//===========================================================================

#include <critical/graphics/RenderWindow.h>
#include <critical/core/FileManager.h>
#include <critical/core/Clock.h>
#include <critical/core/SystemClock.h>
#include <iostream>

namespace critical {
	
	class memoryManagerAnalyser{
	private:
		//...
	public:
		void* getPointerTable(){
			return MemoryManager::pointerTable;
		}
		
		size_t getTableSize(){
			return critical::MemoryManager::table_size;
		}
		size_t getTableUsedSize(){
			return critical::MemoryManager::used_table_size;
		}
		size_t getTableEntryCount(){
			return critical::MemoryManager::tableEntryCount;
		}
		size_t getTablePages(){
			return critical::MemoryManager::table_pages;
		}
		size_t getPageSize(){
			return critical::MemoryManager::pageSize;
		}
		bool isInizialised(){
			return critical::MemoryManager::initialized;
		}
		void* getTableLocation(){
			return critical::MemoryManager::pointerTable;
		}
		bool ptrIsAllocated(MemoryManager::ConnectedPointer<void> &ptr){
			return ptr.allocated;
		}
		template <class T>
		void* getPtr(MemoryManager::ConnectedPointer<T> &ptr){
			return ptr.ptr;
		}
		
		void* getHeapBase(){
			return MemoryManager::heap_base_address;
		}
		void* getHeapBreak(){
			return MemoryManager::heap_break;
		}
		void* getHeapLimit(){
			return MemoryManager::heap_limit;
		}
		_heap_meta_block* getHeapUsedLimit(){
			return MemoryManager::heap_used_limit;
		}
		
		template<class T>
		int getRefCount(SharedPointer<T> &ptr){
			return *ptr.refcounter;
		}
		
		template<class T>
		_heap_meta_block* getHeapMetaBlock(MemoryManager::ConnectedPointer<T> &ptr){
			return ptr.block;
		}
		
		size_t getMetaBlockSize(){
			return MemoryManager::heap_meta_block_size;
		}
		
		
		template<class T>
		memoryLocationCode getPLocation(MemoryManager::ConnectedPointer<T> &ptr){
			return ptr.location;
		}
		template<class T>
		int getPtrEindex(MemoryManager::ConnectedPointer<T> &ptr){
			return ptr.eindex;
		}
		template<class T>
		int isAllocated(MemoryManager::ConnectedPointer<T> &ptr){
			return ptr.allocated;
		}
		
	};

	uint32_t RenderWindow::defWindowHeight=480;
	uint32_t RenderWindow::defWindowWidth=640;
	bool RenderWindow::_initialized=false;
	GLuint RenderWindow::shader_programme=0;
	
	void RenderWindow::init(){
		
		//TODO ...
		
		//if(information->attributes.isKey()){
		
		//}
		
	}
	void RenderWindow::terminate(){
		glfwTerminate();
	};
	
	bool RenderWindow::openWindow(RenderInformation* information, String title){
		//printf("\n\nworks!\n\n");
		
		
		if(!_initialized){
			if(!glfwInit()){
				throw 1;
			}
		}
		
	#if defined(CRITICAL_MACOSX)
		glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
		glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	#endif
		
		_w=glfwCreateWindow(defWindowWidth, defWindowHeight, title.c_str(), NULL, NULL);
		if(!_w){
			throw 1;
			return false;
		}
		glfwMakeContextCurrent(_w);
		
		if(!_initialized) {
			if(information->type==CROpenGL){
			
				glewExperimental=true;
				glewInit();
				
				
				
				//To change
				
			}else{
				throw 1;
			}
			
			// Todo (trivial) create a manager for shaders and windows
			String &vs=information->attributes.elementForKey(CRVertexShader);
			String &fs=information->attributes.elementForKey(CRFragmentShader);
			
			FileManager::FileStream s1=FileManager::openStream(vs.c_str(), "r");
			FileManager::FileStream s2=FileManager::openStream(fs.c_str(), "r");
			
			size_t size1=0;
			size_t size2=0;
			
			MemoryManager::ConnectedPointer<char> vertex_source=s1.loadFile(&size1);
			MemoryManager::ConnectedPointer<char> fragment_source=s2.loadFile(&size2);
			
			char* vs_pointer=&vertex_source;
			char* fs_pointer=&fragment_source;
			
			
//			printf("\n\n");
//			for (int k=0; k<size1; k++){
//				printf("%c", vertex_source[k]);
//			}
//			printf("\n\n");
//			for (int k=0; k<size1; k++){
//				printf("%c", fragment_source[k]);
//			}
			
			
			GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
			glShaderSource(vertex_shader, 1, &vs_pointer, NULL);
			glCompileShader(vertex_shader);
			GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
			glShaderSource(fragment_shader, 1, &fs_pointer, NULL);
			glCompileShader(fragment_shader);
			
			shader_programme = glCreateProgram();
			glAttachShader(shader_programme, vertex_shader);
			glAttachShader(shader_programme, fragment_shader);
			glLinkProgram(shader_programme);
			
			// debug
			char buffer[60000];
			glGetShaderInfoLog(vertex_shader, 60000, NULL, buffer);
			std::cout<<"\n"<<buffer<<"\n";
			glGetShaderInfoLog(fragment_shader, 60000, NULL, buffer);
			std::cout<<"\n"<<buffer<<"\n";
			// debug end

			_initialized=true;
			
		}
		return true;
	}
	
	struct RenderObject{
		GLuint vao;
		SceneNode *node;
		TriangulatedMesh mesh;
		uint32_t material_index;
		GLuint p_vbo;
		GLuint n_vbo;
		GLuint t_vbo;
		GLuint bt_vbo;
		GLuint tex_vbo;
	};
	
	
	bool RenderWindow::renderScene(Scene *scene){
		Vector<RenderObject> renderObjects;
		Vector<LightProperties> lights;
		Vector<MaterialProperties> materials;
		
		List<SceneNode>::Iterator it=scene->rootNode._children.begin();
		
		for(int i=0; i<scene->materials.size(); i++) {
			materials.pushBack(MaterialProperties());
			
			critical::Array<glmath_real> &ambient=scene->materials[i]._attributes[CRMaterialAmbient];
			critical::Array<glmath_real> &emission=scene->materials[i]._attributes[CRMaterialEmission];
			critical::Array<glmath_real> &diffuse=scene->materials[i]._attributes[CRMaterialDiffuse];
			critical::Array<glmath_real> &specular=scene->materials[i]._attributes[CRMaterialSpecular];
			critical::Array<glmath_real> &shininess=scene->materials[i]._attributes[CRMaterialShininess];
			
			materials[i].ambient={{ambient[0], ambient[1], ambient[2]}};
			materials[i].emission={{emission[0], emission[1], emission[2]}};
			materials[i].diffuse={{diffuse[0], diffuse[1], diffuse[2]}};
			materials[i].specular={{specular[0], specular[1], specular[2]}};
			materials[i].shininess=shininess[0];
		}
		
		for(int i=0; i<scene->lights.size(); i++) {
			lights.pushBack(LightProperties());
			
			lights[i].isEnabled=true;
			glmath_vec4 c=scene->lights[i]._color.get();
			lights[i].color={{c.r, c.g, c.b}};
			lights[i].ambient={{1.0f, 1.0f, 1.0f}};
			lights[i].position=scene->lights[i].position;
			
			critical::LightSource::LightSourceType type=scene->lights[i]._type;
			if(type!=critical::LightSource::CRAmbientLight && type!=critical::LightSource::CRDirectionalLight) {
				lights[i].isLocal=true;
				lights[i].constantAttenuation=scene->lights[i]._attributes[CRConstantAttenuation];
				lights[i].linearAttenuation=scene->lights[i]._attributes[CRLinearAttenuation];
				lights[i].quadraticAttenuation=scene->lights[i]._attributes[CRQuadraticAttenuation];
			}else{
				lights[i].isLocal=false;
				lights[i].halfVector=glmath_vec3_norm(glmath_vec3_add(scene->lights[i].position, scene->lights[i].direction));
			}
			if(type==critical::LightSource::CRSpotLight){
				lights[i].isSpot=true;
				lights[i].spotExponent=scene->lights[i]._attributes[CRFallofExponent];
				lights[i].spotCosCutoff=glmath_cos(scene->lights[i]._attributes[CRFallofAngle]/180*M_PI);
				lights[i].coneDirection=scene->lights[i].direction;
			}

		}
//		Vector<LightPropertyIdentifiers> light_identifiers(materials.size());
//		for(int i=0; i<light_identifiers.size(); i++){
//			
//			
//			
//			char buffer[50];
//			sprintf(buffer, "lights[%d].isEnabled", 1);
//			light_identifiers[i].enabled=String(buffer);
//			bzero(buffer, 50);
//			char buffer[50];
//			sprintf(buffer, "lights[%d].isLocal", 1);
//			light_identifiers[i].enabled=String(buffer);
//			bzero(buffer, 50);
//			
//			
//			
//			lock=glGetUniformLocation(shader_programme, "lights[0].isEnabled");
//			glUniform1i(lock, lights[i].isEnabled);
//			lock=glGetUniformLocation(shader_programme, "lights[0].isLocal");
//			glUniform1i(lock, lights[i].isLocal);
//			lock=glGetUniformLocation(shader_programme, "lights[0].isSpot");
//			glUniform1i(lock, lights[i].isSpot);
//			lock=glGetUniformLocation(shader_programme, "lights[0].ambient");
//			glUniform3fv(lock, 1, lights[i].ambient.v);
//			lock=glGetUniformLocation(shader_programme, "lights[0].color");
//			glUniform3fv(lock, 1, lights[i].color.v);
//			lock=glGetUniformLocation(shader_programme, "lights[0].position");
//			glUniform3fv(lock, 1, lights[i].position.v);
//			lock=glGetUniformLocation(shader_programme, "lights[0].halfVector");
//			glUniform3fv(lock, 1, lights[i].halfVector.v);
//			lock=glGetUniformLocation(shader_programme, "lights[0].coneDirection");
//			glUniform3fv(lock, 1, lights[i].coneDirection.v);
//			lock=glGetUniformLocation(shader_programme, "lights[0].spotCosCutoff");
//			glUniform1f(lock, lights[i].spotCosCutoff);
//			lock=glGetUniformLocation(shader_programme, "lights[0].spotExponent");
//			glUniform1f(lock, lights[i].spotExponent);
//			lock=glGetUniformLocation(shader_programme, "lights[0].constantAttenuation");
//			glUniform1f(lock, lights[i].constantAttenuation);
//			lock=glGetUniformLocation(shader_programme, "lights[0].linearAttenuation");
//			glUniform1f(lock, lights[i].linearAttenuation);
//			lock=glGetUniformLocation(shader_programme, "lights[0].quadraticAttenuation");
//			glUniform1f(lock, lights[i].quadraticAttenuation);
//			
//		}

		
		
		for(int i=0; i<scene->rootNode._children.size(); i++){
			
			if(i){
    			++it;
			}
			
			renderObjects.pushBack(RenderObject());
			
			renderObjects[i].mesh=TriangulatedMesh();
			
			renderObjects[i].mesh.triangulateMesh(scene->meshes[it->mindex[0]], it->_tmat);
			
			int mindex=scene->meshes[it->mindex[0]]._primitives[0].material_index;
			
			renderObjects[i].material_index=mindex;
			
			TriangulatedMesh *m=&renderObjects[i].mesh;
			
			SceneNode *testnode=&it;
			
			
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LESS);
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			glFrontFace(GL_CCW);
			
			//points
			
			bool hasPoints=false;
			bool hasNormals=false;
			bool hasTangents=false;
			bool hasBitangents=false;
			bool hasTextureCoords=false;
		
			//memoryManagerAnalyser analyser;
			//_mem_table_entry* pagetable=static_cast<_mem_table_entry*>(analyser.getPointerTable());
			
			//_mem_table_entry* entry=&pagetable[5];
			
			// position
			if(m->positions.size()){
				printf("positions");
				glGenBuffers (1, &renderObjects[i].p_vbo);
				glBindBuffer (GL_ARRAY_BUFFER, renderObjects[i].p_vbo);
				glBufferData (GL_ARRAY_BUFFER, 3*m->positions.size()*sizeof(glmath_real), m->positions.data(), GL_STATIC_DRAW);
				hasPoints=true;
			}
			
			// normals
			if(m->normals.size()){
				printf("normals");
				glGenBuffers (1, &renderObjects[i].n_vbo);
				glBindBuffer (GL_ARRAY_BUFFER, renderObjects[i].n_vbo);
				glBufferData (GL_ARRAY_BUFFER, 3*m->normals.size()*sizeof(glmath_real), m->normals.data(), GL_STATIC_DRAW);
				hasNormals=true;
			}
			
			// tangents
			if(m->tangents.size()){
				printf("tangents");
				glGenBuffers (1, &renderObjects[i].t_vbo);
				glBindBuffer (GL_ARRAY_BUFFER, renderObjects[i].t_vbo);
				glBufferData (GL_ARRAY_BUFFER, 3*m->tangents.size()*sizeof(glmath_real), m->tangents.data(), GL_STATIC_DRAW);
				hasTangents=true;
			}
			
			// bitangents
			if(m->bitangents.size()){
				printf("bitangents");
				glGenBuffers (1, &renderObjects[i].bt_vbo);
				glBindBuffer (GL_ARRAY_BUFFER, renderObjects[i].bt_vbo);
				glBufferData (GL_ARRAY_BUFFER, 3*m->bitangents.size()*sizeof(glmath_real), m->bitangents.data(), GL_STATIC_DRAW);
				hasBitangents=true;
			}
			
			// textureCoords
			if(m->textureCoords.size()){
				printf("textcoords");
				glGenBuffers (1, &renderObjects[i].tex_vbo);
				glBindBuffer (GL_ARRAY_BUFFER, renderObjects[i].tex_vbo);
				glBufferData (GL_ARRAY_BUFFER, 3*m->textureCoords.size()*sizeof(glmath_real), m->textureCoords.data(), GL_STATIC_DRAW);
				hasTextureCoords=true;
			}
			
			
			GLuint vao = 0;
			glGenVertexArrays(1, &vao);
			glBindVertexArray(vao);
			
			//int k=0;
			
			if(hasPoints){
				glBindBuffer(GL_ARRAY_BUFFER, renderObjects[i].p_vbo);
				glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
				glEnableVertexAttribArray(0);
				//k++;
			}
			if(hasNormals){
				glBindBuffer(GL_ARRAY_BUFFER, renderObjects[i].n_vbo);
				glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
				glEnableVertexAttribArray(1);
				//k++;
			}
			if(hasTangents){
				glBindBuffer(GL_ARRAY_BUFFER, renderObjects[i].t_vbo);
				glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);
				glEnableVertexAttribArray(2);
				//k++;
			}
			if(hasBitangents){
				glBindBuffer(GL_ARRAY_BUFFER, renderObjects[i].bt_vbo);
				glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, NULL);
				glEnableVertexAttribArray(3);
				//k++;
			}
			if(hasTextureCoords){
				glBindBuffer(GL_ARRAY_BUFFER, renderObjects[i].tex_vbo);
				glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 0, NULL);
				glEnableVertexAttribArray(4);
				//k++;
			}
			
			renderObjects[i].vao=vao;
			renderObjects[i].node=&it;
			//renderObjects[i].vertices=m->positions.size();
		}
		
		//TriangulatedMesh *mesh=&renderObjects[0].mesh;
		
		//GLuint* vaoindex=&renderObjects[1].vao;
		
		glmath_mat4 proj, view;
		glmath_mat3 normal;
		//glmath_mat4 proj=glmath_mat4_create_perspective(50.0f/180*M_PI, (float)640/(float)480, 0.1f, 100.0f);
		
		//glmath_real camX=4;
		//glmath_real camY=4;
		//glmath_real camZ=4;
		
		// to
		float strength=0.5f;
		
		Camera *camera=&scene->cameras[0];
		
		
		//uint64_t start=0, end=0;
		uint64_t dt=0;
		
		
		
		SystemClock<CRMonotonicClock> clock;
		
		
		// Main Render Loop
		// TODO Aoutsource in Thread
		do{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			
			clock.start();
			
			// matrix calculations
			view=glmath_mat4_create_view(camera->_position, camera->_forward, camera->_up);
			proj=glmath_mat4_create_perspective(camera->_fov/180*M_PI, camera->_aspect, camera->_znear, camera->_zfar);
			
			glUseProgram(shader_programme);
			
			//uniform locations
			GLuint modelmatrix_location = glGetUniformLocation (shader_programme, "model_matrix");
			GLuint projmatrix_location = glGetUniformLocation (shader_programme, "perspective_matrix");
			GLuint cameramatrix_location = glGetUniformLocation (shader_programme, "camera_matrix");
			GLuint strength_location=glGetUniformLocation(shader_programme, "strength");
			GLuint eyed_location=glGetUniformLocation(shader_programme, "eye_direction");
			GLuint material_index_location=glGetUniformLocation(shader_programme, "material_index");
			GLuint nmatrix_location=glGetUniformLocation(shader_programme, "normal_matrix");
			GLuint lock;
			
			// eye direction
			glmath_vec3 direction=glmath_vec3_neg(scene->cameras[0]._forward);
			glUniform3fv(eyed_location, 1, direction.v);
			
			
			// matrices
			glUniformMatrix4fv(projmatrix_location, 1, GL_FALSE, proj.m);
			glUniformMatrix4fv(cameramatrix_location, 1, GL_FALSE, view.m);
			
			
			// TODO move out of renderloop or solve with uniform buffers
			for(int i=0; i<lights.size(); i++){
				char buffer[50];
				sprintf(buffer, "lights[%d].isEnabled", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform1i(lock, lights[i].isEnabled);
				bzero(buffer, 50);
				sprintf(buffer, "lights[%d].isLocal", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform1i(lock, lights[i].isLocal);
				bzero(buffer, 50);
				sprintf(buffer, "lights[%d].isSpot", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform1i(lock, lights[i].isSpot);
				bzero(buffer, 50);
				sprintf(buffer, "lights[%d].ambient", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform3fv(lock, 1, lights[i].ambient.v);
				bzero(buffer, 50);
				sprintf(buffer, "lights[%d].color", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform3fv(lock, 1, lights[i].color.v);
				bzero(buffer, 50);
				sprintf(buffer, "lights[%d].position", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform3fv(lock, 1, lights[i].position.v);
				bzero(buffer, 50);
				sprintf(buffer, "lights[%d].halfVector", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform3fv(lock, 1, lights[i].halfVector.v);
				bzero(buffer, 50);
				sprintf(buffer, "lights[%d].coneDirection", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform3fv(lock, 1, lights[i].coneDirection.v);
				bzero(buffer, 50);
				sprintf(buffer, "lights[%d].spotCosCutoff", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform1f(lock, lights[i].spotCosCutoff);
				bzero(buffer, 50);
				sprintf(buffer, "lights[%d].spotExponent", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform1f(lock, lights[i].spotExponent);
				bzero(buffer, 50);
				sprintf(buffer, "lights[%d].constantAttenuation", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform1f(lock, lights[i].constantAttenuation);
				bzero(buffer, 50);
				sprintf(buffer, "lights[%d].linearAttenuation", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform1f(lock, lights[i].linearAttenuation);
				bzero(buffer, 50);
				sprintf(buffer, "lights[%d].quadraticAttenuation", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform1f(lock, lights[i].quadraticAttenuation);
				bzero(buffer, 50);
			}
			
			for(int i=0; i<materials.size(); i++) {
				char buffer[50];
				sprintf(buffer, "materials[%d].ambient", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform3fv(lock, 1, materials[i].ambient.v);
				bzero(buffer, 50);
				sprintf(buffer, "materials[%d].emission", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform3fv(lock, 1, materials[i].emission.v);
				bzero(buffer, 50);
				sprintf(buffer, "materials[%d].diffuse", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform3fv(lock, 1, materials[i].diffuse.v);
				bzero(buffer, 50);
				sprintf(buffer, "materials[%d].specular", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform3fv(lock, 1, materials[i].specular.v);
				bzero(buffer, 50);
				sprintf(buffer, "materials[%d].shininess", i);
				lock=glGetUniformLocation(shader_programme, buffer);
				glUniform1f(lock, materials[i].shininess);
				bzero(buffer, 50);
				
			}
			
			glUniform1f(strength_location, strength);
			
			for(int s=0; s<renderObjects.size(); s++){
				int invertable=0;
				
				glmath_mat3 mv=glmath_mat3_mul(glmath_mat4_get_mat3(*renderObjects[s].node->_tmat), glmath_mat4_get_mat3(view));
				
				normal=glmath_mat3_transpose(glmath_mat3_invert(mv, &invertable));
				if(!invertable)
					throw 1;
				
				glUniform1i(material_index_location, renderObjects[s].material_index);
				//glmath_real sh=materials[0].shininess;
				glUniformMatrix3fv(nmatrix_location, 1, GL_FALSE, normal.m);
				glUniformMatrix4fv(modelmatrix_location, 1, GL_FALSE, renderObjects[s].node->_tmat->m);
				glBindVertexArray(renderObjects[s].vao);
				glDrawArrays(GL_TRIANGLES, 0, (GLsizei)3*renderObjects[s].mesh.positions.size());
			}
			
			glfwSwapBuffers(_w);
			glfwPollEvents();
	
			if (glfwGetKey(_w, GLFW_KEY_UP)==GLFW_PRESS) {
				glmath_vec3 right=glmath_vec3_cross(camera->_forward, camera->_up);
				camera->_up=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(M_PI/180, right), camera->_up));
				camera->_forward=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(M_PI/180, right), camera->_forward));
			}
			if(glfwGetKey(_w, GLFW_KEY_DOWN)==GLFW_PRESS){
				glmath_vec3 right=glmath_vec3_cross(camera->_forward, camera->_up);
				camera->_up=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(-M_PI/180, right), camera->_up));
				camera->_forward=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(-M_PI/180, right), camera->_forward));
			}
			if(glfwGetKey(_w, GLFW_KEY_RIGHT)==GLFW_PRESS){
				camera->_forward=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(M_PI/180, camera->_up), camera->_forward));
			}
			if(glfwGetKey(_w, GLFW_KEY_LEFT)==GLFW_PRESS){
				camera->_forward=glmath_vec3_norm(glmath_mat3_mul_vec3(glmath_mat3_create_rotation(-M_PI/180, camera->_up), camera->_forward));
			}
			if(glfwGetKey(_w, GLFW_KEY_S)==GLFW_PRESS){
				camera->_position.x+=0.1;
			}
			if(glfwGetKey(_w, GLFW_KEY_A)==GLFW_PRESS){
				camera->_position.x-=0.1;
			}
			if(glfwGetKey(_w, GLFW_KEY_W)==GLFW_PRESS){
				camera->_position.y+=0.1;
			}
			if(glfwGetKey(_w, GLFW_KEY_S)==GLFW_PRESS){
				camera->_position.y-=0.1;
			}
			
			if (glfwGetKey(_w, GLFW_KEY_ESCAPE)==GLFW_PRESS){
				glfwSetWindowShouldClose (_w, 1);
			}
			
			dt=clock.stop();
			printf("dt: %llu seconds:%f\n", dt, dt*GLMATH_NANO);
			dt=0;
			
		}while (!glfwWindowShouldClose(_w));
		
		return true;
	}
	
}
