//
//  tests.cpp
//
//
//  Created by Armon Carigiet on 15.11.14.
//
//

#include <GL/glew.h>

#include <critical/core/MemoryManager.h>
#include <critical/core/FileManager.h>
#include <critical/core/Importer.h>
#include <critical/graphics/RenderWindow.h>


int main(){
	
	critical::MemoryManager::init();
	critical::FileManager::init();
	
	critical::Importer importer;
	
	critical::MemoryManager::ConnectedPointer<critical::Scene> scene=importer.loadSceneFromFile("/Users/armoncarigiet/Desktop/smoothcone.dae");
	
	
	critical::Scene *s=&scene;
	
	critical::RenderInformation info;
	
	info.type=critical::CROpenGL;
	info.attributes.addElement(critical::CRVertexShader, critical::String("/Users/armoncarigiet/Documents/Maturaarbeit/Renderbibliothek/shaders/vertex/vertex_classic_lighting.glsl"));
	info.attributes.addElement(critical::CRFragmentShader, critical::String("/Users/armoncarigiet/Documents/Maturaarbeit/Renderbibliothek/shaders/fragment/fragment_point.glsl"));
	
	
	//critical::RenderWindow::init();
	
	critical::RenderWindow window;
	
	window.openWindow(&info ,critical::String("Test"));
	
	window.renderScene(&scene);
	
	
}


