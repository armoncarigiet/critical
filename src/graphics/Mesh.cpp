//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *  @file Mesh.cpp
 *	@date 15.01.2015.
 *  @author Armon Carigiet
 *  @see Mesh.h
 *  @ingroup graphics
 */
//===========================================================================

#include <critical/graphics/Mesh.h>

namespace critical{

//bool Mesh::resolveIndices(){
//	size_t primitiveCount=_primitives.size();
//	for(int i=0; i<primitiveCount; i++) {
//		if(_primitives[i].type==CRPolylist){
//			
//		}else if(_primitives[i].type==CRTriangles){
//		
//		}else{
//			throw 1;
//		}
//	}
//}

//! not working! Do not use!
bool Mesh::triangulate(){
	size_t primitiveCount=_primitives.size();
	Array<int> p;
	int triangleCount=0;
	
	for(int i=0; i<primitiveCount; ++i) {
		if(_primitives[i].type==CRTriangles){
//			Array<glmath_real> p;
//			Array<glmath_real> p;
//			Array<glmath_real> p;
//			Array<glmath_real> p;
//Array
		}else if(_primitives[i].type==CRPolylist){
			int current=0;
			int currt=0;
			for (int k=0; k<_primitives[i].count; ++k) {
				int vcount=_primitives[i].vcount[k];
				if(vcount<3){
					return false;
				}else if(vcount==3){
					triangleCount++;
					for(int r=0; r<3; ++r){
						for (int w=0; w<_primitives[i].vertexCount; ++w) {
							p[currt]=_primitives[i].vindex[current];
							++current;
							++currt;
						}
					}
				}else if(vcount>3){
					triangleCount+=vcount-2;
					int first=_primitives[i].vindex[current];
					current++;
					for(int r=0; r<vcount-2; ++r){
						for (int w=0; w<_primitives[i].vertexCount; w++) {
							p[currt]=first;
							p[currt+1]=_primitives[i].vindex[current];
							p[currt+2]=_primitives[i].vindex[current+1];
							++current;
							currt+=3;
						}
					}
				}
			}
			return true;
		}else if(_primitives[i].type==CRPolygons){
			//TODO
			throw 1;
		}else if(_primitives[i].type==CRTriangleStrips){
			//TODO
			throw 1;
		}else if(_primitives[i].type==CRTriangleFans){
			//TODO
			throw 1;
		}
	}
	return false;
}
	
}