//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Thread.cpp
 *	@date 11.07.2014.
 *  @author Armon Carigiet
 *  @see Thread.h
 */
//===========================================================================

#include <critical/core/Thread.h>

//thread

critical::Thread::Thread(){
	this->allocated=false;
}

critical::Thread::Thread(Thread::ThreadFunc f, void *arg){
	this->init(f, arg);
}

critical::Thread::~Thread(){
	this->terminate();
}

bool critical::Thread::init(Thread::ThreadFunc f, void *arg){
	if(!allocated){
	#if defined(CRITICAL_POSIX)
		pthread_create(&_t, NULL, f, arg);
	#elif defined(CRITICAL_WINAPI)
		_t=CreateThread(NULL, 0, f, arg, 0, NULL);
	#endif
		return true;
	}else{
		return false;
	}
}

bool critical::Thread::terminate(){
	if(allocated){
	#if defined(CRITICAL_POSIX)
		pthread_cancel(_t);
	#elif defined(CRITICAL_USE_WINAPI)
		TerminateThread(&_t);
	#endif
		return true;
	}else{
		return false;
	}
}

void critical::Thread::detach(){
#if defined(CRITICAL_POSIX)
	pthread_detach(_t);
#elif defined(CRITICAL_WINAPI)
	CloseHandle(&_t);
#endif
}
void critical::Thread::join(){
#if defined(CRITICAL_POSIX)
	pthread_join(_t, NULL);
#elif defined(CRITICAL_WINAPI)
	WaitForSingleObject(&_t, INFINITE);
#endif
}