//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Mutex.cpp
 *	@date 15.11.2014.
 *  @author Armon Carigiet
 *  @see Mutex.h
 */
//===========================================================================

#include <critical/core/Mutex.h>

critical::Mutex::Mutex(){
#if defined(CRITICAL_POSIX)
	pthread_mutex_init(&_m, NULL);
#elif defined(CRITICAL_WINAPI)
	InitializeCriticalSection(&_m);
#endif
}

critical::Mutex::~Mutex(){
#if defined(CRITICAL_POSIX)
	pthread_mutex_destroy(&_m);
#elif defined(CRITICAL_WINAPI)
	DeleteCriticalSection(&_m);
#endif
}

void critical::Mutex::lock(){
#if defined(CRITICAL_POSIX)
	pthread_mutex_lock(&_m);
#elif defined(CRITICAL_WINAPI)
	EnterCriticalSection(&_m);
#endif

}

bool critical::Mutex::trylock(){
#if defined(CRITICAL_POSIX)
	return pthread_mutex_trylock(&_m);
#elif defined(CRITICAL_WINAPI)
	return TryEnterCriticalSection(&_m) ? 0 : EBUSY;
#endif
}

void critical::Mutex::unlock(){
#if defined(CRITICAL_POSIX)
	pthread_mutex_unlock(&_m);
#elif defined(CRITICAL_WINAPI)
	LeaveCriticalSection(&_m);
#endif
}


critical::FastMutex::FastMutex(){
	_m=0;
}
critical::FastMutex::~FastMutex(){}

bool critical::FastMutex::trylock(){
	//int i=1;
	int ret=0;
	
#if defined(CRITICAL_GCC) || defined(CRITICAL_CLANG)
/*	__asm__ __volatile__ ("lock"
						  "cmpxchgl %2,%0"
						  "sete	 %1"
						  :"=m" (_m), "=q"(ret)
						  :"r" (i)
						  :"memory"
						  );
 */
#elif defined(CRITICAL_MSVC)
	//#elif defined(etc....)
#endif
	return ret;
}

void critical::FastMutex::unlock(){
	_m=0;
}