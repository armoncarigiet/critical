//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file FileManager.cpp
 *	@date 07.12.2014.
 *  @author Armon Carigiet
 *  @see FileManager.h
 */
//===========================================================================

#include <critical/core/FileManager.h>
#include <fcntl.h>
#include <critical/core/Mutex.h>

#include <cstdio>

namespace critical{
	Vector<FileManager::_file_table_entry> FileManager::_v=Vector<FileManager::_file_table_entry>();
	
#if defined(CRITICAL_WINAPI)
	#include <io.h>
	
	#define O_RDONLY _O_RDONLY
	#define O_WRONLY _O_WRONLY
	#define O_CREAT _O_CREAT
	#define O_TRUNC	_O_TRUNC
	#define O_APPEND _O_APPEND
	#DEFINE O_RDWR _O_RDWR
	
	static inline int open(const char* p, int f){ return _open(p, f); }
	static inline int read(int fd, void* buf, size_t count){ return _read(fd, buf, count); }
	static inline int write(int fd, const void* buf, size_t count){ return _write(fd, buf, count); }
	static inline off_t lseek(int fd, off_t offset, int origin){ return _lseeki64(fd, offset, origin); }
	static inline int close(int fd){ return _close(fd); }
#endif
	
int __sread(void *cookie, char *buf, int n){
	FILE *fp=(FILE*)cookie;
	int ret=read(fp->_file, buf, n);
	if(ret >= 0){
		fp->_offset+=ret;
	}else{
		fp->_flags &= ~__SOFF;
	}
	
	return ret;
}
int __swrite(void *cookie, const char *buf, int n){
	FILE *fp=(FILE*)cookie;
	
	if(fp->_flags & __SAPP){
		lseek(fp->_file, (off_t)0, SEEK_END);
	}
	fp->_flags &= ~__SOFF;
	return (write(fp->_file, buf, n));
}
	
fpos_t __sseek(void *cookie, fpos_t offset, int whence){
	FILE *fp = (FILE*)cookie;
	off_t ret;
	ret = lseek(fp->_file, (off_t)offset, whence);
	
	if (ret==-1){
		fp->_flags &= ~__SOFF;
	}else {
		fp->_flags |= __SOFF;
		fp->_offset = ret;
	}
	return ret;
}
int __sclose(void *cookie){
	return close(((FILE*)cookie)->_file);
}
	
FileManager::_file_dec FileManager::__sfp(){
	_file_dec p;
	p._i=-1;
	_file_table_entry *e=NULL;
	
	size_t s=_v.size();
	
	for(int i=0; i<s; i++) {
		if(_v[i].allocated==false){
			p._i=i;
			e=&_v[i];
			break;
		}
	}
	if(p._i==-1){
		_v.expand(1);
		_file_table_entry tmp;
		_v.pushBack(tmp);
		p._i=_v.size()-1;
		e=&_v[p._i];
	}
	
	e->allocated=true;
	e->_fp._flags = 1;
	e->_fp._p = NULL;
	e->_fp._w = 0;
	e->_fp._r = 0;
	e->_fp._bf._base = NULL;
	e->_fp._bf._size = 0;
	e->_fp._lbfsize = 0;
	e->_fp._file = -1;
	//e->_d._cookie = <any>;
	e->_fp._ub._base = NULL;
	e->_fp._ub._size = 0;
	e->_fp._lb._base = NULL;
	e->_fp._lb._size = 0;
	e->_fp._offset=0;
	e->_fp._blksize=0;
	e->_fp._ur=0;
	e->_fp._extra=&e->ex;
#if defined(CRITICAL_MACOSX)
	e->_fp._extra->up=NULL;
	e->_fp._extra->fl_mutex=(pthread_mutex_t)PTHREAD_RECURSIVE_MUTEX_INITIALIZER;
	e->_fp._extra->orientation = 0;
	memset(&(e->_fp)._extra->mbstate, 0, sizeof(mbstate_t));
	e->_fp._extra->counted = 1;
#endif
	p._fp=&e->_fp;
	
	return p;
}
	
int FileManager::__sflags(char *mode, int *optr){
	int ret, m, o;
	
	switch (*mode++) {
			
		case 'r':
			ret = __SRD;
			m = O_RDONLY;
			o = 0;
			break;
			
		case 'w':
			ret = __SWR;
			m = O_WRONLY;
			o = O_CREAT | O_TRUNC;
			break;
			
		case 'a':
			ret = __SWR;
			m = O_WRONLY;
			o = O_CREAT | O_APPEND;
			break;
			
		default:
			errno = EINVAL;
			return 0;
	}
	
	if (*mode == '+' || (*mode == 'b' && mode[1] == '+')) {
		ret = __SRW;
		m = O_RDWR;
	}
	
	*optr = m | o;
	return ret;
}
	
FileManager::_file_dec FileManager::__fopen(const char* p, const char* m){
	_file_dec fd;
	fd._fp=NULL;
	fd._i=-1;
	int f, flags, oflags;
	
	if((flags = __sflags((char*)m, &oflags)) == 0){
		return fd;
	}
	fd = __sfp();
	if(fd._fp == NULL){
		return fd;
	}
	if(oflags & O_CREAT){
		f=open(p, oflags, DEFFILEMODE);
	}else{
		f=open(p, oflags);
	}
	if(f==-1){
		fd._fp->_flags=0;
		throw FileManager::FileException(errno);
		return fd;
	}
	fd._fp->_file=f;
	fd._fp->_flags=flags;
	fd._fp->_cookie=fd._fp;
	fd._fp->_read=&critical::__sread;
	fd._fp->_write=&critical::__swrite;
	fd._fp->_seek=&critical::__sseek;
	fd._fp->_close=&critical::__sclose;
	if(oflags & O_APPEND)
		fseek(fd._fp, (fpos_t)0, SEEK_END);
		//critical::__sseek((void*)fd._fp, (fpos_t)0, SEEK_END);
	return fd;
	
//	_file_dec dec;
//	dec._fp=fopen(p, m);
//	dec._i=-1;
//	return dec;
}

	int FileManager::__fclose(uint32_t index){
		close(_v[index]._fp._file);
		_v[index].allocated=false;
        return 1;
	}

}