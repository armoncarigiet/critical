//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Chrono.cpp
 *	@date 13.04.2015.
 *  @author Armon Carigiet
 *  @see Chrono.h
 */
//===========================================================================

#include <critical/core/Chrono.h>
#include <assert.h>

#if defined(__MACH__)

int clock_gettime(clockid_t clock_id, struct timespec *tp){
	if(clock_id==CLOCK_REALTIME  || clock_id==CLOCK_MONOTONIC){
		clock_serv_t cclock;
		mach_timespec_t mts;
		
		host_get_clock_service(mach_host_self(), clock_id, &cclock);
		int retval = clock_get_time(cclock, &mts);
		mach_port_deallocate(mach_task_self(), cclock);
		
		tp->tv_sec = mts.tv_sec;
		tp->tv_nsec = mts.tv_nsec;
		return retval;
	}else{
		throw 1;
	}
}
#elif defined(CRITICAL_WINDOWS)
int
clock_gettime(clockid_t clock_id, struct timespec *tp)
{
	LARGE_INTEGER           t;
	FILETIME            f;
	double                  nanoseconds;
	static LARGE_INTEGER    offset;
	static double           frequencyToNanoseconds;
	static int              initialized = 0;
	
	if (!initialized) {
		LARGE_INTEGER performanceFrequency;
		initialized = 1;
		bool a=QueryPerformanceFrequency(&performanceFrequency);
		if(!a){
			throw 1;
		}
		QueryPerformanceCounter(&offset);
		frequencyToNanoseconds = (double)performanceFrequency.QuadPart*GLMATH_NANO;
	}
	
	QueryPerformanceCounter(&t);
	
	t.QuadPart -= offset.QuadPart;
	nanoseconds = (double)t.QuadPart / frequencyToNanoseconds;
	t.QuadPart = nanoseconds;
	tp->tv_sec = t.QuadPart * GLMATH_NANO;
	tp->tv_nsec = t.QuadPart % GLMATH_GIGA;
	return (0);
}
#endif