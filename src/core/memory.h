//
//  darwinmem.h
//  
//
//  Created by Armon Carigiet on 14.07.14.
//
//

#ifndef CRITICAL_CORE_MEMORY_H_
#define CRITICAL_CORE_MEMORY_H_

#define CRITICAL_CEIL(n, to) 	((int)(n/to)+1)*to
#define CRITICAL_FLOOR(n, to) 	(int)(n/to)*to

#include <stddef.h>
#include <sys/types.h>
#include <critical/core/thread.h>
#include <critical/core/mutex.h>

#if defined(CRITICAL_POSIX) && defined(__MACH__) && defined(__APPLE__)
	#include <sys/mman.h>
	#include <unistd.h>
#elif defined(CRITICAL_USE_WINAPI)
	#include <windows.h>
#endif

#ifdef __cplusplus
extern "C"{
#endif
	
#if defined(CRITICAL_WINAPI)
	extern inline void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset);
	extern inline int munmap(void *addr, size_t length);
#endif
	
struct _mem_zone{
	const void* begin;
	void* _break;
	void* limit;
	size_t size;
	//critical::Mutex mutex;
};
typedef struct _mem_zone mem_zone_t;
	
//size is rounded up to next page bondary
int mem_zone_init(mem_zone_t *memzone, size_t size);
	
int mem_zone_free(mem_zone_t *memzone);
	
int mem_zone_sbrk(mem_zone_t* memzone, intptr_t inc);
int mem_zone_brk(mem_zone_t* memzone, void* addr);
	
#ifdef __cplusplus
}
#endif

#endif