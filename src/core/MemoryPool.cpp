//
//  MemoryPool.cpp
//  
//
//  Created by Armon Carigiet on 04.07.14.
//
//

#include <critical/core/MemoryPool.h>

namespace critical{

size_t MemoryPool::_mem_block_size=sizeof(bool);

MemoryPool::MemoryPool(size_t blocksize, size_t count){
	_p=MemoryManager::salloc<void>((blocksize+_mem_block_size)*count);
	blockSize=blocksize;
	size=count;
	limit=0;
}
	
MemoryPool::~MemoryPool(){
	this->freePool();
}
	
MemoryPool::PoolPointer MemoryPool::allocateBlock(){
	if(limit==size){
		throw MemoryException(CRMemoryExceptionNoSpaceLeft);
	}
	_mem_block *ptr;
	for (int i=limit; i<size; ++i) {
		ptr=reinterpret_cast<_mem_block*>(static_cast<char*>(_p.getPtr())+(blockSize+_mem_block_size)*i);
		if(!ptr->allocated){
			ptr->allocated=true;
			break;
		}
	}
}
	
void MemoryPool::freeBlock(PoolPointer& p){
	_mem_block *ptr=reinterpret_cast<_mem_block*>(static_cast<char*>(_p.getPtr())+(blockSize+_mem_block_size)*p.pindex);
	ptr->allocated=false;
	limit=p.pindex;
}
	//bool freeBlocks(PoolPointer& p, size_t count);
//bool freeBlocksRange(size_t low, size_t high);

	
void MemoryPool::expand(size_t count){
	size+=count;
	MemoryManager::srealloc(_p, size*blockSize);
}
	
void MemoryPool::freePool(){
	MemoryManager::sfree(_p);
	size=0;
	blockSize=0;
	limit=0;
}
	
}