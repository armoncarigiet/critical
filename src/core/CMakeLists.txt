
cmake_minimum_required(VERSION 3.0)

set(CORE_INC ${CMAKE_SOURCE_DIR}/include/critical/core)
set(CORE_SRC ${CMAKE_SOURCE_DIR}/src/core)
set(CORE_TEST ${CMAKE_SOURCE_DIR}/src/core/unittests)

set(CRITICAL_CORE_SRC
	${CORE_INC}/core.h
	${CORE_INC}/MemoryManager.h
	${CORE_SRC}/MemoryManager.cpp
	${CORE_INC}/MemoryStack.h
	${CORE_SRC}/MemoryStack.cpp
	${CORE_INC}/MemoryPool.h
	${CORE_SRC}/MemoryPool.cpp
	${CORE_INC}/MemoryTable.h
	${CORE_SRC}/MemoryTable.cpp
	${CORE_INC}/Memory.h


	${CORE_INC}/Pointer.h
	${CORE_SRC}/Pointer.cpp
	${CORE_INC}/SharedPointer.h
	${CORE_SRC}/SharedPointer.cpp
	${CORE_INC}/WeakPointer.h
	${CORE_SRC}/WeakPointer.cpp

	${CORE_INC}/FileManager.h
	${CORE_SRC}/FileManager.cpp

	${CORE_INC}/ColladaHelper.h
	${CORE_SRC}/ColladaHelper.cpp
	${CORE_INC}/Importer.h
	${CORE_SRC}/Importer.cpp

	${CORE_INC}/RenderInformation.h
	${CORE_SRC}/RenderInformation.cpp

	${CORE_INC}/Iterator.h
	${CORE_SRC}/Iterator.cpp
	${CORE_INC}/Array.h
	${CORE_SRC}/Array.cpp
	${CORE_INC}/Vector.h
	${CORE_SRC}/Vector.cpp
	${CORE_INC}/List.h
	${CORE_SRC}/List.cpp
	${CORE_INC}/AlignedList.h
	${CORE_SRC}/AlignedList.cpp
	${CORE_INC}/Dictionary.h
	${CORE_SRC}/Dictionary.cpp
	${CORE_INC}/Map.h
	${CORE_SRC}/Map.cpp
	${CORE_INC}/Tuple.h
	${CORE_SRC}/Tuple.cpp

	${CORE_INC}/String.h
	${CORE_SRC}/String.cpp

	${CORE_INC}/Clock.h
	${CORE_SRC}/Clock.cpp
	${CORE_INC}/SystemClock.h
	${CORE_SRC}/SystemClock.cpp
	${CORE_INC}/Chrono.h
	${CORE_SRC}/Chrono.cpp

	${CORE_INC}/Thread.h
	${CORE_SRC}/Thread.cpp
	${CORE_INC}/Mutex.h
	${CORE_SRC}/Mutex.cpp
	#${CORE_SRC}/memory.h
	#${CORE_SRC}/memory.c

	${CORE_INC}/Socket.h
	${CORE_SRC}/Socket.cpp

)


set(CRITICAL_CORE_TEST_SRC
	${CMAKE_SOURCE_DIR}/include/unit/unit_tests.h
	${CMAKE_SOURCE_DIR}/include/unit/unit_tests.c

	${CORE_TEST}/Analysers.h
	${CORE_TEST}/memoryManagerTests.h
	${CORE_TEST}/memoryStackTests.h
	${CORE_TEST}/memoryTableTests.h
	${CORE_TEST}/pointerTests.h
	${CORE_TEST}/fileManagerTests.h
	${CORE_TEST}/containerTests.h
)

critical_add_library(${CRITICAL_CORE_LIB} TYPE ${CRITICAL_LIBRARY_TYPE} SOURCES ${CRITICAL_CORE_SRC})
target_link_libraries(${CRITICAL_CORE_LIB} xml2)

add_executable(${CRITICAL_CORE_UNITTESTS} unittests/tests.cpp ${CRITICAL_CORE_TEST_SRC})
target_link_libraries(${CRITICAL_CORE_UNITTESTS} ${CRITICAL_CORE_LIB} ${CRITICAL_GRAPHICS_LIB})


#copy files
install(TARGETS ${CRITICAL_CORE_LIB}
	RUNTIME DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
	LIBRARY DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
	ARCHIVE DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
	FRAMEWORK DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
)
install(CODE "execute_process(COMMAND install_name_tool -id ${CMAKE_INSTALL_PREFIX}/lib/lib${CRITICAL_CORE_LIB}.dylib ${CMAKE_INSTALL_PREFIX}/lib/lib${CRITICAL_CORE_LIB}.dylib) ")

install(TARGETS ${CRITICAL_CORE_UNITTESTS}
	RUNTIME DESTINATION "${CMAKE_SOURCE_DIR}/tests/unittests"
)

