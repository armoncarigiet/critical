//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file ColladaHelper.cpp
 *	@date 31.01.2015.
 *  @author Armon Carigiet
 *  @see ColladaHelper.h
 */
//===========================================================================

#include <critical/core/ColladaHelper.h>

#include <stdio.h>

namespace critical {
	
const char ColladaHelper::mainTag[]="COLLADA";
const char ColladaHelper::techniqueCommonTag[]="technique_common";
const char ColladaHelper::extraTag[]="extra";
const char ColladaHelper::techniqueTag[]="technique";
const char ColladaHelper::assetTag[]="asset";

 const char ColladaHelper::contributorTag[]="contributor";
 const char ColladaHelper::authorTag[]="author";
 const char ColladaHelper::authoringToolTag[]="authoring_tool";
 const char ColladaHelper::createdTag[]="created";
 const char ColladaHelper::modifiedTag[]="modified";
 const char ColladaHelper::unitTag[]="unit";
 const char ColladaHelper::upAxisTag[]="up_axis";
	
const char ColladaHelper::libraryCamerasTag[]="library_cameras";
const char ColladaHelper::instanceCameraTag[]="instance_camera";
	
const char ColladaHelper::cameraTag[]="camera";
const char ColladaHelper::imagerTag[]="imager";
const char ColladaHelper::opticsTag[]="optics";
const char ColladaHelper::orthographic[]="orthografic";
const char ColladaHelper::perspectiveTag[]="perspective";
const char ColladaHelper::xfovTag[]="xfov";
const char ColladaHelper::yfovTag[]="yfov";
const char ColladaHelper::xmagTag[]="xmag";
const char ColladaHelper::ymagTag[]="ymag";
const char ColladaHelper::aspectRatioTag[]="aspect_ratio";
const char ColladaHelper::znearTag[]="znear";
const char ColladaHelper::zfarTag[]="zfar";
	
const char ColladaHelper::libraryLightsTag[]="library_lights";
const char ColladaHelper::instanceLightTag[]="instance_light";
	
const char ColladaHelper::lightTag[]="light";
const char ColladaHelper::ambientTag[]="ambient";
const char ColladaHelper::directionalTag[]="directional";
const char ColladaHelper::pointTag[]="point";
const char ColladaHelper::spotTag[]="spot";
const char ColladaHelper::colorTag[]="color";
const char ColladaHelper::constantAttenuationTag[]="constant_attenuation";
const char ColladaHelper::linearAttenuationTag[]="linear_attenuation";
const char ColladaHelper::quadraticAttenuationTag[]="quadratic_attenuation";
const char ColladaHelper::fallofAngleTag[]="fallof_angle";
const char ColladaHelper::fallofExponentTag[]="fallof_exponent";
	
const char ColladaHelper::libraryGeometriesTag[]="library_geometries";
const char ColladaHelper::instanceGeometryTag[]="instance_geometry";
	
const char ColladaHelper::meshTag[]="mesh";
const char ColladaHelper::splineTag[]="spline";
const char ColladaHelper::convexMeshTag[]="convex_mesh";
const char ColladaHelper::linesTag[]="lines";
const char ColladaHelper::linestripsTag[]="linestrips";
const char ColladaHelper::polygonsTag[]="polygons";
const char ColladaHelper::polylistTag[]="polylist";
const char ColladaHelper::geometryTag[]="geometry";
const char ColladaHelper::trianglesTag[]="triangles";
const char ColladaHelper::trifansTag[]="trifans";
const char ColladaHelper::tristripsTag[]="tristrips";
const char ColladaHelper::verticesTag[]="vertices";
const char ColladaHelper::pTag[]="p";
const char ColladaHelper::hTag[]="h";
const char ColladaHelper::phTag[]="ph";
const char ColladaHelper::vcountTag[]="vcount";
	
const char ColladaHelper::accesssorTag[]="accessor";
const char ColladaHelper::boolArrayTag[]="bool_array";
const char ColladaHelper::floatArrayTag[]="float_array";
const char ColladaHelper::IDREFArrayTag[]="IDREF_array";
const char ColladaHelper::intArrayTag[]="int_array";
const char ColladaHelper::nameArrayTag[]="Name_array";
const char ColladaHelper::paramTag[]="param";
const char ColladaHelper::sourceTag[]="source";
const char ColladaHelper::inputTag[]="input";
	
const char ColladaHelper::libraryVisualScenesTag[]="library_visual_scenes";
const char ColladaHelper::libraryNodesTag[]="library_nodes";
	
const char ColladaHelper::instanceNodeTag[]="instance_node";
const char ColladaHelper::instanceVisualSceneTag[]="instance_visual_scene";
const char ColladaHelper::nodeTag[]="node";
const char ColladaHelper::sceneTag[]="scene";
const char ColladaHelper::visualSceneTag[]="visual_scene";
	
const char ColladaHelper::lookatTag[]="lookat";
const char ColladaHelper::matrixTag[]="matrix";
const char ColladaHelper::rotateTag[]="rotate";
const char ColladaHelper::scaleTag[]="scale";
const char ColladaHelper::skewTag[]="skew";
const char ColladaHelper::translateTag[]="translate";
	
const char ColladaHelper::libraryMaterialsTag[]="library_materials";
const char ColladaHelper::materialTag[]="material";
const char ColladaHelper::libraryEffectsTag[]="library_effects";
const char ColladaHelper::effectTag[]="effect";
const char ColladaHelper::instanceEffectTag[]="instance_effect";
const char ColladaHelper::phongTag[]="phong";
const char ColladaHelper::profile_COMMONTag[]="profile_COMMON";
const char ColladaHelper::shininessTag[]="shininess";
const char ColladaHelper::emissionTag[]="emission";
const char ColladaHelper::diffuseTag[]="diffuse";
const char ColladaHelper::specularTag[]="specular";
const char ColladaHelper::reflectiveTag[]="reflective";
const char ColladaHelper::reflecitvityTag[]="reflectivity";
const char ColladaHelper::transparentTag[]="transparent";
const char ColladaHelper::transparencyTag[]="transarency";
const char ColladaHelper::indexOfRefractionTag[]="index_of_refraction";

const char ColladaHelper::PositionSemantic[]="POSITION";
const char ColladaHelper::VertexSemantic[]="VERTEX";
const char ColladaHelper::NormalSemantic[]="NORMAL";
const char ColladaHelper::TangentSemantic[]="TANGENT";
const char ColladaHelper::BitangentSemantic[]="BINORMAL";
const char ColladaHelper::TexturecoordSemantic[]="TEXCOORD";

    
xmlNode* ColladaHelper::findNodeWithId(xmlNode *node, const char* i){
	for(xmlNode* n=node->children; n!=NULL; n=n->next){
		char* str=(char*)xmlGetProp((xmlNode*)n, (const xmlChar*)"id");
		if(str && strcmp(str, i)==0){
			return n;
		}
	}
	return NULL;
}
	
xmlNode* ColladaHelper::findeNodeWithName(xmlNode* node, const char* name){
	for (xmlNode* n=node->children; n!=NULL; n=n->next) {
		if(strcmp((char*)n->name, name)==0){
			return n;
		}
	}
	return NULL;
}
	
int ColladaHelper::getNodeOffsetWithId(xmlNode *node, const char* i){
	int k=0;
	for(xmlNode* n=node->children; n!=NULL; n=n->next){
		if(strcmp((char*)n->name, "text")!=0){
			char* str=(char*)xmlGetProp((xmlNode*)n, (const xmlChar*)"id");
			if(str && strcmp(str, i)==0){
				return k;
			}else{
				++k;
			}
		}
	}
	return -1;
}
	
Array<float> ColladaHelper::resolveFloatArray(xmlNode *node){
	if(strcmp((char*)node->name, floatArrayTag)!=0){
		return Array<float>();
	}
	size_t size=atoi(((char*)xmlGetProp((xmlNode*)node, (const xmlChar*)"count")));
	Array<float> array(size);
	char* astring=(char*)node->children->content;
	
	char* tmp=NULL;
	
	for(int i=0; i<size; i++){
		array[i]=strtof(astring, &tmp);
		astring=tmp;
	}
	return array;
}
	
Array<glmath_vec2> ColladaHelper::resolveFloat2Array(xmlNode *node){
	if(strcmp((char*)node->name, floatArrayTag)!=0){
		return Array<glmath_vec2>();
	}
	size_t size=(int)atoi(((char*)xmlGetProp((xmlNode*)node, (const xmlChar*)"count")))/2;
	Array<glmath_vec2> array(size);
	char* astring=(char*)node->children->content;
	
	char* tmp=NULL;
	
	for(int i=0; i<size; i++){
		array[i].x=strtof(astring, &tmp);
		astring=tmp;
		array[i].y=strtof(astring, &tmp);
		astring=tmp;
	}
	return array;
}
	
Array<glmath_vec3> ColladaHelper::resolveFloat3Array(xmlNode *node, Scene::SceneAttributes upaxis){
	if(strcmp((char*)node->name, floatArrayTag)!=0){
		return Array<glmath_vec3>();
	}
	size_t size=(int)atoi(((char*)xmlGetProp((xmlNode*)node, (const xmlChar*)"count")))/3;
	Array<glmath_vec3> array(size);
	char* astring=(char*)node->children->content;
	
	char* tmp=NULL;
	
	for(int i=0; i<size; i++){
		array[i].x=strtof(astring, &tmp);
		astring=tmp;
		array[i].y=strtof(astring, &tmp);
		astring=tmp;
		array[i].z=strtof(astring, &tmp);
		astring=tmp;
	}
		
//	for(int i=0; i<size; i++){
//		if(upaxis==Scene::CRUpAxisX){
//			array[i].y=strtof(astring, &tmp);
//			astring=tmp;
//			array[i].z=strtof(astring, &tmp);
//			astring=tmp;
//			array[i].x=strtof(astring, &tmp);
//			astring=tmp;
//		}else if(upaxis==Scene::CRUpAxisY){
//			array[i].x=strtof(astring, &tmp);
//			astring=tmp;
//			array[i].y=strtof(astring, &tmp);
//			astring=tmp;
//			array[i].z=strtof(astring, &tmp);
//			astring=tmp;
//		}else if(upaxis==Scene::CRUpAxisZ){
//			array[i].z=strtof(astring, &tmp);
//			astring=tmp;
//			array[i].x=strtof(astring, &tmp);
//			astring=tmp;
//			array[i].y=strtof(astring, &tmp);
//			astring=tmp;
//		}
//		
//		
//	}
	return array;
}
	
Array<glmath_vec4> ColladaHelper::resolveFloat4Array(xmlNode *node){
	if(strcmp((char*)node->name, floatArrayTag)!=0){
		return Array<glmath_vec4>();
	}
	size_t size=(int)atoi(((char*)xmlGetProp((xmlNode*)node, (const xmlChar*)"count")))/4;
	Array<glmath_vec4> array(size);
	char* astring=(char*)node->children->content;
	
	char* tmp=NULL;
	
	for(int i=0; i<size; i++){
		array[i].x=strtof(astring, &tmp);
		astring=tmp;
		array[i].y=strtof(astring, &tmp);
		astring=tmp;
		array[i].z=strtof(astring, &tmp);
		astring=tmp;
		array[i].w=strtof(astring, &tmp);
		astring=tmp;
	}
	return array;
}
	
Array<int> ColladaHelper::resolveIntArray(xmlNode *node){
	if(strcmp((char*)node->name, intArrayTag)!=0){
		return Array<int>();
	}
	size_t size=atoi(((char*)xmlGetProp((xmlNode*)node, (const xmlChar*)"count")));

	Array<int> array(size);
	char* astring=(char*)node->children->content;
	
	char* tmp=NULL;
	
	for(int i=0; i<size; i++){
		array[i]=strtol(astring, &tmp, 10);
		astring=tmp;
	}
	return array;
}
	
Array<int> ColladaHelper::resolvePArray(xmlNode *node, int count){
	Array<int> array(count);
	char* astring=(char*)node->children->content;
	char* tmp=NULL;
	for(int i=0; i<count; i++){
		array[i]=strtol(astring, &tmp, 10);
		astring=tmp;
	}
	return array;
}
	
Array<int> ColladaHelper::resolveArray(xmlNode *node){
	char* start=(char*)node->children->content;
	char* tmp=start;
	char* end=nullptr;
	int count=0;
	Array<int> a;
	while(true){					// igit!
		strtol(tmp, &end, 10);
		if(tmp==end){
			break;
		}
		tmp=end;
		count++;
	};
	a=Array<int>(count);
	tmp=start;
	end=nullptr;
	for(int i=0; i<count; i++){
		int k=strtol(tmp, &end, 10);
		a[i]=k;
		tmp=end;
	}
	return a;
}
	
Array<float> ColladaHelper::resolvefArray(xmlNode *node){
	char* start=(char*)node->children->content;
	char* tmp=start;
	char* end=nullptr;
	int count=0;
	Array<float> a;
	while(true){
		strtof(tmp, &end);
		if(tmp==end){
			break;
		}
		tmp=end;
		count++;
	};
	a=Array<float>(count);
	tmp=start;
	end=nullptr;
	for(int i=0; i<count; i++){
		float k=strtof(tmp, &end);
		a[i]=k;
		tmp=end;
	}
	return a;
}
	
glmath_vec3 ColladaHelper::resolveVec3(xmlNode *node){
		glmath_vec3 v;
		char* start=(char*)node->children->content;
		char* tmp=nullptr;
		v.v[0]=(glmath_real)strtof(start, &tmp);
		start=tmp;
		v.v[1]=(glmath_real)strtof(start, &tmp);
		start=tmp;
		v.v[2]=(glmath_real)strtof(start, &tmp);
		//start=tmp;
		return v;
	}
	
glmath_vec4 ColladaHelper::resolveVec4(xmlNode *node){
		glmath_vec4 v;
		char* start=(char*)node->children->content;
		char* tmp=nullptr;
		v.v[0]=(glmath_real)strtof(start, &tmp);
		start=tmp;
		v.v[1]=(glmath_real)strtof(start, &tmp);
		start=tmp;
		v.v[2]=(glmath_real)strtof(start, &tmp);
		start=tmp;
		v.v[3]=(glmath_real)strtof(start, &tmp);
	//start=tmp;
		return v;
	}
	
void ColladaHelper::resolveMat4(glmath_mat4 *mat, xmlNode *node, Scene::SceneAttributes attr){
	char* start=(char*)node->children->content;
	char* tmp=nullptr;
	
	for(int i=0; i<4; i++){
		mat->m[i]=(glmath_real)strtof(start, &tmp);
		start=tmp;
		mat->m[4+i]=(glmath_real)strtof(start, &tmp);
		start=tmp;
		mat->m[8+i]=(glmath_real)strtof(start, &tmp);
		start=tmp;
		mat->m[12+i]=(glmath_real)strtof(start, &tmp);
		start=tmp;
	}
	
	// axis adjustment
//	glmath_real tmpf=0.0f;
//	if(attr==Scene::CRUpAxisX){
//		tmpf=mat->m[12];
//		mat->m[12]=mat->m[14];
//		mat->m[13]=tmpf;
//		mat->m[14]=mat->m[13];
//	}else if(attr==Scene::CRUpAxisY){
//		// nothing!
//	}else if(attr==Scene::CRUpAxisZ){
//		tmpf=mat->m[12];
//		mat->m[12]=mat->m[13];
//		mat->m[13]=mat->m[14];
//		mat->m[14]=tmpf;
//	}
	
	
}
	
bool ColladaHelper::resolveNode(SceneNode &node, xmlNode *n, Scene *scene, xmlNode *sceneNode, bool root){
	bool hasMeshes=false;
	bool hasNodes=false;
	bool hasCamsOrLights=false;
    bool camerasss=false;
	glmath_mat4 tmat;
	if(strcmp((char*)n->name, nodeTag)==0 || root){
		char* type=(char*)xmlGetProp((xmlNode*)n, (const xmlChar*)"type");
		if(type==NULL || strcmp(type, "NODE")==0){
			//printf("\nroot: %d",root);
			SceneNode *last=nullptr;
			for(xmlNode *l=n->children; l!=NULL; l=l->next){
				if(strcmp((char*)l->name, "text")!=0){
					bool isGeometry=false;
					if(strcmp((char*)l->name, instanceCameraTag)==0){
						hasCamsOrLights=true;
                        camerasss=true;
					}else if(strcmp((char*)l->name, instanceLightTag)==0){
						hasCamsOrLights=true;
					}else if(strcmp((char*)l->name, instanceGeometryTag)==0){
						hasMeshes=true;
					}else if(strcmp((char*)l->name, instanceNodeTag)==0){
						throw 1;
					}else if(strcmp((char*)l->name, lookatTag)==0){
						throw 1;
					}else if(strcmp((char*)l->name, matrixTag)==0){
						resolveMat4(&tmat, l, scene->attr["UpAxis"]);
						//printf("mat: %f", node._tmat.m00);
						
					}else if(strcmp((char*)l->name, rotateTag)==0){
						throw 1;
					}else if(strcmp((char*)l->name, scaleTag)==0){
						throw 1;
					}else if(strcmp((char*)l->name, skewTag)==0){
						throw 1;
					}else if(strcmp((char*)l->name, translateTag)==0){
						throw 1;
					}else if(strcmp((char*)l->name, nodeTag)==0){
						hasNodes=true;
						SceneNode newNode;
						List<SceneNode>::Iterator it=node._children.pushBack(newNode);
						bool isNode=resolveNode(*it, l, scene, sceneNode, false);
						if(isNode){
							it->_parent=&node;
							it->_last=last;
							it->_next=NULL;
							if(last){
								last->_next=&(*it);
							}
						}else{
							node._children.popBack();
						}
					}
				}
			}
			if(hasCamsOrLights){
				//printf("\n\n2: %f", node._tmat.m[0]);
				
				glmath_transform_params params=glmath_mat4_decompose(tmat);
				Scene::SceneAttributes a=scene->attr["UpAxis"];
				glmath_real tmp=0;
				glmath_vec3 up;
				glmath_vec3 forward;
				
				
				glmath_quat rq=glmath_quat_create_mat4(tmat);
				
//				if(a==Scene::CRUpAxisX){
//					tmp=rq.x;
//					rq.x=rq.z;
//					rq.z=rq.y;
//					rq.y=tmp;
//					
//				}else if(a==Scene::CRUpAxisY){
//					
//				}else if(a==Scene::CRUpAxisZ){
//					tmp=rq.x;
//					rq.x=rq.y;
//					rq.y=rq.z;
//					rq.z=tmp;
//				}
				
//				forward=glmath_quat_rotate_vec3({{0,0,-1}}, rq);
//				up=glmath_quat_rotate_vec3({{1,0,0}}, rq);
				
				
				// Why y==up?
                
				if(a==Scene::CRUpAxisX){
                    throw 1;
                    
                    //forward=glmath_mat3_mul_vec3(glmath_mat4_get_mat3(rmat), {{-1,0,0}});
                    //up=glmath_mat3_mul_vec3(glmath_mat4_get_mat3(rmat), {{0,0,1}});
					forward=glmath_quat_rotate_vec3({{-1,0,0}}, rq);
					up=glmath_quat_rotate_vec3({{0,0,1}}, rq);

				}else if(a==Scene::CRUpAxisY){
                    throw 1;
                    
                    //forward=glmath_mat3_mul_vec3(glmath_mat4_get_mat3(rmat), {{0,0,-1}});
                    //up=glmath_mat3_mul_vec3(glmath_mat4_get_mat3(rmat), {{0,1,0}});
					forward=glmath_quat_rotate_vec3({{0,0,-1}}, rq);
					up=glmath_quat_rotate_vec3({{0,1,0}}, rq);

				}else if(a==Scene::CRUpAxisZ){
                    //forward=glmath_mat3_mul_vec3(glmath_mat4_get_mat3(rmat), {{0,1,0}});
                    //up=glmath_mat3_mul_vec3(glmath_mat4_get_mat3(rmat), {{0,0,1}});
                    
                    //forward={{0,1,0}};
                    //up={{0,0,1}};
                    
					forward=glmath_quat_rotate_vec3({{0,0,-1}}, rq);
                    up=glmath_quat_rotate_vec3({{0,1,0}}, rq);
                    
                    int i=1;
				}
				
				// chage system
//				if(a==Scene::CRUpAxisX){
//					tmp=forward.x;
//					forward.x=forward.z;
//					forward.z=forward.y;
//					forward.y=tmp;
//					
//					tmp=up.x;
//					up.x=up.z;
//					up.z=up.y;
//					up.y=tmp;
//					
////					tmp=params.t.x;
////					params.t.x=params.t.z;
////					params.t.z=params.t.y;
////					params.t.y=tmp;
//				}else if(a==Scene::CRUpAxisY){
//				
//				}else if(a==Scene::CRUpAxisZ){
//					tmp=forward.x;
//					forward.x=forward.y;
//					forward.y=forward.z;
//					forward.z=tmp;
//					
//					tmp=up.x;
//					up.x=up.y;
//					up.y=up.z;
//					up.z=tmp;
//					
////					tmp=params.t.x;
////					params.t.x=params.t.y;
////					params.t.y=params.t.z;
////					params.t.z=tmp;
//				}

				
			
				
				
				//glmath_vec3 s=params.s;
				//glmath_vec3 t=params.t;
				
				//glmath_vec3 axis=glmath_quat_axis(rq);
				//glmath_real angle=glmath_quat_angle(rq);
				
				
				
//				glmath_mat4 rot=glmath_mat4_create_rotation_euler_angles(params.r);
//				glmath_mat4 scale=glmath_mat4_create_scale_vec3(params.s);
//				glmath_mat4 translate=glmath_mat4_create_translation_vec3(params.t);
//				
//				glmath_mat4 rmat=glmath_mat4_mul(glmath_mat4_mul(translate, scale), rot);
				
				printf("");

				for(xmlNode *l=n->children; l!=0; l=l->next){
					if(strcmp((char*)l->name, "text")!=0){
						if(strcmp((char*)l->name, instanceCameraTag)==0){
							char* url=(char*)xmlGetProp((xmlNode*)l, (const xmlChar*)"url");
							int index=0;
							for(xmlNode *w=sceneNode->children; w!=NULL; w=w->next) {
								if(strcmp((char*)w->name, libraryCamerasTag)==0){
									index=getNodeOffsetWithId(w, ++url);			//"#..."
									break;
								}
							}
							//scene->cameras[index]._position=params.t;
							
							//glmath_vec3 up;
							//glmath_vec3 d;
							
							// to adjust camera so dot(up, right)==0
							glmath_vec3 right=glmath_vec3_cross(forward, up);
							up=glmath_vec3_norm(glmath_vec3_cross(right, forward));
							
							scene->cameras[index]._forward=glmath_vec3_norm(forward);
							scene->cameras[index]._up=up;
							
//							glmath_real tmp2=0;
//							if(a==Scene::CRUpAxisX){
//								tmp=scene->cameras[index]._forward.x;
//								tmp2=scene->cameras[index]._up.x;
//								scene->cameras[index]._forward.x=scene->cameras[index]._forward.z;
//								scene->cameras[index]._up.x=scene->cameras[index]._up.z;
//								scene->cameras[index]._forward.z=scene->cameras[index]._forward.y;
//								scene->cameras[index]._up.z=scene->cameras[index]._up.y;
//								scene->cameras[index]._forward.y=tmp;
//								scene->cameras[index]._up.y=tmp2;
//							}else if(a==Scene::CRUpAxisY){
//								
//							}else if(a==Scene::CRUpAxisZ){
//								tmp=scene->cameras[index]._forward.x;
//								tmp2=scene->cameras[index]._up.x;
//								scene->cameras[index]._forward.x=scene->cameras[index]._forward.y;
//								scene->cameras[index]._up.x=scene->cameras[index]._up.y;
//								scene->cameras[index]._forward.y=scene->cameras[index]._forward.z;
//								scene->cameras[index]._up.y=scene->cameras[index]._up.z;
//								scene->cameras[index]._forward.z=tmp;
//								scene->cameras[index]._up.z=tmp2;
//							}
							
							
							//glmath_real dot=glmath_vec3_dot(scene->cameras[index]._forward, scene->cameras[index]._up);
							
							scene->cameras[index]._position=params.t;
							
						}else if(strcmp((char*)l->name, instanceLightTag)==0){
							char* url=(char*)xmlGetProp((xmlNode*)l, (const xmlChar*)"url");
							int index=0;
							for(xmlNode *w=sceneNode->children; w!=NULL; w=w->next){
								if(strcmp((char*)w->name, libraryLightsTag)==0){
									index=getNodeOffsetWithId(w, ++url);			//"#..."
									break;
								}
							}
							scene->lights[index].position=params.t;
							if(scene->lights[index]._type==LightSource::CRAmbientLight || scene->lights[index]._type==LightSource::CRPointLight){
								scene->lights[index].direction={{0,0,0}};
							}else if(scene->lights[index]._type==LightSource::CRDirectionalLight || scene->lights[index]._type==LightSource::CRSpotLight){
								scene->lights[index].direction=glmath_vec3_norm(forward);
							}
						}
					}
				}
			}
			if(hasMeshes){
                
				AlignedList<glmath_mat4>::Iterator it=scene->matrices.pushBack(tmat);
				node._tmat=&it;
				node.mindex=Vector<uint32_t>(1);
			
				
				for(xmlNode *l=n->children; l!=0; l=l->next){
					if(strcmp((char*)l->name, instanceGeometryTag)==0){
						char* url=(char*)xmlGetProp((xmlNode*)l, (const xmlChar*)"url");
						for(xmlNode *w=sceneNode->children; w!=NULL; w=w->next){
							if(strcmp((char*)w->name, libraryGeometriesTag)==0){
								int index=getNodeOffsetWithId(w, ++url);
								node.mindex.pushBack(index);			//"#..."
								break;
							}
						}
					}
				}
				//node._tmat=tmat;
				//glmath_mat4 mat3=tmat;
			}
		}else if(strcmp((char*)n->name, "JOINT")==0){
			throw 1;
		}
	}
	return hasMeshes || hasNodes;
}
	
ColladaHelper::metadata ColladaHelper::getMetaData(xmlNode *node){
	uint32_t count=0;
	xmlNode* current = nullptr;
	for(xmlNode* n=node->children; n!=NULL; n=n->next){
		if(strcmp((char*)n->name, assetTag)==0){
			current=n;
			break;
		}
	}
	//count
	for(xmlNode *k=current->children; k!=NULL; k=k->next){
		//printf("\n%s", k->name);
		if(strcmp((char*)k->name, "text")!=0){
			++count;
		}
		bool onlyText=true;
		for (xmlNode* n=k->children; n!=NULL ; n=n->next) {
			//printf("\n->\t%s", n->name);
			if(strcmp((char*)n->name, "text")!=0){
				onlyText=false;
				++count;
			}
		}
		if(!onlyText){
			--count;
		}
	}
	metadata m(count);
	//copy
	for (xmlNode *k=current->children; k!=NULL; k=k->next) {
		if(strcmp((char*)k->name, "text")!=0){
			bool onlyText=true;
			for (xmlNode* n=k->children; n!=NULL ; n=n->next) {
				if(strcmp((char*)n->name, "text")!=0){
					//printf("\n%s\t=>\t%s", n->name, n->children->content);
					m.v.pushBack({ String((char*)n->name), String((char*)n->children->name) });
					onlyText=false;
				}
			}
			if(onlyText){
				if(!k->children && strcmp((char*)k->name, unitTag)==0){
					char* a=(char*)xmlGetProp((xmlNode*)k, (const xmlChar*)"name");
					if(a){
						String s(a);
						m.v.pushBack({ unitTag,  s});
						m.v.pushBack({ String("unitval"), String((char*)xmlGetProp((xmlNode*)k, xmlGetProp((xmlNode*)k, (const xmlChar*)"name"))) });
					}
				}else{
					m.v.pushBack({ String((char*)k->name), String((char*)k->children->content) });
				}
				//printf("\n%s\t=>\t%s", m.v[i].v1.c_str() , m.v[i].v2.c_str());
			}
		}
	}
	return m;
}
	
Camera ColladaHelper::getMainCamera(xmlNode* node){
	Camera cam;
	xmlNode* current = nullptr;
	for(xmlNode* n=node->children; n!=NULL; n=n->next){
		if(strcmp((char*)n->name, libraryCamerasTag)==0){
			current=n;
			break;
		}
	}
	if (!current) {
		return Camera();
	}
	
	for(xmlNode* n=current->children; n!=NULL; n=n->next){
		if(strcmp((char*)n->name, opticsTag)==0){
			current=n;
			break;
		}
	}
	if (!current) {
		return Camera();
	}
	
	for(xmlNode* n=current->children; n!=NULL; n=n->next){
		if(strcmp((char*)n->name, techniqueCommonTag)==0){
			current=n;
			break;
		}
	}
	if (!current || !current->children) {
		return Camera();
	}
	
	current=current->children;
	
	
	
	for(xmlNode* n=current->children; n!=NULL; n=n->next){
		if(strcmp((char*)n->name, xfovTag)==0){
			
		}else if(strcmp((char*)n->name, yfovTag)){
			
		}else if(strcmp((char*)n->name, xmagTag)){
			
		}else if(strcmp((char*)n->name, ymagTag)){
			
		}else if(strcmp((char*)n->name, aspectRatioTag)){
		
		}else if(strcmp((char*)n->name, znearTag)){
			
		}else if(strcmp((char*)n->name, zfarTag)){
			
		}
	}
	
	
//	if(strcmp((char*)current->name, perspectiveTag)==0){
//		
//	}else if(strcmp((char*)current->name, orthographic)==0){
//		
//	}
	
	return cam;
	
}
	
MemoryManager::ConnectedPointer<Scene> ColladaHelper::getScene(xmlNode* node, uint32_t offset){
	
	MemoryManager::ConnectedPointer<Scene> scene=MemoryManager::salloc<Scene>(sizeof(Scene));
	//Scene scene;
	xmlNode *current=nullptr;
	metadata m=getMetaData(node);
	
	scene->attr=Dictionary<Scene::SceneAttributes>(2);
	char* tmp=NULL;
	if(m.isKey("unit")){
		tmp=m.getValueForKey("unit").c_str();
		if(strcmp(tmp,"meter")==0){
			scene->attr.addElement("Unit", Scene::CRUnitMeters);
		}
	}
	tmp=m.getValueForKey("up_axis").c_str();
	if(strcmp(tmp,"X_UP")==0){
		scene->attr.addElement("UpAxis", Scene::CRUpAxisX);
	}else if(strcmp(tmp,"Y_UP")==0){
		scene->attr.addElement("UpAxis", Scene::CRUpAxisY);
	}else if(strcmp(tmp,"Z_UP")==0){
		scene->attr.addElement("UpAxis", Scene::CRUpAxisZ);
	}
	
	Scene *sc=&scene;
	
	// load materials
	
	for(xmlNode* n=node->children; n!=NULL; n=n->next){
		//printf("\n%s\n",n->name);
		if(strcmp((char*)n->name, libraryMaterialsTag)==0){
			loadMaterials(scene->materials, n);
			break;
		}
	}
	
	if(scene->materials.size()==0) {
		Material m;
		Array<glmath_real> a(3);
		a[0]=0; a[1]=0; a[2]=0;
		m._attributes.addElement(CRMaterialAmbient, a);
		m._attributes.addElement(CRMaterialEmission, a);
		
		Array<glmath_real> c(3);
		c[0]=1; c[1]=1; c[2]=1;
		m._attributes.addElement(CRMaterialDiffuse, c);
		
		Array<glmath_real> d(3);
		d[0]=0.6; d[1]=0.6; d[2]=0.6;
		m._attributes.addElement(CRMaterialSpecular, d);
		
		
		Array<glmath_real> b(1);
		b[0]=1;
		m._attributes.addElement(CRMaterialShininess, b);
		scene->materials.pushBack(m);
	}

	
	// load meshes
	for(xmlNode* n=node->children; n!=NULL; n=n->next){
		if(strcmp((char*)n->name, libraryGeometriesTag)==0){
			int c=0;
			for(xmlNode *k=n->children; k!=NULL; k=k->next){
				if(strcmp((char*)k->name, geometryTag)==0) {
					++c;
				}
			}
			//printf("\n\ncount: %d\n\n", c);
			scene->meshes=Vector<Mesh>(c);
			
			int i=0;
			for(xmlNode *k=n->children; k!=NULL; k=k->next){
				if(strcmp((char*)k->name, geometryTag)==0) {
					for(xmlNode *w=k->children; w!=NULL; w=w->next) {
						if(strcmp((char*)w->name, meshTag)==0){
							scene->meshes.pushBack(Mesh());
							loadMesh(scene->meshes[i], w, scene->materials, scene->attr);
							++i;
						}
					}
				}
			}
		}
	}
	
	// load cameras
	for(xmlNode* n=node->children; n!=NULL; n=n->next){
		if(strcmp((char*)n->name, libraryCamerasTag)==0){
			scene->cameras=Vector<Camera>(1);
			loadCameras(scene->cameras, n);
			break;
		}
	}
	
	// load lights
	for(xmlNode* n=node->children; n!=NULL; n=n->next){
		if(strcmp((char*)n->name, libraryLightsTag)==0){
			scene->lights=Vector<LightSource>(1);
			loadLightSources(scene->lights, n);
			break;
		}
	}

	// load nodes
	
	scene->matrices.allocate(GLMATH_PACK);
	
	int i=0;
	for(xmlNode* n=node->children; n!=NULL; n=n->next){
		if(strcmp((char*)n->name, libraryVisualScenesTag)==0){
			for(xmlNode *k=n->children; k!=NULL; k=k->next){
				if(strcmp((char*)k->name, visualSceneTag)==0){
					if(i==offset){
						scene->rootNode._parent=nullptr;
						scene->rootNode._next=nullptr;
						scene->rootNode._last=nullptr;
						resolveNode(scene->rootNode, k, &scene, node, true);
						break;
					}
					++i;
				}
			}
			break;
		}
	}
//	if (!current) {
//		return Scene();
//	}
	
//	printf("\n Scene: Camera's  ");
//	scene->cameras.printref();
//	printf("\n Scene: Light's  ");
//	scene->lights.printref();
//	printf(" size: %zu\n", scene->lights.size());
	
	return scene;
}
	
bool ColladaHelper::loadCameras(Vector<Camera> &cameras, xmlNode *node){
	int count=0;
	for(xmlNode *n=node->children; n!=NULL; n=n->next) {
		if(strcmp((char*)n->name, cameraTag)==0){
			count++;
		}
	}
	cameras=Vector<Camera>(count);
	for(xmlNode *n=node->children; n!=NULL; n=n->next) {
		if(strcmp((char*)n->name, cameraTag)==0){
			Camera c;
			for(xmlNode* l=n->children; l!=NULL; l=l->next){
				if(strcmp((char*)l->name, opticsTag)==0){
					for(xmlNode* k=l->children; k!=NULL; k=k->next){
						if(strcmp((char*)k->name, techniqueCommonTag)==0){
							for(xmlNode* w=k->children; w!=NULL; w=w->next){
								bool isPerspective=strcmp((char*)w->name, perspectiveTag)==0;
								bool isOrthographic=strcmp((char*)w->name, orthographic)==0;
								if(isPerspective || isOrthographic){
									for(xmlNode* r=w->children; r!=NULL; r=r->next){
										if(strcmp((char*)r->name, aspectRatioTag)==0){
											c._aspect=strtof((const char*)r->children->content, NULL);
											//printf("\n\naspect: %f\n\n", c._aspect);
										}else if(strcmp((char*)r->name, znearTag)==0){
											c._znear=strtof((const char*)r->children->content, NULL);
										}else if(strcmp((char*)r->name, zfarTag)==0){
											c._zfar=strtof((const char*)r->children->content, NULL);
										}
										if(isPerspective){
											c._type=Camera::CRPerspective;
											if(strcmp((char*)r->name, xfovTag)==0){
												c._fov=strtof((const char*)r->children->content, NULL);
												c._fovType=0;
											}else if(strcmp((char*)r->name, yfovTag)==0){
												c._fov=strtof((const char*)r->children->content, NULL);
												c._fovType=1;
											}
										}else if(isOrthographic){
											c._type=Camera::CROrthographic;
											throw 1;
										}
									}
								}
							}
						}
					}
				}
			}
			cameras.pushBack(c);
		}
	}
	
	return true;
}
	
bool ColladaHelper::loadLightSources(Vector<LightSource> &lights, xmlNode *node){
	if(strcmp((char*)node->name, libraryLightsTag)==0){
		for(xmlNode *n=node->children; n!=NULL; n=n->next){
			if(strcmp((char*)n->name, lightTag)==0){
				Vector<LightSource>::Iterator it=lights.pushBack(LightSource());
				for(xmlNode *k=n->children; k!=NULL; k=k->next){
					if(strcmp((char*)k->name, techniqueCommonTag)==0){
						for(xmlNode *w=k->children; w!=NULL; w=w->next){
							bool isAmbientLight=strcmp((char*)w->name, ambientTag)==0;
							bool isDirectionalLight=strcmp((char*)w->name, directionalTag)==0;
							bool isPointLight=strcmp((char*)w->name, pointTag)==0;
							bool isSpotLight=strcmp((char*)w->name, spotTag)==0;
							//printf("\nname: %s\n", w->name);
							if(isAmbientLight){
								printf("ambient");
								it->_type=LightSource::CRAmbientLight;
							}else if(isDirectionalLight){
								printf("directional");
								it->_type=LightSource::CRDirectionalLight;
							}else if(isPointLight){
								printf("point");
								it->_type=LightSource::CRPointLight;
							}else if(isSpotLight){
								printf("spot");
								it->_type=LightSource::CRSpotLight;
							}
							
							if(isAmbientLight || isDirectionalLight || isPointLight || isSpotLight){
								it->isEnabled=true;
								for(xmlNode *l=w->children; l!=NULL; l=l->next){
									if(strcmp((char*)l->name, "text")!=0){
										if(strcmp((char*)l->name, colorTag)==0){
											it->_color=Color(resolveVec4(l));
										}else if(strcmp((char*)l->name, constantAttenuationTag)==0){
											it->_attributes.addElement(CRConstantAttenuation, strtof((const char*)l->children->content, NULL));
										}else if(strcmp((char*)l->name, linearAttenuationTag)==0){
											it->_attributes.addElement(CRLinearAttenuation, strtof((const char*)l->children->content, NULL));
										}else if(strcmp((char*)l->name, quadraticAttenuationTag)==0){
											it->_attributes.addElement(CRQuadraticAttenuation, strtof((const char*)l->children->content, NULL));
										}else if(strcmp((char*)l->name, fallofAngleTag)==0){
											it->_attributes.addElement(CRFallofAngle, strtof((const char*)l->children->content, NULL));
										}else if(strcmp((char*)l->name, fallofExponentTag)==0){
											it->_attributes.addElement(CRFallofExponent, strtof((const char*)l->children->content, NULL));
										}else{
											//it->_attr.addElement((char*)l->name, strtof((const char*)l->children->content, NULL));
										}
									}
								}
							}
						}
					}
				}
				
			}
		}
	}
	return true;
}

bool ColladaHelper::loadMaterials(Vector<Material> &materials, xmlNode* node){
	if(strcmp((char*)node->name, libraryMaterialsTag)==0){
		xmlNode *enode;
		
		for(xmlNode *n=node->parent->children; n!=NULL; n=n->next){
			if(strcmp((char*)n->name, libraryEffectsTag)==0){
				enode=n;
				break;
			}
		}
		int i=0;
		for(xmlNode *n=node->children; n!=NULL; n=n->next){
			if(strcmp((char*)n->name, materialTag)==0){
				i++;
			}

		}
		
		materials=Vector<Material>(i);
		i=0;
		
		for(xmlNode *n=node->children; n!=NULL ; n=n->next){
			if(strcmp((char*)n->name, materialTag)==0){
				//materials.pushBack(Material());
				Material m;
				//m.d.allocate(1);
				for(xmlNode *k=n->children; k!=NULL; k=k->next){
					if(strcmp((char*)k->name, instanceEffectTag)==0){
						char *url=(char*)xmlGetProp((xmlNode*)k, (const xmlChar*)"url");
						xmlNode *effect=findNodeWithId(enode, ++url);
						for(xmlNode *l=effect->children; l!=NULL; l=l->next) {
							if(strcmp((char*)l->name, profile_COMMONTag)==0){
								for(xmlNode *w=l->children; w!=NULL; w=w->next) {
									if(strcmp((char*)l->name, profile_COMMONTag)==0){
										for (xmlNode *r=w->children; r!=NULL; r=r->next){
											if(strcmp((char*)r->name, phongTag)==0){
												int count=0;
												for(xmlNode *q=r->children; q!=NULL; q=q->next){
													if(strcmp((char*)q->name, "text")!=0){
														xmlNode *in=q->children->next;
														if(strcmp((char*)in->name, "param")==0){
															//TODO
															throw 0;
														}else{
															Array<float> a=resolvefArray(in);
															if(strcmp((const char*)q->name, emissionTag)==0){
																m._attributes.addElement(CRMaterialEmission, a);
															}else if(strcmp((const char*)q->name, ambientTag)==0){
																m._attributes.addElement(CRMaterialAmbient, a);
															}else if(strcmp((const char*)q->name, diffuseTag)==0){
																m._attributes.addElement(CRMaterialDiffuse, a);
															}else if(strcmp((const char*)q->name, specularTag)==0){
																m._attributes.addElement(CRMaterialSpecular, a);
															}else if(strcmp((const char*)q->name, shininessTag)==0){
																m._attributes.addElement(CRMaterialShininess, a);
															}else if(strcmp((const char*)q->name, indexOfRefractionTag)==0){
																m._attributes.addElement(CRMaterialIndexOfRefraction, a);
															}else{
																//m.d.addElement((const char*)q->name, a);
															}
															
                                                        
															//Array<float> f=m._attributes.elementAtIndex(m._attributes.size()-1);
															//printf("\n name: %s value: %s", q->name, in->children->content);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				materials.pushBack(m);
				//i++;
			}
		}
	}
	return true;
}

bool ColladaHelper::loadMesh(Mesh &mesh, xmlNode* node, Vector<Material> &materials, Dictionary<Scene::SceneAttributes> &attr){
	xmlNode *meshNode=node, *primitiveNode=NULL, *vcountNode=NULL;
	int vertexOffset=0, normalOffset=0, tangentOffset=0, bitangentOffset=0, texturecoordOffset=0;
	size_t count=0, vertexCount=0;
	int pcount;

	if(strcmp((char*)node->name, meshTag)!=0){
		return false;
	}
	// count
	int pc=0;
	Scene::SceneAttributes a=attr.elementForKey("UpAxis");
	for (xmlNode* n=node->children; n!=NULL; n=n->next) {
		//printf("NodeName: %s\n", n->name);
		if(strcmp((char*)n->name, linesTag)==0 ||
		   strcmp((char*)n->name, linestripsTag)==0 ||
		   strcmp((char*)n->name, polygonsTag)==0 ||
		   strcmp((char*)n->name, polylistTag)==0 ||
		   strcmp((char*)n->name, trianglesTag)==0 ||
		   strcmp((char*)n->name, tristripsTag)==0 ||
		   strcmp((char*)n->name, trifansTag)==0 )
		{
			
			pc++;
			
			for(xmlNode *k=n->children; k!=NULL; k=k->next){
				if(strcmp((char*)k->name, inputTag)==0){
					char* semantic=(char*)xmlGetProp((xmlNode*)k, (const xmlChar*)"semantic");
					char* str=(char*)xmlGetProp((xmlNode*)k, (const xmlChar*)"source");
					++str;
					xmlNode* tmp=findNodeWithId(meshNode, str);
					//printf("\nname: %s\tsematic: %s\n",tmp->name, semantic);
					
					if(strcmp((char*)semantic, VertexSemantic)==0 && !mesh._vertices.size()){
						for(xmlNode *l=tmp->children; l!=0; l=l->next){
							if(strcmp((char*)l->name, inputTag)==0){
								char* semantic2=(char*)xmlGetProp((xmlNode*)l, (const xmlChar*)"semantic");
								char* str2=(char*)xmlGetProp((xmlNode*)l, (const xmlChar*)"source");
								++str2;
								if(strcmp((char*)semantic2, PositionSemantic)==0){
									xmlNode *tmp2=findNodeWithId(meshNode, str2);
									for(xmlNode *u=tmp2->children; u!=NULL; u=u->next){
										if(strcmp((char*)u->name, floatArrayTag)==0){
											//TODO accessor
											mesh._vertices=resolveFloat3Array(u, a);
											break;
										}
									}
									//printf("\nPosition Node: %s\n", tmp2->name);
								}
							}
						}
					}else if(strcmp((char*)semantic, NormalSemantic)==0 && !mesh._normals.size()){
						for(xmlNode *l=tmp->children; l!=0; l=l->next){
							if(strcmp((char*)l->name, floatArrayTag)==0){
								mesh._normals=resolveFloat3Array(l, a);
							}
						}
					}else if(strcmp((char*)semantic, TangentSemantic)==0 && !mesh._tangents.size()){
						for(xmlNode *l=tmp->children; l!=0; l=l->next){
							if(strcmp((char*)l->name, floatArrayTag)==0){
								mesh._tangents=resolveFloat3Array(l, a);
							}
						}
					}else if(strcmp((char*)semantic, BitangentSemantic)==0 && !mesh._bitangents.size()){
						for(xmlNode *l=tmp->children; l!=0; l=l->next){
							if(strcmp((char*)l->name, floatArrayTag)==0){
								mesh._bitangents=resolveFloat3Array(l, a);
							}
						}
					}else if(strcmp((char*)semantic, TexturecoordSemantic)==0 && !mesh._texturecoords.size()){
						for(xmlNode *l=tmp->children; l!=0; l=l->next){
							if(strcmp((char*)l->name, floatArrayTag)==0){
								mesh._texturecoords=resolveFloat3Array(l, a);
							}
						}
					}
				}
			}
		}
	}
	
	mesh._primitives=Array<Primitive>(pc);
	int w=0;
	
	xmlNode *mnode=NULL;
	
	for(xmlNode *n=node->parent->parent->parent->children; n!=NULL; n=n->next){
		if(strcmp((char*)n->name, libraryMaterialsTag)==0){
			mnode=n;
		}
	}
	
	for(xmlNode* n=node->children; n!=NULL; n=n->next) {
		
		bool isPrimitive=strcmp((char*)n->name, linesTag)==0 ||
		strcmp((char*)n->name, linestripsTag)==0 ||
		strcmp((char*)n->name, polygonsTag)==0 ||
		strcmp((char*)n->name, polylistTag)==0 ||
		strcmp((char*)n->name, trianglesTag)==0 ||
		strcmp((char*)n->name, tristripsTag)==0 ||
		strcmp((char*)n->name, trifansTag)==0;
		
		if(isPrimitive){
			char* material=(char*)xmlGetProp((xmlNode*)n, (const xmlChar*)"material");
			if(material!=NULL){
                int len=strlen(material);
                if(material[len-2]=='_' && material[len-1]=='1'){
                    material[len-2]='\0';
                }
                
				int off=getNodeOffsetWithId(mnode, material);
				mesh._primitives[w].material=&(materials[off]);
				mesh._primitives[w].material_index=off;
			
				if(off==-1){
					mesh._primitives[w].material=&(materials[materials.size()-1]);
					mesh._primitives[w].material_index=materials.size()-1;
				}
				
			}
			
			count=atoi((char*)xmlGetProp((xmlNode*)n, (const xmlChar*)"count"));
			mesh._primitives[w].count=count;
			int inputCount=0;
			for(xmlNode *k=n->children; k!=NULL; k=k->next){
				if(strcmp((char*)k->name, inputTag)==0){
					++inputCount;
					char* semantic=(char*)xmlGetProp((xmlNode*)k, (const xmlChar*)"semantic");
					int offset=atoi((char*)xmlGetProp((xmlNode*)k, (const xmlChar*)"offset"));
					if(strcmp((char*)semantic, VertexSemantic)==0){
						mesh._primitives[w]._hasPositions=true;
						vertexOffset=offset;
					}else if(strcmp((char*)semantic, NormalSemantic)==0){
						mesh._primitives[w]._hasNormals=true;
						normalOffset=offset;
					}else if(strcmp((char*)semantic, TangentSemantic)==0){
						mesh._primitives[w]._hasTangents=true;
						tangentOffset=offset;
					}else if(strcmp((char*)semantic, BitangentSemantic)==0){
						mesh._primitives[w]._hasBitangents=true;
						bitangentOffset=offset;
					}else if(strcmp((char*)semantic, TexturecoordSemantic)==0){
						mesh._primitives[w]._hasTextureCoords=true;
						texturecoordOffset=offset;
					}
				}else if(strcmp((char*)k->name, pTag)==0){
					//mesh._primitives[w].vindex=resolveArray(k);
					char* start=(char*)k->children->content;
					char* tmp=start;
					char* end=nullptr;
					int count=0;
					while(true){					// iiii!
						strtol(tmp, &end, 10);
						if(tmp==end){
							break;
						}
						tmp=end;
						count++;
					};
					mesh._primitives[w].vindex=Array<int>(count);
					tmp=start;
					end=nullptr;
					for(int i=0; i<count; i++){
						int k=strtol(tmp, &end, 10);
						mesh._primitives[w].vindex[i]=k;
						tmp=end;
					}
					
					//Array<int> abvs(73);
					//printf("\nArray3: %p\n",&abvs[0]);
				}else if(strcmp((char*)k->name, vcountTag)==0){
					vcountNode=k;
				}
			}
			
			for(int i=0; i<inputCount; i++){
				if(vertexOffset==i || normalOffset==i || tangentOffset==i || bitangentOffset==i || texturecoordOffset==i){
					++vertexCount;
				}
			}
			
		}
		
		if(strcmp((char*)n->name, linesTag)==0){
			//TODO
		}else if(strcmp((char*)n->name, linestripsTag)==0){
			//TODO
		}else if(strcmp((char*)n->name, polygonsTag)==0){
			mesh._primitives[w].vcount=resolveArray(vcountNode);
			mesh._primitives[w].type=CRPolygons;
		}else if(strcmp((char*)n->name, polylistTag)==0){
			//mesh._primitives[w].vcount=resolveArray(vcountNode);
			mesh._primitives[w].type=CRPolylist;
			char* start=(char*)vcountNode->children->content;
			char* tmp=start;
			char* end=nullptr;
			int count=0;
			while(true){					// iiii!
				strtol(tmp, &end, 10);
				if(tmp==end){
					break;
				}
				tmp=end;
				count++;
			};
			mesh._primitives[w].vcount=Array<int>(count);
			tmp=start;
			end=nullptr;
			for(int i=0; i<count; i++){
				int k=strtol(tmp, &end, 10);
				mesh._primitives[w].vcount[i]=k;
				tmp=end;
			}
		}else if(strcmp((char*)n->name, trianglesTag)==0){
			mesh._primitives[w].type=CRTriangles;
		}else if(strcmp((char*)n->name, tristripsTag)==0){
			mesh._primitives[w].type=CRTriangleStrips;
		}else if(strcmp((char*)n->name, trifansTag)==0){
			mesh._primitives[w].type=CRTriangleFans;
		}
		if(isPrimitive){
			//printf("\n\nvindex:%p\tvcount:%p\n\n", &mesh._primitives[0].vindex[0], &mesh._primitives[0].vcount[0]);
			mesh._primitives[w].vertexCount=vertexCount;
			++w;
		}
		
	}
	
	return true;
}
	
}