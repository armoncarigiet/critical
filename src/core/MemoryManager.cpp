//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file MemoryManager.cpp
 *	@date 04.07.2014.
 *  @author Armon Carigiet
 *  @see MemoryManager.h
 */
//===========================================================================

#include <critical/core/MemoryManager.h>

//Implementations

namespace critical {
	// static definitions
	size_t MemoryManager::tableEntryCount=0;
	struct _mem_table_entry* MemoryManager::pointerTable=nullptr;
	size_t MemoryManager::table_size=0;
	Mutex MemoryManager::tableMutex;
	Mutex MemoryManager::heapMutex;
	long int MemoryManager::pageSize=0;
	//bool MemoryManager::defragmenting_heap=false;
	//bool MemoryManager::defragmenting_heap_seq=false;
	//uint32 MemoryManager::lastDefragmentedBlock=0;
	size_t MemoryManager::used_table_size=0;
	size_t MemoryManager::table_pages=0;
	bool MemoryManager::initialized=false;
	
	size_t MemoryManager::heap_meta_block_size=CRITICAL_MEMORY_HEAPMETABLOCK_SIZE;
	
	void* MemoryManager::heap_base_address=nullptr;
	void* MemoryManager::heap_break=nullptr;
	void* MemoryManager::heap_limit=nullptr;
	
	_heap_meta_block* MemoryManager::heap_used_limit=nullptr;
	
	MemoryManager::MemoryManager(){
		#if defined(CRITICAL_POSIX)
		
		#elif defined(CRITICAL_WINAPI)
			// TODO
		#endif
	}
	
	MemoryManager::~MemoryManager(){}

void MemoryManager::init(size_t heap_size){
	if(!initialized){
		bool init=true;
	#if defined(CRITICAL_POSIX)
		pageSize=sysconf(_SC_PAGE_SIZE);	// normally 4KB, else mostly 2MB(PAE and 64bit) or 4MB
	#elif defined(CRITICAL_WINAPI)
		SYSTEM_INFO sysinf;
		GetSystemInfo(&sysinf);
		pageSize=sysinf.dwPageSize;
	#endif
		size_t hs=CRITICAL_CEIL(heap_size, pageSize);
	#if defined(CRITICAL_POSIX)
		#if defined(__APPLE__) && defined(__MACH__)
		heap_base_address=mmap(0, hs, PROT_NONE, MAP_ANON|MAP_PRIVATE, -1, 0);
		pointerTable=static_cast<_mem_table_entry*>(mmap(0, pageSize, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, -1, 0));
		#else
		heap_base_address=mmap(0, hs, PROT_NONE, MAP_ANONYMOUS|MAP_PRIVATE, -1, 0);
		pointerTable=static_cast<_mem_table_entry*>(mmap(0, pageSize, PROT_READ|PROT_WRITE, MAP_ANONYMOUS|MAP_PRIVATE, -1, 0));
		#endif
		if(pointerTable==MAP_FAILED){
			int error=errno;
			throw MemoryException(error);
			init=false;
		}
		int ret=mprotect(heap_base_address, 1, PROT_READ|PROT_WRITE);
		if(ret==-1){
			int error=errno;
			throw MemoryException(error);
		}
	#elif defined(CRITICAL_WINAPI)
		pointerTable=static_cast<_mem_table_entry*>(VirtualAlloc(0, pageSize, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE));
		heap_base_address=VirtualAlloc(0, pageSize, MEM_RESERVE, PAGE_READWRITE)
		if(pointerTablePages[0]==NULL){
			throw MemoryException(-1);
			init=false;
		}
		void* ptr=VirtualAlloc(heap_base_address, 1, MEM_COMMIT, PAGE_READWRITE);
		if(ptr==NULL){
			throw MemoryException(-1);
		}
	#endif
		heap_break=heap_base_address;
		heap_limit=static_cast<void*>(static_cast<char*>(heap_base_address)+hs);
		if(init){
			//printf("%lu",sizeof(_mem_table_entry));
			table_size=(int)pageSize/sizeof(_mem_table_entry);
			used_table_size=0;
			table_pages=1;
		}
		initialized=init;
	}
}
	
void MemoryManager::terminate(){
	initialized=false;
	for(int i=0; i<used_table_size; i++){
		if(pointerTable[i].allocated){
		#if defined(CRITICAL_POSIX)
			int ret=munmap(static_cast<void*>(pointerTable[i].memoryPointer),pointerTable[i].size);
			
			if(ret==-1){
				int error=errno;
				throw MemoryException(error);
			}
		#elif defined(CRITICAL_WINAPI)
			int ret=VirtualFree(static_cast<void*>(pointerTable[i].memoryPointer), 0, MEM_RELEASE);
			
			if(ret==NULL){
				throw MemoryException(-1);
			}
		#endif
		}
	}
#if defined(CRITICAL_POSIX)
	int ret=munmap(static_cast<void*>(pointerTable),table_size);
	int ret2=munmap(heap_base_address, static_cast<char*>(heap_limit)-static_cast<char*>(heap_base_address));
	if(ret==-1 && ret2==-1){
		int error=errno;
		throw MemoryException(error);
	}
#elif defined(CRITICAL_WINAPI)
	int ret=VirtualFree(static_cast<void*>(pointerTable), 0, MEM_RELEASE);
	int ret2=VirtualFree(heap_base_address, 0, MEM_RELEASE);
	if(ret==NULL && ret2==NULL){
		throw MemoryException(-1);
	}
#endif
}
	
void MemoryManager::exp_tbl(){
	void* tmppointer;
	size_t tmpsize=table_size;
	table_pages++;
#if defined(CRITICAL_POSIX)
	#if defined(__APPLE__) && defined(__MACH__)
	tmppointer=static_cast<_mem_table_entry*>(mmap(0,  table_pages*pageSize, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, -1, 0));
	#else
	tmppointer=static_cast<_mem_table_entry*>(mmap(0, table_pages*pageSize, PROT_READ|PROT_WRITE, MAP_ANONYMOUS|MAP_PRIVATE, -1, 0));
	#endif
	if(tmppointer==MAP_FAILED){
		int error=errno;
		throw MemoryException(error);
	}
#elif defined(CRITICAL_WINAPI)
	tmppointer=static_cast<_mem_table_entry*>(VirtualAlloc(0, table_pages*pageSize, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE));
	if(tmppointer==NULL){
		throw MemoryException(-1);
	}
#endif
	memcpy(tmppointer, pointerTable, tmpsize);
#if defined(CRITICAL_POSIX)
	int ret=munmap(pointerTable,tmpsize);
	if(ret==-1){
		int error=errno;
		throw MemoryException(error);
	}
#elif defined(CRITICAL_WINAPI)
	int ret=VirtualFree(pointerTable, 0, MEM_RELEASE);
	if(ret==NULL){
		throw MemoryException(-1);
	}
#endif
	pointerTable=static_cast<_mem_table_entry *>(tmppointer);
	table_size=(table_pages*pageSize)/sizeof(_mem_table_entry);
	//std::cout<<"\n\nfulltable\t size:"<<table_pages*pageSize<<"\tptr:"<<pointerTable;
}

void MemoryManager::expandTable(){
	if(tableEntryCount>=table_size){
		exp_tbl();
	}
}

//-------------------------------
// Heap
//-------------------------------
	
_heap_meta_block* MemoryManager::allocHeapBlock(size_t size){
	size_t s=CRITICAL_ALIGN8(size);
	_heap_meta_block* block=findHeapBlock(static_cast<_heap_meta_block*>(heap_used_limit),s);
	if(block){
		if((block->size-s)>=(heap_meta_block_size+CRITICAL_MEM_ALIGN)){
			splitHeapBlock(block, s);
		}
	}else{
		block=expandHeap(s);
		if(!block){
			return nullptr;
		}
		//printf("\n%lx\n",block);
		
		heap_used_limit=block;
	}
	block->references=0;
	block->alignment=0;
	return block;
}
	
bool MemoryManager::freeHeapBlock(_heap_meta_block *b){
	int a=0;
	heapMutex.lock();
	if(b->allocated){
		a=1;
		b->allocated=0;
		b->references=0;
	}
	heapMutex.unlock();
	if(a){
		return true;
	}
	return false;
}
	
bool MemoryManager::freeTableEntry(uint32_t index){
	bool a=false;
	tableMutex.lock();
	if(pointerTable[index].allocated){
		int ret;
	#if defined(CRITICAL_POSIX)
		ret=munmap(pointerTable[index].memoryPointer,pointerTable[index].size);
		if(ret==-1){
			int error=errno;
			throw MemoryException(error);
		}
	#elif defined(CRITICAL_WINAPI)
		ret=VirtualFree(pointerTable[index].memoryPointer, 0, MEM_RELEASE);
		if(ret==NULL){
			throw MemoryException(-1);
		}
	#endif
		pointerTable[index].allocated=false;
		pointerTable[index].memoryPointer=nullptr;
		pointerTable[index].size=0;
		if(index==used_table_size-1){
			used_table_size--;
		}
		tableEntryCount--;
		a=true;
	}
	tableMutex.unlock();
	if(a){
		return true;
	}
	return false;
}

_heap_meta_block* MemoryManager::findHeapBlock(_heap_meta_block* start, size_t size){
	_heap_meta_block *l=nullptr, *block=start;
	while(block && (block->size<size+heap_meta_block_size+CRITICAL_MEM_ALIGN || block->allocated)){
		l=block;
		block=block->next;
	}
	heap_used_limit=l;
	return block;
}
	
_heap_meta_block* MemoryManager::expandHeap(size_t size){
	
	char* p=static_cast<char*>(heap_break);

	//printf("\nmeta:%lu meta*:%lu int:%lu size_t:%lu char:%lu\n", sizeof(_heap_meta_block), sizeof(_heap_meta_block*), sizeof(int), sizeof(size_t), sizeof(char[1]));
	//printf("start:%lx void:%lx char:%lx newvoid:%lx", heap_base_address, heap_break, p, br);
	_heap_meta_block* next=reinterpret_cast<_heap_meta_block*>(p);
	
	ptrdiff_t l=p-static_cast<char*>(heap_base_address);
	void* nextPageBound=static_cast<char*>(heap_base_address)+CRITICAL_CEIL(l, pageSize);
	
	void* new_break=static_cast<void*>(p+size+heap_meta_block_size);
	
	if(new_break>=nextPageBound){
		//commit new memory
	#if defined(CRITICAL_POSIX)
		ptrdiff_t diff=static_cast<char*>(new_break)-static_cast<char*>(nextPageBound);
		int ret=mprotect(nextPageBound, size+heap_meta_block_size+diff, PROT_READ|PROT_WRITE);
		if(ret==-1){
			int error=errno;
			throw MemoryException(error);
		}
	#elif defined(CRITICAL_WINAPI)
		void* ptr=VirtualAlloc(heap_break, size+heap_meta_block_size, MEM_COMMIT, PAGE_READWRITE);
		if(ptr==NULL){
			throw MemoryException(-1);
		}
	#endif
	}
	heap_break=new_break;
	
	next->size=size;
	next->allocated=1;
	next->next=nullptr;
	next->last=heap_used_limit;
	next->references=0;
	if(heap_used_limit) {
		heap_used_limit->next=next;
	}
	return next;
}
	
_heap_meta_block* MemoryManager::splitHeapBlock(_heap_meta_block *b, size_t size){
	_heap_meta_block *newb=reinterpret_cast<_heap_meta_block*>(b->data+size);
	newb->size=b->size-size-heap_meta_block_size;
	newb->allocated=0;
	newb->last=b;
	newb->next=b->next;
	newb->references=0;
	b->size=size;
	b->next=newb;
	return newb;
}

//
//	bool MemoryManager::defragmentHeap(){
//		if(!MemoryManager::defragmenting_heap && !MemoryManager::defragmenting_heap_seq){
//			defragmenting_heap=true;
//			for(int i=0; i<used_table_size; i++){
//				if(MemoryManager::pointerTable[i].allocated){
//					//_block localblock=MemoryManager::pointerTable[i];
//					size_t size=MemoryManager::pointerTable[i].size;
//					void* p=alloca(size);
//					memcpy(p, MemoryManager::pointerTable[i].memoryPointer, size);
//					free(MemoryManager::pointerTable[i].memoryPointer);
//					void* l=malloc(size);
//					//std::cout<<"\n\nrealloc pointer"<<i+1<<":\t"<<l<<"\n\n";
//					MemoryManager::pointerTable[i].memoryPointer=l;
//					memcpy(MemoryManager::pointerTable[i].memoryPointer, p, size);
//				}
//			}
//			defragmenting_heap=false;
//			return true;
//		}else{
//			return false;
//		}
//	}
//	// TO TEST
//	bool MemoryManager::defragmentHeapSequential(const uint32 blocks){
//		if(!MemoryManager::defragmenting_heap){
//			defragmenting_heap_seq=true;
//			const uint32 block=lastDefragmentedBlock;
//			for(int i=block; i<block+blocks; i++){
//				if(MemoryManager::pointerTable[i].allocated){
//					//_block localblock=MemoryManager::pointerTable[i];
//					size_t size=MemoryManager::pointerTable[i].size;
//					void* p=alloca(size);
//					memcpy(p, MemoryManager::pointerTable[i].memoryPointer, size);
//					free(MemoryManager::pointerTable[i].memoryPointer);
//					void* l=malloc(size);
//					//std::cout<<"\n\nrealloc pointer"<<i+1<<":\t"<<l<<"\n\n";
//					MemoryManager::pointerTable[i].memoryPointer=l;
//					memcpy(MemoryManager::pointerTable[i].memoryPointer, p, size);
//					lastDefragmentedBlock++;
//					if(i==used_table_size){
//						lastDefragmentedBlock=0;
//						defragmenting_heap_seq=false;
//						return true;
//					}
//				}
//			}
//			return false;
//	}

}