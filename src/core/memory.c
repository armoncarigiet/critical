//
//  darwinmem.c
//  
//
//  Created by Armon Carigiet on 14.07.14.
//
//

#include "memory.h"
//static int mem_mutex_is_allocated=0;
//static pthread_mutex_t mem_zone_mutex;

#if defined(CRITICAL_WINAPI)
inline void* mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset){
	return VirtualAlloc(0, length, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);
}

inline int munmap(void *addr, size_t length){
	return VirtualFree(addr, 0, MEM_RELEASE);
}
#endif

int mem_zone_init(mem_zone_t *memzone, size_t size){
#if defined(CRITICAL_POSIX)
	long int pagesize=sysconf(_SC_PAGESIZE);
#elif defined(CRITICAL_WINAPI)
	SYSTEM_INFO sysinf;
	GetSystemInfo(&sysinf);
	long int pagesize=sysinf.dwPageSize;
#endif
	mem_zone_t zone;
	zone.size=CRITICAL_CEIL(size, pagesize);
	zone.begin=mmap(NULL, zone.size, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, -1, 0);
	if(zone.begin==MAP_FAILED){
		zone._break=(void*)zone.begin;
		zone.limit=(void*)((char*)zone._break+zone.size);
		CRMutexInit(&memzone->mutex);
		return 1;
	}else{
		return 0;
	}
}

int mem_zone_free(mem_zone_t *memzone){
	CRMutexLock(&memzone->mutex);
	int r=munmap((void*)memzone->begin, memzone->size);
	CRMutexUnlock(&memzone->mutex);
	return r;
}

int mem_zone_sbrk(mem_zone_t* memzone, intptr_t inc){
	CRMutexLock(&memzone->mutex);
	if((char*)memzone->limit>=(char*)memzone->_break+inc) {
		memzone->_break=(void*)((char*)memzone->_break+inc);
		return 1;
	}else{
		return 0;
	}
	CRMutexUnlock(&memzone->mutex);
}

int mem_zone_brk(mem_zone_t* memzone, void* addr){
	CRMutexLock(&memzone->mutex);
	if((char*)memzone->begin<(char*)addr && (char*)memzone->limit>=(char*)addr){
		memzone->_break=addr;
		return 1;
	}else{
		return 0;
	}
	CRMutexUnlock(&memzone->mutex);
}



