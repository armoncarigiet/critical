//
//  MemoryTable.cpp
//  critical
//
//  Created by Armon Carigiet on 07.12.14.
//
//

#include <critical/core/MemoryTable.h>

namespace critical {

MemoryTable::MemoryTable(size_t blocksize){
	this->ptr=this->memoryManager.salloc<void>(blocksize);
	if(ptr.allocated){
		this->tableSize=0;
		this->usedTableSize=0;
		this->tableEntryCount=0;
		this->blockSize=blocksize;
	}
}
	
MemoryTable::MemoryTable(size_t blocksize, int capacity){
	ptr=memoryManager.salloc<void>(blocksize*capacity);
	
	//printf("ptrt[0] mem:%x size:%zu allocated:%d\n", &MemoryManager::pointerTable[0], MemoryManager::pointerTable[0].size, MemoryManager::pointerTable[0].allocated);
	//this->ptr=this->memoryManager.salloc<void*>(3000);
	//critical::MemoryManager::connected_pointer<void*> p=memoryManager.salloc<void*>(blocksize*capacity);
	//MemoryManager::connected_pointer<void*> d;
	//printf("\n%d\n",d.allocated);
	//d=memoryManager.salloc<void*>(blocksize*capacity);
	//printf("\n%d\n",d.allocated);
	//this->ptr=memoryManager.salloc<void*>(blocksize*capacity);
	/*
	if(ptr.allocated){
		this->tableSize=capacity;
		this->usedTableSize=0;
		this->tableEntryCount=0;
		this->blockSize=blocksize;
		for(int i=1; i<=capacity; i++){
			_mem_tbl_block block;
			block.address=NULL;
			block.allocated=false;
			reg.push_back(block);
		}
	}
	*/
	//printf("ptrt[0] mem:%x size:%zu allocated:%d\n", &MemoryManager::pointerTable[0], MemoryManager::pointerTable[0].size, MemoryManager::pointerTable[0].allocated);
	//printf("ptrt[1] mem:%x size:%zu allocated:%d\n", &MemoryManager::pointerTable[1], MemoryManager::pointerTable[1].size, MemoryManager::pointerTable[1].allocated);
	//printf("ptrt[2] mem:%x size:%zu allocated:%d", &MemoryManager::pointerTable[2], MemoryManager::pointerTable[2].size, MemoryManager::pointerTable[2].allocated);
	//printf("\nmem:%x\n",ptr.tableEntry);
}

MemoryTable::~MemoryTable(){
	this->memoryManager.sfree(this->ptr);
}

void MemoryTable::expand(size_t bytes){
	this->memoryManager.srealloc(this->ptr, this->tableSize*blockSize+bytes);
	this->tableSize=(int)(this->tableSize*this->blockSize+bytes)/this->blockSize;
}
	
void MemoryTable::retract(size_t bytes){
	this->memoryManager.srealloc(this->ptr, this->tableSize*blockSize-bytes);
	this->tableSize=(int)(this->tableSize*this->blockSize-bytes)/this->blockSize;
}
	
void MemoryTable::expandc(int capacity){
	this->memoryManager.srealloc(this->ptr, (this->tableSize+capacity)*this->blockSize);
	this->tableSize+=capacity;
}
	
void MemoryTable::retractc(int capacity){
	this->memoryManager.srealloc(this->ptr, (this->tableSize-capacity)*this->blockSize);
	this->tableSize-=capacity;
}
	
MemoryTable::tb_ptr MemoryTable::addBlock(){
/*	void* pointer=NULL;
	uint32 index=0;
	bool a=false;
	for(int i=0; i<tableSize; i++){
		if(reg[i].allocated==false){
			reg[i].allocated=true;
			reg[i].address=static_cast<void*>(static_cast<char*>(&this->ptr)+(i+1)*this->blockSize);
			pointer=reg[i].address;
			index=i;
			this->tableEntryCount++;
			a=true;
			break;
		}
	}
	if(!a){
		expand(this->memoryManager.getPageSize());
		reg[this->tableSize].address=static_cast<void*>(static_cast<char*>(&this->ptr)+tableEntryCount*this->blockSize);
		pointer=reg[this->tableSize].address;
		this->tableEntryCount++;
		this->tableSize++;
	}
	return tb_ptr(index, pointer);
 */
}
	
bool MemoryTable::deleteBlock(tb_ptr &ptr){
	return false;
}
	
}