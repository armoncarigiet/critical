//
//  tests.cpp
//  
//
//  Created by Armon Carigiet on 15.11.14.
//
//

#include "memoryManagerTests.h"
//#include "memoryStackTests.h"
//#include "memoryTableTests.h"
#include "pointerTests.h"
#include "containerTests.h"
#include "fileManagerTests.h"

#include <critical/core/FileManager.h>
#include <critical/core/Pointer.h>

int main(){
	//memoryManagerTest_Run();
	//memoryStackTest_Run();
	//pointerTest_Run();
	//containerTest_Run();
	fileManagerTest_Run();
	//memoryTableTest_Run();
	
	//size_t f1=sizeof(critical::FileManager::FileStream);
	//size_t f2=sizeof(critical::FileManager::InputFileStream);
	//size_t f3=sizeof(critical::FileManager::OutputFileStream);
	//size_t f=sizeof(FILE);
	
	//printf("\n\nfile:%zu\tinput:%zu\toutput:%zu\t\n",f1,f2,f3);
	//printf("cfile:%zu\n\n",f);
	
}

