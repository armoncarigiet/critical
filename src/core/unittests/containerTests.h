//
//  containerTests.h
//  critical
//
//  Created by Armon Carigiet on 11.01.15.
//
//

#ifndef critical_containerTests_h
#define critical_containerTests_h

#include "memoryManagerTests.h"
#include <critical/core/Vector.h>

#include <cstdio>

struct _a{
	int i;
	size_t s;
	ptrdiff_t d;
};

int vector_construction(){
	critical::vectorAnalyser analyser;
	//critical::memoryManagerAnalyser memanalyser;
	int status, line;

	line=__LINE__+1;
	critical::Vector<_a> vector(5);
	size_t s=analyser.getPtr(vector).size();
	
	if(s==5*sizeof(_a) && vector.size()==0 && vector.capacity()==5){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Vector Construction", status, __FILE__, line);
}

int vector_push_pop_back_size(){
	critical::vectorAnalyser analyser;
	//critical::memoryManagerAnalyser memanalyser;
	int status, line;
	
	int v=4, v2=5;
	
	critical::Vector<int> vector(2);
	line=__LINE__+1;
	vector.pushBack(v);
	size_t ps1=analyser.getPtr(vector).size(), s1=vector.size(), c1=vector.capacity();
	vector.pushBack(7);
	size_t ps2=analyser.getPtr(vector).size(), s2=vector.size(), c2=vector.capacity();
	vector.pushBack(v2);
	size_t ps3=analyser.getPtr(vector).size(), s3=vector.size(), c3=vector.capacity();
	vector.popBack(); vector.popBack(); vector.popBack();
	size_t s4=vector.size(), c4=vector.capacity();
	
	
	if(ps1==2*sizeof(int) && s1==1 && c1==2 && ps2==ps1 && s2==2 && c2==2  && ps3==CRITICAL_ALIGN8(3*sizeof(int)) && s3==3 && c3==3 && s4==0 && c4==3){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Vector Push/Pop Back Size", status, __FILE__, line);
}

int vector_push_pop_back_constent_ra(){
	//critical::memoryManagerAnalyser memanalyser;
	int status, line;
	
	int v=4, v2=5;
	
	critical::Vector<int> vector(2);
	vector.pushBack(v);
	vector.pushBack(7);
	vector.pushBack(v2);
	printf("d");
	
	
	line=__LINE__+1;
	if(vector[0]==v && vector[1]==7 && vector[2]==v2){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Vector Push/Pop Back Content Random Access", status, __FILE__, line);
}

int vector_random_insert(){
	//critical::memoryManagerAnalyser memanalyser;
	int status, line;
	bool a=false, b=false;
	critical::Vector<int> vector(2);
	vector.pushBack(4);
	vector.pushBack(6);
	vector.pushBack(9);
	vector.pushBack(15);
	
	critical::Vector<int>::Iterator itr=vector.begin();
	itr++;
	vector.insert(itr, 67);
	if(vector[0]==4 && vector[1]==67 && vector[2]==6 && vector[3]==9 && vector[4]==15 && vector.size()==5 && vector.capacity()==5){
		a=true;
	}
	
	itr++;
	vector.erase(itr);
	
	//printf("\n0:%d 1:%d 2:%d 3:%d size:%zu capacity:%zu\n", vector[0], vector[1], vector[2], vector[3], vector.size(), vector.capacity());
	
	if(vector[0]==4 && vector[1]==67 && vector[2]==9 && vector[3]==15 && vector.size()==4 && vector.capacity()==5){
		b=true;
	}
	
	line=__LINE__+1;
	if(a && b){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Vector Random Insert & Erase", status, __FILE__, line);
}

int dictionary_construction(){
	critical::vectorAnalyser vanalyser;
	critical::dictionaryAnalyser analyser;
	//critical::memoryManagerAnalyser memanalyser;
	int status, line;
	
	line=__LINE__+1;
	critical::Dictionary<int> dic;
	
	if(!analyser.getAlloc(dic)){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Dictionary Construction", status, __FILE__, line);
}

int dictionary_allocation(){
	critical::vectorAnalyser vanalyser;
	critical::dictionaryAnalyser analyser;
	//critical::memoryManagerAnalyser memanalyser;
	int status, line;
	
	line=__LINE__+1;
	critical::Dictionary<int> dic(5);
	
	if(analyser.getAlloc(dic)){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Dictionary Allocation", status, __FILE__, line);
}

int dictionary_instertation(){
	int status, line;
	
	line=__LINE__+1;
	critical::Dictionary<int> dic(5);
	dic.addElement("first", 1);
	dic.addElement("second", 2);
	dic.addElement("third", 3);
	dic.addElement("forth", 4);
	dic.addElement("fifth", 5);
	
	if(dic.elementAtIndex(0)==1 && dic.elementAtIndex(1)==2 && dic.elementAtIndex(2)==3 && dic.elementAtIndex(3)==4 && dic.elementAtIndex(4)==5){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Dictionary Insert", status, __FILE__, line);
}

int dictionary_find(){
	int status, line;
	
	line=__LINE__+1;
	critical::Dictionary<int> dic(5);
	dic.addElement("first", 1);
	dic.addElement("second", 2);
	dic.addElement("third", 3);
	dic.addElement("forth", 4);
	dic.addElement("fifth", 5);
	
	if(dic["third"]==3){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Dictionary Find", status, __FILE__, line);
}

int dictionary_erase(){
	int status, line;
	
	line=__LINE__+1;
	critical::Dictionary<int> dic(5);
	dic.addElement("first", 1);
	dic.addElement("second", 2);
	dic.addElement("third", 3);
	dic.addElement("forth", 4);
	dic.addElement("fifth", 5);
	
	dic.eraseElementForKey("forth");
	
	if(!dic.isKey("forth") && dic.elementAtIndex(0)==1 && dic.elementAtIndex(1)==2 && dic.elementAtIndex(2)==3 && dic.elementAtIndex(3)==5){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Dictionary Erase", status, __FILE__, line);
}

int list_construction(){
	critical::listAnalyser analyser;
	//critical::memoryManagerAnalyser memanalyser;
	int status, line;
	
	line=__LINE__+1;
	critical::List<int> list;
	size_t s=list.size();
	
	if(analyser.getBack(list)==nullptr && s==0 && analyser.getFront(list)==nullptr){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("List Construction", status, __FILE__, line);
}

int list_push(){
	critical::listAnalyser analyser;
	//critical::memoryManagerAnalyser memanalyser;
	int status, line;
	
	line=__LINE__+1;
	critical::List<int> list;
	list.pushBack(45);
	list.pushFront(56);
	list.pushBack(104);
	size_t s=list.size();
	critical::List<int>::Iterator it=list.begin();
	
	int first=*it++;
	int second=*it++;
	int third=*it;
	
	//printf("\nf: %d\ts: %d\tt: %d\t\n",first,second,third);
	
	//printf("size: %zu\tfront: %p\tback: %p\tvalue[0]: %d", list.size(),analyser.getFront(list), analyser.getBack(list), list.front());
	
	if(analyser.getBack(list)!=nullptr && analyser.getFront(list)!=nullptr && s==3 && first==56 && second==45 && third==104){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("List Push Items", status, __FILE__, line);
}

int list_refcounting(){
	critical::listAnalyser analyser;
	//critical::memoryManagerAnalyser memanalyser;
	int status, line;
	
	line=__LINE__+1;
	critical::List<int> list;
	list.pushBack(45);
	list.pushBack(45);
	list.pushBack(45);
	list.size();
	
	int ref1=analyser.getRefcount(list);
	int ref2;
	{
		critical::List<int> l2(list);
		ref2=analyser.getRefcount(l2);
	}
	int ref3=analyser.getRefcount(list);
	
	//printf("size: %zu\tfront: %p\tback: %p\tvalue[0]: %d", list.size(),analyser.getFront(list), analyser.getBack(list), list.front());
	
	if(ref1==1 && ref2==2 && ref3==1){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("List Push Back", status, __FILE__, line);
}


void containerTest_Run(){
	critical::MemoryManager::init();
	
	
	glmath_unittest containerTest;
	glmath_unittest_function *containerFunctions;
	int count=11;
	
	containerFunctions=(glmath_unittest_function*)malloc(count*sizeof(glmath_unittest_function));
	
	containerFunctions[0]=&vector_construction;
	containerFunctions[1]=&vector_push_pop_back_size;
	containerFunctions[2]=&vector_push_pop_back_constent_ra;
	containerFunctions[3]=&vector_random_insert;
	containerFunctions[4]=&dictionary_construction;
	containerFunctions[5]=&dictionary_allocation;
	containerFunctions[6]=&dictionary_instertation;
	containerFunctions[7]=&dictionary_find;
	containerFunctions[8]=&dictionary_erase;
	containerFunctions[9]=&list_construction;
	containerFunctions[10]=&list_push;
	
	glmath_unittest_create(&containerTest, "Containers", 1, containerFunctions, count);
	glmath_unittest_run(&containerTest);
	
	critical::MemoryManager::terminate();
}

#endif
