//
//  memoryStackTests.h
//  critical
//
//  Created by Armon Carigiet on 30.11.14.
//
//

#ifndef critical_memoryStackTests_h
#define critical_memoryStackTests_h

#include <stdio.h>
#include <unit/unit_tests.h>
#include <critical/core/MemoryStack.h>

void memoryStackTest_Run(){
	critical::MemoryManager::init();
	
	critical::MemoryManager::connected_pointer<void*> ptr=manager.salloc<void*>(4096);
	critical::MemoryStack(ptr, 0, 0);
	
//	glmath_unittest memoryManagerTest;
//	glmath_unittest_function *memoryManagerFunctions;
//	int count=6;
//	
//	memoryManagerFunctions=(glmath_unittest_function*)malloc(count*sizeof(glmath_unittest_function));
//	
//	memoryManagerFunctions[0]=&memorymanager_init;
//	memoryManagerFunctions[1]=&memorymanager_pageSize;
//	memoryManagerFunctions[2]=&memorymanager_salloc;
//	memoryManagerFunctions[3]=&memorymanager_sfree;
//	memoryManagerFunctions[4]=&memorymanager_alloc_size;
//	memoryManagerFunctions[5]=&memorymanager_alloc_valid;
//	
//	glmath_unittest_create(&memoryManagerTest, "Memorymanager", 1, memoryManagerFunctions, count);
//	glmath_unittest_run(&memoryManagerTest);
	critical::MemoryManager::terminate();
}


#endif
