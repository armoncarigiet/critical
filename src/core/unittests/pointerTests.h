//
//  pointerTests.h
//  critical
//
//  Created by Armon Carigiet on 29.12.14.
//
//

#ifndef critical_pointerTests_h
#define critical_pointerTests_h

#include <unit/unit_tests.h>
#include "memoryManagerTests.h"
#include <critical/core/Pointer.h>
#include <critical/core/SharedPointer.h>


int pointer_offset_constructor(){
	int status, line;
	int a[10]={0,1,2,3,4,5,6,7,8,9};
	
	line=__LINE__+1;
	critical::Pointer<int> ptr(&a, 3);
	
	if(&ptr==&a[3] && *ptr==a[3]){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Pointer Offset constructor", status, __FILE__, line);
}

int pointer_shifting(){
	int status, line;
	int a[10]={0,1,2,3,4,5,6,7,8,9};
	
	line=__LINE__+1;
	critical::Pointer<int> p(&a);
	critical::Pointer<int> ptr=p+3;
	critical::Pointer<int> ptr2=ptr-2;
	critical::Pointer<int> ptr3=ptr2;
	ptr3+=1;
	
	if(&ptr==&a[3] && &ptr2==&a[1] && &ptr3==&a[2]){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Pointer shifting", status, __FILE__, line);
}

int pointer_void(){
	int status, line;
	int a[3]={0,1,2};
	
	line=__LINE__+1;
	critical::Pointer<void> ptr(&a, 2*sizeof(int));
	
	critical::Pointer<void> ptr2=ptr-sizeof(int);
	
	if(&ptr==&a[2] && &ptr2==&a[1]){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Pointer Void", status, __FILE__, line);
}

int pointer_structs(){
	struct _pointer_test{
		char i;
		int a;
		size_t s;
	};
	int status, line;
	_pointer_test a[3]={{'a', 54, 5634534},{'x', 39, 9356724},{'g', 13, 67245113}};
	
	line=__LINE__+1;
	critical::Pointer<_pointer_test> ptr(&a, 2);
	
	if(&ptr==&a[2] && (*ptr).i==a[2].i && ptr->i==a[2].i){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Pointer Struct compatibility", status, __FILE__, line);
}

int shared_pointer_construction(){
	int status, line;
	line=__LINE__+1;
	
	critical::SharedPointer<int> ptr(critical::MemoryManager::salloc<int>(450));
	printf("as");
	if(&ptr!=0){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Shared Pointer Construction", status, __FILE__, line);
}

int shared_pointer_ref_counting(){
	critical::memoryManagerAnalyser analyser;
	int status, line, ref=0, ref2=0, ref3=0, alloc=0;
	line=__LINE__+1;
	
	critical::SharedPointer<int> ptr(critical::MemoryManager::salloc<int>(400));
	ptr.printref();
	critical::SharedPointer<int> ptr2(ptr);
	ptr.printref();
	critical::SharedPointer<int> ptr3(ptr);
	ptr.printref();
	
	
	ref=analyser.getRefCount(ptr);
	
	ptr2.reset();
	ptr3.reset();
	ref2=analyser.getRefCount(ptr);
	ptr.reset();
	ref3=analyser.getRefCount(ptr);
	
	printf("\nref1: %d\tref2: %d\tref3: %d", ref, ref2, ref3);
	
	if(&ptr!=0 && ref==3 && ref2==1 && ref3==0){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Shared Pointer Reference Counting", status, __FILE__, line);
}


void pointerTest_Run(){
	critical::MemoryManager::init();
	glmath_unittest pointerTest;
	glmath_unittest_function *pointerFunctions;
	int count=6;
	
	pointerFunctions=(glmath_unittest_function*)malloc(count*sizeof(glmath_unittest_function));
	
	pointerFunctions[0]=&pointer_offset_constructor;
	pointerFunctions[1]=&pointer_shifting;
	pointerFunctions[2]=&pointer_void;
	pointerFunctions[3]=&pointer_structs;
	pointerFunctions[4]=&shared_pointer_construction;
	pointerFunctions[5]=&shared_pointer_ref_counting;
	
	glmath_unittest_create(&pointerTest, "Pointers", 1, pointerFunctions, count);
	glmath_unittest_run(&pointerTest);
	critical::MemoryManager::terminate();
}

#endif
