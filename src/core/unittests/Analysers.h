//
//  Analysers.h
//  critical
//
//  Created by Armon Carigiet on 10.01.15.
//
//

#ifndef critical_Analysers_h
#define critical_Analysers_h

#include <critical/core/MemoryManager.h>
#include <critical/core/SharedPointer.h>
#include <critical/core/Vector.h>
#include <critical/core/Dictionary.h>
#include <critical/core/List.h>
namespace critical{
	class memoryManagerAnalyser{
	private:
		//...
	public:
		void* getPointerTable(){
			return MemoryManager::pointerTable;
		}
		
		size_t getTableSize(){
			return critical::MemoryManager::table_size;
		}
		size_t getTableUsedSize(){
			return critical::MemoryManager::used_table_size;
		}
		size_t getTableEntryCount(){
			return critical::MemoryManager::tableEntryCount;
		}
		size_t getTablePages(){
			return critical::MemoryManager::table_pages;
		}
		size_t getPageSize(){
			return critical::MemoryManager::pageSize;
		}
		bool isInizialised(){
			return critical::MemoryManager::initialized;
		}
		void* getTableLocation(){
			return critical::MemoryManager::pointerTable;
		}
		bool ptrIsAllocated(MemoryManager::ConnectedPointer<void> &ptr){
			return ptr.allocated;
		}
		template <class T>
		void* getPtr(MemoryManager::ConnectedPointer<T> &ptr){
			return ptr.ptr;
		}
		
		void* getHeapBase(){
			return MemoryManager::heap_base_address;
		}
		void* getHeapBreak(){
			return MemoryManager::heap_break;
		}
		void* getHeapLimit(){
			return MemoryManager::heap_limit;
		}
		_heap_meta_block* getHeapUsedLimit(){
			return MemoryManager::heap_used_limit;
		}
		
		template<class T>
		int getRefCount(SharedPointer<T> &ptr){
			return *ptr.refcounter;
		}
		
		template<class T>
		_heap_meta_block* getHeapMetaBlock(MemoryManager::ConnectedPointer<T> &ptr){
			return ptr.block;
		}
		
		size_t getMetaBlockSize(){
			return MemoryManager::heap_meta_block_size;
		}
		
	
		template<class T>
		memoryLocationCode getPLocation(MemoryManager::ConnectedPointer<T> &ptr){
			return ptr.location;
		}
		template<class T>
		int getPtrEindex(MemoryManager::ConnectedPointer<T> &ptr){
			return ptr.eindex;
		}
		template<class T>
		int isAllocated(MemoryManager::ConnectedPointer<T> &ptr){
			return ptr.allocated;
		}
		
	};

	class vectorAnalyser{
	public:
		template <class T> MemoryManager::ConnectedPointer<T>& getPtr(Vector<T> &v){
			return v._p;
		}
		template <class T> bool getAlloc(Vector<T> &v){
			return v.__alloc;
		}
	};
	
	class dictionaryAnalyser{
	public:
		template <class T> Vector<struct Dictionary<T>::_dictionary_entry>& getVector(Dictionary<T> &dic){
			return dic._v;
		}
		template <class T> bool getAlloc(Dictionary<T> &dic){
			return dic.isAllocated();
		}
	};
	
	class listAnalyser{
	public:
		template <class T> typename List<T>::_list_block* getFront(List<T> &list){
			return list._front;
		}
		template <class T> typename List<T>::_list_block* getBack(List<T> &list){
			return list._back;
		}
		template <class T> int getRefcount(List<T> &list){
			return *list._refcounter;
		}
		
	};
}

#endif
