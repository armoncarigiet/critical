//
//  memoryManagerTests.h
//  critical
//
//  Created by Armon Carigiet on 29.11.14.
//
//

#ifndef critical_memoryManagerTests_h
#define critical_memoryManagerTests_h


#include <unit/unit_tests.h>
#include "Analysers.h"
#include <critical/core/MemoryManager.h>
#include <cstdio>



int memorymanager_init(){
	critical::MemoryManager memoryManager;
	critical::memoryManagerAnalyser analyser;
	int status, a, line;
	//int a=analyser.isInizialised();
	//printf("%d",analyser.getTableEntryCount());
	line=__LINE__+1;
	critical::MemoryManager::init();
	a=analyser.isInizialised();
	if(a){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	return glmath_unittest_log("Intitialisation", status, __FILE__, line);
}

int memorymanager_pageSize(){
	critical::MemoryManager memoryManager;
	critical::memoryManagerAnalyser analyser;
	int status, a, line;
	line=__LINE__+1;
	a=analyser.getPageSize();
	
	critical::MemoryManager::ConnectedPointer<char> pointer1=memoryManager.salloc<char>(5000);
	pointer1.reset();
	if(a){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	return glmath_unittest_log("Page Size", status, __FILE__, line);
}


int memorymanager_salloc(){
	critical::MemoryManager memoryManager;
	critical::memoryManagerAnalyser analyser;
	
	int status, a, alloc, line, t1, t2, error = 0;
	try{
		t1=analyser.getTableEntryCount();
		line=__LINE__+1;
//		critical::MemoryManager::connected_pointer<void*> p;
//		p=memoryManager.salloc<void*>(5000);
		critical::MemoryManager::ConnectedPointer<void> pointer1=memoryManager.salloc<void>(5000);
		//printf("\n\nallocated:%d eindex:%d tablealloc:%d mem:%lx\n\n", analyser.isAllocated(pointer1), analyser.getPtrEindex(pointer1), reinterpret_cast<critical::_mem_table_entry*>(analyser.getPointerTable())[analyser.getPtrEindex(pointer1)].allocated,&pointer1);
		//printf("table: %lx\n", &critical::MemoryManager::pointerTable[0]);
		t2=analyser.getTableEntryCount();
		a=analyser.ptrIsAllocated(pointer1);
		critical::_mem_table_entry *tableEntry=&static_cast<critical::_mem_table_entry*>(analyser.getPointerTable())[analyser.getPtrEindex(pointer1)];
		alloc=tableEntry->allocated;
		pointer1.reset();
	}catch(critical::MemoryException e) {
		error=e.getExceptionCode();
	}
	
	//std::cout<<"\ntableEntryCount: "<<analyser.getTableEntryCount()<<"\n";
	//printf("\n\n t1:%d t2:%d alloc:%d \n\n",t1,t2,alloc);
	if(a && (t2-t1==1) && (error==0) && alloc){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	char msg[256]="Allocation";
	if(error){
		snprintf(msg, sizeof(msg), "%s: %s", msg, strerror(error));
	}
	return glmath_unittest_log(msg, status, __FILE__, line);
}

int memorymanager_sfree(){
	critical::MemoryManager memoryManager;
	critical::memoryManagerAnalyser analyser;
	int status, f, line, t1, t2, error = 0;
	critical::MemoryManager::ConnectedPointer<void> pointer1=memoryManager.salloc<void>(5000);
	try {
		t1=analyser.getTableEntryCount();
		line=__LINE__+1;
		f=memoryManager.sfree(pointer1);
		t2=analyser.getTableEntryCount();
	}catch(critical::MemoryException e){
		error=e.getCError();
	}
	
	
	//printf("\n\ntc:%zu diff:%d f:%d\n\n", analyser.getTableEntryCount(), t1-t2, f);
	if(!analyser.getTableEntryCount() && (t1-t2==1) && f){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	char msg[256]="Deallocation";
	if(error){
		snprintf(msg, sizeof(msg), "%s: %s", msg, strerror(error));
	}
	return glmath_unittest_log(msg, status, __FILE__, line);
}

int memorymanager_alloc_size(){
	critical::MemoryManager memoryManager;
	critical::memoryManagerAnalyser analyser;
	int status, line;
	line=__LINE__+1;
	critical::MemoryManager::ConnectedPointer<void> pointer=memoryManager.salloc<void>(6000);
	if(static_cast<critical::_mem_table_entry*>(analyser.getPointerTable())[analyser.getPtrEindex(pointer)].size==8192){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	pointer.reset();
	return glmath_unittest_log("Allocation Size", status, __FILE__, line);
}

int memorymanager_alloc_valid(){
	critical::MemoryManager memoryManager;
	critical::memoryManagerAnalyser analyser;
	int status, line;
	line=__LINE__+1;
	critical::MemoryManager::ConnectedPointer<void> pointer2=memoryManager.salloc<void>(6000);
	if(static_cast<critical::_mem_table_entry*>(analyser.getPointerTable())[analyser.getPtrEindex(pointer2)].memoryPointer!=(void*)-1){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	pointer2.reset();
	return glmath_unittest_log("Allocation Valid", status, __FILE__, line);
}

int memorymanager_srealloc(){
	critical::MemoryManager memoryManager;
	critical::memoryManagerAnalyser analyser;
	int status, a, b, line, t1, t2, error = 0;
	try{
		t1=analyser.getTableEntryCount();
		line=__LINE__+1;
		critical::MemoryManager::ConnectedPointer<void> pointer1=memoryManager.salloc<void>(10000);
		
		critical::MemoryManager::ConnectedPointer<void> ptr=pointer1+5;
		t2=analyser.getTableEntryCount();
		a=analyser.ptrIsAllocated(pointer1);
		if (ptr.getVoidPtr()==static_cast<void*>(static_cast<char*>(pointer1.getVoidPtr())+5)) {
			b=true;
		}else{
			b=false;
		}
		pointer1.reset();
	}catch(critical::MemoryException e) {
		error=e.getExceptionCode();
	}
	
	if(a && (t2-t1==1) && (error==0) && b){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	char msg[256]="Allocation";
	if(error){
		snprintf(msg, sizeof(msg), "%s: %s", msg, strerror(error));
	}
	return glmath_unittest_log(msg, status, __FILE__, line);
}


int memoryManager_heap_allocation1(){
	critical::MemoryManager memoryManager;
	critical::memoryManagerAnalyser analyser;
	int status, line, error = 0;
	void* ptr1=nullptr;
	void* base=analyser.getHeapBase();
	size_t s=0;
	bool alloc=false;
	
	try {
		line=__LINE__+1;
		critical::MemoryManager::ConnectedPointer<void> ptr=memoryManager.salloc<void>(50);
		ptr1=ptr.getVoidPtr();
		critical::_heap_meta_block* block=reinterpret_cast<critical::_heap_meta_block*>(analyser.getHeapBase());
		s=block->size;
		alloc=analyser.getHeapUsedLimit()->allocated;
		ptr.reset();
		
	} catch (critical::MemoryException e){
		error=e.getExceptionCode();
	}
	
	ptrdiff_t metaoffset=static_cast<char*>(ptr1)-static_cast<char*>(base);
	
	
	
	if( alloc && metaoffset==analyser.getMetaBlockSize() && s==CRITICAL_ALIGN8(50)){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Heap 1. Allocation", status, __FILE__, line);
};

int memoryManager_heap_allocation2(){
	critical::MemoryManager memoryManager;
	critical::memoryManagerAnalyser analyser;
	int status, line, error = 0;
	bool alloc=false;
	try {
		line=__LINE__+1;
		critical::MemoryManager::ConnectedPointer<void> ptr=memoryManager.salloc<void>(400);
		alloc=analyser.getHeapUsedLimit()->allocated;
		ptr.reset();
	} catch (critical::MemoryException e){
		error=e.getExceptionCode();
	}
	
	
	//printf("\nmem:%lx size:%lu last:%lx\n",b, b->size, b->last);
	
	if(alloc){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Heap 2. Allocation", status, __FILE__, line);
}

int memoryManager_heap_block_spliting(){
	critical::MemoryManager memoryManager;
	critical::memoryManagerAnalyser analyser;
	int status, line, error = 0;
	ptrdiff_t blocksize1=0, blocksize2=0, block12=0;
	bool alloc=false;
	try {
		line=__LINE__+1;
		critical::MemoryManager::ConnectedPointer<void> ptr=memoryManager.salloc<void>(100);
		critical::_heap_meta_block* b=analyser.getHeapMetaBlock(ptr);
		blocksize1=reinterpret_cast<char*>(b->next)-reinterpret_cast<char*>(b);
		block12=CRITICAL_ALIGN8(400)-b->next->size-analyser.getMetaBlockSize();
		blocksize2=b->size;
		ptr.reset();
	} catch (critical::MemoryException e){
		error=e.getExceptionCode();
	}
	
	if(blocksize1==CRITICAL_ALIGN8(blocksize1) && block12==blocksize2){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	
	return glmath_unittest_log("Heap Block Spliting", status, __FILE__, line);
}





void memoryManagerTest_Run(){
	critical::MemoryManager manager;
	
	glmath_unittest memoryManagerTest;
	glmath_unittest_function *memoryManagerFunctions;
	int count=9;
	
	memoryManagerFunctions=(glmath_unittest_function*)valloc(count*sizeof(glmath_unittest_function));
	
	memoryManagerFunctions[0]=&memorymanager_init;
	memoryManagerFunctions[1]=&memorymanager_pageSize;
	memoryManagerFunctions[2]=&memorymanager_salloc;
	memoryManagerFunctions[3]=&memorymanager_sfree;
	memoryManagerFunctions[4]=&memorymanager_alloc_size;
	memoryManagerFunctions[5]=&memorymanager_alloc_valid;
	memoryManagerFunctions[6]=&memoryManager_heap_allocation1;
	memoryManagerFunctions[7]=&memoryManager_heap_allocation2;
	memoryManagerFunctions[8]=&memoryManager_heap_block_spliting;
	
	glmath_unittest_create(&memoryManagerTest, "Memorymanager", 1, memoryManagerFunctions, count);
	glmath_unittest_run(&memoryManagerTest);
	critical::MemoryManager::terminate();
}


#endif
