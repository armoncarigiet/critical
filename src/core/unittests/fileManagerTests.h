//
//  fileManagerTests.h
//  critical
//
//  Created by Armon Carigiet on 31.01.15.
//
//

#ifndef critical_fileManagerTests_h
#define critical_fileManagerTests_h

#include <unit/unit_tests.h>
#include "Analysers.h"
#include <critical/core/FileManager.h>
#include <critical/core/String.h>
#include <critical/core/ColladaHelper.h>
#include <critical/core/SharedPointer.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlerror.h>
#include <cstdio>
#include <cstdarg>
#include <iostream>
#include <critical/core/Importer.h>
#include <critical/graphics/TriangulatedMesh.h>
#include <critical/graphics/RenderWindow.h>

#include <glmath/glmath_mat4.h>
#include <glmath/glmath_quat.h>

void parseError(void* ctx, const char *msg, ...){
	char string[256];
	va_list arg_ptr;
	va_start(arg_ptr, msg);
	vsnprintf(string, 256, msg, arg_ptr);
	va_end(arg_ptr);
	std::cout << string << std::endl;
	exit(-1);
}

int filemanager_init(){
//	critical::MemoryManager memoryManager;
//	critical::memoryManagerAnalyser analyser;
	
	printf("\n\npointerSize: %zu", sizeof(critical::MemoryManager::ConnectedPointer<void>));
	
	critical::memoryManagerAnalyser analyser;
	int status, a=1, line;
	
	line=__LINE__+1;
	critical::FileManager::init();
	

	
	
	
	//critical::FileManager::_v.printAlloc();
//	critical::FileManager::FileStream stream=critical::FileManager::openStream((char*)"/Users/armoncarigiet/Desktop/testfile.dae", (char*)"r");
//	size_t size=0;
//
//	critical::MemoryManager::ConnectedPointer<char> p=stream.loadFile(&size);
//
////	char* buf=p.getPtr();
////
////	printf("\n\n\n\n File Content:\n\n\n");
////	for(int i=0; i<size; i++) {
////		printf("%c", buf[i]);
////	}
////	printf("\n\n\n\n EOF \n\n\n");
////
////critical::FileManager::terminate();
//
//	
//	LIBXML_TEST_VERSION
//	xmlDocPtr doc=xmlParseMemory(&p, size);
//	if(doc==NULL){
//		printf("\nparseError!\n");
//	}
//	xmlSetGenericErrorFunc(nullptr, &parseError);
//	xmlNodePtr root=xmlDocGetRootElement(doc);
//	critical::String str(root->name);
//	str.print();
//	
//
//	
//	critical::ColladaHelper h;
//	
//	critical::Scene s=h.getScene(root$9
	
	
	// Serious Math :)
	
	
//	glmath_mat4 mat1=glmath_mat4_create(0.685880482, 0.727633774, -0.0108167799, 0, -0.317370087, 0.312468588, 0.895343184, 0,0.654861927, -0.610665619, 0.445245385, 0, -6.50763988, 5.34366512, 7.48113203, 1);
//	
//	
//	glmath_quat q=glmath_quat_create_mat4(mat1);
//	
//	glmath_mat4 mat2=glmath_mat4_create_quat(q);
//	
//	glmath_quat q2=glmath_quat_create_mat4(mat2);
//	
//	glmath_vec3 axis=glmath_quat_axis(q);
//	glmath_real angle=glmath_quat_angle(q);
//	
//	glmath_vec4 v1=glmath_mat4_mul_vec4(mat1, {{1.0f, 1.0f, 1.0f}});
//	glmath_vec4 v2=glmath_quat_rotate_vec4({{1.0f, 1.0f, 1.0f}}, q);
//	
//	printf("");
	
	
	critical::Importer importer;
	
	critical::MemoryManager::ConnectedPointer<critical::Scene> ptr=importer.loadSceneFromFile("/Users/armoncarigiet/Desktop/testfile.dae");
	
	critical::Scene *sc=&ptr;
	
	//printf("\nUpAxis: %d",sc->attr["UpAxis"]);
	
	printf("meshPositions:\n");
	
	sc->cameras.printref();
	
	
	//printf("\nx:%f y:%f z:%f", sc->cameras[0]._forward.x, sc->cameras[0]._forward.y, sc->cameras[0]._forward.z);
	
	printf("");
	
	
	
	
//	printf("\n\nFileManager: ");
//	critical::FileManager::_v.printAlloc();
//	printf(" ");
//	critical::FileManager::_v.printref();
//	printf(" Capacity: %zu", critical::FileManager::_v.capacity());
//	printf("\n\n");
	
	//printf("");
	
//	critical::ColladaHelper::metadata m=h.getMetaData(root);
//	
//	//m.print();
//	
//	xmlNode* ar=NULL;
//	xmlNode* materialNode=NULL;
//	
//	for(xmlNode *n=root->children; n!=NULL; n=n->next){
//		if(strcmp((char*)n->name, critical::ColladaHelper::libraryGeometriesTag)==0){
//			ar=n->children;
//			ar=ar->next;
//			FileManageren;
//			ar=ar->next;
//		}else if(strcmp((char*)n->name, critical::ColladaHelper::libraryMaterialsTag)==0){
//			materialNode=n;
//		}
//	}
//	
//	critical::Vector<critical::Material> material;
//	critical::Mesh mesh;
//	
//	h.loadMaterials(material, materialNode);
//	
//	
//	
//	
//	h.loadMesh(mesh, ar, material);
//	
//	for (int t=0; t<mesh._primitives[0].vindex.size(); t++) {
//		printf("%d / ",mesh._primitives[0].vindex[t]);
//	}
//	printf("\n\nambient:");
//	for(int i=0; i<mesh._primitives[0].material->d["ambient"].size(); i++) {
//		printf(" %f", mesh._primitives[0].material->d["ambient"][i]);
//	}
//	
//	printf("\n\n\n\nsize: %zu\n\n\n\n", material.size());
	
//	printf("\n");
//	for(int i=0; i<mesh._normals.size(); i++) {
//		printf("%e\t%e\t%e\n ", mesh._normals[i].x, mesh._normals[i].y, mesh._normals[i].z);
//	}
//	
//	printf("\n\n meshnsize: %zu\n", mesh._normals.size());
//	for(int i=0; i<mesh._vertices.size(); i++) {
//		printf("%e\t%e\t%e\n ", mesh._vertices[i].x, mesh._vertices[i].y, mesh._vertices[i].z);
//	}

	
	
//	ar=ar->children;
//	ar=ar->next;
//	ar=ar->children;
//	ar=ar->next;
//	critical::Array<float> array=h.resolveFloatArray(ar);
	
//	printf("\n");
//	for(int i=0; i<array.size(); i++) {
//		printf("%e ",array[i]);
//	}
	
	//critical::Scene s=h.getMainScene(root);
	
	
	
//	if (xmlStrcmp(root->name, (const xmlChar *) "COLLADA")) {
//		printf("document of the wrong type, root node != COLLADA");
//		xmlFreeDoc(doc);
//	}else{
//		printf("%s", root->name);
//	}
	
	if(a){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	return glmath_unittest_log("Intitialisation", status, __FILE__, line);
}
void fileManagerTest_Run(){
	critical::MemoryManager::init();
	
	glmath_unittest fileManagerTest;
	glmath_unittest_function *fileManagerFunctions;
	int count=1;
	
	fileManagerFunctions=(glmath_unittest_function*)valloc(count*sizeof(glmath_unittest_function));
	
	fileManagerFunctions[0]=&filemanager_init;
	
	glmath_unittest_create(&fileManagerTest, "FileManager", 1, fileManagerFunctions, count);
	glmath_unittest_run(&fileManagerTest);
	critical::FileManager::terminate();
	critical::MemoryManager::terminate();
}


#endif
