//
//  memoryTableTests.h
//  critical
//
//  Created by Armon Carigiet on 07.12.14.
//
//

#ifndef critical_memoryTableTests_h
#define critical_memoryTableTests_h

#include <unit/unit_tests.h>

#include <critical/core/MemoryTable.h>

namespace critical{
	class memoryTableAnalyser{
	private:
		MemoryTable &table;
	public:
		memoryTableAnalyser(MemoryTable &t): table(t){}
		~memoryTableAnalyser(){}
		
		size_t getTableSize(){
			return table.tableSize;
		}
		size_t getTableUsedSize(){
			return table.usedTableSize;
		}
		
		size_t getBlockSize(){
			return table.blockSize;
		}
		
		int getTableEntryCount(){
			return table.tableEntryCount;
		}
		MemoryManager::connected_pointer<void>& getMemoryPtr(){
			return table.ptr;
		}
	};
}

int memorytable_init(){
	int c=6;
	int size=500;
	critical::MemoryManager manager;
	critical::MemoryTable table(size, c);
	critical::memoryTableAnalyser analyser(table);
	int status, a, line;
	//int a=analyser.isInizialised();
	//printf("%d",analyser.getTableEntryCount());
	line=__LINE__+1;
	size_t s=analyser.getTableSize();
	size_t b=analyser.getBlockSize();
	int e=analyser.getTableEntryCount();
	critical::MemoryManager::connected_pointer<void> &tentry=analyser.getMemoryPtr();
	
	printf("tmem:%lx\n", &tentry);
	
	//printf("s:%zu b:%zu e:%d \ntentry allocated:%d size:%zu\n", s, b, e, tentry.tableEntry->allocated, tentry.tableEntry->size);
	//printf("table[0] allocated:%d size:%zu\n", critical::MemoryManager::pointerTable[0].allocated, critical::MemoryManager::pointerTable[0].size);
	//printf("entry: mem:%x \t table: mem:%x\n",tentry.tableEntry, &critical::MemoryManager::pointerTable[0]);
	
	if(s==c && b==size && e==0 && analyser. tentry.tableEntry->allocated && tentry.tableEntry->size==CRITICAL_CEIL(size*5, manager.getPageSize())){
		status=GLMATH_UNITTEST_STATUS_OK;
	}else{
		status=GLMATH_UNITTEST_STATUS_FATAL_ERROR;
	}
	return glmath_unittest_log("Intitialisation", status, __FILE__, line);

}

void memoryTableTest_Run(){
	critical::MemoryManager::init();
	
	//critical::MemoryManager::connected_pointer<void*> ptr=manager.salloc<void*>(4096);
	
	glmath_unittest memoryTableTest;
	glmath_unittest_function *memoryTableFunctions;
	int count=1;
	
	memoryTableFunctions=(glmath_unittest_function*)malloc(count*sizeof(glmath_unittest_function));
	
	memoryTableFunctions[0]=&memorytable_init;
	
	glmath_unittest_create(&memoryTableTest, "MemoryTable", 1, memoryTableFunctions, count);
	glmath_unittest_run(&memoryTableTest);
	critical::MemoryManager::terminate();
}


#endif
