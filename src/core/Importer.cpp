//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file Importer.cpp
 *	@date 21.01.2015.
 *  @author Armon Carigiet
 *  @see Importer.h
 */
//===========================================================================

#include <critical/core/Importer.h>

#include <critical/core/FileManager.h>
#include <critical/core/ColladaHelper.h>

#include <libxml/parser.h>

namespace critical {
	
static const char* getFileExtension(const char* path){
    char* ptr=strrchr(path, '.');
    return ++ptr;
}
	
static void __throw_unsupportet_file_format(){
    printf("\nUnsupported File Format!\n");
    throw 1;
}
	
MemoryManager::ConnectedPointer<Scene> Importer::loadSceneFromFile(const char* path){
	Scene scene;
	FileManager::FileStream stream=FileManager::openStream(path, "r");
	size_t fileSize=0;
	MemoryManager::ConnectedPointer<char> p=stream.loadFile(&fileSize);
	MemoryManager::ConnectedPointer<Scene> s;
	if(strcmp(getFileExtension(path), "dae")==0){
		
		xmlDocPtr doc=xmlParseMemory(&p, fileSize);
		ColladaHelper helper;
		 s=helper.getScene(xmlDocGetRootElement(doc));
		xmlFreeDoc(doc);
	}else{
		__throw_unsupportet_file_format();
	}
	FileManager::closeStream(stream);
	
	//MemoryManager::ConnectedPointer<Scene> s=MemoryManager::salloc<Scene>(sizeof(Scene));
	//*(s.getPtr())=Scene(scene);
	
	return s;
}
	
}