cmake_minimum_required(VERSION 3.0)

set(PHYSICS_INC ${CMAKE_SOURCE_DIR}/include/critical/physics)
set(PHYSICS_SRC ${CMAKE_SOURCE_DIR}/src/physics)
set(PHYSICS_TEST ${CMAKE_SOURCE_DIR}/src/physics/unittests)

set(CRITICAL_PHYSICS_SRC
	${PHYSICS_INC}/physics.h
	${PHYSICS_INC}/pherror.h
	${PHYSICS_INC}/pforcegen.h

	${PHYSICS_INC}/Constants.h
	${PHYSICS_SRC}/Constants.cpp

	${PHYSICS_INC}/PhysicsVector.h
	${PHYSICS_SRC}/PhysicsVector.cpp
	${PHYSICS_INC}/ParticleList.h
	${PHYSICS_SRC}/ParticleList.cpp
	${PHYSICS_INC}/RigidBodyList.h
	${PHYSICS_SRC}/RigidBodyList.cpp


	${PHYSICS_INC}/ForceObject.h
	${PHYSICS_SRC}/ForceObject.cpp
	${PHYSICS_INC}/Particle.h
	${PHYSICS_SRC}/Particle.cpp
	${PHYSICS_INC}/SParticle.h
	${PHYSICS_SRC}/SParticle.cpp
	${PHYSICS_INC}/RigidBody.h
	${PHYSICS_SRC}/RigidBody.cpp
	${PHYSICS_INC}/PointMass.h
	${PHYSICS_SRC}/PointMass.cpp
	
	${PHYSICS_INC}/BSphere.h
	${PHYSICS_SRC}/BSphere.cpp
	${PHYSICS_INC}/ConvexHull.h
	${PHYSICS_SRC}/ConvexHull.cpp


	${PHYSICS_INC}/ForceGenerator.h
	${PHYSICS_SRC}/ForceGenerator.cpp
	${PHYSICS_INC}/ForceGeneratorList.h
	${PHYSICS_SRC}/ForceGeneratorList.cpp
	${PHYSICS_INC}/ParticleForceGenerator.h
	${PHYSICS_SRC}/ParticleForceGenerator.cpp
	${PHYSICS_INC}/SimpleParticleGravityGenerator.h
	${PHYSICS_SRC}/SimpleParticleGravityGenerator.cpp
	${PHYSICS_INC}/ParticleGravityGenerator.h
	${PHYSICS_SRC}/ParticleGravityGenerator.cpp
	${PHYSICS_INC}/ParticleDragGenerator.h
	${PHYSICS_SRC}/ParticleDragGenerator.cpp
	${PHYSICS_INC}/RigidBodyForceGenerator.h
	${PHYSICS_SRC}/RigidBodyForceGenerator.cpp
	${PHYSICS_INC}/RigidBodySpinGenerator.h
	${PHYSICS_SRC}/RigidBodySpinGenerator.cpp

	${PHYSICS_INC}/ParticleLink.h
	${PHYSICS_SRC}/ParticleLink.cpp
	${PHYSICS_INC}/ParticleSpring.h
	${PHYSICS_SRC}/ParticleSpring.cpp
	${PHYSICS_INC}/FixedParticleSpring.h
	${PHYSICS_SRC}/FixedParticleSpring.cpp
	${PHYSICS_INC}/ParticleBungeeRope.h
	${PHYSICS_SRC}/ParticleBungeeRope.cpp
	${PHYSICS_INC}/FixedParticleBungeeRope.h
	${PHYSICS_SRC}/FixedParticleBungeeRope.cpp
	
	${PHYSICS_INC}/ParticleSpace.h
	${PHYSICS_SRC}/ParticleSpace.cpp
	${PHYSICS_INC}/RigidBodySpace.h
	${PHYSICS_SRC}/RigidBodySpace.cpp
	${PHYSICS_INC}/ParticleSystem.h
	${PHYSICS_SRC}/ParticleSystem.cpp
	${PHYSICS_INC}/PointMassSystem.h
	${PHYSICS_SRC}/PointMassSystem.cpp


	${PHYSICS_INC}/CollisionDetector.h
	${PHYSICS_SRC}/CollisionDetector.cpp


	${PHYSICS_INC}/Geometry.h
	${PHYSICS_SRC}/Geometry.cpp
	
	${PHYSICS_INC}/PhysicsRenderWindow.h
	${PHYSICS_SRC}/PhysicsRenderWindow.cpp

)

set(CRITICAL_PHYSICS_TEST_SRC

)

critical_add_library(${CRITICAL_PHYSICS_LIB} TYPE ${CRITICAL_LIBRARY_TYPE} SOURCES ${CRITICAL_PHYSICS_SRC})
target_link_libraries(${CRITICAL_PHYSICS_LIB} ${CRITICAL_CORE_LIB})

add_executable(${CRITICAL_PHYSICS_UNITTESTS} unittests/tests.cpp ${CRITICAL_PHYSICS_TEST_SRC})
target_link_libraries(${CRITICAL_PHYSICS_UNITTESTS} ${CRITICAL_PHYSICS_LIB})

install(TARGETS  ${CRITICAL_PHYSICS_LIB}
	RUNTIME DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
	LIBRARY DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
	ARCHIVE DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
	FRAMEWORK DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
)

install(CODE "execute_process(COMMAND install_name_tool -id ${CMAKE_SOURCE_DIR}/lib/lib${CRITICAL_PHYSICS_LIB}.dylib ${CMAKE_INSTALL_PREFIX}/lib/lib${CRITICAL_PHYSICS_LIB}.dylib) ")

install(CODE "execute_process(COMMAND install_name_tool -change @rpath/libcriticalcore.dylib ${CMAKE_INSTALL_PREFIX}/lib/lib${CRITICAL_CORE_LIB}.dylib ${CMAKE_INSTALL_PREFIX}/lib/lib${CRITICAL_PHYSICS_LIB}.dylib) ")

install(TARGETS ${CRITICAL_PHYSICS_UNITTESTS}
RUNTIME DESTINATION "${CMAKE_SOURCE_DIR}/tests/unittests"
)
