//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file ParticleSpace.cpp
 *	@date 08.05.2015.
 *  @author Armon Carigiet
 *  @see ParticleSpace.h
 *  @ingroup physics
 */
//===========================================================================

#include <critical/physics/ParticleSpace.h>

namespace critical { namespace physics {
	
Particle* ParticleSpace::addObject(Particle &p){
	ParticleList<>::Iterator it=plist.pushBack(p);
	return &(*it);
}
	
Particle* ParticleSpace::addObject(Particle &&p){
	ParticleList<>::Iterator it=plist.pushBack(p);
	return &(*it);
}
	
bool ParticleSpace::removeObject(Particle *p){
	for(ParticleList<>::Iterator it=plist.begin(); it.isOutOfRange(); it++) {
		if(&(*it)==p){
			plist.erase(it);
			return true;
		}
	}
	return false;
}
	
bool ParticleSpace::removeObject(String &n){
	for(ParticleList<>::Iterator it=plist.begin(); it.isOutOfRange(); it++) {
		if(it->identifier.isEqual(n)){
			plist.erase(it);
			return true;
		}
	}
	return false;
}
	
bool ParticleSpace::removeObject(String &&n){
	for(ParticleList<>::Iterator it=plist.begin(); it.isOutOfRange(); it++) {
		if(it->identifier.isEqual(n)){
			plist.erase(it);
			return true;
		}
	}
	return false;
}
	
void ParticleSpace::initFrame(){
	integrate(CRITICAL_IDEAL_FRAME/2);
}
	
void ParticleSpace::applyForces(){
	if(simulationActive){
        for(ForceGeneratorList<ParticleForceGenerator>::Iterator<ParticleForceGenerator> it=fgenList.begin<ParticleForceGenerator>(); !it.isOutOfRange(); it++){
            if(it.getForceGeneratorType()==CRSimpleParticleGravityGenerator){
                SimpleParticleGravityGenerator* g=it.getForceGenerator<SimpleParticleGravityGenerator>();
                g->applyForce();
            }else if(it.getForceGeneratorType()==CRParticleGravityGenerator){
                ParticleGravityGenerator* g=it.getForceGenerator<ParticleGravityGenerator>();
                g->applyForce();
            }else if(it.getForceGeneratorType()==CRParticleDragGenerator){
                ParticleDragGenerator* g=it.getForceGenerator<ParticleDragGenerator>();
                g->applyForce();
            }
        }
    }
    
//		for(ForceGeneratorList<SimpleParticleGravityGenerator>::Iterator it=sGravGenList.begin(); !it.isOutOfRange(); it++) {
//			it->applyForce();
//		}
//		for(ForceGeneratorList<ParticleGravityGenerator>::Iterator it=gravGenList.begin(); !it.isOutOfRange(); it++) {
//			it->applyForce();
//		}
//		for(ForceGeneratorList<ParticleDragGenerator>::Iterator it=dragGenList.begin(); !it.isOutOfRange(); it++) {
//			it->applyForce();
//		}
	
}
	
void ParticleSpace::integrate(glmath_real dt){
	if(simulationActive){
		plist.integrate(dt);
	}
}
	
void ParticleSpace::writeData(void *mem){
	glmath_vec3 *ptr=static_cast<glmath_vec3*>(mem);
	
	int i=0;
	for(ParticleList<>::Iterator it=plist.begin(); !it.isOutOfRange(); it++) {
		ptr[i]=it->position;
		i++;
	}
}
	
}}