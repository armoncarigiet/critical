//
//  tests.cpp
//  
//
//  Created by Armon Carigiet on 15.11.14.
//
//


#include <stdio.h>
#include <critical/physics/Particle.h>

int main(){
	//memoryManagerTest_Run();
	
	critical::physics::Particle p("sdf",0.5);
	glmath_vec3 p0=p.getPosition();
	glmath_vec3 v0=p.getVelocity();
	
	printf("mass: %f position: {%f,%f,%f} velocity: {%f,%f,%f}", p.getMass(), p0.x,p0.y,p0.z,v0.x,v0.y,v0.z);
    
    // tests may be needed for rigidbodies.
}

