//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file ParticleDragGenerator.cpp
 *	@date 17.04.2015.
 *  @author Armon Carigiet
 *  @see ParticleDragGenerator.h
 *  @ingroup physics
 */
//===========================================================================

#include <critical/physics/ParticleDragGenerator.h>

namespace critical { namespace physics {
	
void ParticleDragGenerator::setDensity(glmath_real density){
	d=density;
}
	
glmath_real ParticleDragGenerator::getDensity(){
	return d;
}
	
void ParticleDragGenerator::setDragCoefficient(glmath_real drag_coefficient){
	cw=drag_coefficient;
}
	
glmath_real ParticleDragGenerator::getDragCoefficient(){
	return cw;
}
	
void ParticleDragGenerator::setSurface(glmath_real surface){
	a=surface;
}
	
glmath_real ParticleDragGenerator::getSurface(){
	return a;
}
	
void ParticleDragGenerator::applyForce(){
    if(active){
        int size=_p.size();
        for(int i=0; i<size; i++){
            Particle *p=_p[i];
            glmath_real k=-0.5*a*d*cw;
            glmath_vec3 force=glmath_vec3_mul_s(p->velocity,k*glmath_vec3_mag(p->velocity));
            p->addForce(force);
        }
    }
}

}}
