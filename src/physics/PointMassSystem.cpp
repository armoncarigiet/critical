//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file PointMassSystem.cpp
 *	@date 30.07.2015.
 *  @author Armon Carigiet
 *  @see PointMassSystem.h
 *  @ingroup physics
 */
//===========================================================================

#include <critical/physics/PointMassSystem.h>

namespace critical { namespace physics {

void PointMassSystem::addObject(PointMass &m){
    pm.pushBack(m);
}
void PointMassSystem::addObject(PointMass &&m){
        pm.pushBack(m);
}
    
glmath_vec3 PointMassSystem::calculateCenterOfMass(){
    glmath_vec3 cm=glmath_vec3_create(0, 0, 0);
    glmath_real mass=0;
    for(List<PointMass>::Iterator it=pm.begin(); !it.isOutOfRange(); it++){
        cm=glmath_vec3_add(cm, glmath_vec3_mul_s(it->position,it->mass));
        mass+=it->mass;
    }
    return cm;
}

glmath_mat3 PointMassSystem::calculateInertiaTensor(){
    glmath_mat3 in={{0,0,0,0,0,0,0,0,0}};
    //glmath_vec3 cm=calculateCenterOfMass();
    for(List<PointMass>::Iterator it=pm.begin(); !it.isOutOfRange(); it++){
        glmath_vec3 r=it->position;
        in.m[0]+=(r.y*r.y + r.z*r.z)*it->mass;
        in.m[1]-=(r.x*r.y)*it->mass;
        in.m[2]-=(r.x*r.z)*it->mass;
        in.m[3]-=(r.x*r.y)*it->mass;
        in.m[4]+=(r.x*r.x + r.z*r.z)*it->mass;
        in.m[5]-=(r.y*r.z)*it->mass;
        in.m[6]-=(r.x*r.z)*it->mass;
        in.m[7]-=(r.y*r.z)*it->mass;
        in.m[8]+=(r.x*r.x + r.y*r.y)*it->mass;
    }
    
    return in;
}
    
}}