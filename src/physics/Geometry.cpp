//
//  Geometry.cpp
//  critical
//
//  Created by Armon Carigiet on 08.05.15.
//
//

#include <critical/physics/Geometry.h>

namespace critical { namespace math {

Line::Line(glmath_vec3 p, glmath_vec3 d){
	position=p;
	direction=d;
}

glmath_vec3 Line::getPosition(){
	return position;
}

glmath_vec3 Line::getDirection(){
	return direction;
}

void Line::setPosition(glmath_vec3 &p){
	position=p;
}

void Line::setDirection(glmath_vec3 &d){
	direction=d;
}

bool Line::isElement(glmath_vec3 &point){
	glmath_real tx=(point.x-position.x)/direction.x;
	glmath_real ty=(point.y-position.y)/direction.y;
	glmath_real tz=(point.z-position.z)/direction.z;
	if(tx==ty && tx==tz){
		return true;
	}
	return false;
}

bool Line::intersects(Line &line, glmath_vec3 *intersection_point, glmath_real *intersection_angle){
	
	if(intersection_point){
	
	}
	if(intersection_angle){
		
	}
    return false;
}

bool Line::intersects(Plane &plane, Line *intersection_line, glmath_real *intersection_angle){
    return false;
}

glmath_real Line::distance(glmath_vec3 &point){
	return false;
}

glmath_real Line::distance(Line &line){
	return false;
}

glmath_real Line::distance(Plane &plane){
	return false;
}
	
	
}}




