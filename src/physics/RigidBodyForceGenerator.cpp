//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file RigidBodyForceGenerator.cpp
 *	@date 09.08.2015.
 *  @author Armon Carigiet
 *  @see RigidBodyForceGenerator.h
 *  @ingroup physics
 */
//===========================================================================

#include <critical/physics/RigidBodyForceGenerator.h>

namespace critical { namespace physics {

Vector<RigidBody*>::Iterator RigidBodyForceGenerator::addObject(RigidBody *rigidBody){
    return _rb.pushBack(rigidBody);
}

Vector<RigidBody*>::Iterator RigidBodyForceGenerator::addObjects(RigidBodyList<>*list){
    bool f=true;
    Vector<RigidBody*>::Iterator i;
    _rb.allocate(list->size()*sizeof(RigidBody*));
    for(RigidBodyList<>::Iterator it=list->begin(); !it.isOutOfRange(); it++){
        if(f){
            i=_rb.pushBack(&it);
            f=false;
        }else{
            _rb.pushBack(&it);
        }
    }
    return i;
}
}}