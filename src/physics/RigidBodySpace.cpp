//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file RigidBodySpace.cpp
 *	@date 14.05.2015.
 *  @author Armon Carigiet
 *  @see RigidBodySpace.h
 *  @ingroup physics
 */
//===========================================================================

#include <critical/physics/RigidBodySpace.h>

namespace critical { namespace physics {
    
RigidBody* RigidBodySpace::addObject(RigidBody &rb){
    RigidBodyList<>::Iterator it=rblist.pushBack(rb);
    return &(*it);
}
    
RigidBody* RigidBodySpace::addObject(RigidBody &&rb){
    RigidBodyList<>::Iterator it=rblist.pushBack(rb);
    return &(*it);
}
    
bool RigidBodySpace::removeObject(RigidBody *rb){
    for(RigidBodyList<>::Iterator it=rblist.begin(); it.isOutOfRange(); it++) {
        if(&(*it)==rb){
            rblist.erase(it);
            return true;
        }
    }
    return false;
}
    
bool RigidBodySpace::removeObject(String &n){
    for(RigidBodyList<>::Iterator it=rblist.begin(); it.isOutOfRange(); it++) {
        if(it->identifier.isEqual(n)){
            rblist.erase(it);
            return true;
        }
    }
    return false;
}
    
bool RigidBodySpace::removeObject(String &&n){
    for(RigidBodyList<>::Iterator it=rblist.begin(); it.isOutOfRange(); it++) {
        if(it->identifier.isEqual(n)){
            rblist.erase(it);
            return true;
        }
    }
    return false;
}
    
    //void RigidBodySpace::initFrame();
void RigidBodySpace::applyForces(){
    if(simulationActive){
        for(ForceGeneratorList<RigidBodyForceGenerator>::Iterator<RigidBodyForceGenerator> it=fgenList.begin<RigidBodyForceGenerator>(); !it.isOutOfRange(); it++){
            if(it.getForceGeneratorType()==CRRigidBodySpinGenerator){
                RigidBodySpinGenerator* g=it.getForceGenerator<RigidBodySpinGenerator>();
                g->applyForce();
            }else if(it.getForceGeneratorType()==CRUnknowen){
               // ...
            }
        }
    }
}
    
void RigidBodySpace::integrate(glmath_real dt){
    if(simulationActive){
        rblist.integrate(dt);
    }
}

}}