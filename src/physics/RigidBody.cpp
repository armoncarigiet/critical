//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file RigidBody.cpp
 *	@date 15.04.2015.
 *  @author Armon Carigiet
 *  @see RigidBody.h
 *  @ingroup physics
 */
//===========================================================================

#include <critical/physics/RigidBody.h>

namespace critical { namespace physics {
    
void RigidBody::addForceAtPoint(glmath_vec3 force, glmath_vec3 point){
    glmath_vec3 ra=glmath_vec3_sub(point, position);
    glmath_vec3 torque=glmath_vec3_cross(ra, force);
    glmath_vec3 f=glmath_vec3_proj(force, ra);
    
    torqueAccumulator=glmath_vec3_add(torqueAccumulator, torque);
    ForceAccumulator=glmath_vec3_add(ForceAccumulator, f);
}
    
void RigidBody::addForceAtBodyPoint(glmath_vec3 force, glmath_vec3 point){
    //glmath_vec3 p=glmath_quat_rotate_vec3(point, orientation);
    //glmath_vec3 fo=glmath_quat_rotate_vec3(force, orientation);
    glmath_vec3 torque=glmath_vec3_cross(point, force);
    
    
    
    glmath_vec3 f=glmath_vec3_proj(force, point);
    
    torqueAccumulator=glmath_vec3_add(torqueAccumulator, torque);
    ForceAccumulator=glmath_vec3_add(ForceAccumulator, f);
}
	
void RigidBody::clearTorqueAccumulator(){
    torqueAccumulator=glmath_vec3_create(0, 0, 0);
}
    
void RigidBody::clearAccumulators(){
    clearForceAccumulator();
    clearTorqueAccumulator();
}
    
void RigidBody::integrate(glmath_real dt){
    calculateWorldInertiaTensor();
    
	position=glmath_vec3_add(position, glmath_vec3_mul_s(velocity, dt));
	//glmath_real speed=glmath_vec3_mag(velocity);	// expensive!
	glmath_vec3 force_acceleration=glmath_vec3_mul_s(ForceAccumulator, inverseMass);
	velocity=glmath_vec3_add(velocity, glmath_vec3_mul_s(force_acceleration, dt));
	
    glmath_quat q=glmath_quat_create_vec3(0, angularvelocity);
    
    q=glmath_quat_mul(orientation, q);
    
    
    orientation.w += q.w*0.5*dt;
    orientation.x += q.x*0.5*dt;
    orientation.y += q.y*0.5*dt;
    orientation.z += q.z*0.5*dt;
    
    orientation=glmath_quat_norm(orientation);
    
//    glmath_vec3 angulardisplacement=glmath_vec3_mul_s(angularvelocity, dt);
//    glmath_real m=glmath_vec3_mag(angulardisplacement)*2*M_PI;
//    
//    if(m!=0){
//        angulardisplacement=glmath_vec3_norm(angulardisplacement);
//    }
//    
//    morientation=glmath_mat4_mul(morientation, glmath_mat4_create_rotation(m, angulardisplacement));
//
//    if(m!=0){
//        angulardisplacement=glmath_vec3_norm(angulardisplacement);
//    }
//
//    glmath_quat q=glmath_quat_create_rotation(m, angulardisplacement);
    
    glmath_transform_params i=glmath_mat4_decompose(glmath_mat4_create_quat(orientation));
    
    //printf("x: %f\ty: %f\tz: %f\n", angularvelocity.x, angularvelocity.y, angularvelocity.z);
    
    //orientation=glmath_quat_mul(q, orientation);
    
    //glmath_transform_params k=glmath_mat4_decompose(glmath_mat4_create_quat(orientation));
    
    //glmath_vec3 angular_acceleration=glmath_vec3_create(0, 0, 1);
    glmath_vec3 angular_acceleration=glmath_mat3_mul_vec3(WorldInverseInertiaTensor, torqueAccumulator);
    //glmath_vec3 vec=glmath_vec3_mul_s(angular_acceleration, dt);
   //// printf("rot: %f\tt: %f\tm: %f\tav: %f\tor: %f\n", i.r.z, dt, m, angularvelocity.z, k.r.z);
	angularvelocity=glmath_vec3_add(angularvelocity, glmath_vec3_mul_s(angular_acceleration, dt));
    
    //printf("x: %f\ty: %f\tz: %f\n", angularvelocity.x, angularvelocity.y, angularvelocity.z);
    
    this->clearAccumulators();
}
    
bool RigidBody::setInertiaTensor(glmath_mat3 t){
    int inv=false;
    InverseInertiaTensor=glmath_mat3_invert(t, &inv);
    return inv;
    //InertiaTensor=t;
}
    
void RigidBody::setCeneterOfMass(glmath_vec3 cm){
    centerOfMass=cm;
}
	
glmath_mat3 RigidBody::calculateInertiaTensorWithRotationPoint(glmath_vec3 rotationPoint){
    glmath_mat3 t=glmath_mat3_invert(InverseInertiaTensor, nullptr);
    glmath_real rsquared=glmath_vec3_dot(rotationPoint, rotationPoint);
    t.m[0]=t.m[0]+(rsquared-rotationPoint.x*rotationPoint.x)/inverseMass;
    t.m[4]=t.m[4]+(rsquared-rotationPoint.y*rotationPoint.y)/inverseMass;
    t.m[8]=t.m[8]+(rsquared-rotationPoint.z*rotationPoint.z)/inverseMass;
    glmath_real xy=(rotationPoint.x*rotationPoint.y)/inverseMass;
    glmath_real xz=(rotationPoint.x*rotationPoint.z)/inverseMass;
    glmath_real yz=(rotationPoint.y*rotationPoint.z)/inverseMass;
    
    t.m[1]=t.m[1]+xy;
    t.m[2]=t.m[2]+xz;
    t.m[3]=t.m[3]+xy;
    t.m[5]=t.m[5]+yz;
    t.m[6]=t.m[6]+xy;
    t.m[7]=t.m[7]+yz;
    
    return t;
}

void RigidBody::recalculateInertiaTensorWithRotationPoint(glmath_vec3 rotationPoint){
    setInertiaTensor(calculateInertiaTensorWithRotationPoint(rotationPoint));
}
    
//void RigidBody::recalculateInertiaTensor(glmath_vec3 ds, glmath_quat dr){
//    glmath_mat3 rotation=glmath_mat3_create_quat(dr);
//    InertiaTensor=glmath_mat3_mul(glmath_mat3_mul(rotation, InertiaTensor), glmath_mat3_transpose(rotation));
//    glmath_mat3 identity=glmath_mat3_create_identity();
//    for(int i=0; i<9; i++){
//        InertiaTensor.m[i]=InertiaTensor.m[i]+(glmath_vec3_dot(ds, ds)*identity.m[i]-ds.v[i%3]*ds.v[(int)i/(int)3])/inverseMass;
//    }
//}
    
//void RigidBody::calculateTransformationMatrix(){
//    tmat=glmath_mat4_create_quat(orientation);
//    tmat.m[12]=position.x;
//    tmat.m[13]=position.y;
//    tmat.m[14]=position.z;
//}

void RigidBody::calculateWorldInertiaTensor(){
    //WorldInverseInertiaTensor=InverseInertiaTensor;
    glmath_mat3 r=glmath_mat3_create_quat(orientation);
    WorldInverseInertiaTensor=glmath_mat3_mul(glmath_mat3_mul(r, InverseInertiaTensor), glmath_mat3_transpose(r));
}
    
//void RigidBody::calculateDerivedData(){
//	//tmat=glmath_mat4_translate_vec3(glmath_mat4_create_quat(orientation), position);
//}
    
}}