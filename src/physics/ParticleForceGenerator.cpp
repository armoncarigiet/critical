//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file ParticleForceGenerator.cpp
 *	@date 16.04.2015.
 *  @author Armon Carigiet
 *  @see ParticleForceGenerator.h
 *  @ingroup physics
 */
//===========================================================================

#include <critical/physics/ParticleForceGenerator.h>

namespace critical { namespace physics {

Vector<Particle*>::Iterator ParticleForceGenerator::addObject(Particle *particle){
	return _p.pushBack(particle);
}
	
Vector<Particle*>::Iterator ParticleForceGenerator::addObjects(ParticleList<>*list){
	bool f=true;
	Vector<Particle*>::Iterator i;
	_p.allocate(list->size()*sizeof(Particle*));
	for(ParticleList<>::Iterator it=list->begin(); !it.isOutOfRange(); it++){
		if(f){
			i=_p.pushBack(&it);
			f=false;
		}else{
			_p.pushBack(&it);
		}
	}
	return i;
}}

};