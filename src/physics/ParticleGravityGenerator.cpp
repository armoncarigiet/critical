//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file ParticleGravityGenerator.cpp
 *	@date 16.04.2015.
 *  @author Armon Carigiet
 *  @see ParticleGravityGenerator.h
 *  @ingroup physics
 */
//===========================================================================

#include <critical/physics/ParticleGravityGenerator.h>

namespace critical { namespace physics {
	
void ParticleGravityGenerator::applyForce(){
    if(active){
        glmath_vec3 f;
        
        for(int i=0; i<_p.size(); ++i) {
            for(int k=i+1; k<_p.size(); ++k) {
                
                f=glmath_vec3_norm(glmath_vec3_sub(_p[k]->position, _p[i]->position));
                glmath_real mk=(physics::constants::G()/_p[i]->inverseMass/_p[k]->inverseMass)/glmath_vec3_dot(f, f);
                f=glmath_vec3_mul_s(f, mk);
                _p[i]->ForceAccumulator=glmath_vec3_add(_p[i]->ForceAccumulator, glmath_vec3_neg(f));
                _p[k]->ForceAccumulator=glmath_vec3_add(_p[k]->ForceAccumulator, f);
                
            }
        }
    }
}
	
}}