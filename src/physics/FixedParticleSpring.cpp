//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file FixedParticleSpring.cpp
 *	@date 18.04.2015.
 *  @author Armon Carigiet
 *  @see FixedParticleSpring.h
 *  @ingroup physics
 */
//===========================================================================

#include <critical/physics/FixedParticleSpring.h>

namespace critical { namespace physics {
	void FixedParticleSpring::applyForce(){
		glmath_vec3 force=glmath_vec3_sub(fixed_point, _p->position);
		glmath_real dist=glmath_vec3_mag(force);
		
		glmath_real l=dist-slength;
		
		force=glmath_vec3_mul_s(force, (l*l*sconstant)/dist);
			
		// action=reactio
		_p->ForceAccumulator=glmath_vec3_add(_p->ForceAccumulator, force);
		
	}
}}