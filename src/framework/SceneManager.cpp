//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file SceneManager.cpp
 *	@date 16.05.2015.
 *  @author Armon Carigiet
 *
 *  @ingroup framework
 *  @see SceneManager.h
 */
//===========================================================================

#include <critical/framework/SceneManager.h>

namespace critical {
	
void SceneManager::resolveNode(SceneNode *n, Scene* scene){
	for(int i=0; i<n->mindex.size(); i++) {
		Vector<TriangulatedMesh>::Iterator it=meshes.pushBack(TriangulatedMesh());
		uint64_t index=n->mindex[i];
		AlignedList<glmath_mat4>::Iterator mit=phymatrices.pushBack(glmath_mat4_create_identity());
		it->triangulateMesh(scene->meshes[index], n->_tmat, &(*mit));
		it->_material=scene->meshes[index]._primitives[0].material;
		it->_m_index=scene->meshes[index]._primitives[0].material_index;
	}
	for(List<SceneNode>::Iterator it=n->_children.begin(); !it.isOutOfRange(); it++){
		resolveNode(&(*it), scene);
	}
}

void SceneManager::loadScene(Scene* scene){
	cameras=Vector<Camera>(scene->cameras);
	lightSources=Vector<LightSource>(scene->lights);
	materials=Vector<Material>(scene->materials);
	matrices=AlignedList<glmath_mat4>(scene->matrices);
	
	resolveNode(&scene->rootNode, scene);
	
}
	
TriangulatedMesh* SceneManager::getMesh(uint64_t index){
	return &meshes[index];
}

}