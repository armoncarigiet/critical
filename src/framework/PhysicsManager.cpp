//===========================================================================
/*
 *  Critical - Physics Simulation Library
 *  Copyright (C) 2014-2015 Armon Carigiet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *//*===================================================================*//**
 *
 *	@file PhysicsManager.cpp
 *	@date 14.05.2015.
 *  @author Armon Carigiet
 *
 *  @ingroup framework
 *  @see PhysicsManager.h
 */
//===========================================================================

#include <critical/framework/PhysicsManager.h>

namespace critical {
	
physics::ParticleSpace* PhysicsManager::createParticleSpace(){
	_pSpace=physics::ParticleSpace();
	return &_pSpace;
}
	
physics::RigidBodySpace* PhysicsManager::createRigidBodySpace(){
    _rbSpace=physics::RigidBodySpace();
    return &_rbSpace;
}
	
physics::ParticleSystem* PhysicsManager::createParticleSystem(){
    throw 0;
    return nullptr;
}
	
physics::Particle* PhysicsManager::addPhyisicalObject(physics::Particle &particle, TriangulatedMesh *mesh){
	physics::Particle* p=_pSpace.addObject(particle);
	if(mesh){
		addLink(mesh, p);
	}
	return p;
}
	
physics::RigidBody* PhysicsManager::addPhyisicalObject(physics::RigidBody &rigidBody, TriangulatedMesh *mesh){
	physics::RigidBody* rb=_rbSpace.addObject(rigidBody);
	if(mesh){
		addLink(mesh, rb);
	}
	return rb;
}
	
bool PhysicsManager::removePhyisicalObject(physics::Particle *particle){
	return _pSpace.removeObject(particle);
}
	
bool PhysicsManager::removePhyisicalObject(physics::RigidBody *rigidBody){
	return _rbSpace.removeObject(rigidBody);
}
	
bool PhysicsManager::removePhyisicalObject(String &identifier){
	if(!_pSpace.removeObject(identifier)){
		return _rbSpace.removeObject(identifier);
	}
	return true;
}

void PhysicsManager::addLink(TriangulatedMesh* mesh, physics::Particle* particle){
	particle->position.x+=mesh->_mmat->m[12];
	particle->position.y+=mesh->_mmat->m[13];
	particle->position.z+=mesh->_mmat->m[14];
	mesh->_mmat->m[12]=0;
	mesh->_mmat->m[13]=0;
	mesh->_mmat->m[14]=0;
	
	_plist.pushBack(_plistEntry(particle, mesh));
}

void PhysicsManager::addLink(TriangulatedMesh* mesh, physics::RigidBody* rigidBody){
	rigidBody->position.x+=mesh->_mmat->m[12];
	rigidBody->position.y+=mesh->_mmat->m[13];
	rigidBody->position.z+=mesh->_mmat->m[14];
	mesh->_mmat->m[12]=0;
	mesh->_mmat->m[13]=0;
	mesh->_mmat->m[14]=0;
	
    
    //glmath_mat4 m=*mesh->_mmat;
	glmath_quat q=glmath_quat_create_mat3(glmath_mat4_get_mat3(*mesh->_mmat));
	
    
    // todo
	rigidBody->orientation=glmath_quat_mul(rigidBody->orientation, q);
    //rigidBody->orientation=glmath_quat_mul(q, rigidBody->orientation);
    
    //*mesh->_mmat=glmath_mat4_create_identity();
	
	// adjust inertia Tensor;
	
	
	
	_rblist.pushBack(_rblistEntry(rigidBody, mesh));
}
	
void PhysicsManager::removeLink(TriangulatedMesh *mesh){
	for(List<_plistEntry>::Iterator it=_plist.begin() ; !it.isOutOfRange() ; it++) {
		if(it->_t2==mesh){
			_plist.erase(it);
			return;
		}
	}
	for(List<_rblistEntry>::Iterator it=_rblist.begin() ; !it.isOutOfRange() ; it++) {
		if(it->_t2==mesh){
			_rblist.erase(it);
			break;
		}
	}
}
    
void PhysicsManager::removeLink(physics::Particle *particle){
	for(List<_plistEntry>::Iterator it=_plist.begin() ; !it.isOutOfRange() ; it++) {
		if(it->_t1==particle){
			_plist.erase(it);
			break;
		}
	}
}

void PhysicsManager::removeLink(physics::RigidBody *rigidBody){
	for(List<_rblistEntry>::Iterator it=_rblist.begin() ; !it.isOutOfRange() ; it++) {
		if(it->_t1==rigidBody){
			_rblist.erase(it);
			break;
		}
	}
}

void PhysicsManager::updateModelMatrices(){
	glmath_mat4 m;
	for(List<_plistEntry>::Iterator it=_plist.begin() ; !it.isOutOfRange() ; it++){
		m=glmath_mat4_create_translation(it->_t1->position.x, it->_t1->position.y, it->_t1->position.z);
		*it->_t2->_phymat=m;
	}
	for(List<_rblistEntry>::Iterator it=_rblist.begin() ; !it.isOutOfRange() ; it++){
        //glmath_quat qq=it->_t1->orientation;
        m=glmath_mat4_create_quat(it->_t1->orientation);
    
        //glmath_quat dsf=it->_t1->orientation;
        
        
		m.m[12]=it->_t1->position.x;
		m.m[13]=it->_t1->position.y;
		m.m[14]=it->_t1->position.z;
        *it->_t2->_phymat=m;
	}
}
	
}