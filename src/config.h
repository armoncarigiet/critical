//
//  config.h
//  
//
//  Created by Armon Carigiet on 14.11.14.
//
//

#ifndef CRITICAL_CONFIG_H
#define CRITICAL_CONFIG_H

#if defined(_WIN32)
	#define CRITICAL_WIN 1
	#define CRITICAL_WINAPI 1
	#if defined(_WIN64)
		#define CRITICAL_WIN64 1
	#endif
#elif defined(__APPLE__)
	#include "TargetConditionals.h"
	#if TARGET_IPHONE_SIMULATOR
		#error IOS not supported yet!
	#elif TARGET_OS_IPHONE
		#error IOS not supported yet!
	#elif TARGET_OS_MAC
		#define CRITICAL_OSX 1
	#else
		#error Unsupported Platform!
	#endif
#elif defined(__unix__)
	#if defined(__linux__)
		#define CRITICAL_LINUX 1
	#elif defined(__FreeBSD__)
		#define CRITICAL_FREEBSD 1
	#endif
#else
	#error Unsupported Platform!
#endif

#if (defined(__unix__) || defined(__APPLE__) && defined(__MACH__)) && (defined(_POSIX_VERSION)&&_POSIX_VERSION==200809L)
	#defined CRITICAL_POSIX 1
#else
	#error POSIX.1-2008 Standart not available!
#endif

#if !(defined(__cplusplus) && (__cplusplus==201103L))
	#error C++11 Compiler Support Missing!
#endif

#if defined(__GNUC__)
	#define CRITICAL_GNU 1
#elif defined(__llvm__) && defined(__clang__)
	#define CRITICAL_CLANG_LLVM 1
#elif defined(_MSC_VER)
	#define CRITICAL_MSVC 1
#elif defined(__MINGW32__)
	#define CRITICAL_MINGW 1
#endif

#define






#endif
